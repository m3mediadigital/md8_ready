<h1><?php echo "<?php echo \$this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('{$pluralHumanName}'); ?>"; ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo "<?php echo \$this->Html->link(\$this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>\n"; ?>
            <?php echo "<?php echo \$this->Html->link(__('Back'), array('action'=>'index')); ?>\n"; ?>
        </li>
        <?php if (strpos($action, 'add') === false): ?>
            <li>
                <?php echo "<?php echo \$this->Form->postLink(\$this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', \$this->Form->value('{$modelClass}.{$primaryKey}')), array('escape'=>false), __('Are you sure you want to delete # %s?', \$this->Form->value('{$modelClass}.{$primaryKey}'))); ?>\n"; ?>
                <?php echo "<?php echo \$this->Form->postLink(__('Delete'), array('action' => 'delete', \$this->Form->value('{$modelClass}.{$primaryKey}')), null, __('Are you sure you want to delete # %s?', \$this->Form->value('{$modelClass}.{$primaryKey}'))); ?>\n"; ?>
            </li>
        <?php endif;?>
    </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <?php
                    foreach ($fields as $field) {
                        echo '<tr>';
                        $isKey = false;
                        if (!empty($associations['belongsTo'])) {
                            foreach ($associations['belongsTo'] as $alias => $details) {
                                if ($field === $details['foreignKey']) {
                                    $isKey = true;
                                    echo "\t\t<td><h4><?php echo __('" . Inflector::humanize(Inflector::underscore($alias)) . "'); ?></h4></td>\n";
                                    echo "\t\t<td>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?></td>\n";
                                    break;
                                }
                            }
                        }
                        if ($isKey !== true) {
                            if (in_array($field, array('image'))) {
                                echo "\t\t<td><h4><?php echo __('" . Inflector::humanize($field) . "'); ?></h4></td>\n";
                                echo "\t\t<td><?php if(empty(\${$singularVar}['{$modelClass}']['{$field}'])){ echo \$this->Html->image('/coreadmin/img/nopicture_list.jpg'); }else{ echo \$this->Html->link(\$this->Html->image('uploads/".strtolower($modelClass)."/admin_list/'.\${$singularVar}['{$modelClass}']['{$field}']), '/img/uploads/".strtolower($modelClass)."/large/'.\${$singularVar}['{$modelClass}']['{$field}'], array('escape'=>false, 'class'=>'fancybox')); }  ?></td>\n";
                            }else{
                                echo "\t\t<td><h4><?php echo __('" . Inflector::humanize($field) . "'); ?></h4></td>\n";
                                echo "\t\t<td>\n\t\t\t<?php echo \${$singularVar}['{$modelClass}']['{$field}']; ?></td>\n";
                            }
                        }
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo "<?php echo \$this->Html->link(\$this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>\n"; ?>
            <?php echo "<?php echo \$this->Html->link(__('Back'), array('action'=>'index')); ?>\n"; ?>
        </li>
        <?php if (strpos($action, 'add') === false): ?>
            <li>
                <?php echo "<?php echo \$this->Form->postLink(\$this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', \$this->Form->value('{$modelClass}.{$primaryKey}')), array('escape'=>false), __('Are you sure you want to delete # %s?', \$this->Form->value('{$modelClass}.{$primaryKey}'))); ?>\n"; ?>
                <?php echo "<?php echo \$this->Form->postLink(__('Delete'), array('action' => 'delete', \$this->Form->value('{$modelClass}.{$primaryKey}')), null, __('Are you sure you want to delete # %s?', \$this->Form->value('{$modelClass}.{$primaryKey}'))); ?>\n"; ?>
            </li>
        <?php endif;?>
    </ul>
</div>
</br>
</br>
</br>
</br>
</br>
<?php
if (!empty($associations['hasOne'])) :
    foreach ($associations['hasOne'] as $alias => $details):
        ?>
        <div class="related">
            <h3><?php echo "<?php echo __('Related " . Inflector::humanize($details['controller']) . "');?>"; ?></h3>
            <?php echo "<?php if (!empty(\${$singularVar}['{$alias}'])):?>\n"; ?>
            <dl><?php echo "\t<?php \$i = 0; \$class = ' class=\"altrow\"';?>\n"; ?>
                <?php
                foreach ($details['fields'] as $field) {
                    echo "\t\t<dt<?php if (\$i % 2 == 0) echo \$class;?>><?php echo __('" . Inflector::humanize($field) . "');?></dt>\n";
                    echo "\t\t<dd<?php if (\$i++ % 2 == 0) echo \$class;?>>\n\t<?php echo \${$singularVar}['{$alias}']['{$field}'];?>\n&nbsp;</dd>\n";
                }
                ?>
            </dl>
            <?php echo "<?php endif; ?>\n"; ?>
            <div class="actions">
                <ul>
                    <li><?php echo "<?php echo \$this->Html->link(__('Edit " . Inflector::humanize(Inflector::underscore($alias)) . "', true), array('controller' => '{$details['controller']}', 'action' => 'edit', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?></li>\n"; ?>
                </ul>
            </div>
        </div>
        <?php
    endforeach;
endif;
?>

<?php
if (empty($associations['hasMany'])) {
    $associations['hasMany'] = array();
}
if (empty($associations['hasAndBelongsToMany'])) {
    $associations['hasAndBelongsToMany'] = array();
}
$relations = array_merge($associations['hasMany'], $associations['hasAndBelongsToMany']);
$i = 0;
foreach ($relations as $alias => $details):
    $otherSingularVar = Inflector::variable($alias);
    $otherPluralHumanName = Inflector::humanize($details['controller']);
    $otherSingularHumanName = Inflector::singularize($otherPluralHumanName);
    ?>
    <h1><?php echo "<?php echo \$this->Html->image('admin/icons/posts.png') ?>" ?> <?php echo "<?php echo __('Related " . $otherPluralHumanName . "');?>"; ?></h1>
    <div class="actionsList">
        <ul>
            <?php
            $done = array();
            foreach ($associations as $type => $data) {
                foreach ($data as $alias => $details) {
                    if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
                        echo "\t\t<li><?php echo \$this->Html->link(__('List " . Inflector::humanize($details['controller']) . "', true), array('controller' => '{$details['controller']}', 'action' => 'index')); ?> </li>\n";
                        echo "\t\t<li><?php echo \$this->Html->link(__('New " . Inflector::humanize(Inflector::underscore($alias)) . "', true), array('controller' => '{$details['controller']}', 'action' => 'add')); ?> </li>\n";
                        $done[] = $details['controller'];
                    }
                }
            }
            ?>
        </ul>
    </div>
    <div class="bloc">
        <div class="title">
            <?php echo "<?php echo __('Related " . $otherPluralHumanName . "');?>"; ?>
        </div>
        <div class="content">
            <table>
                <thead>
                    <tr>
                        <?php foreach ($details['fields'] as $field): ?>
                            <th><?php echo $field; ?></th>
                        <?php endforeach; ?>
                        <th class="actions"><?php echo "<?php __('Actions');?>"; ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    echo "<?php
                    \$i = 0;
                    foreach (\${$singularVar}['{$alias}'] as \${$otherSingularVar}):
                            \$class = null;
                            if (\$i++ % 2 == 0) {
                                    \$class = ' class=\"altrow\"';
                            }
                    ?>\n";
                    foreach ($details['fields'] as $field) {
                        if (in_array($field, array('active'))) {
                            echo "
                        <?php
                            if(\${$otherSingularVar}['{$field}'] == 1){
                                echo '<td>'.\$this->Html->image('admin/icons/ativo.png', array('title' => 'Ativo', 'class'=>'ListImage')).'</td>';
                            }else{
                                echo '<td>'.\$this->Html->image('admin/icons/inativo.png', array('title' => 'Desativado', 'class'=>'ListImage')).'</td>';
                            }
                        ?>";
                        } elseif (in_array($field, array('created'))) {
                            echo "
                        <?php
                            echo '<td>'.\$this->Time->format('d/m/Y H:i:s', \${$otherSingularVar}['{$field}']).'</td>';
                        ?>";
                        } elseif (in_array($field, array('modified'))) {
                            echo "
                        <?php
                            echo '<td>'.\$this->Time->format('d/m/Y H:i:s', \${$otherSingularVar}['{$field}']).'</td>';
                        ?>";
                        } else {
                            echo "\t\t<td><?php echo \${$otherSingularVar}['{$field}']; ?>&nbsp;</td>\n";
                        }
                    }

                    echo "\t\t<td class=\"actions\">\n";

                    if (in_array($field, array('album_id'))) {
                        echo "
                        <?php
                            if(isset(\${$otherSingularVar}['{$field}'])){
                                \t\t\t echo \$this->Html->link(\$this->Html->image('admin/icons/photo.png', array('title' => 'Fotos')), array('plugin'=>'albuns', 'controller' => 'photos', 'action' => 'add', \${$otherSingularVar}['{$field}']), array('escape' => false));\n;
                            }else{
                                \t\t\t echo \$this->Html->link(\$this->Html->image('admin/icons/photo.png', array('title' => 'Fotos')), array('action' => 'adicionarAlbum', \${$otherSingularVar}['{$primaryKey}']), array('escape' => false));\n;
                            }
                        ?>";
                    }

                    echo "\t\t\t<?php echo \$this->Html->link(\$this->Html->image('admin/icons/edit.png', array('title' => 'Editar')), array('action' => 'edit', \${$otherSingularVar}['{$primaryKey}']), array('escape' => false)); ?>\n";
                    echo "\t\t\t<?php echo \$this->Html->link(\$this->Html->image('admin/icons/delete.png', array('title' => 'Remover')), array('action' => 'delete', \${$otherSingularVar}['{$primaryKey}']), array('escape' => false), sprintf(__('Are you sure you want to delete # %s?', true), \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n";
                    echo "\t\t</td>\n";
                    echo "\t</tr>\n";
                    echo "<?php endforeach; ?>\n";
                    ?>


                </tbody>
            </table>
            <div class="left input">
                <select name="data[<?= $modelClass ?>][action]" id="tableaction">
                    <option value="">Action</option>
                    <option value="ativardesativar">Ativar / Desativar</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actionsList">
        <ul>
            <?php
            $done = array();
            foreach ($associations as $type => $data) {
                foreach ($data as $alias => $details) {
                    if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
                        echo "\t\t<li><?php echo \$this->Html->link(__('List " . Inflector::humanize($details['controller']) . "', true), array('controller' => '{$details['controller']}', 'action' => 'index')); ?> </li>\n";
                        echo "\t\t<li><?php echo \$this->Html->link(__('New " . Inflector::humanize(Inflector::underscore($alias)) . "', true), array('controller' => '{$details['controller']}', 'action' => 'add')); ?> </li>\n";
                        $done[] = $details['controller'];
                    }
                }
            }
            ?>
        </ul>
    </div>
<?php endforeach; ?>
<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<h1><?php echo "<?php echo \$this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('{$pluralHumanName}'); ?>"; ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo "<?php echo \$this->Html->link(\$this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>\n"; ?>
            <?php echo "<?php echo \$this->Html->link(__('Adicionar'), array('action'=>'add')); ?>\n"; ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <?php echo "<?php echo \$this->Form->create('{$modelClass}', array('url' => array('action' => 'allActionsListData'))); ?>\n"; ?>
        <table>
            <thead>
                <tr>
                    <th><input type="checkbox" class="checkall"/></th>
                    <?php foreach ($fields as $field): ?>
<?php if ($field != 'created' && $field != 'modified'): ?><?php echo "\t\t<th><?php echo \$this->Paginator->sort('{$field}');?></th>\n"; ?>
                    <?php endif; ?>
                    <?php endforeach; ?><th class="actions">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                echo "<?php
                    \$i = 0;
                    foreach (\${$pluralVar} as \${$singularVar}):
                        \$class = null;
                        if (\$i++ % 2 == 0) {
                            \$class = ' class=\"altrow\"';
                        }
                ?>";
                echo "
                <tr>
                    <td>
                        <?= \$this->Form->input('$singularHumanName.id.'.\${$singularVar}['$singularHumanName']['id'],array('value'=>\${$singularVar}['$singularHumanName']['id'] ,'type'=>'checkbox','label'=>false, 'div'=>false))?>
                    </td>\n";
                foreach ($fields as $field) {
                    $isKey = false;
                    if (!empty($associations['belongsTo'])) {
                        foreach ($associations['belongsTo'] as $alias => $details) {
                            if ($field === $details['foreignKey']) {
                                $isKey = true;
                                echo "\t<td>\n";
                                echo "\t\t<?= \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n";
                                echo "\t</td>\n";
                                break;
                            }
                        }
                    }
                    if ($isKey !== true) {
                        if ($field == 'active') {
                            echo "\t\t<?php if (\${$singularVar}['{$modelClass}']['active'] == 1): ?>\n";
                            echo "\t\t<td>\n";
                            echo "\t\t\t<?= \$this->Html->image('/coreadmin/img/icons/ativar.png', array('title' => 'Ativo', 'class' => 'ListImage')) ?>\n";
                            echo "\t\t</td>\n";
                            echo "\t\t<?php else: ?>\n";
                            echo "\t\t<td>\n";
                            echo "\t\t\t<?= \$this->Html->image('/coreadmin/img/icons/desativar.png', array('title' => 'Desativado', 'class' => 'ListImage')) ?>\n";
                            echo "\t\t</td>\n";
                            echo "\t\t<?php endif; ?>\n";
                        } elseif ($field == 'image') {
                            echo "\t\t<?php if (!empty(\${$singularVar}['{$modelClass}']['image'])): ?>\n";
                            echo "\t\t<td>\n";
                            echo "\t\t\t<?= \$this->Html->link(\$this->PhpThumb->thumbnail('uploads/' . \${$singularVar}['{$modelClass}']['image'], array('w' => 20, 'h' => 23, 'zc' => 1)), \$this->PhpThumb->url('uploads/' . \${$singularVar}['{$modelClass}']['image'], array('w' => 640, 'h' => 480, 'zc' => 1)), array('escape' => false, 'class' => 'fancybox')) ?>\n";
                            echo "\t\t</td>\n";
                            echo "\t\t<?php else: ?>\n";
                            echo "\t\t<td>\n";
                            echo "\t\t\t<?= \$this->Html->image('/coreadmin/img/nopicture_list.jpg') ?>\n";
                            echo "\t\t</td>\n";
                            echo "\t\t<?php endif; ?>\n";
                        } elseif ($field != 'created' && $field != 'modified') {
                            echo "\t\t<td>\n";
                            echo "\t\t\t<?= \$this->Html->link(\${$singularVar}['{$modelClass}']['{$field}'], array('action'=>'edit', \${$singularVar}['{$modelClass}']['id'])); ?>\n";
                            echo "\t\t</td>\n";
                        }
                    }
                }
                echo "
                    <td class=\"actions\">
                        <ul>\n";
                
                echo "      <?php if(!empty(\$addImages)): ?>
                            <li>
                                <?php echo \$this->Html->link('Anexar Imagens', array('controller' => 'photos', 'action' => 'anexar', \${$singularVar}['{$modelClass}']['id'], '?' => array('tabela' => '{$pluralVar}')), array('escape' => false, 'class' => 'bt-anexar')); ?>
                            </li>
                            <?php endif; ?>";

                echo "
                            <li>
                                <?php echo \$this->Html->link(\$this->Html->image('jqueryui/ico-03.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('escape' => false)); ?>
                            </li>";
                echo "
                            <li>
                                <?php echo \$this->Html->link(\$this->Html->image('jqueryui/ico-04.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('escape' => false)); ?>
                            </li>";
                echo "
                            <li>
                                <?php echo \$this->Html->link(\$this->Html->image('jqueryui/ico-05.jpg', array('style' => 'width:29px;margin:0;padding:0;')), '#', array('escape' => false, 'class' => 'deleteButtonRow', 'rel' => \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>
                            </li>";
                echo "
                        </ul>
                    </td>";
                echo "
                </tr>";
                echo "
            <?php endforeach; ?>\n";
                ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo "<?php echo \$this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}')));?>"; ?></p>
        <?php echo "<?php if(count($$pluralVar)>0): ?>"; ?>
        <div class="acoesMultiplas">
            <button type="submit" name="data[<?= $modelClass; ?>][action]" value="delete" class="bt-deletar">Remover selecionados</button>

            <button name="data[<?= $modelClass; ?>][action]" value="activate" class="bt-ativar">Ativar selecionados</button>
            <button name="data[<?= $modelClass; ?>][action]" value="deactivate" class="bt-desativar">Desativar selecionados</button>
        </div>
        <?php echo "<?php endif; ?>"; ?>
        <div class="pagination">
            <?php echo "<?php echo \$this->Paginator->prev('<button class=\"bt-esquerda\">Anterior</button>', array('escape'=>false), null, array('class' => 'prev disabled'));?>\n"; ?>
            <?php echo "<?php echo \$this->Paginator->numbers(array('separator' => ' '));?>\n" ?>
            <?php echo "<?php echo \$this->Paginator->next('<button class=\"bt-direita\">Próxima</button>', array('escape'=>false), null, array('class' => 'next disabled'));?>\n"; ?>
        </div>
    </div>
</div>
<?php echo "<?php echo \$this->Form->end(); ?>"; ?>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo "<?php echo \$this->Html->link(\$this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>\n"; ?>
            <?php echo "<?php echo \$this->Html->link('Adicionar', array('action'=>'add')); ?>\n"; ?>
        </li>
    </ul>
</div>
<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<h1><?= "<?= \$this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('{$pluralHumanName}'); ?>"; ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= "<?= \$this->Html->link(\$this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>\n"; ?>
            <?= "<?= \$this->Html->link(__('Back'), array('action'=>'index')); ?>\n"; ?>
        </li>
        <?php if (strpos($action, 'add') === false): ?>
            <li>
                <?= "<?= \$this->Form->postLink(\$this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', \$this->Form->value('{$modelClass}.{$primaryKey}')), array('escape'=>false), __('Are you sure you want to delete # %s?', \$this->Form->value('{$modelClass}.{$primaryKey}'))); ?>\n"; ?>
                <?= "<?= \$this->Form->postLink(__('Delete'), array('action' => 'delete', \$this->Form->value('{$modelClass}.{$primaryKey}')), null, __('Are you sure you want to delete # %s?', \$this->Form->value('{$modelClass}.{$primaryKey}'))); ?>\n"; ?>
            </li>
        <?php endif; ?>
    </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= "<?= \$this->Form->create('{$modelClass}', array('type'=>'file'));?>\n"; ?>
        <?php
        foreach ($fields as $field) {
            if (strpos($action, 'add') !== false && $field == $primaryKey) {
                continue;
            } elseif ($field == 'image') {
                echo "\t<?= \$this->Form->input('{$field}', array('type'=>'file'));?>\n";
                if (strpos($action, 'edit') !== false) {
                    echo "
                    <div class='input'>
                        <label>Foto Atual</label>
                        <div class='input'>
                            <?= \$this->Html->link(\$this->PhpThumb->thumbnail('uploads/' . \$this->data['{$modelClass}']['image'], array('w' => 300, 'h' => 250, 'zc' => 1)), \$this->PhpThumb->url('uploads/' . \$this->data['{$modelClass}']['image'], array('w' => 640, 'h' => 480, 'zc' => 1)), array('escape' => false, 'class' => 'fancybox')) ?>
                        </div>
                    </div>
                            ";
                }
            } elseif ($field == 'active') {
                if (strpos($action, 'edit') !== false) {
                    echo "\t<?= \$this->Form->input('{$field}');?>\n";
                } else {
                    echo "\t<?= \$this->Form->input('{$field}', array('checked'=>'checked'));?>\n";
                }
            } elseif ($field != 'created' && $field != 'modified') {
                echo "\t<?= \$this->Form->input('{$field}');?>\n";
            }
        }
        if (!empty($associations['hasAndBelongsToMany'])) {
            foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
                echo "\t\t<?= \$this->Form->input('{$assocName}');?>\n";
            }
        }
        ?>
        <?= "<?= \$this->Form->end(__('Submit', true));?>\n"; ?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= "<?= \$this->Html->link(\$this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>\n"; ?>
            <?= "<?= \$this->Html->link(__('Back'), array('action'=>'index')); ?>\n"; ?>
        </li>
        <?php if (strpos($action, 'add') === false): ?>
            <li>
                <?= "<?= \$this->Form->postLink(\$this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', \$this->Form->value('{$modelClass}.{$primaryKey}')), array('escape'=>false), __('Are you sure you want to delete # %s?', \$this->Form->value('{$modelClass}.{$primaryKey}'))); ?>\n"; ?>
                <?= "<?= \$this->Form->postLink(__('Delete'), array('action' => 'delete', \$this->Form->value('{$modelClass}.{$primaryKey}')), null, __('Are you sure you want to delete # %s?', \$this->Form->value('{$modelClass}.{$primaryKey}'))); ?>\n"; ?>
            </li>
        <?php endif; ?>
    </ul>
</div>
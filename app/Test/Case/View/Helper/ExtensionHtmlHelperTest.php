<?php

App::uses('Controller', 'Controller');
App::uses('View', 'View');
App::uses('ExtensionHtmlHelper', 'View/Helper');

class ExtensionHtmlHelperTest extends CakeTestCase {
    
    public function setUp() {
        parent::setUp();
	    $Controller = new Controller();
	    $View = new View($Controller);
	    $this->ExtensionHtml = new ExtensionHtmlHelper($View);
    }

    public function testYoutube() {
    	$linkVideo = "http://www.youtube.com/watch?v=6AiKvItAHKI";

    	$result = $this->ExtensionHtml->youtube($linkVideo);
    	$this->assertEquals('<iframe width="420" height="315" src="http://www.youtube.com/embed/6AiKvItAHKI?rel=0&wmode=transparent" frameborder="0" allowfullscreen></iframe>', $result);
    }

    public function testUrlImageVideoYoutubeForZero() {
        $result = $this->ExtensionHtml->urlImageVideoYoutube("http://www.youtube.com/watch?v=6AiKvItAHKI");
        $this->assertEquals("http://img.youtube.com/vi/6AiKvItAHKI/0.jpg", $result);
    }

    public function testUrlImageVideoYoutubeForOne() {
        $result = $this->ExtensionHtml->urlImageVideoYoutube("http://www.youtube.com/watch?v=6AiKvItAHKI", 1);
        $this->assertEquals("http://img.youtube.com/vi/6AiKvItAHKI/1.jpg", $result);
    }

    public function testUrlImageVideoYoutubeWithTagImage() {
        $result = $this->ExtensionHtml->urlImageVideoYoutube("http://www.youtube.com/watch?v=6AiKvItAHKI", 0, true);
        $this->assertEquals('<img src="http://img.youtube.com/vi/6AiKvItAHKI/0.jpg" alt="" />', $result);
    }

    public function testCodeVideoYoutubeReturn() {
        $result = $this->ExtensionHtml->codeVideoYoutube("http://www.youtube.com/watch?v=6AiKvItAHKI");
        $this->assertEquals("6AiKvItAHKI", $result);
    }

    public function testIsActiveForZero(){
        $result = $this->ExtensionHtml->isActive(0);
        $this->assertEquals("Não", $result);
    }

    public function testIsActiveForOne(){
        $result = $this->ExtensionHtml->isActive(1);
        $this->assertEquals("Sim", $result);
    }

    public function tearDown() {
    	unset($this->ExtensionHtml);
    	parent::tearDown();
    }
}
<?php
/* Newsletters Test cases generated on: 2012-08-20 08:38:19 : 1345462699*/
App::uses('NewslettersController', 'Controller');

/**
 * NewslettersController Test Case
 *
 */
class NewslettersControllerTestCase extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Newsletters = new TestNewslettersController();
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Newsletters);

		parent::tearDown();
	}

}

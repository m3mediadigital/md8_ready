<?php
/* Subcategory Test cases generated on: 2013-04-15 17:34:06 : 1366058046*/
App::uses('Subcategory', 'Model');

/**
 * Subcategory Test Case
 *
 */
class SubcategoryTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.subcategory', 'app.category', 'app.parentcategory', 'app.product', 'app.album', 'app.table_files', 'app.photo');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Subcategory = ClassRegistry::init('Subcategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Subcategory);

		parent::tearDown();
	}

}

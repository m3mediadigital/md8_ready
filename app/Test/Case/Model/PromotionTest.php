<?php
/* Promotion Test cases generated on: 2013-02-28 15:10:57 : 1362075057*/
App::uses('Promotion', 'Model');

/**
 * Promotion Test Case
 *
 */
class PromotionTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.promotion');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Promotion = ClassRegistry::init('Promotion');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Promotion);

		parent::tearDown();
	}

}

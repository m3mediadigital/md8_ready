<?php
/* Frequentquestion Test cases generated on: 2013-04-11 17:26:29 : 1365711989*/
App::uses('Frequentquestion', 'Model');

/**
 * Frequentquestion Test Case
 *
 */
class FrequentquestionTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.frequentquestion');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Frequentquestion = ClassRegistry::init('Frequentquestion');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Frequentquestion);

		parent::tearDown();
	}

}

<?php
/* Accommodation Test cases generated on: 2013-02-28 13:43:42 : 1362069822*/
App::uses('Accommodation', 'Model');

/**
 * Accommodation Test Case
 *
 */
class AccommodationTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.accommodation');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Accommodation = ClassRegistry::init('Accommodation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Accommodation);

		parent::tearDown();
	}

}

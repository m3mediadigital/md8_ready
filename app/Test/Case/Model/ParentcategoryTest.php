<?php
/* Parentcategory Test cases generated on: 2013-04-15 17:33:21 : 1366058001*/
App::uses('Parentcategory', 'Model');

/**
 * Parentcategory Test Case
 *
 */
class ParentcategoryTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.parentcategory', 'app.product', 'app.album', 'app.table_files', 'app.photo', 'app.subcategory', 'app.category');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Parentcategory = ClassRegistry::init('Parentcategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Parentcategory);

		parent::tearDown();
	}

}

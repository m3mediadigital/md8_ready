<?php
/* Testis Test cases generated on: 2012-06-06 17:42:36 : 1339015356*/
App::uses('Testis', 'Model');

/**
 * Testis Test Case
 *
 */
class TestisTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.testis');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Testis = ClassRegistry::init('Testis');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Testis);

		parent::tearDown();
	}

}

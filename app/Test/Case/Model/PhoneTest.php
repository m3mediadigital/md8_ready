<?php
/* Phone Test cases generated on: 2012-06-11 09:39:51 : 1339418391*/
App::uses('Phone', 'Model');

/**
 * Phone Test Case
 *
 */
class PhoneTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.phone', 'app.profile', 'app.user', 'app.group', 'app.address');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Phone = ClassRegistry::init('Phone');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Phone);

		parent::tearDown();
	}

}

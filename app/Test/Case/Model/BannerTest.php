<?php
/* Banner Test cases generated on: 2013-04-16 15:39:48 : 1366137588*/
App::uses('Banner', 'Model');

/**
 * Banner Test Case
 *
 */
class BannerTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.banner');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Banner = ClassRegistry::init('Banner');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Banner);

		parent::tearDown();
	}

}

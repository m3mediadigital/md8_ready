<?php
/* Album Test cases generated on: 2013-02-28 14:30:23 : 1362072623*/
App::uses('Album', 'Model');

/**
 * Album Test Case
 *
 */
class AlbumTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.album');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Album = ClassRegistry::init('Album');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Album);

		parent::tearDown();
	}

}

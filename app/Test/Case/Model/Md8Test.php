<?php
/* Md8 Test cases generated on: 2013-04-12 09:02:25 : 1365768145*/
App::uses('Md8', 'Model');

/**
 * Md8 Test Case
 *
 */
class Md8TestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.md8');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Md8 = ClassRegistry::init('Md8');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Md8);

		parent::tearDown();
	}

}

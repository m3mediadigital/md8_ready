<?php
/* Newsletter Test cases generated on: 2012-08-20 08:37:23 : 1345462643*/
App::uses('Newsletter', 'Model');

/**
 * Newsletter Test Case
 *
 */
class NewsletterTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.newsletter');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Newsletter = ClassRegistry::init('Newsletter');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Newsletter);

		parent::tearDown();
	}

}

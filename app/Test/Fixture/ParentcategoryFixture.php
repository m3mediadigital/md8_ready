<?php
/* Parentcategory Fixture generated on: 2013-04-15 17:33:20 : 1366058000 */

/**
 * ParentcategoryFixture
 *
 */
class ParentcategoryFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'product_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'category' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'subcategory_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'fk_parentcategories_products1_idx' => array('column' => 'product_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'product_id' => 1,
			'category' => 1,
			'subcategory_id' => 1
		),
	);
}

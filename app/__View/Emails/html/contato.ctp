<tr>
    <td colspan="3">
        <h1 style="color:#585b5d; font:18px Arial; background:#e6e6e6; padding:14px 0 12px; text-indent:20px;">Formulário de contato</h1>
    </td>
</tr>
<tr>
    <td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:25px 20px 0 20px;">Olá, um formulário de contato foi enviado através do site.</p>
    </td>
</tr>
<tr>
    <td colspan="3">
        <h2 style="color:#585b5d; font:12px Arial; margin:25px 20px 5px 20px; font-weight: bold;">Nome:</h2>
    </td>
</tr>
<tr>
    <td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:0 20px 0 20px;"><?= $data['nome'] ?></p>
    </td>
</tr>
<tr>
    <td colspan="3">
        <h2 style="color:#585b5d; font:12px Arial; margin:25px 20px 5px 20px; font-weight: bold;">Email:</h2>
    </td>
</tr>
<tr>
    <td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:0 20px 0 20px;"><?= $data['email'] ?></p>
    </td>
</tr>
<tr>
    <td colspan="3">
        <h2 style="color:#585b5d; font:12px Arial; margin:25px 20px 5px 20px; font-weight: bold;">Telefone:</h2>
    </td>
</tr>
<tr>
    <td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:0 20px 0 20px;"><?= $data['telefone'] ?></p>
    </td>
</tr>
<tr>
    <td colspan="3">
        <h2 style="color:#585b5d; font:12px Arial; margin:25px 20px 5px 20px; font-weight: bold;">Assunto:</h2>
    </td>
</tr>
<tr>
    <td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:0 20px 0 20px;"><?= $data['assunto'] ?></p>
    </td>
</tr>
<tr>
    <td colspan="3">
        <h2 style="color:#585b5d; font:12px Arial; margin:15px 20px 5px 20px; font-weight: bold;">Mensagem:</h2>
    </td>
</tr>
<tr>
    <td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:0 20px 20px 20px; text-align:justify"><?= $data['comentario'] ?></p> 
    </td>
</tr>
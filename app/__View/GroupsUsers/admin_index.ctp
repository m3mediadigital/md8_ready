<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Groups Users'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Adicionar'), array('action'=>'add')); ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <table>
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" class="checkall"/>
                    </th>
                                                                        <th>
                                <?php echo $this->Paginator->sort('id');?>
                            </th>
                        
                                                                        <th>
                                <?php echo $this->Paginator->sort('user_id');?>
                            </th>
                        
                                                                        <th>
                                <?php echo $this->Paginator->sort('group_id');?>
                            </th>
                        
                    <th class="actions">
                        Ações
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                    foreach ($groupsUsers as $groupsUser):
                        $class = null;
                        if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                        }
                ?>
                <tr>
                    <td>
                        <?= $this->Form->input('Groups User.id.'.$groupsUser['Groups User']['id'],array('value'=>$groupsUser['Groups User']['id'] ,'type'=>'checkbox','label'=>false, 'div'=>false))?>
                    </td>
		<td>
			<?= $this->Html->link($groupsUser['GroupsUser']['id'], array('action'=>'edit', $groupsUser['GroupsUser']['id'])); ?>
		</td>
	<td>
		<?= $this->Html->link($groupsUser['User']['id'], array('controller' => 'users', 'action' => 'view', $groupsUser['User']['id'])); ?>
	</td>
	<td>
		<?= $this->Html->link($groupsUser['Group']['name'], array('controller' => 'groups', 'action' => 'view', $groupsUser['Group']['id'])); ?>
	</td>

                    <td class="actions">
                        <ul>
                            <li>
                                <?php echo $this->Html->link('<button class="bt-ver">Ver</button>', array('action' => 'view', $groupsUser['GroupsUser']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link('<button class="bt-editar">Editar</button>', array('action' => 'edit', $groupsUser['GroupsUser']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Form->postLink('<button class="bt-deletar">Remover</button>', array('action' => 'delete', $groupsUser['GroupsUser']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $groupsUser['GroupsUser']['id'])); ?>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}')));?></p>
        <?php if(count($groupsUsers)>0): ?>        <div class="acoesMultiplas">
            <button class="bt-deletar">Remover selecionados</button>
            <button class="bt-ativar">Ativar selecionados</button>
            <button class="bt-desativar">Desativar selecionados</button>
        </div>
        <?php endif; ?>        <div class="pagination">
            <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape'=>false), null, array('class' => 'prev disabled'));?>
            <?php echo $this->Paginator->numbers(array('separator' => ' '));?>
            <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape'=>false), null, array('class' => 'next disabled'));?>
        </div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action'=>'add')); ?>
        </li>
    </ul>
</div>
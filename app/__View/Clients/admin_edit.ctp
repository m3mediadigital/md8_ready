<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Clients'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Client.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Client.id'))); ?>
                <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Client.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Client.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('Client', array('type'=>'file'));?>
        	<?= $this->Form->input('id');?>
	<?= $this->Form->input('cliente');?>
	<?= $this->Form->input('image', array('type'=>'file'));?>

                    <div class='input'>
                        <label>Foto Atual</label>
                        <div class='input'>
                            <?= $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $this->data['Client']['image'], array('w' => 300, 'h' => 250, 'zc' => 1)), $this->PhpThumb->url('uploads/' . $this->data['Client']['image'], array('w' => 640, 'h' => 480, 'zc' => 1)), array('escape' => false, 'class' => 'fancybox')) ?>
                        </div>
                    </div>
                            	<?= $this->Form->input('site');?>
        <?= $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('Client.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Client.id'))); ?>
                <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Client.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Client.id'))); ?>
            </li>
            </ul>
</div>
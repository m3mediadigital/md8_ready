<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Subcategories'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array( 'controller' => 'categories', 'action'=>'index' ), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array( 'controller' => 'categories', 'action'=>'index' )); ?>
        </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('Subcategory', array('type'=>'file'));?>
     	<?= $this->Form->input('subcategory', array( 'type' => 'text', 'label' => 'Adicionar Sub categoria' ));?>
        <?= $this->Form->input('category_id', array( 'type' => 'hidden', 'value' => $id ));?>    
        <?= $this->Form->end(array( 'label' => 'Adicionar' ));?>
     
    <?php if ( !empty($subcategories) ): ?>

        <div class="input"><h5><p>Sub categorias relacionadas</p></h5></div>
        <?php foreach ($subcategories as $id => $category): ?>
            <div class="input">
                <h6><?php echo $category.' - '.$this->Html->link( $this->Html->image( 'edit.png', array( 'width' => '18px' )), array( 'action' => 'edit', $id ), array( 'escape' => false, 'title' => 'Editar' ) ).' - '.$this->Html->link( $this->Html->image( 'remover.png', array( 'width' => '12px' )), array( 'action' => 'delete', $id ), array( 'escape' => false, 'title' => 'Remover' ), 'Deseja realmente remover esta Sub categoria ?' ) ?></h6>
            </div>
        <?php endforeach ?>
    
    <?php else: ?>
        <div class="input"><h5><p>Nenhuma Sub categorias relacionada</p></h5></div>
    <?php endif ?>    


    <div class="clear"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array( 'controller' => 'categories', 'action'=>'index' ), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array( 'controller' => 'categories', 'action'=>'index' )); ?>
        </li>
            </ul>
</div>
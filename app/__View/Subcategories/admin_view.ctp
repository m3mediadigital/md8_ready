<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Subcategories'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Subcategory.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Subcategory.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Subcategory.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Subcategory.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		<td><h4><?php echo __('Id'); ?></h4></td>
		<td>
			<?php echo $subcategory['Subcategory']['id']; ?></td>
</tr><tr>		<td><h4><?php echo __('Subcategory'); ?></h4></td>
		<td>
			<?php echo $subcategory['Subcategory']['subcategory']; ?></td>
</tr><tr>		<td><h4><?php echo __('Category'); ?></h4></td>
		<td>
			<?php echo $this->Html->link($subcategory['Category']['category'], array('controller' => 'categories', 'action' => 'view', $subcategory['Category']['id'])); ?></td>
</tr>                </tbody>
            </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Subcategory.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Subcategory.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Subcategory.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Subcategory.id'))); ?>
            </li>
            </ul>
</div>
</br>
</br>
</br>
</br>
</br>

    <h1><?php echo $this->Html->image('admin/icons/posts.png') ?> <?php echo __('Related Parentcategories');?></h1>
    <div class="actionsList">
        <ul>
            		<li><?php echo $this->Html->link(__('List Categories', true), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category', true), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Parentcategories', true), array('controller' => 'parentcategories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parentcategory', true), array('controller' => 'parentcategories', 'action' => 'add')); ?> </li>
        </ul>
    </div>
    <div class="bloc">
        <div class="title">
            <?php echo __('Related Parentcategories');?>        </div>
        <div class="content">
            <table>
                <thead>
                    <tr>
                                                    <th>id</th>
                                                    <th>product_id</th>
                                                    <th>subcategory_id</th>
                                                    <th>category_id</th>
                                                <th class="actions"><?php __('Actions');?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach ($subcategory['Parentcategory'] as $parentcategory):
                            $class = null;
                            if ($i++ % 2 == 0) {
                                    $class = ' class="altrow"';
                            }
                    ?>
		<td><?php echo $parentcategory['id']; ?>&nbsp;</td>
		<td><?php echo $parentcategory['product_id']; ?>&nbsp;</td>
		<td><?php echo $parentcategory['subcategory_id']; ?>&nbsp;</td>
		<td><?php echo $parentcategory['category_id']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link($this->Html->image('admin/icons/edit.png', array('title' => 'Editar')), array('action' => 'edit', $parentcategory['id']), array('escape' => false)); ?>
			<?php echo $this->Html->link($this->Html->image('admin/icons/delete.png', array('title' => 'Remover')), array('action' => 'delete', $parentcategory['id']), array('escape' => false), sprintf(__('Are you sure you want to delete # %s?', true), $subcategory['Subcategory']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>


                </tbody>
            </table>
            <div class="left input">
                <select name="data[Subcategory][action]" id="tableaction">
                    <option value="">Action</option>
                    <option value="ativardesativar">Ativar / Desativar</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actionsList">
        <ul>
            		<li><?php echo $this->Html->link(__('List Categories', true), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category', true), array('controller' => 'categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Parentcategories', true), array('controller' => 'parentcategories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parentcategory', true), array('controller' => 'parentcategories', 'action' => 'add')); ?> </li>
        </ul>
    </div>

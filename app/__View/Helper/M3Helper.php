<?php

App::uses('Helper', 'View');

class M3Helper extends Helper {

	public function image($image, $options = null) {

		$im_abs = WWW_ROOT . 'uploads/jcrop/' . implode("_", $options) . $image;

		$im_rel = $this->webroot . 'uploads/jcrop/' . implode("_", $options) . $image;

		if(file_exists($im_abs)) {
			$im = "<img src='" . $im_rel . "' />";
		} else {
			$im = $this->PhpThumb->thumbnail('uploads/' . $image, array('w' => $options['h'], 'h' => $options['h'], 'zc' => '1', 'q' => '100'));
		}
		
		return $im;
	}
}

<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Submenus'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Submenu.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Submenu.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Submenu.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Submenu.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc submenus clear">
    <div class="content">
        <?php echo $this->Form->create('Submenu', array('type'=>'file'));?>
        	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('menu_id');
		echo $this->Form->input('label');
		echo $this->Form->input('controller');
		echo $this->Form->input('action');
		echo $this->Form->input('plugin');
	?>
        <?php echo $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('Submenu.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Submenu.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Submenu.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Submenu.id'))); ?>
        </li>
    </ul>
</div>
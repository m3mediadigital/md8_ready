<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Categories'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Category.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Category.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Category.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Category.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		<td width="120"><h4><?php echo __('Id'); ?></h4></td>
		<td>
			<?php echo $category['Category']['id']; ?></td>
</tr><tr>		<td><h4><?php echo __('Category'); ?></h4></td>
		<td>
			<?php echo $category['Category']['category']; ?></td>
</tr>          </tbody>
            </table>
        </div>

    <?php if ( !empty($subcategories) ): ?>

        <div class="input"><h5><p>Sub categorias relacionadas</p></h5></div>
        <?php foreach ($subcategories as $id => $category): ?>
            <div class="input">
                <h6><?php echo $category.' - '.$this->Html->link( $this->Html->image( 'edit.png', array( 'width' => '18px' )), array( 'controller' => 'subcategories', 'action' => 'edit', $id ), array( 'escape' => false, 'title' => 'Editar' ) ).' - '.$this->Html->link( $this->Html->image( 'remover.png', array( 'width' => '12px' )), array( 'controller' => 'subcategories', 'action' => 'delete', $id ), array( 'escape' => false, 'title' => 'Remover' ), 'Deseja realmente remover esta Sub categoria ?' ) ?></h6>
            </div>
        <?php endforeach ?>
    
    <?php else: ?>
        <div class="input"><h5><p>Nenhuma Sub categorias relacionada</p></h5></div>
    <?php endif ?>    

        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Category.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Category.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Category.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Category.id'))); ?>
            </li>
            </ul>
</div>

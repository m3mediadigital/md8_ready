<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Newsletters'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Newsletter.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Newsletter.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Newsletter.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Newsletter.id'))); ?>
        </li>
    </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		
                        <td><h4><?php echo __('Id'); ?></h4></td>
                        <td><?php echo $newsletter['Newsletter']['id']; ?></td>
                     </tr>
                     <tr>		
                        <td><h4><?php echo __('Name'); ?></h4></td>
                        <td><?php echo $newsletter['Newsletter']['name']; ?></td>
                     </tr>
                     <tr>		
                        <td><h4><?php echo __('Email'); ?></h4></td>
                        <td><?php echo $newsletter['Newsletter']['email']; ?></td>
                     </tr>                
                 </tbody>
             </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Newsletter.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Newsletter.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Newsletter.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Newsletter.id'))); ?>
        </li>
    </ul>
</div>
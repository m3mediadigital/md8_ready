<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Newsletters'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Adicionar'), array('action'=>'add')); ?>
        </li>
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/import.png', array('title'=>'Exportar')), array('action'=>'exportCsv', 'admin' => false), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Exportar'), array('action'=>'exportCsv', 'admin' => false)); ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">

        <?php echo $this->Form->create('Newsletter', array('url' => array('action' => 'allActionsListData'))); ?>
        <table>
            <thead>
                <tr>
                    <th><input type="checkbox" class="checkall"/></th>
                    <th><?php echo $this->Paginator->sort('id');?></th>
                    <th><?php echo $this->Paginator->sort('name');?></th>
                    <th><?php echo $this->Paginator->sort('email');?></th>
                    <th class="actions">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                    foreach ($newsletters as $newsletter):
                        $class = null;
                        if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                        }
                ?>
                <tr>
                    <td>
                        <?= $this->Form->input('Newsletter.id.'.$newsletter['Newsletter']['id'],array('value'=>$newsletter['Newsletter']['id'] ,'type'=>'checkbox','label'=>false, 'div'=>false))?>
                    </td>
            		<td>
            			<?= $this->Html->link($newsletter['Newsletter']['id'], array('action'=>'edit', $newsletter['Newsletter']['id'])); ?>
            		</td>
            		<td>
            			<?= $this->Html->link($newsletter['Newsletter']['name'], array('action'=>'edit', $newsletter['Newsletter']['id'])); ?>
            		</td>
            		<td>
            			<?= $this->Html->link($newsletter['Newsletter']['email'], array('action'=>'edit', $newsletter['Newsletter']['id'])); ?>
            		</td>

                    <td class="actions">
                        <ul>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-03.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'view', $newsletter['Newsletter']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-04.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'edit', $newsletter['Newsletter']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Form->postLink($this->Html->image('jqueryui/ico-05.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'delete', $newsletter['Newsletter']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $newsletter['Newsletter']['id'])); ?>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd">
            <?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}')));?>
        </p>
        <?php if(count($newsletters)>0): ?>        
        <div class="acoesMultiplas">
            <button type="submit" name="data[Newsletter][action]" value="delete" class="bt-deletar">Remover selecionados</button>
            <button type="submit" name="data[Newsletter][action]" value="activate" class="bt-ativar">Ativar selecionados</button>
            <button type="submit" name="data[Newsletter][action]" value="deactivate" class="bt-desativar">Desativar selecionados</button>
        </div>
        <?php endif; ?>
        <div class="pagination">
            <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape'=>false), null, array('class' => 'prev disabled'));?>
            <?php echo $this->Paginator->numbers(array('separator' => ' '));?>
            <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape'=>false), null, array('class' => 'next disabled'));?>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action'=>'add')); ?>
        </li>
    </ul>
</div>
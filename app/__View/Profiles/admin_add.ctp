<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Profiles'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('Profile', array('type'=>'file'));?>
        	<?= $this->Form->input('user_id');?>
	<?= $this->Form->input('full_name');?>
	<?= $this->Form->input('corporate_name');?>
	<?= $this->Form->input('cnpj');?>
	<?= $this->Form->input('cpf');?>
	<?= $this->Form->input('full_address');?>
	<?= $this->Form->input('cep');?>
	<?= $this->Form->input('city');?>
	<?= $this->Form->input('uf');?>
	<?= $this->Form->input('primary_phone');?>
	<?= $this->Form->input('secondary_phone');?>
	<?= $this->Form->input('fax');?>
	<?= $this->Form->input('email');?>
        <?= $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
            </ul>
</div>
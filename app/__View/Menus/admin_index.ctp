<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Menus'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png'), array('action' => 'add'), array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Add'), array('action' => 'add')); ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <table>
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" class="checkall"/>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('id'); ?>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('title'); ?>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('image'); ?>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('active'); ?>
                    </th>
                    <th class="actions">
                        Ações
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($menus as $menu):
                    $class = null;
                    if ($i++ % 2 == 0) {
                        $class = ' class="altrow"';
                    }
                    ?>
                    <tr>
                        <td>
                            <?= $this->Form->input('Menu.id.' . $menu['Menu']['id'], array('value' => $menu['Menu']['id'], 'type' => 'checkbox', 'label' => false, 'div' => false)) ?>
                        </td>
                        <td>
                            <?php echo $this->Html->link($menu['Menu']['id'], array('action' => 'edit', $menu['Menu']['id'])); ?>
                        </td>
                        <td>
                            <?php echo $this->Html->link($menu['Menu']['title'], array('action' => 'edit', $menu['Menu']['id'])); ?>
                        </td>
                        <td><?= $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $menu['Menu']['image'], array('w' => 35, 'h' => 25, 'zc' => 1)), $this->PhpThumb->url('uploads/' . $menu['Menu']['image'], array('w' => 640, 'h' => 480, 'zc' => 1)), array('escape' => false, 'class'=>'fancybox')) ?></td>
                        <?php
                        if ($menu['Menu']['active'] == 1) {
                            echo '
                            <td>
                            ' . $this->Html->image('/coreadmin/img/icons/ativar.png', array('title' => 'Ativo', 'class' => 'ListImage')) . '
                            </td>';
                        } else {
                            echo '
                            <td>
                            ' . $this->Html->image('/coreadmin/img/icons/desativar.png', array('title' => 'Desativado', 'class' => 'ListImage')) . '
                            </td>';
                        }
                        ?>
                        <td class="actions">
                            <ul>
                                <li>
                                    <?php echo $this->Html->link('Ver', array('action' => 'view', $menu['Menu']['id']), array('escape' => false, 'class' => 'bt-ver')); ?>
                                </li>
                                <li>
                                    <?php echo $this->Html->link('Editar', array('action' => 'edit', $menu['Menu']['id']), array('escape' => false, 'class' => 'bt-editar')); ?>
                                </li>
                                <li>
                                    <?php echo $this->Form->postLink('Remover', array('action' => 'delete', $menu['Menu']['id']), array('escape' => false, 'class' => 'bt-deletar'), __('Are you sure you want to delete # %s?', $menu['Menu']['id'])); ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}'))); ?></p>
        <div class="acoesMultiplas">
            <button class="bt-deletar">Remover selecionados</button>
            <button class="bt-ativar">Ativar selecionados</button>
            <button class="bt-desativar">Desativar selecionados</button>
        </div>
        <div class="pagination">
            <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape' => false), null, array('class' => 'prev disabled')); ?>
            <?php echo $this->Paginator->numbers(array('separator' => ' ')); ?>
            <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape' => false), null, array('class' => 'next disabled')); ?>
        </div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png'), array('action' => 'add'), array('escape' => false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action' => 'add')); ?>
        </li>
    </ul>
</div>
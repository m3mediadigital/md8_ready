<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Photos'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Adicionar'), array('action'=>'add')); ?>
        </li>
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionarm.png', array('title'=>'Adicionar')), array('action'=>'add_massa'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Add Multi'), array('action'=>'add_massa')); ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <table>
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" class="checkall"/>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('id');?>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('title');?>
                    </th>
                    <th class="actions">
                        Ações
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                    foreach ($photos as $photo):
                        $class = null;
                        if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                        }
                ?>
                <tr>
                    <td>
                        <?= $this->Form->input('Photo.id.'.$photo['Photo']['id'],array('value'=>$photo['Photo']['id'] ,'type'=>'checkbox','label'=>false, 'div'=>false))?>
                    </td>
                    <td>
                        <?php echo $this->Html->link($photo['Photo']['id'], array('action'=>'edit', $photo['Photo']['id'])); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->link($photo['Photo']['title'], array('action' => 'edit', $photo['Photo']['id'])); ?>
                    </td>
                    <td class="actions">
                        <ul>  
                            <li>
                                <?php echo $this->Html->link('<button class="bt-recortar">Recortes</button>', array('action' => 'crops', $photo['Photo']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link('<button class="bt-ver">Ver</button>', array('action' => 'view', $photo['Photo']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link('<button class="bt-editar">Editar</button>', array('action' => 'edit', $photo['Photo']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Form->postLink('<button class="bt-deletar">Remover</button>', array('action' => 'delete', $photo['Photo']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $photo['Photo']['id'])); ?>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}')));?></p>
        <?php if(count($photos)>0): ?>        <div class="acoesMultiplas">
            <button class="bt-deletar">Remover selecionados</button>
            <button class="bt-ativar">Ativar selecionados</button>
            <button class="bt-desativar">Desativar selecionados</button>
        </div>
        <?php endif; ?>        <div class="pagination">
            <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape'=>false), null, array('class' => 'prev disabled'));?>
            <?php echo $this->Paginator->numbers(array('separator' => ' '));?>
            <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape'=>false), null, array('class' => 'next disabled'));?>
        </div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action'=>'add')); ?>
        </li>
    </ul>
</div>
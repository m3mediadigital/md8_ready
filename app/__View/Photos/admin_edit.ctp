<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Photos'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Photo.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Photo.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Photo.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Photo.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc photos clear">
    <div class="content">
        <?php echo $this->Form->create('Photo', array('type'=>'file'));?>
        	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title');
		echo $this->Form->input('author');
		echo $this->Form->input('alt');
		echo $this->Form->input('image', array('type' => 'file'));
	?>
        <?php if (!empty($this->data['Photo']['image'])): ?>
            <div class='input'>
                <label>Foto Atual</label>
                <div class='input'>
                    <?= $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $this->data['Photo']['image'], array('w' => 300, 'h' => 250, 'zc' => 1)), '/uploads/' . $this->data['Photo']['image'], array('escape' => false, 'class' => 'fancybox')) ?>
                </div>
            </div>
        <?php endif; ?>
        
        <?php echo $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('Photo.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Photo.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Photo.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Photo.id'))); ?>
        </li>
    </ul>
</div>
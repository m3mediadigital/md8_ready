<script type="text/javascript">
    $(document).ready(function() {

        function showCoords(c)
        {
            $('#x_comeca').val(c.x);
            $('#x_termina').val(c.x2);
            $('#y_comeca').val(c.y);
            $('#y_termina').val(c.y2);
            $('#largura').val(c.w);
            $('#altura').val(c.h);
        };


        $('#jCrop').Jcrop({
            onSelect: showCoords,
            aspectRatio: <?= $this->Form->value('Album.width') ?> / <?= $this->Form->value('Album.height') ?>
        });
    });
</script>

<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Photos'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Photo.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Photo.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Photo.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Photo.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc photos clear">
    <div class="content">
        <?php echo $this->Form->create('Photo', array('type'=>'file'));?>
        	<?php
		echo $this->Form->input('id');
        echo $this->Form->input('image', array('type' => 'hidden'));
        echo $this->Form->input('x_comeca', array('type' => 'hidden', 'id' => 'x_comeca'));
        echo $this->Form->input('x_termina', array('type' => 'hidden', 'id' => 'x_termina'));
        echo $this->Form->input('y_comeca', array('type' => 'hidden', 'id' => 'y_comeca'));
        echo $this->Form->input('y_termina', array('type' => 'hidden', 'id' => 'y_termina'));
        echo $this->Form->input('largura', array('type' => 'hidden', 'id' => 'largura'));
        echo $this->Form->input('altura', array('type' => 'hidden', 'id' => 'altura'));
        echo $this->Form->input('width', array('type' => 'hidden', 'value' => $this->Form->value('Album.width')));
        echo $this->Form->input('height', array('type' => 'hidden', 'value' => $this->Form->value('Album.height')));
        echo $this->Html->image('/uploads/' . $this->Form->value('Photo.image'), array('id' => 'jCrop'));
	?>
        <?php echo $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('Photo.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Photo.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Photo.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Photo.id'))); ?>
        </li>
    </ul>
</div>
<style type="text/css">

            .link-remover-photo { position: relative; top: -121px; right: 30px }

            .gallery-image li { float: left; }
            .gallery-image li img:hover { opacity: 0.5; }

            .how-user { font: 14px Arial; }
            .how-user i { color: red; }

</style>
<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo $albumName['Album']['title']; ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array( 'controller' => 'albums', 'action' => 'index' ), array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Back'), array( 'controller' => 'albums', 'action' => 'index' ) ); ?>
        </li>
    </ul>
</div>

<div class="bloc examples clear">
            <?php echo $this->Session->flash(); ?>  
    <div class="content galleryMainContent">
        <h2>Imagens:</h2>       
        <div class="clear"></div>

        <div class="outsideGallery">
            <div class="insideGallery">
                <ul class="gallery"  id="droppable2">

                    <?php echo $this->Form->create( 'Photo', array( 'type' => 'file', 'action' => 'anexar' ) ) ?>
                    <?php echo $this->Form->input( 'album_id', array( 'type' => 'hidden', 'value' => $album_id ) ) ?>
                    
                    <span class="how-user">
                        Seleciona as imagens para serem adicionadas 
                        <br />
                        <i>Dica: Segure Ctrl e clique nas imagens para serems selecionadas.</i>                    
                    </span>
                    <br />    
                    <?php echo $this->Form->input( 'images.', array( 'type' => 'file', 'multiple' ) ) ?>
                    <?php echo $this->Form->end( 'Upload' ) ?>
                    
                </ul> 
            </div>
        </div>
    </div>
</div>

<div class="bloc">
    <div class="content" style="position:static;">
        <h2>Arquivos Anexados:</h2>        
        <ul class="gallery-image" id="droppable">
            <?php if ( $actualFotos ): ?>
                <?php foreach ( $actualFotos as $photo ): ?>
                    <li>
                        <?php echo $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $photo['Photo']['image'], array('w' => 200, 'h' => 150, 'zc' => 1)), '/uploads/' . $photo['Photo']['image'], array('escape' => false, 'class' => 'fancybox')); ?>

                        <?php echo $this->Html->link( $this->Html->image('/coreadmin/img/icons/remover2.png', array( 'width' => 16 ) ) , array( 'action' => 'delete',$photo['Photo']['id'],$album_id ), array( 'class' => 'link-remover-photo', 'escape' => false ),'Realmente deseja remover esta imagem ?' ); ?>
                    </li>
                <?php endforeach; ?>
            <?php else: ?>
                <center> <h3> Nenhua imagem cadastrada cadastrada! </h3> </center>                
            <?php endif ?>
        </ul>
        <div class="cb"></div>
    </div>
</div>
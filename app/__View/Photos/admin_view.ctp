<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Photos'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Photo.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Photo.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Photo.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Photo.id'))); ?>
        </li>
    </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		
                        <td><h4><?php echo __('Id'); ?></h4></td>
                        <td><?php echo $photo['Photo']['id']; ?></td>
                     </tr>
                     <tr>		
                        <td><h4><?php echo __('Título'); ?></h4></td>
                        <td><?php echo $photo['Photo']['title']; ?></td>
                     </tr>
                     <tr>		
                        <td><h4><?php echo __('Autor'); ?></h4></td>
                        <td><?php echo $photo['Photo']['author']; ?></td>
                     </tr>
                     <tr>		
                        <td><h4><?php echo __('Alt'); ?></h4></td>
                        <td><?php echo $photo['Photo']['alt']; ?></td>
                     </tr>
                     <tr>		
                        <td><h4><?php echo __('Imagem'); ?></h4></td>
                        <td><?php echo $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $photo['Photo']['image'], array('w' => 300, 'h' => 250, 'zc' => 1)), '/uploads/' . $photo['Photo']['image'], array('escape' => false, 'class' => 'fancybox')); ?></td>
                     </tr>                
                 </tbody>
             </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Photo.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Photo.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Photo.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Photo.id'))); ?>
        </li>
    </ul>
</div>
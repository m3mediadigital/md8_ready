<script type="text/javascript">
    jQuery(function($){
        function attachTextListener(input, func) {
            if (window.addEventListener) {
                  input.addEventListener('input', func, false);
            } else
                  input.attachEvent('onpropertychange', function() {
                    func.call(input);
                  });
          }

          var myInput = document.getElementById('searchPhoto');
          attachTextListener(myInput, function() {
                  var Valor = this.value;

                  if (Valor == '') {
                          $('.bloc.examples li.draggable.ui-draggable').css('display', '');
                  } else {
                          $('.bloc.examples li.draggable.ui-draggable').css('display', 'none');
                          $('.bloc.examples li.draggable.ui-draggable img[alt*="'+Valor+'"]').parent('li').css('display', 'inline-block');
                  }

          });
    });
</script>
<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __($tabela); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('controller' => $tabela, 'action' => 'index'), array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Back'), array('controller' => $tabela, 'action' => 'index')); ?>
        </li>
    </ul>
</div>

<div class="bloc examples clear">
    <div class="content galleryMainContent">
        
        <?= $this->Form->input('search', array('id' => 'searchPhoto', 'label' => false, 'style' => 'float:right', 'div' => false, 'placeHolder' => 'Buscar Foto')) ?>
        <h2>Imagens:</h2>

        <div class="buttonToUp ui-state-default ui-corner-all alignLeft">
            <?= $this->Html->link('up', '#', array('class' => 'galleryUp ui-icon ui-icon-carat-1-s')) ?>
        </div>
        <div class="buttonToDown ui-state-default ui-corner-all alignLeft">
            <?= $this->Html->link('down', '#', array('class' => 'galleryDown ui-icon ui-icon-carat-1-n')) ?>
        </div>
        
        <div class="clear"></div>

        <div class="outsideGallery">
            <div class="insideGallery">
                <ul class="gallery"  id="droppable2">
                    <?php
                    $erased = 1;
                    foreach ($photos as $photo) {
                        $listed = false;
                        foreach ($table_files as $file) {
                            if ($file['TableFiles']['table_id'] == $id && $file['TableFiles']['photo_id'] == $photo['Photo']['id']) {
                                $listed = true;
                            }
                        }

                        if ($listed == false) {
                            $displayed = 'nonerased';
                            if ($erased > 15) {
                                $displayed = 'erased';
                            }
                            echo "<li class='draggable ". $displayed ."' baseUrl='" . Router::url(array('controller' => 'photos', 'action' => 'anexarAjax', '?' => array('table' => $tabela, 'tableId' => $id, 'photoId' => $photo['Photo']['id']))) . "'>";
                            echo $this->PhpThumb->thumbnail('/uploads/' . $photo['Photo']['image'], array('w' => '200', 'h' => '150', 'zc' => '1', 'q' => '100'), array('alt' => $photo['Photo']['title']));
                            echo "<p><b>".$photo['Photo']['title']."</b></p>";
                            echo "</li>";
                            $erased++;
                        }
                    }
                    ?>
                </ul> 
            </div>
        </div>
    </div>
</div>

<div class="bloc">
    <div class="content" style="position:static;">
        <h2>Arquivos Anexados:</h2>
        <ul class="gallery" id="droppable">
            <?php foreach ($table_files as $files): ?>
                <li class="draggable" baseUrl="<?php echo Router::url(array('controller' => 'photos', 'action' => 'anexarAjax', '?' => array('table' => $tabela, 'tableId' => $id, 'photoId' => $files['Photo']['id']))) ?>">
                    <?= $this->PhpThumb->thumbnail('uploads/' . $files['Photo']['image'], array('w' => 200, 'h' => 150, 'zc' => 1)) ?>
                    <?= "<p><b>".$file['Photo']['title']."</b></p>" ?>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="cb"></div>
    </div>
</div>
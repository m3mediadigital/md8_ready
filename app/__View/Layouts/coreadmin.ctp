<?php //pr(); exit;  ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'/>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array('reset', '/coreadmin/css/jqueryUi', '/coreadmin/css/chosen', '/coreadmin/css/colorpicker', '/coreadmin/css/fancybox', '/coreadmin/css/fileupload', '/coreadmin/css/estilo', '/coreadmin/css/jquery.Jcrop'));
        echo $this->Html->script(array('jquery', 'jqueryUi', 'cycle', 'https://www.google.com/jsapi', '/coreadmin/js/colorpicker', '/coreadmin/js/money', '/coreadmin/ckeditor/ckeditor', '/coreadmin/js/maskedinput', '/coreadmin/js/easing', '/coreadmin/js/mousewheel', '/coreadmin/js/fancybox', '/coreadmin/js/chosen', '/coreadmin/js/iphone-checkboxes', '/coreadmin/js/tooltipsy', '/coreadmin/js/uniform', '/coreadmin/js/visualize', '/coreadmin/js/cookie/cookie', '/coreadmin/js/tmpl', '/coreadmin/js/loadimage', '/coreadmin/js/canvas', '/coreadmin/js/iframe-transport', '/coreadmin/js/fileupload', '/coreadmin/js/fileuploadFp', '/coreadmin/js/fileuploadUi', '/coreadmin/js/locale', '/coreadmin/js/jquery.Jcrop', '/coreadmin/js/jquery.color', '/coreadmin/js/actions'));
        if (isset($grafs)) {
            echo $this->element('grafs');
        }
        echo $scripts_for_layout;
        ?>
    </head>
    <body>
        <div id="head">
            <div class="left">
                <a href="#" class="button profile"><?= $this->Html->image('/coreadmin/img/icons/huser.png'); ?></a>
                Olá, <?= $this->Session->read('user.Profile.name'); ?>
                |
                <?= $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout', 'admin' => false)) ?>
            </div>
        </div>
        <div id="sidebar">
            <ul>
                <li>
                    <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/menu/layout.png') . ' Dashboard', array('controller' => 'pages', 'action' => 'index', 'plugin' => false, 'admin' => true), array('escape' => false)) ?>
                </li>
                <?php foreach ($itensmenu as $itenmenu): ?>
                    <li><?= $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $itenmenu['Menu']['image'], array('w' => 16, 'h' => 16, 'zc' => 1, 'f' => 'png')) . ' ' . $itenmenu['Menu']['title'], '#', array('escape' => false)) ?>
                        <ul>
                            <?php foreach ($itenmenu['Submenu'] as $submenu): ?>
                                <li><?= $this->Html->link($submenu['label'], array('plugin' => $submenu['plugin'], 'controller' => $submenu['controller'], 'action' => $submenu['action'], 'admin' => true)) ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php endforeach; ?>
                <li>
                    <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/menu/settings.png') . ' Configurações', '#', array('escape' => false)) ?>
                    <ul>
                        <li><?= $this->Html->link('Configurações', array('controller' => 'settings', 'action' => 'index', 'admin' => true), array('escape' => false)) ?></li>
                        <li><?= $this->Html->link('Menus', array('controller' => 'menus', 'action' => 'index', 'admin' => true), array('escape' => false)) ?></li>
                        <li><?= $this->Html->link('Submenus', array('controller' => 'submenus', 'action' => 'index', 'admin' => true), array('escape' => false)) ?></li>
                    </ul>
                </li>
                <li><?= $this->Html->link($this->Html->image('/coreadmin/img/icons/menu/user.png') . ' Usuarios', '#', array('escape' => false)) ?>
                    <ul>
                        <li><?= $this->Html->link('Perfil', array('controller' => 'profiles', 'action' => 'editProfile')) ?></li>
                        <li><?= $this->Html->link('Usuários', array('controller' => 'users', 'action' => 'index')) ?></li>
                        <li><?= $this->Html->link('Grupos', array('controller' => 'groups', 'action' => 'index')) ?></li>
                    </ul>
                </li>
            </ul>
            <a href="#collapse" id="menucollapse">&#9664; Esconder menu</a>
        </div>
        <div id="content" class="white">
            <?php echo $this->Session->flash(); ?>
            <?php echo $content_for_layout; ?>
        </div>
    </body>
</html>
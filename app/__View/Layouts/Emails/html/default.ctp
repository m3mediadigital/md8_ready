<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<?php
$logo = 'http://www.portalepisteme.com.br/img/logo.png';
$url = 'http://www.portalepisteme.com.br';
$endereco = 'Rua Floresta Nova, 8 - Vila Floresta Nova - Fernando de Noronha/PE - 53990-000';
$contato = 'Fone: +55 (81) 3619-1951';
?>
<html>
    <head>
        
    </head>
    <body>
        <table width="600" cellspacing="0" cellpadding="0" style="border: 1px solid #e6e6e6; width:600px; margin: 10px auto;">
            <tr style="text-align:center">
                <td colspan="3"><a href="<?= $url; ?>"><img src="<?= $logo; ?>" width="282" style="margin:30px 0;" /></td>
            </tr>
            <?php echo $content_for_layout; ?>
            <tr>
                <td width="538" style="border-top: 1px solid #e6e6e6;">
                    <p style="color:#a2a3a3; font:12px Arial; margin:15px 20px 5px;"><?= $endereco; ?></p>
                    <p style="color:#a2a3a3; font:12px Arial; margin:5px 20px 15px;">Fone: <?= $contato; ?></p>
                </td>
                <td width="26" style="border-top: 1px solid #e6e6e6;">
                    <a href="" target="_blank"><img src="http://www.m3mediadigital.com.br/emailcake/email_facebook.gif" width="16" height="16" /></a>    
                </td>
                <td width="36" style="border-top: 1px solid #e6e6e6;">
                    <a href="" target="_blank"><img src="http://www.m3mediadigital.com.br/emailcake/email_twitter.gif" width="16" height="16" /></a>     
                </td>
            </tr>
        </table>
    </body>
</html>
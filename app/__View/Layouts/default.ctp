<?php    
    
    // Tratamento para Rules do admin, assim imprimindo só os link do menu Administration
    // Só os submenus permitidos !  
    $expR = "/:/";
    if ( preg_match($expR,  $groupUserRules['Group']['rules'] ) > 0 ) {
       
        $controllers = strtolower( $groupUserRules['Group']['rules'] );
        $options = array( '{','}','"' );
        $controllers = str_replace( $options, '', $controllers );
        $controllers = str_replace( array( ':[' ), '-', $controllers );
        $controllers = str_replace( array( '],' ), '|', $controllers );
        $controllers = explode( '|', $controllers );

            $permitions = array();    
            foreach ( $controllers as $value ) {
                $value = explode('-', $value);
                $control = $value[0];        
                $act = explode(',', $value[1]);
                
                $permitions[$control] = $act;            
            }
        $admin = false;
       
        } else { $admin = true; }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'/>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array('reset','/coreadmin/css/jqueryUi', '/coreadmin/css/chosen', '/coreadmin/css/colorpicker', '/coreadmin/css/fancybox', '/coreadmin/css/fileupload', '/coreadmin/css/estilo', '/coreadmin/css/jquery.Jcrop'));
        
        echo $this->Html->script(array('jquery', '/coreadmin/ckeditor/ckeditor.js','jqueryUi', 'https://www.google.com/jsapi', '/coreadmin/js/colorpicker', '/coreadmin/js/money', '/coreadmin/ckeditor/ckeditor', '/coreadmin/js/maskedinput', '/coreadmin/js/easing', '/coreadmin/js/mousewheel', '/coreadmin/js/fancybox', '/coreadmin/js/chosen', '/coreadmin/js/iphone-checkboxes', '/coreadmin/js/tooltipsy', '/coreadmin/js/uniform', '/coreadmin/js/visualize', '/coreadmin/js/cookie/cookie', '/coreadmin/js/tmpl', '/coreadmin/js/loadimage', '/coreadmin/js/canvas', '/coreadmin/js/iframe-transport', '/coreadmin/js/fileupload', '/coreadmin/js/fileuploadFp', '/coreadmin/js/fileuploadUi', '/coreadmin/js/locale', '/coreadmin/js/jquery.Jcrop', '/coreadmin/js/jquery.color','/coreadmin/js/actions', 'https://www.google.com/jsapi'));
        if (isset($grafs)) {
            echo $this->element('grafs');
        }
        echo $scripts_for_layout;
        ?>
        <script type="text/javascript">
            google.load('visualization', '1.0', {'packages':['corechart']});
            google.load('visualization', '1', {'packages': ['geochart']});

            google.setOnLoadCallback(drawAnalitcs);
            google.setOnLoadCallback(drawMobile);
            google.setOnLoadCallback(drawMapWorld);

            function drawAnalitcs() {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Semana');
                data.addColumn('number', 'Visualizações');
                data.addColumn('number', 'Visitas');
                data.addRows([
                <?php
                if(isset($grafAnalitycs)){
                    foreach ($grafAnalitycs as $dados) {
                        $i = 0;
                        foreach ($dados as $dado) {
                            if($i == 0){
                                echo "['" . $i . "', " . $dado[0]['pageviews'] . ", " . $dado[0]['visits'] . "]";
                            } else {
                                echo ",['" . $i . "', " . $dado[0]['pageviews'] . ", " . $dado[0]['visits'] . "]";
                            }
                            $i++;
                        }
                    }
                }
                ?>
                ]);

                var chart = new google.visualization.AreaChart(document.getElementById('graf_area'));
                chart.draw(data, {height: 220, title: 'Visitas e Pageviews por semana', backgroundColor: '#f7f7f7',
                    hAxis: {title: 'Semana', titleTextStyle: {color: '#8a8a8a'}},
                    chartArea:{left:"10%"}
                });
            };

            function drawMobile() {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Acesso');
                data.addColumn('number', 'Visitas');
                data.addRows(2);
                <?php
                if(isset($analyticsMobile)){
                    foreach($analyticsMobile as $key => $value){
                        echo "data.setValue({$key}, 0, '{$value['isMobile']}');\n";
                        echo "data.setValue({$key}, 1, {$value['visits']});\n";
                    }
                }
                ?>
                var chart = new google.visualization.PieChart(document.getElementById('graf_mobile'));
                chart.draw(data, {width: 500, height: 220, title: 'Acessos mobile do mês anterior', backgroundColor: '#f7f7f7'});
              };

            function drawMapWorld() {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'País');
                data.addColumn('number', 'Visitas');
                data.addRows([
                <?php
                if(isset($analyticsWorld)){
                    $j = 0;
                    foreach($analyticsWorld as $value){
                        echo ($j == 0) ? "['{$value['country']}', {$value['visits']}]" : ",['{$value['country']}', {$value['visits']}]";
                        $j++;
                    }
                }
                ?>
                ]);

                var chart = new google.visualization.GeoChart(document.getElementById('map_world'));
                chart.draw(data, {height: '400px', backgroundColor: '#f7f7f7', title: 'Acesso mundial do mês anterior'});
            };
        </script>
    </head>
    <body>
        <div id="head">
            <div class="left">
                <?php $profileImage = $this->Session->read('Logged.Profile.image'); ?>
                <?= (!empty($profileImage)) ? '<a href="#" class="imageProfile">' . $this->PhpThumb->thumbnail('uploads/' . $profileImage, array('w' => 27, 'h' => 27, 'zc' => 1, 'f' => 'jpg')) . '</a>' : '<a href="#" class="button profile">' . $this->Html->image('/coreadmin/img/icons/huser.png') . '</a>'; ?>
                Olá, <?= $this->Session->read('Logged.User.name'); ?>
                |
                <?= $this->Html->link('Sair', array('controller' => 'users', 'action' => 'logout', 'admin' => false)) ?>
            </div>
            <!--
            <div class="right">
            <?= $this->Form->create(substr(ucfirst($this->request->params['controller']), 0, -1), array('url' => array('action' => 'search'), 'class' => 'search placeholder', 'id' => 'search')) ?>
                <label>Busca</label>
                <input type="text" value="" name="data" class="text"/>
                <input type="submit" value="rechercher" class="submit"/>
            <?= $this->Form->end() ?>
            </div>
            -->
        </div>
        <div id="sidebar">
            <ul>
                <li>
                    <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/menu/layout.png') . ' Dashboard', array('controller' => 'dashboards', 'action' => 'index', 'plugin' => false, 'admin' => true), array('escape' => false)) ?>
                </li>
                <li>
                    <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/menu/chart.png') . ' Analytics', array('controller' => 'dashboards', 'action' => 'analytics', 'plugin' => false, 'admin' => true), array('escape' => false)) ?>
                </li>
                <?php foreach ($itensmenu as $itenmenu): ?>
                    <li><?= $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $itenmenu['Menu']['image'], array('w' => 16, 'h' => 16, 'zc' => 1, 'f' => 'png')) . ' ' . $itenmenu['Menu']['title'], '#', array('escape' => false)) ?>
                        <ul>
                            <?php foreach ($itenmenu['Submenu'] as $submenu): ?>
                                <?php if ( $admin === false ): ?>
                                    
                                    <?php if ( array_key_exists( $submenu['controller'], $permitions) ): ?>
                                        <li><?= $this->Html->link($submenu['label'], array('plugin' => $submenu['plugin'], 'controller' => $submenu['controller'], 'action' => $submenu['action'], 'admin' => true)) ?></li>    
                                    <?php endif ?>
                                <?php else: ?>
                                    
                                    <li><?= $this->Html->link($submenu['label'], array('plugin' => $submenu['plugin'], 'controller' => $submenu['controller'], 'action' => $submenu['action'], 'admin' => true)) ?></li>

                                <?php endif ?>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php endforeach; ?>
                
                <?php if ( $this->Session->read('Logged.Group.id') == 1 ): ?>
                    <li>
                    <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/menu/settings.png') . ' Configurações', '#', array('escape' => false)) ?>
                    <ul>
                        <li><?= $this->Html->link('Configurações', array('controller' => 'settings', 'action' => 'index', 'admin' => true), array('escape' => false)) ?></li>
                        <li><?= $this->Html->link('Recortes', array('controller' => 'crops', 'action' => 'index', 'admin' => true), array('escape' => false)) ?></li>
                        <li><?= $this->Html->link('Menus', array('controller' => 'menus', 'action' => 'index', 'admin' => true), array('escape' => false)) ?></li>
                        <li><?= $this->Html->link('Submenus', array('controller' => 'submenus', 'action' => 'index', 'admin' => true), array('escape' => false)) ?></li>
                    </ul>
                    </li>
                    
                    <li><?= $this->Html->link($this->Html->image('/coreadmin/img/icons/menu/user.png') . ' Usuarios', '#', array('escape' => false)) ?>
                        <ul>
                            <li><?= $this->Html->link('Mudar senha', array('controller' => 'users', 'action' => 'passwordChange')) ?></li>
                            <li><?= $this->Html->link('Usuários', array('controller' => 'users', 'action' => 'index')) ?></li>
                            <li><?= $this->Html->link('Grupos', array('controller' => 'groups', 'action' => 'index')) ?></li>
                        </ul>
                    </li>
                <?php endif ?>
                

            </ul>
            <a href="#collapse" id="menucollapse">&#9664; Esconder menu</a>
        </div>
        <div id="content" class="white">
            <?php echo $this->Session->flash(); ?>
            <?php echo $content_for_layout; ?>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.changeState img').live('click', function(){
                    var image = $(this);
                    original = image.attr('src');
                    image.attr('src', '<?= $base_url ?>/coreadmin/img/icons/load.gif')
                    var controller = $(this).parent().attr('model')+'s';
                    controller = controller.toLowerCase();
                    var model = $(this).parent().attr('model');
                    var id = $(this).parent().attr('id');
                    $.ajax({
                        url: '<?= $base_url ?>/admin/'+controller+'/changeState/'+id+'/'+model,
                        success: function(data) {
                            if(data == 1){
                                image.attr('src', '<?= $base_url ?>/coreadmin/img/icons/ativar.png')
                            }else if(data == 0){
                                image.attr('src', '<?= $base_url ?>/coreadmin/img/icons/desativar.png')
                            }else{
                                image.attr('src', original)
                            }
                        }
                    });
                });
            });
        </script>
    </body>
</html>
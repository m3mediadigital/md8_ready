<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array('estilos', 'font-awesome.min', 'font-awesome-ie7.min', 'prettyPhoto', 'jqueryUi'));
        echo $this->Html->script(array('jquery', 'lazyload', 'cycle', 'jqueryUi', 'prettyPhoto', 'jquery.maskMoney', 'jquery.maskedinput-1.3.min', 'actions'));
        echo $scripts_for_layout;
        ?>
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <meta property="fb:app_id" content="162396560593052"/>
    </head>

	<body id="<?php echo $page ?>">
<div id="container">
<?php echo $this->Session->flash(); ?>
    <div id="header-out"><div id="header-in">
        
        <?php echo $this->Html->link('Logo - MD8', array('action' => 'index'), array('escape' => false, 'id' => 'logo')) ?>

		<div class="dadosCliente">
        	<p>Olá, visitante. Acesse <?php echo $this->Html->link('sua conta', '#') ?> ou <?php echo $this->Html->link('cadastre-se', array('action' => 'cadastro')) ?></p>
        </div>
		
        <div id="carrinho">
        	<h1><i class="ico-carrinho"></i>
            <?php echo $this->Html->link('MEU CARRINHO<small>- 2 item(ns) - R$ 100,00</small>', 'javascript:;', array('escape' => false, 'id'=>'openCarrinho')) ?></h1>
        </div>
        
        <div id="compras">
        	<div class="produto">
            	<p class="nome">Nome do produto completo Nome do produto completo...</p>
				<p class="quantidade">x1</p>
                <p class="valor">R$ 50,00</p>
                <?php echo $this->Html->link('x', 'javascript:;', array('escape' => false, 'id'=>'excluir', 'class'=>'excluir')) ?>
            </div>
        	<div class="produto">
            	<p class="nome">Nome do produto completo...</p>
				<p class="quantidade">x1</p>
                <p class="valor">R$ 50,00</p>
                <?php echo $this->Html->link('x', 'javascript:;', array('escape' => false, 'id'=>'excluir', 'class'=>'excluir')) ?>
            </div>
            
            <div class="total">
                <p>Sub-Total: R$ 100,00</p>
                <p>Total: R$ 100,00</p>
            </div>
			<?php echo $this->Html->link('Finalizar Pedido', 'javascript:;', array('escape' => false, 'class'=>'bt-pedido')) ?>
        </div>
        
        <ul id="nav">
            <li><?php echo $this->Html->link('Home', array('action' => 'index'), array('class' => 'notranslate menu01')) ?></li>
            <li><?php echo $this->Html->link('A MD8', array('action' => 'amd8'), array('class' => 'menu02')) ?></li>
            <li><?php echo $this->Html->link('Clientes', array('action' => 'clientes'), array('class' => 'menu03')) ?></li>
            <li><?php echo $this->Html->link('Produtos', array('action' => 'produtos'), array('class' => 'menu04')) ?></li>
            <li><?php echo $this->Html->link('Notícias', array('action' => 'noticias'), array('class' => 'menu05')) ?></li>
            <li><?php echo $this->Html->link('Dúvidas Frequentes', array('action' => 'duvidasfrequentes'), array('class' => 'menu06')) ?></li>
            <li><?php echo $this->Html->link('Contato', array('action' => 'contato'), array('class' => 'menu07')) ?></li>
        </ul>

        <ul id="redessociais">
            <li><?php echo $this->Html->link('Facebook - MD8', 'http://www.facebook.com', array('escape' => false, 'class' => 'facebook', 'target' => '_blank')) ?></li>
            <li><?php echo $this->Html->link('Twitter - MD8', 'http://www.twitter.com', array('escape' => false, 'class' => 'twitter', 'target' => '_blank')) ?></li>
            <li><?php echo $this->Html->link('Busca - MD8', 'javascript:;', array('escape' => false, 'class' => 'busca', 'id'=>'openBusca')) ?></li>
		</ul>

		<?php //echo $this->Session->flash(); ?>    
        <?php echo $this->Form->create('Page', array('class' => 'formBusca', 'id'=>'busca', 'url' => '/produtos')) ?>
            <?php echo $this->Form->input('pesquisa', array('label' => false, 'div' => false, 'placeholder' => 'pesquisar por...')) ?>
            <?php echo $this->Form->submit('pesquisar', array('div' => false)) ?>
        <?php echo $this->Form->end() ?>
                                           
    </div></div><!-- #header-in #header-out -->
<div id="content">

<div id="mainContent"> 
	<?php echo $content_for_layout; ?>
</div><!-- #mainContent -->
              
        <div class="clear"></div>
        </div><!-- #content -->
                
		<div id="footer-out"><div id="footer-in">
			<?php echo $this->Html->link('M3 Media Digital', 'http://www.m3mediadigital.com.br', array('escape' => false, 'id' => 'm3', 'target' => '_blank')) ?>

            <ul id="end">
            	<li><i class="ico-maps"></i> Rua Vereador Cícero Azevedo, 39 | Lagoa Seca - Natal - RN</li>
            	<li><i class="ico-phone"></i> Telefone: (84) 3213-2264 / 3081-3014</li>
            	<li><i class="ico-email"></i> E-mail: <?php echo $this->Html->link('contato@md8.com.br', 'mailto:contato@md8.com.br') ?></li>
            </div><!-- #end -->
            
		</div></div><!-- #footer-in #footer-out -->
                
        </div><!-- #container -->
	</body>
</html>
<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('News'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
            <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('News.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('News.id'))); ?>
            <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('News.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('News.id'))); ?>
        </li>
    </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('News', array('type'=>'file'));?>
    	<?= $this->Form->input('id');?>
	    <?= $this->Form->input('title');?>
        <?= $this->Form->input('publication');?>
	    <?= $this->Form->input('text');?>
        <?= $this->Form->input('image', array( 'type' => 'file' ));?>

        <div class="input">
            <?php if ( !$this->request->data['News']['image'] ): ?>

                <?php echo $this->Html->link($this->PhpThumb->thumbnail('/img/image-not-found.jpg', array('w' => 240, 'h' => 240, 'zc' => 1)), '/img/image-not-found.jpg', array('escape' => false, 'class' => 'fancybox')); ?>
            <?php else: ?>

                <?php if( file_exists( APP.'webroot/uploads/'.$this->request->data['News']['image'] ) ): ?>
                    <?php echo $this->Html->link($this->PhpThumb->thumbnail('/uploads/' . $this->request->data['News']['image'], array('w' => 240, 'h' => 240, 'zc' => 1)), '/uploads/' . $this->request->data['News']['image'], array('escape' => false, 'class' => 'fancybox')); ?>
                <?php else: ?>
                    <?php echo $this->Html->link($this->PhpThumb->thumbnail('/img/image-not-found.jpg', array('w' => 240, 'h' => 240, 'zc' => 1)), '/img/image-not-found.jpg', array('escape' => false, 'class' => 'fancybox')); ?>
                <?php endif ?>

            <?php endif ?>
        </div>

	    <?= $this->Form->input('source');?>
        <?= $this->Form->input('active');?>

        <?= $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
            <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('News.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('News.id'))); ?>
            <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('News.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('News.id'))); ?>
        </li>
    </ul>
</div>
<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('News'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Adicionar'), array('action'=>'add')); ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <?php echo $this->Form->create('News', array('url' => array('action' => 'allActionsListData'))); ?>
        <table>
            <thead>
                <tr>
                    <th><input type="checkbox" class="checkall"/></th>
                    <th><?php echo $this->Paginator->sort('id');?></th>
                    <th><?php echo $this->Paginator->sort('title');?></th>
                    <th><?php echo $this->Paginator->sort('publication');?></th>
                    <th><?php echo $this->Paginator->sort('image');?></th>                    
                    <th><?php echo $this->Paginator->sort('source');?></th>
                    <th><?php echo $this->Paginator->sort('active');?></th>
                    <th class="actions">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                    foreach ($news as $news):
                        $class = null;
                        if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                        }
                ?>
                <tr>
                    <td>
                        <?= $this->Form->input('News.id.'.$news['News']['id'],array('value'=>$news['News']['id'] ,'type'=>'checkbox','label'=>false, 'div'=>false))?>
                    </td>
            		<td>
            			<?= $this->Html->link($news['News']['id'], array('action'=>'edit', $news['News']['id'])); ?>
            		</td>
            		<td>
            			<?= $this->Html->link($news['News']['title'], array('action'=>'edit', $news['News']['id'])); ?>
            		</td>
            		<td>
            			<?= $this->Html->link($this->Time->format('d/m/Y', $news['News']['publication']), array('action'=>'edit', $news['News']['id'])); ?>
            		</td>
                    <td>                        
                        <?php if ( !$news['News']['image'] ): ?>

                            <?php echo $this->Html->link($this->PhpThumb->thumbnail('/img/image-not-found.jpg', array('w' => 24, 'h' => 24, 'zc' => 1)), '/img/image-not-found.jpg', array('escape' => false, 'class' => 'fancybox')); ?>
                        <?php else: ?>

                            <?php if( file_exists( APP.'webroot/uploads/'.$news['News']['image'] ) ): ?>
                                <?php echo $this->Html->link($this->PhpThumb->thumbnail('/uploads/' . $news['News']['image'], array('w' => 24, 'h' => 24, 'zc' => 1)), '/uploads/' . $news['News']['image'], array('escape' => false, 'class' => 'fancybox')); ?>
                            <?php else: ?>
                                <?php echo $this->Html->link($this->PhpThumb->thumbnail('/img/image-not-found.jpg', array('w' => 24, 'h' => 24, 'zc' => 1)), '/img/image-not-found.jpg', array('escape' => false, 'class' => 'fancybox')); ?>
                            <?php endif ?>

                        <?php endif ?>
                                                
                    </td>
                    <td>
            			<?= $this->Html->link($news['News']['source'], array('action'=>'edit', $news['News']['id'])); ?>
            		</td>
            		<?php if ($news['News']['active'] == 1): ?>
            		<td>
            			<?= $this->Html->image('/coreadmin/img/icons/ativar.png', array('title' => 'Ativo', 'class' => 'ListImage')) ?>
            		</td>
            		<?php else: ?>
            		<td>
            			<?= $this->Html->image('/coreadmin/img/icons/desativar.png', array('title' => 'Desativado', 'class' => 'ListImage')) ?>
            		</td>
            		<?php endif; ?>

                    <td class="actions">
                        <ul>

                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-03.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'view', $news['News']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-04.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'edit', $news['News']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Form->postLink($this->Html->image('jqueryui/ico-05.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'delete', $news['News']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $news['News']['id'])); ?>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd">
            <?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}')));?>
        </p>
        <?php if(count($news)>0): ?>        
        <div class="acoesMultiplas">
            <button type="submit" name="data[News][action]" value="delete" class="bt-deletar">Remover selecionados</button>
            <button type="submit" name="data[News][action]" value="activate" class="bt-ativar">Ativar selecionados</button>
            <button type="submit" name="data[News][action]" value="deactivate" class="bt-desativar">Desativar selecionados</button>
        </div>
        <?php endif; ?>        
        <div class="pagination">
            <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape'=>false), null, array('class' => 'prev disabled'));?>
            <?php echo $this->Paginator->numbers(array('separator' => ' '));?>
            <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape'=>false), null, array('class' => 'next disabled'));?>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action'=>'add')); ?>
        </li>
    </ul>
</div>
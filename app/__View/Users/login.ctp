<?= $this->Html->image('/coreadmin/img/logo.png', array('class' => 'logoAdmin')) ?>
<h1><?= $this->Html->image('/coreadmin/img/icons/lock-closed.png') ?> Painel de controle</h1>
<?php echo $this->Form->create('User', array('action' => 'login')); ?> 
<?php echo $this->Session->flash(); ?>
<div class="input placeholder">
    <label for="email">Login</label>
    <?= $this->Form->input('login', array('label' => false, 'div' => false)); ?>
</div>
<div class="input placeholder">
    <label for="pass">Senha</label>
    <?= $this->Form->input('password', array('label' => false, 'div' => false)); ?>
</div>
<div class="submit">
    <p class="lostpassword"><?php echo $this->Html->link(__("Esqueci minha senha"), array('action' => 'passwordRecovery')) . "<br/>"; ?></p>
    <?php if (Configure::read('Authake.registration') == true) { ?>
            <!-- <p class="register"><?php echo $this->Html->link(__("Quero me registrar"), array('action' => 'register')) . "<br/>"; ?></p> -->
    <?php }; ?>
    <?= $this->Form->submit('Enviar', array('label' => false, 'div' => false)); ?>
</div>
<?php echo $this->Form->end() ?>
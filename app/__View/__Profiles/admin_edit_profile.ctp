<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Profiles'); ?></h1>
<div class="bloc clear">
    <div class="content">
        <?php $estados = array("AC" => "Acre", "AL" => "Alagoas", "AM" => "Amazonas", "AP" => "Amapá", "BA" => "Bahia", "CE" => "Ceará", "DF" => "Distrito Federal", "ES" => "Espírito Santo", "GO" => "Goiás", "MA" => "Maranhão", "MT" => "Mato Grosso", "MS" => "Mato Grosso do Sul", "MG" => "Minas Gerais", "PA" => "Pará", "PB" => "Paraíba", "PR" => "Paraná", "PE" => "Pernambuco", "PI" => "Piauí", "RJ" => "Rio de Janeiro", "RN" => "Rio Grande do Norte", "RO" => "Rondônia", "RS" => "Rio Grande do Sul", "RR" => "Roraima", "SC" => "Santa Catarina", "SE" => "Sergipe", "SP" => "São Paulo", "TO" => "Tocantins"); ?>
        <?= $this->Form->create('Profile', array('type' => 'file')); ?>
        <?= $this->Form->submit('Enviar') ?>
        <br/>
        <div class='input'>
            <label>Foto do perfil</label>
            <div class='input'>
                <?= $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $this->data['Profile']['image'], array('w' => 100, 'h' => 100, 'zc' => 1)), $this->PhpThumb->url('uploads/' . $this->data['Profile']['image'], array('w' => 640, 'h' => 480, 'zc' => 1)), array('escape' => false, 'class' => 'fancybox')) ?>
            </div>
        </div>
        <?= $this->Form->input('image', array('type' => 'file')); ?>
        <?= $this->Form->input('name', array('label' => 'Nome completo')); ?>
        <?= $this->Form->input('nickname', array('label' => 'Como deseja ser chamado?')); ?>
        <?= $this->Form->input('email'); ?>            
        <?= $this->Form->input('phone', array('type' => 'text', 'class' => 'phone', 'label' => 'Telefone')); ?>            
        <?= $this->Form->input('title', array('label' => 'Título')); ?>
        <?= $this->Form->input('cpf', array('class' => 'cpf')); ?>
        <?= $this->Form->input('rg'); ?>
        <?= $this->Form->input('birth', array('type' => 'text', 'class' => 'data', 'label' => 'Data de nascimento')); ?>
        <?= $this->Form->input('facebook'); ?>
        <?= $this->Form->input('twitter'); ?>
        <?= $this->Form->input('msn'); ?>
        <?= $this->Form->input('skype'); ?>
        <?= $this->Form->input('site'); ?>
        <?= $this->Form->input('blood', array('empty' => 'Tipo sanguíneo', 'label' => 'Tipo sanguíneo', 'options' => array('O+' => 'O+', 'A+' => 'A+', 'B+' => 'B+', 'AB+' => 'AB+', 'O-' => 'O-', 'A-' => 'A-', 'B-' => 'B-', 'AB-' => 'AB-'))); ?>
        <?= $this->Form->input('description', array('label' => 'Descrição')); ?>
        <?= $this->Form->input('cep', array('label' => 'CEP', 'class' => 'cep')); ?>
        <?= $this->Form->input('state', array('empty' => 'Estado', 'options' => $estados)); ?>
        <?= $this->Form->input('address'); ?>
        <?= $this->Form->input('city'); ?>
        <?= $this->Form->input('neighborhood'); ?>
        <?= $this->Form->end(__('Submit', true)); ?>
    </div>
</div>
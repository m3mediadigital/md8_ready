<div id="sidebar">
	<?php echo $this->element('categorias') ?><!-- CATERGORIAS -->
    <?php echo $this->element('atendimento') ?><!-- ATENDIMENTO -->
    <?php echo $this->element('orcamento') ?><!-- ORÇAMENTO -->
    <?php echo $this->element('newsletter') ?><!-- NEWSLETTER -->
    <?php echo $this->element('localizacao') ?><!-- LOCALIZAÇÃO -->
</div><!-- #sidebar -->

<div id="conteudo">

<ul class="slider">
    <?php foreach($banners as $banner): ?>
    <li>
        <?php echo $this->PhpThumb->thumbnail('uploads/' . $banner['Banner']['banner'], array('w' => 760, 'h' => 300, 'zc' => 1, 'q' => 100)); ?>
    	<span><?php echo $banner['Banner']['title']; ?></span>
    </li>
    <?php endforeach; ?>
</ul>
<i class="icon-angle-left icon-2x slideLeft"></i>
<i class="icon-angle-right icon-2x slideRight"></i>

<h1>Produtos em destaques</h1>
<?php foreach($products as $product): ?>
<?php  $categorias = array(); ?>
<div class="boxProdutos">
    <?php echo $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $product['Product']['cover'], array('w' => 220, 'h' => 110, 'zc' => 1, 'q' => 100)), "/produto".DS.Inflector::slug(strtolower($product['Product']['title']), '-').DS.$product['Product']['id'], array('escape' => false)); ?>
    <p><?php echo $this->Html->link($product['Product']['title'], "/produto".DS.Inflector::slug(strtolower($product['Product']['title']), '-').DS.$product['Product']['id']) ?></p>
    
    <small>
    <?php 
        foreach($product['Category'] as $category) {
            $categorias[$category['id']] = $category['category'];
        }
     ?>

    <?php $i = 1; ?>
    <?php foreach($categorias as $id => $categoria): ?>
        <?php echo $this->Html->link($categoria, DS."produtos".DS.Inflector::slug(strtolower($categoria),"-").DS.$id); ?>
        <?php echo ($i == count($categorias)) ? ""  : ","; ?>
        <?php $i++; ?>    
    <?php endforeach; ?>
    </small>
</div>
<?php endforeach; ?>


</div>
<div id="sidebar">
	<?php echo $this->element('categorias') ?><!-- CATERGORIAS -->
    <?php echo $this->element('atendimento') ?><!-- ATENDIMENTO -->
    <?php echo $this->element('orcamento') ?><!-- ORÇAMENTO -->
    <?php echo $this->element('newsletter') ?><!-- NEWSLETTER -->
</div><!-- #sidebar -->
<div id="internas">
	<h1>A MD8</h1>

	<?php echo $this->Html->link($this->PhpThumb->thumbnail(DS."uploads".DS.$md8['Md8']['image'], array('w' => 300, 'h' => 198, 'zc' => 1, 'q' => 100), array('class'=>'foto-empresa')), $this->PhpThumb->url(DS."uploads".DS.$md8['Md8']['image'], array('w' => 640, 'h' => 480, 'zc' => 1, 'q' => 100)), array('escape' => false, 'rel' => 'prettyPhoto')) ?>
    
	<?php echo $md8['Md8']['description']; ?>

	<div class="clear">&nbsp;</div>

	<h1>Localização</h1>

    <iframe width="760" height="295" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=C%C3%ADcero+de+Azevedo,+Lagoa+Seca,+Natal+-+RN+59031-020&amp;aq=&amp;sll=-5.802019,-35.211761&amp;sspn=0.005422,0.010568&amp;ie=UTF8&amp;hq=&amp;hnear=R.+C%C3%ADcero+de+Azevedo+-+Lagoa+Seca,+Natal+-+Rio+Grande+do+Norte,+59031-020&amp;t=m&amp;ll=-5.797856,-35.212083&amp;spn=0.010674,0.032573&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>

</div>
<div id="sidebar">
	<?php echo $this->element('categorias') ?><!-- CATERGORIAS -->
    <?php echo $this->element('atendimento') ?><!-- ATENDIMENTO -->
    <?php echo $this->element('orcamento') ?><!-- ORÇAMENTO -->
    <?php echo $this->element('newsletter') ?><!-- NEWSLETTER -->
    <?php echo $this->element('localizacao') ?> <!-- LOCALIZAÇÃO -->
</div><!-- #sidebar -->

<div id="internas">
	<h1>Dúvidas Frequentes</h1>
    <?php foreach($duvidas as $duvida): ?>
    <div class="duviva">
        <h2><?php echo $duvida['Frequentquestion']['title']; ?></h2>
        <p><?php echo strip_tags($duvida['Frequentquestion']['answer']); ?></p>
	</div>
    <?php endforeach; ?>
</div>
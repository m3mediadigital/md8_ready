<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Groups'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title' => 'Adicionar')), array('action' => 'add'), array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Adicionar'), array('action' => 'add')); ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <table>
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" class="checkall"/>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('id'); ?>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('name'); ?>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('active'); ?>
                    </th>
                    <th class="actions">
                        Ações
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($groups as $group):
                    $class = null;
                    if ($i++ % 2 == 0) {
                        $class = ' class="altrow"';
                    }
                    ?>
                    <tr>
                        <td>
                            <?= $this->Form->input('Group.id.' . $group['Group']['id'], array('value' => $group['Group']['id'], 'type' => 'checkbox', 'label' => false, 'div' => false)) ?>
                        </td>
                        <td>
                            <?= $this->Html->link($group['Group']['id'], array('action' => 'edit', $group['Group']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($group['Group']['name'], array('action' => 'edit', $group['Group']['id'])); ?>
                        </td>
                        <?php if ($group['Group']['active'] == 1): ?>
                            <td>
                                <?= $this->Html->image('/coreadmin/img/icons/ativar.png', array('title' => 'Ativo', 'class' => 'ListImage')) ?>
                            </td>
                        <?php else: ?>
                            <td>
                                <?= $this->Html->image('/coreadmin/img/icons/desativar.png', array('title' => 'Desativado', 'class' => 'ListImage')) ?>
                            </td>
                        <?php endif; ?>
                        <td class="actions">
                            <ul>
                                <li>
                                    <?php echo $this->Html->link('<button class="bt-ver">Ver</button>', array('action' => 'view', $group['Group']['id']), array('escape' => false)); ?>
                                </li>
                                <li>
                                    <?php echo $this->Html->link('<button class="bt-editar">Editar</button>', array('action' => 'edit', $group['Group']['id']), array('escape' => false)); ?>
                                </li>
                                <li>
                                    <?php echo $this->Form->postLink('<button class="bt-deletar">Remover</button>', array('action' => 'delete', $group['Group']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $group['Group']['id'])); ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}'))); ?></p>
        <?php if (count($groups) > 0): ?>        <div class="acoesMultiplas">
                <button class="bt-deletar">Remover selecionados</button>
                <button class="bt-ativar">Ativar selecionados</button>
                <button class="bt-desativar">Desativar selecionados</button>
            </div>
        <?php endif; ?>        <div class="pagination">
        <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape' => false), null, array('class' => 'prev disabled')); ?>
        <?php echo $this->Paginator->numbers(array('separator' => ' ')); ?>
        <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape' => false), null, array('class' => 'next disabled')); ?>
        </div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title' => 'Adicionar')), array('action' => 'add'), array('escape' => false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action' => 'add')); ?>
        </li>
    </ul>
</div>
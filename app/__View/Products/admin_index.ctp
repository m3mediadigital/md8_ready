<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Products'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Adicionar'), array('action'=>'add')); ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <?php echo $this->Form->create('Product', array('url' => array('action' => 'allActionsListData'))); ?>
        <table>
            <thead>
                <tr>
                    <th><input type="checkbox" class="checkall"/></th>
                    		<th><?php echo $this->Paginator->sort('id');?></th>
                                        		<th><?php echo $this->Paginator->sort('title');?></th>
                                        		<!-- <th><?php echo $this->Paginator->sort('description');?></th> -->
                                        		<th><?php echo $this->Paginator->sort('value');?></th>
                                        		<th><?php echo $this->Paginator->sort('tags');?></th>
                                        		<th><?php echo $this->Paginator->sort('cover');?></th>
                                        		<th><?php echo $this->Paginator->sort('album_id');?></th>
                                        		<!-- <th><?php echo $this->Paginator->sort('video');?></th> -->
                                                                                <th class="actions">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                    foreach ($products as $product):
                        $class = null;
                        if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                        }
                ?>
                <tr>
                    <td>
                        <?= $this->Form->input('Product.id.'.$product['Product']['id'],array('value'=>$product['Product']['id'] ,'type'=>'checkbox','label'=>false, 'div'=>false))?>
                    </td>
		<td>
			<?= $this->Html->link($product['Product']['id'], array('action'=>'edit', $product['Product']['id'])); ?>
		</td>
		<td>
			<?= $this->Html->link($product['Product']['title'], array('action'=>'edit', $product['Product']['id'])); ?>
		</td>
<!-- 		<td>
			<?= $this->Html->link($product['Product']['description'], array('action'=>'edit', $product['Product']['id'])); ?>
		</td> -->
		<td>
			<?= $this->Html->link(number_format($product['Product']['value'], 2,',', '.'), array('action'=>'edit', $product['Product']['id'])); ?>
		</td>
		<td>
			<?= $this->Html->link(strip_tags($product['Product']['tags']), array('action'=>'edit', $product['Product']['id'])); ?>
		</td>
		<td>

            <?php if ( $product['Product']['cover'] ): ?>
                <?php echo $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $product['Product']['cover'], array('w' => 24, 'h' => 24, 'zc' => 1)), '/uploads/' . $product['Product']['cover'], array('escape' => false, 'class' => 'fancybox')); ?>
            <?php else: ?> 
                <?php echo $this->Html->link($this->PhpThumb->thumbnail('/img/image-not-found.jpg', array('w' => 24, 'h' => 24, 'zc' => 1)), '/img/image-not-found.jpg', array('escape' => false, 'class' => 'fancybox')); ?>            
            <?php endif ?>
            
        </td>
	<td>
		<?= $this->Html->link($product['Album']['title'], array('controller' => 'albums', 'action' => 'view', $product['Album']['id'])); ?>
	</td>
		<!-- <td>
			<?= $this->Html->link($product['Product']['video'], array('action'=>'edit', $product['Product']['id'])); ?>
		</td> -->

                    <td class="actions">
                        <ul>
      <?php if(!empty($addImages)): ?>
                            <li>
                                <?php echo $this->Html->link('Anexar Imagens', array('controller' => 'photos', 'action' => 'anexar', $product['Product']['id'], '?' => array('tabela' => 'products')), array('escape' => false, 'class' => 'bt-anexar')); ?>
                            </li>
                            <?php endif; ?>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-03.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'view', $product['Product']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-04.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'edit', $product['Product']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-05.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array( 'action' => 'delete', $product['Product']['id']), array('escape' => false )); ?>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}')));?></p>
        <?php if(count($products)>0): ?>        <div class="acoesMultiplas">
            <button type="submit" name="data[Product][action]" value="delete" class="bt-deletar">Remover selecionados</button>

            <button name="data[Product][action]" value="activate" class="bt-ativar">Ativar selecionados</button>
            <button name="data[Product][action]" value="deactivate" class="bt-desativar">Desativar selecionados</button>
        </div>
        <?php endif; ?>        <div class="pagination">
            <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape'=>false), null, array('class' => 'prev disabled'));?>
            <?php echo $this->Paginator->numbers(array('separator' => ' '));?>
            <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape'=>false), null, array('class' => 'next disabled'));?>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?><div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action'=>'add')); ?>
        </li>
    </ul>
</div>
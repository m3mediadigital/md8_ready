<?php

App::uses('AppModel', 'Model');

/**
 * Photo Model
 *
 * @property Album $Album
 */
class Photo extends AppModel {

    var $actsAs = array(
        'MeioUpload.MeioUpload' => array(
            'image' => array(
                'dir' => 'uploads',
                'create_directory' => true,
                //'allowedMime' => array('application/pdf', 'application/msword', 'application/vnd.ms-powerpoint', 'application/vnd.ms-excel', 'application/rtf', 'application/zip'),
                //'allowedExt' => array('.pdf', '.doc', '.ppt', '.xls', '.rtf', '.zip'),
                'default' => false,

            )
        )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'album_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        )
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $hasMany = array(
        'TableFiles' => array(
            'className' => 'TableFiles',
            'foreignKey' => 'photo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}

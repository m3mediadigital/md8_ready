<?php
App::uses('AppModel', 'Model');
/**
* News Model
*
*/
class News extends AppModel {

var $actsAs = array(
	'MeioUpload.MeioUpload' => array(
		'image' => array(
			'dir' => 'uploads',
			'create_directory' => true,
			//'allowedMime' => array('application/pdf', 'application/msword', 'application/vnd.ms-powerpoint', 'application/vnd.ms-excel', 'application/rtf', 'application/zip'),
			//'allowedExt' => array('.pdf', '.doc', '.ppt', '.xls', '.rtf', '.zip'),
			'default' => false
		)
	)
);
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Esse campo é obrigatório',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'publication' => array(
			'date' => array(
				'rule' => array('date'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'text' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Esse campo é obrigatório',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
        
    public $hasMany = array(
        'TableFiles' => array(
            'className' => 'TableFiles',
            'foreignKey' => 'table_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => "SELECT TableFiles.id, TableFiles.table_id, TableFiles.photo_id, TableFiles.table_title, 
                                    Photo.id, Photo.image, Photo.title, Photo.alt, Photo.author
                                FROM table_files AS TableFiles
                                LEFT JOIN photos AS Photo ON (TableFiles.photo_id = Photo.id) 
                                WHERE TableFiles.table_title = 'news'",
            'counterQuery' => ''
        )
    );
}

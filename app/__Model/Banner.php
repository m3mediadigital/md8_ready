<?php
App::uses('AppModel', 'Model');
/**
* Banner Model
*
*/
class Banner extends AppModel {
    /**
    * Display field
    *
    * @var string
    */
    public $displayField = 'title';

    var $actsAs = array(
        'MeioUpload.MeioUpload' => array(
            'banner' => array(
                'dir' => 'uploads',
                'create_directory' => true,
                //'allowedMime' => array('application/pdf', 'application/msword', 'application/vnd.ms-powerpoint', 'application/vnd.ms-excel', 'application/rtf', 'application/zip'),
                //'allowedExt' => array('.pdf', '.doc', '.ppt', '.xls', '.rtf', '.zip'),
                'default' => false,

            )
        )
    );
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Esse campo é obrigatório',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}

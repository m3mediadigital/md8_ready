<?php

App::uses('AppModel', 'Model');

class Example extends AppModel {

    public $hasMany = array(
        'TableFiles' => array(
            'className' => 'TableFiles',
            'foreignKey' => 'table_id',
            'dependent' => false,
            'conditions' => array(
                'TableFiles.table_title = "examples"'
            ),
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    
}

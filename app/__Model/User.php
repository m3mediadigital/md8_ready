<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 * @property Group $Group
 */
class User extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'login' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Esse campo é obrigatório',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'O login informado já está sendo utilizado por outro usuário.'
            )
        ),
        'password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo obrigatório',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'active' => array(
            'boolean' => array(
                'rule' => array('boolean'),
                'message' => 'Campo obrigatório',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'group_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Campo obrigatório',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );


// public $hasOne = array(
//         'Profile' => array(
//             'className' => 'Profile',
//             'foreignKey' => 'user_id',
//             'dependent' => false,
//             'conditions' => '',
//             'fields' => '',
//             'order' => '',
//             'limit' => '',
//             'offset' => '',
//             'exclusive' => '',
//             'finderQuery' => '',
//             'counterQuery' => ''
//         )
//     );

    function hash($password){
        return Security::hash($password, 'sha1', true);
    }
    
    function getLoginData($login = '', $password = '') {
        $data = $this->find('first', array('conditions' =>
            array(
                'User.login' => $login,
                'User.password' => $this->hash($password)
                )
            )
        );
        if (is_array($data))
            unset($data['User']['password']);
        
        return $data;
    }

}

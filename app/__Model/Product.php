<?php
App::uses('AppModel', 'Model');
/**
* Product Model
*
 * @property Album $Album
 * @property Parentcategory $Parentcategory
*/
class Product extends AppModel {
    /**
    * Display field
    *
    * @var string
    */
    public $displayField = 'title';

    var $actsAs = array(
        'MeioUpload.MeioUpload' => array(
            'cover' => array(
                'dir' => 'uploads',
                'create_directory' => true,
                //'allowedMime' => array('application/pdf', 'application/msword', 'application/vnd.ms-powerpoint', 'application/vnd.ms-excel', 'application/rtf', 'application/zip'),
                //'allowedExt' => array('.pdf', '.doc', '.ppt', '.xls', '.rtf', '.zip'),
                'default' => false,

            )
        )
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Esse campo é obrigatório',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Esse campo é obrigatório',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

        //The Associations below have been created with all possible keys, those that are not needed can be removed
        
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Album' => array(
			'className' => 'Album',
			'foreignKey' => 'album_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Parentcategory' => array(
			'className' => 'Parentcategory',
			'foreignKey' => 'product_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


	public $hasAndBelongsToMany = array(
        'Category' => array(
                'className'              => 'Category',
                'joinTable'              => 'parentcategories',
                'foreignKey'             => 'product_id',
                'associationForeignKey'  => 'category_id',
                'unique'                 => true,
                'conditions'             => '',
                'fields'                 => '',
                'order'                  => '',
                'limit'                  => '',
                'offset'                 => '',
                'finderQuery'            => '',
                'deleteQuery'            => '',
                'insertQuery'            => ''
            ),
        'Subcategory' => array(
                'className'              => 'Subcategory',
                'joinTable'              => 'parentcategories',
                'foreignKey'             => 'product_id',
                'associationForeignKey'  => 'subcategory_id',
                'unique'                 => true,
                'conditions'             => '',
                'fields'                 => '',
                'order'                  => '',
                'limit'                  => '',
                'offset'                 => '',
                'finderQuery'            => '',
                'deleteQuery'            => '',
                'insertQuery'            => ''
            )
    );

}

<?php
App::uses('AppModel', 'Model');
/**
* Accommodation Model
*
*/
class Accommodation extends AppModel {

	var $actsAs = array(
		'MeioUpload.MeioUpload' => array(
			'image' => array(
				'dir' => 'uploads',
				'create_directory' => true,
				//'allowedMime' => array('application/pdf', 'application/msword', 'application/vnd.ms-powerpoint', 'application/vnd.ms-excel', 'application/rtf', 'application/zip'),
				//'allowedExt' => array('.pdf', '.doc', '.ppt', '.xls', '.rtf', '.zip'),
				'default' => false
			)
		)
	);
}

<?php
App::uses('AppModel', 'Model');
/**
* Category Model
*
 * @property Subcategory $Subcategory
*/
class Category extends AppModel {

        //The Associations below have been created with all possible keys, those that are not needed can be removed
        
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Subcategory' => array(
			'className' => 'Subcategory',
			'foreignKey' => 'category_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


	public $hasAndBelongsToMany = array(
        'Product' => array(
                'className'              => 'Product',
                'joinTable'              => 'parentcategories',
                'foreignKey'             => 'category_id',
                'associationForeignKey'  => 'product_id',
                'unique'                 => true,
                'conditions'             => '',
                'fields'                 => '',
                'order'                  => '',
                'limit'                  => '',
                'offset'                 => '',
                'finderQuery'            => '',
                'deleteQuery'            => '',
                'insertQuery'            => ''
            )
    );

}

<?php
App::uses('AppModel', 'Model');
/**
* Talk Model
*
 * @property Transaction $Transaction
 * @property Filestalk $Filestalk
*/
class Talk extends AppModel {
    /**
    * Display field
    *
    * @var string
    */
    public $displayField = 'name';

        //The Associations below have been created with all possible keys, those that are not needed can be removed
        
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Transaction' => array(
			'className' => 'Transaction',
			'foreignKey' => 'transaction_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Filestalk' => array(
			'className' => 'Filestalk',
			'foreignKey' => 'talk_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}

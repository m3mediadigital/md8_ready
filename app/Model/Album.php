<?php
App::uses('AppModel', 'Model');
/**
* Album Model
*
*/
class Album extends AppModel {

	var $actsAs = array(
		'MeioUpload.MeioUpload' => array(
			'image' => array(
				'dir' => 'uploads',
				'create_directory' => true,
				//'allowedMime' => array('application/pdf', 'application/msword', 'application/vnd.ms-powerpoint', 'application/vnd.ms-excel', 'application/rtf', 'application/zip'),
				//'allowedExt' => array('.pdf', '.doc', '.ppt', '.xls', '.rtf', '.zip'),
				'default' => false
			)
		)
	);

	public $hasMany = array(
        'TableFiles' => array(
            'className' => 'TableFiles',
            'foreignKey' => 'table_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => "SELECT TableFiles.id, TableFiles.table_id, TableFiles.photo_id, TableFiles.table_title, 
                                    Photo.id, Photo.image, Photo.title, Photo.alt, Photo.author
                                FROM table_files AS TableFiles
                                LEFT JOIN photos AS Photo ON (TableFiles.photo_id = Photo.id) 
                                WHERE TableFiles.table_title = 'albums'",
            'counterQuery' => ''
        )
    );

}

<?php
App::uses('AppModel', 'Model');
/**
* Subcategory Model
*
 * @property Category $Category
 * @property Parentcategory $Parentcategory
*/
class Subcategory extends AppModel {

        //The Associations below have been created with all possible keys, those that are not needed can be removed
        
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => 'category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Parentcategory' => array(
			'className' => 'Parentcategory',
			'foreignKey' => 'subcategory_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


	public $hasAndBelongsToMany = array(
        'Product' => array(
                'className'              => 'Product',
                'joinTable'              => 'parentcategories',
                'foreignKey'             => 'subcategory_id',
                'associationForeignKey'  => 'product_id',
                'unique'                 => true,
                'conditions'             => '',
                'fields'                 => '',
                'order'                  => '',
                'limit'                  => '',
                'offset'                 => '',
                'finderQuery'            => '',
                'deleteQuery'            => '',
                'insertQuery'            => ''
            )
    );

}

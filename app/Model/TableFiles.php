<?php
App::uses('AppModel', 'Model');

class TableFiles extends AppModel {

    public $belongsTo = array(
        'Photo' => array(
            'className' => 'Photo',
            'foreignKey' => 'photo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}

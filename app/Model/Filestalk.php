<?php
App::uses('AppModel', 'Model');
/**
* Filestalk Model
*
 * @property Talk $Talk
 * @property User $User
*/
class Filestalk extends AppModel {
    /**
    * Display field
    *
    * @var string
    */
    public $displayField = 'title';

     var $actsAs = array(
        'MeioUpload.MeioUpload' => array(
            'file' => array(
                'dir' => 'uploads',
                'create_directory' => true,
                'maxSize' => 20971520, // 20MB
                'allowedMime' => array(
                    'application/pdf', 
                    'application/msword', 
                    'application/vnd.ms-powerpoint', 
                    'application/vnd.ms-excel', 
                    'application/rtf', 
                    'application/zip',
                    'text/csv',
                    'image/jpeg', 'image/pjpeg', 
                    'image/png', 
                    'image/gif', 
                    'image/bmp', 
                    'image/x-icon', 'image/vnd.microsoft.icon'
                    // Nao funcionou
                    //'application/cdr', 'application/coreldraw', 'application/x-cdr', 'application/x-coreldraw'
                ),
                'allowedExt' => array(
                    '.pdf', 
                    '.doc', '.docx', 
                    '.ppt', 
                    '.xls', 
                    '.rtf', 
                    '.zip', '.rar',
                    '.csv',
                    '.jpg', '.jpeg', 
                    '.png', 
                    '.gif', 
                    '.bmp', 
                    '.ico'
                    // Nao funcionou
                    //'.cdr'
                ),
                'default' => false,
            )
        )
    );


    //The Associations below have been created with all possible keys, those that are not needed can be removed
        
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Talk' => array(
			'className' => 'Talk',
			'foreignKey' => 'talk_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

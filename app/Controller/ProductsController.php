<?php
App::uses('AppController', 'Controller');
/**
 * Products Controller
 *
 * @property Product $Product
 */
class ProductsController extends AppController {


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Product->recursive = 0;
        if( !empty($this->request->query['q']) ){
            $this->paginate = array(
                'conditions' => array(
                    'Product.title LIKE' => '%'.$this->request->query['q'].'%'
                ),
                'limit' => 20,
                'order' => array(
                    'Product.title' => 'asc'
                )
            );
        }
		$this->set('products', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException(__('Invalid product'));
		}
		$this->set('product', $this->Product->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {			
			$categoriesData = $this->request->data['Product']['Parentcategory'];
			unset( $this->request->data['Product']['Parentcategory'] );
			$this->request->data['Product']['Parentcategory'] = array();

			
			$this->Product->create();			
			if ( $this->Product->save( $this->request->data ) ) {			
				//pr($this->request->data);exit();
				$lastId = $this->Product->getLastInsertID();
				$saveParents = array();
				foreach ($categoriesData as $value) {
					$categoyAr = explode('.', $value);
					if ( $categoyAr[0] == 'Category' ) {
						
						$saveParents = array_merge( $saveParents, array( array( 'product_id' => $lastId, 'category_id' => $categoyAr[1] ) ) ) ;

					} else if ( $categoyAr[0] == 'Subcategory' ) {
						$subCategory = $this->Subcategory->read( null, $categoyAr[1] );						
						$saveParents = array_merge( $saveParents, array( array( 'product_id' => $lastId, 'category_id' => $subCategory['Category']['id'], 'subcategory_id' => $categoyAr[1] ) ) ) ;
					}				
				}	
				$this->Parentcategory->saveAll( $saveParents );
				$this->Session->setFlash(__('The product has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product could not be saved. Please, try again.'), 'error');
			}
		}
		$albums = $this->Product->Album->find('list');
		$categories = $this->Category->find('all');

		$this->set(compact('albums', 'categories'));
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			pre( $this->request->data );
			throw new NotFoundException(__('Invalid product'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			$categoriesData = $this->request->data['Product']['Parentcategory'];
			unset( $this->request->data['Product']['Parentcategory'] );
			$this->request->data['Product']['Parentcategory'] = array();


			$saveParents = array();
			foreach ($categoriesData as $value) {
				$categoyAr = explode('.', $value);

				if ( $categoyAr[0] == 'Category' ) {
					
					$saveParents = array_merge( $saveParents, array( array( 'product_id' => $id, 'category_id' => $categoyAr[1] ) ) ) ;

				} else if ( $categoyAr[0] == 'Subcategory' ) {
					$subCategory = $this->Subcategory->read( null, $categoyAr[1] );
					$saveParents = array_merge( $saveParents, array( array( 'product_id' => $id, 'category_id' => $subCategory['Category']['id'], 'subcategory_id' => $categoyAr[1] ) ) ) ;

				}				
			}					
			$this->Parentcategory->saveAll( $saveParents );

			if ($this->Product->save($this->request->data)) {
				$this->Session->setFlash(__('The product has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product could not be saved. Please, try again.'), 'error');
			}
		} else {


			$product = $this->Product->read( null, $id );

			$categoriesSelecteds = null;
			foreach ($product['Parentcategory'] as $idParent) {
				
				if ( $idParent['subcategory_id'] ) {
					
					$subcategoryResult = $this->Subcategory->read( null, $idParent['category_id'] );					
					// Array para verificação de categorias já selecionada
					$categoriesSelectedsAr['Subcategory.'.$subcategoryResult['Subcategory']['id']] = $subcategoryResult['Category']['category'].' -> '.$subcategoryResult['Subcategory']['subcategory'];
					// Array para link de remoção de categria
					$categoriesSelecteds[$idParent['id']] = $subcategoryResult['Category']['category'].' -> '.$subcategoryResult['Subcategory']['subcategory'];

				} else { 
					
					$categoryResult = $this->Category->read( null, $idParent['category_id'] );					
					// Array para verificação de categorias já selecionada
					$categoriesSelectedsAr['Category.'.$categoryResult['Category']['id']] = $categoryResult['Category']['category'];
					// Array para link de remoção de categria
					$categoriesSelecteds[$idParent['id']] = $categoryResult['Category']['category'];
				}
			}
			
			$this->request->data = $product;

		}

		$albums = $this->Product->Album->find('list');
		$categories = $this->Category->find('all');

		$this->set(compact('albums', 'categories', 'categoriesSelecteds','categoriesSelectedsAr'));
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {

		$this->Parentcategory->deleteAll(array('Parentcategory.product_id' => $id), false);
		$this->Product->id = $id;
		if ($this->Product->delete()) {
			$this->Session->setFlash(__('Product deleted'), 'sucess');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Product was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_deletecategory( $id = null ) {

		if ( !$id ) {
			$this->Session->setFlash(__('Erro ao remover a categoria, tente novamente.'), 'error');
			$this->redirect( $this->referer() );
		}
		$this->Parentcategory->id = $id;
		if ( $this->Parentcategory->delete() ) {
			$this->Session->setFlash(__('Categoria removida'), 'sucess');
		} else {
			$this->Session->setFlash(__('Erro ao remover a categoria, tente novamente.'), 'error');			
		}
		$this->redirect( $this->referer() );
	}

	public function admin_feature( $id = null ) {

		$this->Product->id = $id;
		$response = "";
		if (!$this->Product->exists()) {
			pre( $this->request->data );
			throw new NotFoundException(__('Invalid product'));
			$response = "null";
		}else{
	    	$this->Product->saveField("featured", $this->request->data['featured']);
			$response = $this->request->data['featured'];
		}
		$this->set('response', $response);
	}

}

<?php
App::uses('AppController', 'Controller');
/**
 * Talks Controller
 *
 * @property Talk $Talk
 */
class TalksController extends AppController {


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Talk->recursive = 0;
		$this->set('talks', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Talk->id = $id;
		if (!$this->Talk->exists()) {
			throw new NotFoundException(__('Invalid talk'));
		}
		$this->set('talk', $this->Talk->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add($fromSite = null) {
		if ($this->request->is('post')) {
			$this->Talk->create();
			if ($this->Talk->save($this->request->data)) {
				$this->Session->setFlash(__('The talk has been saved'), 'sucess');
			} else {
				$this->Session->setFlash(__('The talk could not be saved. Please, try again.'), 'error');
			}
			if( $fromSite == 1 )
				$this->redirect( array('controller' => 'transactions', 'action' => 'talks', $this->request->data['Talk']['transaction_id'], 'admin' => false ) );
			else
				$this->redirect( $this->referer() );
		}
		$transactions = $this->Talk->Transaction->find('list');
		$this->set(compact('transactions'));
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null, $transactionId = null, $fromSite = null) {
		
		$this->Talk->id = $id;
		if (!$this->Talk->exists()) {
			throw new NotFoundException(__('Invalid talk'));
		}
		$urlToRedirect = array('controller' => 'transactions', 'action' => 'talks', $transactionId );
		if( $fromSite == 1 )
			$urlToRedirect['admin'] = false;
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Talk->save($this->request->data)) {
				$this->Session->setFlash(__('Resposta atualizada com sucesso!'), 'sucess');
				
				if ( $transactionId ) {
					$this->redirect( $urlToRedirect );					
				}
				$this->redirect( $this->referer() );				

			} else {
				$this->Session->setFlash(__('Não foi possivel atualizar a resposta!'), 'error');
				if ( $transactionId ) {
					$this->redirect( $urlToRedirect );					
				}
				$this->redirect( $this->referer() );				
			}
		} else {
			$this->request->data = $this->Talk->read(null, $id);
		}
		$transactions = $this->Talk->Transaction->find('list');
		$this->set(compact('transactions'));

	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Talk->id = $id;
		if (!$this->Talk->exists()) {
			throw new NotFoundException(__('Invalid talk'));
		}
		if ($this->Talk->delete()) {
			$this->Session->setFlash(__('printer_delete_dc(printer_handle)'), 'sucess');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Talk was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}
}

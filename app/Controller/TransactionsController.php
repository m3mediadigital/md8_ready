<?php
App::uses('AppController', 'Controller');
/**
 * Transactions Controller
 *
 * @property Transaction $Transaction
 */
class TransactionsController extends AppController {


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Transaction->recursive = 0;

		$userId = $this->Session->read('Logged.User.id');
		$user = $this->User->findById($userId);
		
		if ( $user['User']['group_id'] != 1 ) {
			$conditions = array( 'Transaction.user_id' => $userId );			
		} else {
			$conditions = array();
		}
		
		$this->paginate = array( 
			'conditions' => $conditions,
			'order' => array( 'Transaction.created' => 'DESC' ),
			'limit' => 15
		);
		$this->set('transactions', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Transaction->id = $id;

		$profile = $this->Transaction->query("select * from transactions inner join profiles on transactions.user_id = profiles.user_id where transactions.id =".$id);

		if (!$this->Transaction->exists()) {
			throw new NotFoundException(__('Invalid transaction'));
		}
		
		$this->set('transaction', $this->Transaction->read(null, $id));
		$this->set(compact('profile'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Transaction->create();
			if ($this->Transaction->save($this->request->data)) {
				$this->Session->setFlash(__('The transaction has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The transaction could not be saved. Please, try again.'), 'error');
			}
		}
		$users = $this->Transaction->User->find('list');
		$products = $this->Transaction->Product->find('list');
		$status = $this->Transaction->Status->find('list', array( 'fields' => array( 'id', 'statu' ) ));
		$this->set(compact('users', 'products', 'status'));
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
//action admin_edit original
	/*public function admin_edit($id = null) {
		$this->Transaction->id = $id;
		if (!$this->Transaction->exists()) {
			throw new NotFoundException(__('Invalid transaction'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Transaction->save($this->request->data)) {
				$this->Session->setFlash(__('The transaction has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The transaction could not be saved. Please, try again.'), 'error');
			}
		} else {
			$this->request->data = $this->Transaction->read(null, $id);
		}
		$users = $this->Transaction->User->find('list');
		$products = $this->Transaction->Product->find('list');
		$status = $this->Transaction->Status->find('list', array( 'fields' => array( 'id', 'statu' ) ));
		$this->set(compact('users', 'products', 'status'));
	}*/
	public function admin_edit($id = null) {
		$this->Transaction->id = $id;

		if (!$this->Transaction->exists()) {
			throw new NotFoundException(__('Invalid transaction'));
		}

		if ($this->request->is('post') || $this->request->is('put') || ($this->request->isAjax() && $_POST['update'])){

			if($this->request->isAjax()){
				$msg="";
				$user_id = $_POST['user_id'];
				$product_id = $_POST['product_id'];
				$status_id = $_POST['status_id'];

				$campos = array('user_id' => $user_id,
				'product_id' => $product_id,
				'status_id' => $status_id);

				if($this->Transaction->save($campos, false, array('status_id'))){
					$msg = array("msg" => " : A transação foi alterada com sucesso !","op" => 1);
				}else{
					$msg = array("msg" => " : Não foi possível alterar a transação !","op" => 0);
				}

				$this->set(compact('msg'));
				}else{
					if($this->Transaction->save($this->request->data)) {
						$this->Session->setFlash(__('The transaction has been saved'), 'sucess');
						$this->redirect(array('action' => 'index'));
					}else{
						$this->Session->setFlash(__('The transaction could not be saved. Please, try again.'), 'error');
					}
				}
		}else{
			$this->request->data = $this->Transaction->read(null, $id);
		}
		$users = $this->Transaction->User->find('list');
		$products = $this->Transaction->Product->find('list');
		$status = $this->Transaction->Status->find('list', array( 'fields' => array( 'id', 'statu' ) ));
		$this->set(compact('users', 'products', 'status'));
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		
		$this->Transaction->id = $id;
				
		$talks = $this->Talk->find( 'list', array( 'conditions' => array( 'Talk.transaction_id' => $id ), 'fields' => array( 'id' )) );
		
		$this->Filestalk->deleteAll(array('Filestalk.talk_id' => $talks));
		$this->Talk->deleteAll(array('Talk.id' => $talks));
		
		if ($this->Transaction->delete()) {
			$this->Session->setFlash(__('Transaction deleted'), 'sucess');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Transaction was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_deleteall() {	

		foreach ($this->request->data['Transaction']['id'] as $idTransaction) {
			if ( $idTransaction != 0 ) { $id[$idTransaction] = $idTransaction; }
		}
				
		$talks = $this->Talk->find( 'list', array( 'conditions' => array( 'Talk.transaction_id' => $id ), 'fields' => array( 'id' )) );
		
		$this->Filestalk->deleteAll(array('Filestalk.talk_id' => $talks));
		$this->Talk->deleteAll(array('Talk.id' => $talks));
		
		if ($this->Transaction->deleteAll( array( 'Transaction.id' => $id ) )) {
			$this->Session->setFlash(__('Transaction deleted'), 'sucess');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Transaction was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}


	public function admin_talks( $id = null ) {

		$talks = $this->Transaction->read(null, $id);
		$talksIds = array();

		foreach ($talks['Talk'] as $talkId) {
			$talksIds[] = $talkId['id']; 
		}
		
		$talkFiles = $this->Filestalk->find( 'all', array( 'conditions' => array( 'Filestalk.talk_id' => $talksIds ), 'ORDER' => 'Transaction.created = DESC' ) );

		$this->set( compact( 'talks','talkFiles' ) );
	}

	public function index() {
    	$this->layout = 'site';
		$this->set('page', 'pg-profiles');
		$this->admin_index();
		//print_r($this->viewVars); exit();
	}

	public function talks( $id = null ) {
    	$this->layout = 'site';
		$this->set('page', 'pg-profiles');
		$this->admin_talks( $id );
		//print_r($this->viewVars); exit();
	}


}

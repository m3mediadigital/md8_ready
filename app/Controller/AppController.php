<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    var $helpers = array('Form', 'Time', 'Html', 'Session', 'Js', 'Text', 'Number', 'M3', 'PhpThumb', 'FilterResults.FilterForm', 'ExtensionHtml');
    var $uses = array('Setting', 'Menu', 'Submenu', 'Album', 'User', 'Group', 'Profile', 'Photo', 'TableFiles', 'Crop', 'Example','Subcategory','Category','Parentcategory','Newsletter','Talk','Filestalk','Status','Transaction');
    var $components = array('Automate', 'Session', 'RequestHandler', 'Authentica', 'Analytics');
    var $counter = 0;

    function beforeFilter() {

        //print_r($this); exit();

        if ( $this->Session->read('Logged.Group.id') != 1 ){
            $path = explode("_", $this->request->params['action']);
            if( count($path) > 1 && $path[0] == 'admin' && $path[1] == 'index' )
                $this->redirect(array('action' => 'index', 'controller' => 'pages', 'admin' => false));
        }

        $user_id = $this->Session->read('Logged.User.id');
        if( in_array($user_id, array(null, 1)) ){
            if( ( $this->name == 'Transactions' && in_array($this->request->params['action'], array('index', 'talks')) ) || 
                    ( $this->name == 'Users' && $this->request->params['action'] == 'index' ) )
                if( $user_id == null )
                    $this->redirect(array('controller' => 'pages', 'action' => 'login'));
                else
                    $this->redirect(array('action' => 'index', 'controller' => 'dashboards', 'admin' => true));
        }

        /** se a requisição for ajax, define o debbuger pra zero, para nao "sujar" a resposta caso seja json */
        if($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
        }

        $this->set('itensmenu', $this->Menu->find('all', array('order' => 'Menu.title asc')));
        $this->set('categories', $this->Category->find('all'));
        $this->authentica();
        $this->settings();
        if ($this->Session->read('Settings.site_title.active') == 1) {
            $this->set('title_for_layout', $this->Session->read('Settings.site_title.value'));
        }
        if ($this->Session->read('Settings.base_url.active') == 1) {
            $this->set('base_url', $this->Session->read('Settings.base_url.value'));
        }


        if ( $this->Session->read('Logged.Group.id') ){
            $groupUserRules = $this->Group->read( null, $this->Session->read('Logged.Group.id') );
            $adminLevel = $this->Session->read('Logged.Group.id');
            $this->set( compact( 'groupUserRules','adminLevel' ) );
        }
    }

    private function authentica() {
        $this->Authentica->beforeFilter($this);
    }

    private function settings() {
        if (!$this->Session->read('Settings')) {
            $settings = $this->Setting->find('all');
            foreach ($settings as $setting) {
                $this->Session->write('Settings.' . $setting['Setting']['name'] . '.value', $setting['Setting']['value']);
                $this->Session->write('Settings.' . $setting['Setting']['name'] . '.active', $setting['Setting']['active']);
            }
        }
    }

    function admin_adicionarAlbum($id) {
        $objName = ucfirst(Inflector::singularize($this->params['controller']));
        $verificacao = $this->$objName->read(null, $id);
        if (!isset($verificacao[$objName]['album_id'])) {
            $this->Album->create(true);
            $this->request->data['Album']['title'] = date('d/m/Y');
            if ($this->Album->save($this->request->data)) {
                $this->request->data[$objName]['id'] = $id;
                $this->request->data[$objName]['album_id'] = $this->Album->id;
                $this->$objName->save($this->request->data);
                $this->redirect(array('controller' => 'photos', 'action' => 'add', $this->Album->id));
            } else {
                $this->Session->setFlash(__('Não foi possível criar um album, tente novamente', true), 'error');
                $this->redirect(array('controller' => $this->params['controller'], 'action' => 'index'));
            }
        } else {
            $this->redirect(array('controller' => 'photos', 'action' => 'add', $verificacao[$objName]['album_id']));
        }
    }

    function beforeRender() {
   
    }

    function convertDate($data, $hora = false) {
        if (strpos($data, '-')) {
            $data2 = explode(' ', $data);
            $d = explode("-", $data2[0]);
            $retorno = $d[2] . '/' . $d[1] . '/' . $d[0];
            if ($hora) {
                $retorno .= ' ' . $data2[1];
            }
            return $retorno;
        } else if (strpos($data, '/')) {
            $data2 = explode(' ', $data);
            $d = explode("/", $data2[0]);
            $retorno = $d[2] . '-' . $d[1] . '-' . $d[0];
            if ($hora) {
                $retorno .= ' ' . $data2[1];
            }
            return $retorno;
        }
    }

    function admin_changeState($id, $model) {
        $this->autoRender = false;
        $obj = $this->$model->read(null, $id);
        $this->$model->id = $id;
        if ($obj[$model]['active'] == 1) {
            $this->$model->saveField('active', 0);
            return 0;
        } else {
            $this->$model->saveField('active', 1);
            return 1;
        }
    }

/**
 * admin_allActionsListData method
 * recebe um post da view admin_index dos registros marcados com checkbox para desativar/ativar/deletar em massa
 * @return void
 * @author wescley matos
 **/
	public function admin_allActionsListData() {
		$modelName = key($this->request->data);
        $action = $this->request->data[$modelName]['action'];
        $arrayIds = $this->request->data[$modelName]['id'];

        switch ($action) {
            case 'delete':
                foreach ($arrayIds as $key => $id) {
                    if($id == 0) {
                        unset($arrayIds[$key]);
                        continue;
                    }
                    $this->$modelName->delete($id);
                }
                $this->Session->setFlash(__('Registro(s) deletado(s) com sucesso.'), 'sucess');
                break;

            case 'activate':
                foreach ($arrayIds as $key => $id) {
                    if($id == 0) {
                        unset($arrayIds[$key]);
                        continue;
                    }
                    $result = $this->$modelName->read(array('id', 'active'), $id);
                    if ($result[$modelName]['active'] == 0) {
                        $this->$modelName->set('active', 1);
                        $this->$modelName->save();
                        $this->Session->setFlash(__('Registro(s) ativado(s) com sucesso.'), 'sucess');
                    }
                }
                break;

            case 'deactivate':
                foreach ($arrayIds as $key => $id) {
                    if($id == 0) {
                        unset($arrayIds[$key]);
                        continue;
                    }
                    $result = $this->$modelName->read(array('id', 'active'), $id);
                    if ($result[$modelName]['active'] == 1) {
                        $this->$modelName->set('active', 0);
                        $this->$modelName->save();
						$this->Session->setFlash(__('Registro(s) desativado(s) com sucesso.'), 'sucess');
					}
				}
				break;
		}
        $this->redirect($this->referer());
	}

    public function initDownload( $url = null ) {
        $absolutDir = APP.'webroot/uploads/';
        
        if ( $url AND file_exists($absolutDir.$url) ) {
           $fileType = pathinfo($absolutDir.$url,PATHINFO_EXTENSION);

          header("Content-Type: ".strtoupper($fileType)); // informa o tipo do arquivo ao navegador
          header("Content-Length: ".filesize($absolutDir.$url)); // informa o tamanho do arquivo ao navegador
          header("Content-Disposition: attachment; filename=".basename($absolutDir.$url)); // informa ao navegador que é tipo anexo e faz abrir a janela de download, tambem informa o nome do arquivo
          readfile($absolutDir.$url); // lê o arquivo
          exit; // aborta pós-ações

        } else {
            $this->Session->setFlash(__('Erro ao baixar o arquivo ( '.$url.' ), por favor contate o responsavel pelo anexo para reenviar o mesmo.'), 'error');
            $this->redirect( $this->referer() );
        }
    }

    function afterFilter(){
        if ($this->request->ext == "json"){}
        if($this->RequestHandler->isAjax()) {}
    }

    /**
     * Função para eviar resposta em formato json. Para uso geral, especialmente em requisições ajax
     *
     * @param array   $response Dados que serão enviados
     *
     * @return void
     */
    // public function sendJson($response){
    //    $this->set('response', $response);
    //    Configure::write('debug', 0);
    //    $this->autoRender = false;
    //    $this->autoLayout = false;
    //    //$this->layout = '';
    //    $view = ROOT . DS . APP_DIR . DS . "View" . DS . "common" . DS . "json.ctp";
    //    $this->render(null, null, $view);
    // }

    // chamado em PagesController::cadastro() e em UsersController::index
    public function atualizaProfile($user_id = null){

        if( $this->request->data ){
            $password_confirmation = ( isset($this->request->data['User']['password_confirmation']) ) ?
                    $this->request->data['User']['password_confirmation'] : null ;
            unset($this->request->data['User']['cpfcasal']);
            unset($this->request->data['User']['password_confirmation']);

            if( ( $user_id == null ) ){
                $userCPF = $this->User->find( 'first', array( 'conditions' => array( 'Profile.cpf' => $this->request->data['Profile']['cpf'] ) ) );
                $userCNPJ = $this->User->find( 'first', array( 'conditions' => array( 'Profile.cnpj' => $this->request->data['Profile']['cnpj'] ) ) );
            }

            $newsletterConsult = $this->Newsletter->find( 
                    'first', 
                    array( 
                        'conditions' => 
                        array( 'Newsletter.email' => $this->request->data['Profile']['email'] )
                    ) 
            );
            if ( empty($newsletterConsult) && $this->request->data['User']['receberNews'] >= 1 ) {
                
                $this->Newsletter->save( array( 'Newsletter' => array( 
                    'name' => $this->request->data['Profile']['full_name'],
                    'email' => $this->request->data['Profile']['email']
                ) ) );                     
            }else if ( !empty($newsletterConsult) && $this->request->data['User']['receberNews'] < 1 ) {
                $this->Newsletter->id = $newsletterConsult['Newsletter']['id'];
                $this->Newsletter->delete();
            }
            unset($this->request->data['User']['receberNews']);
            
            if ( isset($this->request->data['User']['password']) && $this->request->data['User']['password'] != $password_confirmation ) { 
                $this->Session->setFlash('Senha e confirmação não conferem!', 'warning'); 
            } else if ( !empty( $userCPF )  ) {
                $this->Session->setFlash('CPF já cadastrado!', 'warning');                
            } elseif ( !empty( $userCNPJ )  ) {
                $this->Session->setFlash('CNPJ já cadastrado!', 'warning');                
            } elseif (empty($this->request->data['Profile']['full_name'])) {
                $this->Session->setFlash('Por favor, informe o seu nome. O email não foi enviado.', 'error');
            } elseif (empty($this->request->data['Profile']['corporate_name'])) {
                $this->Session->setFlash('Por favor, informe Razão Social. O email não foi enviado.', 'error');
            } elseif (empty($this->request->data['Profile']['email'])) {
                $this->Session->setFlash('Por favor, informe o seu email. O email não foi enviado.', 'error');
            } elseif (!preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(\.[[:lower:]]{2,3})(\.[[:lower:]]{2})?$/", $this->request->data['Profile']['email'])) {
                $this->Session->setFlash('O email informado é inválido. O email não foi enviado.', 'error');
            } elseif (empty($this->request->data['Profile']['primary_phone'])) {
                $this->Session->setFlash('Por favor, informe um telefone. O email não foi enviado.', 'error');
            } elseif (empty($this->request->data['Profile']['cnpj'])) {
                $this->Session->setFlash('Por favor, informe o seu CNPJ. O email não foi enviado.', 'error');
            } else {

                $arrPersistents = $this->User->read( null, $user_id );
                
                if( !$arrPersistents )
                    $arrPersistents = array( 'User' => array(), 'Profile' => array() );
                
                $arrPersistents['User']['name'] = $this->request->data['Profile']['full_name'];
                $arrPersistents['User']['login'] = $this->request->data['Profile']['email'];
                $arrPersistents['User']['active'] = 1;

                if( isset($this->request->data['User']['password']) ){
                    $password = $this->makePassword(
                        $this->request->data['User']['password'], 
                        $this->request->data['User']['password']
                    );
                    $arrPersistents['User']['password'] = $password[0];
                }
                // ID do grupo 
                $arrPersistents['User']['group_id'] = 2;

                $arrPersistents['Profile'] = array_merge($arrPersistents['Profile'], $this->request->data['Profile']);

                //print_r( $arrPersistents ); exit();

                $status = null;

                if( ( $user_id == null ) ){
                    $status = $this->User->saveAll( $arrPersistents );
                }else{
                    $status = $this->User->save( $arrPersistents['User'] ) && $this->Profile->save( $arrPersistents['Profile'] );
                }

                if( $status ) {
                    $this->Session->setFlash('Cadastro efetuado com suceso!', 'sucess');     
                    $this->Session->write('Logged.User', $arrPersistents['User']);
                    if( $user_id != null )
                        $this->redirect( array('controller' => 'users', 'action' => 'index', 'admin' => false ) );               
                } else {
                    $this->Session->setFlash('Não foi possivel efetuar o cadastro!', 'error');
                }

            }

           
        }
    }

}
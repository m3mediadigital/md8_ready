<?php
App::uses('AppController', 'Controller');
/**
 * Filestalks Controller
 *
 * @property Filestalk $Filestalk
 */
class FilestalksController extends AppController {


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Filestalk->recursive = 0;
		$this->set('filestalks', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Filestalk->id = $id;
		if (!$this->Filestalk->exists()) {
			throw new NotFoundException(__('Invalid filestalk'));
		}
		$this->set('filestalk', $this->Filestalk->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {			
			$talk_id = $this->request->data['Filestalk']['talk_id'];
			$user_id = $this->request->data['Filestalk']['user_id'];
			$sucess = 0;
			$error = 0;
			foreach ($this->request->data['Filestalk']['file'] as $file) {
				
				$this->Filestalk->create();
				if ($this->Filestalk->save( array( 'Filestalk' => array( 'talk_id' => $talk_id, 'user_id' => $user_id, 'file' => $file ) ) )) {
					$sucess++;
				} else {
					$error++;
				}
				
			}
			
			if ( $sucess > 0 AND $error == 0 ) {
				$this->Session->setFlash(__('Arquivo(s) anexado(s) com sucesso!'), 'sucess');
				$this->redirect( $this->referer() );
			} else if ( $error > 0 AND $sucess == 0 ) {
				$this->Session->setFlash(__('Não foi possivel anexar o(s) arquivo(s)!'), 'error');
				$this->redirect( $this->referer() );
			} else {
				$this->Session->setFlash(__('Houveram algums erros nos anexos, Anexados: '.$sucess.' Erros: '.$error.'.'), 'warning');
				$this->redirect( $this->referer() );
			}
		$talks = $this->Filestalk->Talk->find('list');
		$users = $this->Filestalk->User->find('list');
		$this->set(compact('talks', 'users'));
		}	
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Filestalk->id = $id;
		if (!$this->Filestalk->exists()) {
			throw new NotFoundException(__('Invalid filestalk'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Filestalk->save($this->request->data)) {
				$this->Session->setFlash(__('The filestalk has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The filestalk could not be saved. Please, try again.'), 'error');
			}
		} else {
			$this->request->data = $this->Filestalk->read(null, $id);
		}
		$talks = $this->Filestalk->Talk->find('list');
		$users = $this->Filestalk->User->find('list');
		$this->set(compact('talks', 'users'));
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		
		$this->Filestalk->id = $id;		
		if ($this->Filestalk->delete()) {
			$this->Session->setFlash(__('Filestalk deleted'), 'sucess');
			$this->redirect( $this->referer() );
		}
		$this->Session->setFlash(__('Filestalk was not deleted'), 'error');
		$this->redirect( $this->referer() );
		
	}
}

<?php
App::uses('AppController', 'Controller');
/**
 * Md8s Controller
 *
 * @property Md8 $Md8
 */
class Md8sController extends AppController {


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Md8->recursive = 0;
		$this->set('md8s', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Md8->id = $id;
		if (!$this->Md8->exists()) {
			throw new NotFoundException(__('Invalid md8'));
		}
		$this->set('md8', $this->Md8->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Md8->create();
			if ($this->Md8->save($this->request->data)) {
				$this->Session->setFlash(__('The md8 has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The md8 could not be saved. Please, try again.'), 'error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Md8->id = $id;
		if (!$this->Md8->exists()) {
			throw new NotFoundException(__('Invalid md8'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Md8->save($this->request->data)) {
				$this->Session->setFlash(__('The md8 has been saved'), 'sucess');
				//$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The md8 could not be saved. Please, try again.'), 'error');
			}
		} else {
			$this->request->data = $this->Md8->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Md8->id = $id;
		if (!$this->Md8->exists()) {
			throw new NotFoundException(__('Invalid md8'));
		}
		if ($this->Md8->delete()) {
			$this->Session->setFlash(__('Md8 deleted'), 'sucess');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Md8 was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}
}

<?php

App::uses('AppController', 'Controller');

/**
 * Crops Controller
 *
 * @property Crop $Crop
 */
class CropsController extends AppController {

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
         $this->Crop->recursive = 0;
        $this->set('crops', $this->paginate());
    }

    /**
     * admin_view method
     *
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Crop->id = $id;
        if (!$this->Crop->exists()) {
            throw new NotFoundException(__('Invalid crop'));
        }
        $this->set('crop', $this->Crop->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Crop->create();
            if ($this->Crop->save($this->request->data)) {
                $this->Session->setFlash(__('The crop has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The crop could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * admin_edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Crop->id = $id;
        if (!$this->Crop->exists()) {
            throw new NotFoundException(__('Invalid crop'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Crop->save($this->request->data)) {
                $this->Session->setFlash(__('The crop has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The crop could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Crop->read(null, $id);
        }
    }

    /**
     * admin_delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Crop->id = $id;
        if (!$this->Crop->exists()) {
            throw new NotFoundException(__('Invalid crop'));
        }
        if ($this->Crop->delete()) {
            $this->Session->setFlash(__('Crop deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Crop was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}

<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    public $name = 'Pages';
    public $helpers = array('Html', 'Session');
    public $uses = array('Banner', 'Product', 'News', 'Frequentquestion', 'Client', 'Md8', 'Profile');

    public function index()
    {
        $this->layout = 'site';
        $this->set('banners', $this->Banner->find('all'));
        $total = 9;
        $products = $this->Product->find('all', array(
            'conditions' => array('Product.featured' => '1'),
            'limit' => $total,
            'order' => array('Product.modified' => 'DESC')
            )
        );
        if (count($products) < $total)
        {
            $tmp = $this->Product->find('all', array(
                'conditions' => array('Product.featured' => '0'),
                'limit' => $total - count($products),
                'order' => array('Product.created' => 'DESC')
                )
            );
            $products = array_merge($products, $tmp);
        }
        $this->set('products', $products);
        $this->set('page', 'pg-home');
    }

    public function amd8()
    {
        $this->layout = 'site';
        $this->set('md8', $this->Md8->find('first'));
        $this->set('page', 'pg-amd8');
    }

    public function clientes()
    {
        $this->layout = 'site';
        $this->set('clientes', $this->Client->find('all'));
        $this->set('page', 'pg-clientes');
    }

    public function produtos()
    {
        $this->layout = 'site';

        if (!empty($this->request->data))
        {
            $this->paginate = array('conditions' => array('Product.title LIKE' => '%' . $this->request->data['Page']['pesquisa'] . '%'));
        }
        else
        {
            $this->paginate = array('limit' => 12);
        }

        $this->set('products', $this->paginate('Product'));
        $this->set('page', 'pg-produtos');
    }

    public function produto_id($title, $id)
    {
        $this->layout = 'site';

        $produto = $this->Product->findById($id);
        //pr($produto);exit();
        $photos = $this->Photo->findAllByAlbumId($produto['Album']['id']);

        $categorias = $this->Parentcategory->findAllByProductId($id, array('Parentcategory.category_id'));
        foreach ($categorias as $categoria)
        {
            $categorias_id[] = $categoria['Parentcategory']['category_id'];
        }

        $produtos_ids = $this->Parentcategory->find('all', array('conditions' => array('Parentcategory.category_id' => array_unique($categorias_id))));
        foreach ($produtos_ids as $produto_id)
        {
            $produtos_id[] = $produto_id['Parentcategory']['product_id'];
        }
        $produtos = $this->Product->find('all', array('conditions' => array('Product.id' => array_unique($produtos_id))));

        $this->set('produto', $produto);
        $this->set('products', $produtos);
        $this->set('photos', $photos);
        $this->set('page', 'pg-produtos');
    }

    public function produtosCategoria($title, $id)
    {
        $this->layout = 'site';

        $joins = array(
            array(
                'table' => 'parentcategories',
                'alias' => 'Parentcategory',
                'type' => 'INNER',
                'conditions' => array(
                    'Product.id = Parentcategory.product_id'
                )
            )
        );

        $this->paginate = array('joins' => $joins, 'conditions' => array('Parentcategory.category_id' => $id), 'group' => 'Product.id');
        $this->set('products', $this->paginate('Product'));
        $this->set('page', 'pg-produtos');
        $this->render('produtos');
    }

    public function produtosSubcategoria($categoria, $subcategoria, $id)
    {
        $this->layout = 'site';

        $joins = array(
            array(
                'table' => 'parentcategories',
                'alias' => 'Parentcategory',
                'type' => 'INNER',
                'conditions' => array(
                    'Product.id = Parentcategory.product_id'
                )
            )
        );

        $this->paginate = array('joins' => $joins, 'conditions' => array('Parentcategory.subcategory_id' => $id), 'group' => 'Product.id');
        $this->set('products', $this->paginate('Product'));
        $this->set('page', 'pg-produtos');
        $this->render('produtos');
    }

    public function noticias()
    {
        $this->layout = 'site';
        $this->paginate = array('limit' => 6);
        $this->set('noticias', $this->paginate('News'));
        $this->set('page', 'pg-noticias');
    }

    public function noticias_id($title, $id)
    {
        $this->layout = 'site';
        $this->set('noticia', $this->News->findById($id));
        $this->set('page', 'pg-noticias');
    }

    public function duvidasfrequentes()
    {
        $this->layout = 'site';

        $this->set('duvidas', $this->Frequentquestion->find('all'));
        $this->set('page', 'pg-duvidas');
    }

    public function login()
    {
        if ($this->Session->read('Logged.User.id') != null)
        {
            $this->redirect(array('controller' => 'profiles', 'action' => 'index'));
        }
        else
        {
            $this->layout = 'site';
            $this->set('page', '');
        }
    }

    public function cadastro()
    {
        $this->layout = 'site';
        $this->set('page', '');
        $this->set('newsletter', '');

        $this->atualizaProfile();
    }

    public function contato()
    {
        $this->layout = 'site';

        if (!empty($this->request->data))
        {
            if (empty($this->request->data['Page']['cpfcasal']))
            {
                if (empty($this->request->data['Page']['nome']))
                {
                    $this->Session->setFlash('Por favor, informe o seu nome. O email não foi enviado.', 'error');
                }
                elseif (empty($this->request->data['Page']['email']))
                {
                    $this->Session->setFlash('Por favor, informe o seu email. O email não foi enviado.', 'error');
                }
                elseif (!preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(\.[[:lower:]]{2,3})(\.[[:lower:]]{2})?$/", $this->request->data['Page']['email']))
                {
                    $this->Session->setFlash('O email informado é inválido. O email não foi enviado.', 'error');
                }
                elseif (empty($this->request->data['Page']['telefone']))
                {
                    $this->Session->setFlash('Por favor, informe seu telefone. O email não foi enviado.', 'error');
                }
                elseif (empty($this->request->data['Page']['comentario']))
                {
                    $this->Session->setFlash('Por favor, escreva a mensagem. O email não foi enviado.', 'error');
                }
                else
                {
                    $email = new CakeEmail();
                    $email->config('smtp');
                    $email->viewVars(array('data' => $this->request->data['Page']));
                    if ($email->template('contato', 'default')->to($this->Session->read('Settings.email_contato.value'))->replyTo($this->request->data['Page']['email'])->subject('Formulário de contato')->send())
                    {
                        $this->Session->setFlash('Email enviado com sucesso!', 'sucess');
                        $this->redirect($this->referer());
                    }
                    else
                    {
                        $this->Session->setFlash('Seu e-mail não pode ser enviado, Por favor, Tente novamente', 'error');
                    }
                }
            }
        }

        $this->set('page', 'pg-contato');
    }

    public function denied()
    {
        $this->layout = 'site';
    }

    public function error()
    {
        $this->layout = 'site';
    }

    public function teste()
    {
        $this->layout = '';
    }

    function makePassword($pass = null, $pass2 = null, $qtd = 8)
    {
        if (empty($pass))
        {
            $caracteresAceitos = 'abcdxywzABCDZYWZ0123456789';
            $max = strlen($caracteresAceitos) - 1;
            $pass = null;
            for ($i = 0; $i < $qtd; $i++)
            {
                $pass .= $caracteresAceitos{mt_rand(0, $max)};
            }
            return (array($this->User->hash($pass), $pass));
        }
        else
        {
            if ($pass2 === null)
            {
                return (array($this->User->hash($pass), $pass));
            }
            elseif ($pass === $pass2)
            {
                return (array($this->User->hash($pass), $pass));
            }
            else
            {
                return false;
            }
        }
    }

    public function addCarrinho()
    {

        if ($this->request->data)
        {

            $product = $this->Product->read(null, $this->request->data['Product']['id']);
            $addProd = array(
                'id' => $product['Product']['id'],
                'product' => $product['Product']['title'],
                'quantity' => $this->request->data['Product']['qtd'],
                'value' => $product['Product']['value'],
            );

            if ($this->Session->write('Carrinho.' . $product['Product']['id'], $addProd))
            {
                $this->Session->setFlash('Item adicionado com sucesso!', 'sucess');
            }
            else
            {
                $this->Session->setFlash('Não foi possivel adicionar o item no carrinho!', 'error');
            }
        }

        $this->redirect($this->referer());
        exit();
    }

    public function removerCarrinho($id = null)
    {

        if ($this->Session->delete('Carrinho.' . $id))
        {
            $this->Session->setFlash('Item removido com sucesso!', 'sucess');
        }
        else
        {
            $this->Session->setFlash('Não foi possivel remover o item do carrinho!', 'error');
        }

        $this->redirect($this->referer());
    }

    public function finalizarpedido()
    {
        if (!$this->Session->read('Logged.User.id'))
        {
            $this->Session->setFlash(__('Por favor, efetue o login para iniciar a transação.'), 'error');
            $this->redirect('/login');
        }

        $carrinho = $this->Session->read('Carrinho');
        $defaultStatus = $this->Status->find('first');
        $userId = $this->Session->read('Logged.User.id');
        $user = $this->User->read(null, $this->Session->read('Logged.User.id'));
        $sucess = 0;
        $error = 0;
        $sendMail['usuario'] = $user['User']['name'];
        $sendMail['produtos'] = $carrinho;

        foreach ($carrinho as $item)
        {
            $saveTransaction['Transaction'] = array('user_id' => $userId, 'product_id' => $item['id'], 'quantity' => $item['quantity'], 'status_id' => $defaultStatus['Status']['id']);
            if ($this->Transaction->save($saveTransaction))
            {
                $sucess++;
            }
            else
            {
                $error++;
            }
        }

        $from = $user['Profile']['email'];
        $to = explode(',', str_replace(' ', '', $this->Session->read('Settings.email_contato.value')));

        $email = new CakeEmail('smtp');
        $email->template('confirmacao_de_compra', 'default')
            ->to($from)
            ->bcc($to)
            ->replyTo('no-reply@md8.com.br')
            ->subject('Nova transação iniciada')
            ->viewVars(array('data' => $sendMail));

        $email->send();

        if ($sucess > 0 AND $error == 0)
        {
            $this->Session->setFlash(__('Todos os itens foram adicionados á sua lista de transações!'), 'sucess');
            $this->Session->delete('Carrinho');
            $this->redirect(array('controller' => 'transactions', 'action' => 'index', 'admin' => false));
        }
        else if ($error > 0 AND $sucess == 0)
        {
            $this->Session->setFlash(__('Não foi possivel adicionar os itens à transação, por favor, tente novamente.'), 'error');
            $this->redirect($this->referer());
        }
        else
        {
            $this->Session->setFlash(__("Houveram algums erros ao adicionar os itens a sua lista de transações! \nItens adicionados: " . $sucess . "\nItens com erro: " . $erro), 'warning');
            $this->redirect(array('controller' => 'transactions', 'action' => 'index', 'admin' => false));
        }

        $this->redirect($this->referer());
        exit();
    }

    public function recuperarsenha()
    {
        $this->layout = 'site';
        $this->set('page', '');
    }

}

<?php

App::uses('AppController', 'Controller');

/**
 * Photos Controller
 *
 * @property Photo $Photo
 */
class PhotosController extends AppController {

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->Photo->recursive = 0;
        $this->set('photos', $this->paginate());
    }

    /**
     * admin_view method
     *
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Photo->id = $id;
        if (!$this->Photo->exists()) {
            throw new NotFoundException(__('Invalid photo'));
        }
        $this->set('photo', $this->Photo->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Photo->create();
            if ($this->Photo->save($this->request->data)) {

                $this->Session->setFlash(__('The photo has been saved'), 'sucess');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The photo could not be saved. Please, try again.'), 'error');
            }
        }
    }
    
    public function admin_add_massa() {
        if ($this->request->is('post')) {
            $this->Photo->create();
            if ($this->Photo->save($this->request->data)) {

                $this->Session->setFlash(__('The photo has been saved'), 'sucess');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The photo could not be saved. Please, try again.'), 'error');
            }
        }
        $fotos = $this->Photo->find('all', array('order' => 'Photo.id Desc'));
        $this->set(compact('fotos'));
    }

    /**
     * admin_edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Photo->id = $id;
        if (!$this->Photo->exists()) {
            throw new NotFoundException(__('Invalid photo'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Photo->save($this->request->data)) {
                $this->Session->setFlash(__('The photo has been saved'), 'sucess');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The photo could not be saved. Please, try again.'), 'error');
            }
        } else {
            $this->request->data = $this->Photo->read(null, $id);
        }
    }
    
    
    public function admin_crops($id = null) {
        $this->Photo->id = $id;
        if (!$this->Photo->exists()) {
            throw new NotFoundException(__('Invalid photo'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $abs_path = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR;
            
            $foto = $this->Photo->read(null, $id);
            $data = $this->request->data;
            if ($data['Photo']['x_comeca'] == 0 && $data['Photo']['x_termina'] == 0 && $data['Photo']['y_comeca'] == 0 &&
            $data['Photo']['y_termina'] == 0 && $data['Photo']['largura'] == 0 && $data['Photo']['altura'] == 0) {
                $this->Session->setFlash(__('Você não recortou a imagem ainda.'), 'error');
            } else {
                
                $imagem_original = getimagesize("uploads/" . $data['Photo']['image']);

                switch($imagem_original["mime"]){
                    case "image/jpeg":

                        $im = imagecreatefromjpeg("uploads/" . $data['Photo']['image']); //jpeg file
                        break;

                    case "image/png":
                        $im = imagecreatefrompng("uploads/" . $data['Photo']['image']); //png file
                        break;

                    case "image/gif":
                        $im = imagecreatefromgif("uploads/" . $data['Photo']['image']); //png file
                        break;

                    default: 
                        $im = false;
                     break;
                }

                $image_p = imagecreatetruecolor($data['Photo']['width'], $data['Photo']['height']);
                imagecopyresized($image_p, $im, 0, 0,
                   $data['Photo']['x_comeca'],$data['Photo']['y_comeca'],
                   $data['Photo']['width'], $data['Photo']['height'], $data['Photo']['largura'], $data['Photo']['altura']);
            }

            header('Content-Type: ' . $imagem_original['mime']); 

            switch($imagem_original["mime"]){
                case "image/jpeg":

                    imagejpeg($image_p, $abs_path . 'app' . DIRECTORY_SEPARATOR . 'webroot' . DIRECTORY_SEPARATOR . 'uploads' . 
                        DIRECTORY_SEPARATOR . 'jcrop' . DIRECTORY_SEPARATOR . $data['Photo']['width'] . "_" .  
                        $data['Photo']['height'] . $data['Photo']['image'], 100);      
                    break;

                case "image/png":
                    
                    imagepng($image_p, $abs_path . 'app' . DIRECTORY_SEPARATOR . 'webroot' . DIRECTORY_SEPARATOR . 'uploads' . 
                        DIRECTORY_SEPARATOR . 'jcrop' . DIRECTORY_SEPARATOR . $data['Photo']['width'] . "_" .  
                        $data['Photo']['height'] . $data['Photo']['image']);    
                    break;

                case "image/gif":
                    
                    imagegif($image_p, $abs_path . 'app' . DIRECTORY_SEPARATOR . 'webroot' . DIRECTORY_SEPARATOR . 'uploads' . 
                        DIRECTORY_SEPARATOR . 'jcrop' . DIRECTORY_SEPARATOR . $data['Photo']['width'] . "_" .  
                        $data['Photo']['height'] . $data['Photo']['image']);    
                    break;

                default: 

                    $im = false;
                    break;
            }
            
            imagedestroy($im);
            
            $this->redirect(array('controller' => 'photos', 'action' => 'index'));
        } else {
            $this->request->data = $this->Photo->read(null, $id);
        }
        $medida = filter_input(INPUT_GET, 'crop_id');
        
        if (!$medida) {
            $crops = $this->Crop->find('all');
        } else {
            $crops = $this->Crop->read(null, $medida);
        }
        
        $this->set('medida', $medida);
        $this->set('crops', $crops);
    }

    public function admin_delete_recorte($id = null) {
        
        $this->Photo->id = $id;
        if (!$this->Photo->exists()) {
            throw new NotFoundException(__('Invalid photo'));
        }
        
        $foto = $this->Photo->read(null, $id);
        $arquivo = WWW_ROOT . 'uploads/jcrop/' . $foto['Album']['width'] . "_" . $foto['Album']['height'] . $foto['Photo']['image'];

        if (file_exists($arquivo)) {
            unlink($arquivo);
        }
        
        $this->redirect(array('action' => 'add', $foto['Photo']['album_id']));
        
        $this->autoRender = false;
        $this->render('/Photos/admin_add');
    }
   
    /**
     * admin_delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete( $id = null, $album_id = mull ) {

        // if (!$this->request->is('post')) {
        //     throw new MethodNotAllowedException();
        // }
        $this->Photo->id = $id;
        if (!$this->Photo->exists()) {
            throw new NotFoundException(__('Invalid photo'));
        }
        $foto = $this->Photo->read(null, $id);
        if ($this->Photo->delete()) {
            $this->Session->setFlash(__('Arquivos deletada!', true), 'sucess');
            $this->redirect( array( 'controller' => 'photos', 'action' => 'anexar', $album_id ) );
        }
        $this->Session->setFlash(__('Não foi possivel deletar este arquivos!'), 'error');
        $this->redirect( array( 'controller' => 'photos', 'action' => 'anexar', $album_id ) );
    }

    public function admin_anexar( $album_id = null )
    {
 
        if( $this->request->data ) {

            $sucess = 0;
            $error = 0;
            foreach ( $this->request->data['Photo']['images'] as $image ) {
                
                $saveImage['Photo'] = array( 'image' => $image, 'album_id' => $this->request->data['Photo']['album_id'] ); 

                if ( $this->Photo->saveAll( $saveImage ) ) {
                    $sucess++;
                } else {
                    $error++;
                }            

            }

            if ( $sucess > 0 AND $error == 0 ) {
                $this->Session->setFlash(__('Todas as imagens foram enviadas!'), 'sucess');
                // $this->redirect( $this->referer() );
            } else if ( $error > 0 AND $sucess == 0 ) {
                $this->Session->setFlash(__('Não foi possivel enviar as imagens!'), 'error');
                // $this->redirect( $this->referer() );
            } else {
                $this->Session->setFlash(__('Houve algums erros no envio das imagens!, Envida(s): ( '.$sucess.' ) Erro(s): ( '.$error.' )' ), 'warning');
            }

        }

        $album_id = ( $album_id )? $album_id : $this->request->data['Photo']['album_id'] ;
        $actualFotos = $this->Photo->find( 'all', array( 'conditions' => array( 'Photo.album_id' => $album_id ) ) );
        $albumName = $this->Album->read( null, $album_id );
        

        $this->set( compact( 'actualFotos', 'album_id', 'albumName' ) );      

    }

    // public function admin_anexar($id = null) {
    //     $tabela = filter_input(INPUT_GET, 'tabela');
    //     $conditions_files = array('table_title' => $tabela, 'table_id' => $id);
    //     $table_files = $this->TableFiles->find('all', array('conditions' => $conditions_files));

    //     $photos = $this->Photo->find('all');
        
    //     $this->set('tabela', $tabela);
    //     $this->set('id', $id);
    //     $this->set('table_files', $table_files);
    //     $this->set('photos', $photos);
    // }
    
    public function admin_anexarAjax() {
        $type = filter_input(INPUT_GET, 'type');
        $horario = filter_input(INPUT_GET, 'rel');
        if ($type == 'droppable2') {
            $array = array('table_title' => filter_input(INPUT_GET, 'table'), 'table_id' => filter_input(INPUT_GET, 'tableId'), 'photo_id' => filter_input(INPUT_GET, 'photoId'));
            if ($this->TableFiles->deleteAll($array)) {
                echo "<script type='text/javascript'>$('div.loading#".$horario."').remove()</script>";
            }
        } else {
            $array = array('TableFiles' => array('table_title' => filter_input(INPUT_GET, 'table'), 'table_id' => filter_input(INPUT_GET, 'tableId'), 'photo_id' => filter_input(INPUT_GET, 'photoId')));
            if ($this->TableFiles->saveAll($array)) {
                echo "<script type='text/javascript'>$('div.loading#".$horario."').remove()</script>";
            }
        }
        $this->render(false);
    }
}

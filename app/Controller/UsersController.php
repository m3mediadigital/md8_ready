<?php

App::uses('AppController', 'Controller');

App::import('Controller', 'Pages');

/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->User->recursive = 0;
        if( !empty($this->request->query['q']) ){
            $this->paginate = array(
                'conditions' => array(
                    'or' => array(
                        'User.name LIKE' => '%'.$this->request->query['q'].'%',
                        'User.login LIKE' => '%'.$this->request->query['q'].'%'
                    )
                ),
                'limit' => 20,
                'order' => array(
                    'User.name' => 'asc'
                )
            );
        }
        $this->set('users', $this->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('user', $this->User->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            if (strlen($this->request->data['User']['password']) < 6 || strlen($this->request->data['User']['password']) > 20) {
                $this->Session->setFlash(__('A senha deve conter de 6 a 20 caracteres!'), 'admin/message/error');
            } else {
                $pass = $this->makePassword($this->request->data['User']['password'], $this->request->data['User']['password2']);
                if (is_array($pass)) {
                    $this->User->create();
                    $this->request->data['User']['password'] = $pass[0];
                    if ($this->User->saveAll($this->request->data)) {
                        $this->Session->setFlash(__('The user has been saved'), 'admin/message/success');
                        $this->redirect(array('action' => 'index'));
                    } else {
                        $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'admin/message/error');
                    }
                } else {
                    $this->Session->setFlash(__('As senhas digitadas são diferentes.'), 'admin/message/error');
                }
            }
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data, array('fieldList' => array('group_id', 'active')))) {
                $this->Session->setFlash(__('The user has been saved'), 'admin/message/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'admin/message/error');
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
        }
        $groups = $this->User->Group->find('list');
        $this->set(compact('groups'));
    }

    /**
     * admin_delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Session->setFlash(__('User deleted'), 'admin/message/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not deleted'), 'admin/message/error');
        $this->redirect(array('action' => 'index'));
    }

    function makePassword($pass = null, $pass2 = null, $qtd = 8) {
        if (empty($pass)) {
            $caracteresAceitos = 'abcdxywzABCDZYWZ0123456789';
            $max = strlen($caracteresAceitos) - 1;
            $pass = null;
            for ($i = 0; $i < $qtd; $i++) {
                $pass .= $caracteresAceitos{mt_rand(0, $max)};
            }
            return (array($this->User->hash($pass), $pass));
        } else {
            if ($pass2 === null) {
                return (array($this->User->hash($pass), $pass));
            } elseif ($pass === $pass2) {
                return (array($this->User->hash($pass), $pass));
            } else {
                return false;
            }
        }
    }


    public function login() {
        $this->layout = 'login';
        if ($this->Authentica->isLogged()) {
            $this->Session->setFlash(__('Olá ' . $this->Authentica->getNome() . ', seja bem vindo ao sistema.'), 'admin/message/success');
            $groups = $this->Authentica->getGroup();
            if (!empty($groups['login_redirect'])) {
                $this->redirect($groups[0]['login_redirect']);
            } else {
                $this->redirect(array('controller' => 'dashboards', 'action' => 'index', 'plugin' => false, 'admin' => true));
            }
        }

        if (!empty($this->request->data)) {
            $login = $this->request->data['User']['login'];
            $password = $this->request->data['User']['password'];
            $user = $this->User->findByLogin($login);
            if (empty($user)) {
                $this->Session->setFlash(__('Login ou senha incorretos!'), 'admin/message/error');
                $this->redirect( $this->referer() );
                return;
            }

            // check for locked account
            if ($user['User']['id'] != 1 && $user['User']['active'] == 0) {
                $this->Session->setFlash(__('Sua conta está bloqueada, entre em contato com o administrador!'), 'admin/message/error');
                $this->redirect($this->Authentica->login);
            }
            // check for not confirmed email
            if (!empty($user['User']['confirmation'])) {
                $this->Session->setFlash(__('Seu cadastro não foi confirmado!'), 'admin/message/warning');
                $this->redirect($this->Authentica->login);
            }

            $userdata = $this->User->getLoginData($login, $password);
            if (empty($userdata)) {
                $this->Session->setFlash(__('Login ou senha incorretos!'), 'admin/message/error');
                $this->redirect( $this->referer() );
                return;
            } else {
                $next = $this->Authentica->getPreviousUrl();
                $this->Authentica->login($userdata);
                $groups = $this->Authentica->getGroup();
                $this->Session->setFlash('Login efetuado com sucesso', 'admin/message/success');
                $this->redirect(array('action' => 'index', 'controller' => 'dashboards', 'admin' => true));
            }
        }
    }

    public function loginSite() {
        $this->layout = 'site';

        if ($this->Authentica->isLogged()) {
            $this->Session->setFlash(__('Olá ' . $this->Authentica->getNome() . ', seja bem vindo.'), 'admin/message/success');
            $groups = $this->Authentica->getGroup();

            if (!empty($groups['login_redirect'])) {
                $this->redirect($groups[0]['login_redirect']);
            } else {
                if( $this->Authentica->getUserId() != 1 )
                    $this->redirect(array('controller' => 'profiles', 'plugin' => false));
                else
                    $this->redirect(array('controller' => 'dashboards', 'action' => 'index', 'plugin' => false, 'admin' => true));
            }
        }

        if (!empty($this->request->data)) {
            $login = $this->request->data['User']['login'];
            $password = $this->request->data['User']['password'];
            $user = $this->User->findByLogin($login);
            if (empty($user)) {
                $this->Session->setFlash(__('Login ou senha incorretos!'), 'admin/message/error');
                $this->redirect( $this->referer() );
                return;
            }

            // check for locked account
            if ($user['User']['id'] != 1 && $user['User']['active'] == 0) {
                $this->Session->setFlash(__('Sua conta está bloqueada, entre em contato com o administrador!'), 'admin/message/error');
                $this->redirect($this->Authentica->login);
            }
            // check for not confirmed email
            if (!empty($user['User']['confirmation'])) {
                $this->Session->setFlash(__('Seu cadastro não foi confirmado!'), 'admin/message/warning');
                $this->redirect($this->Authentica->login);
            }

            $userdata = $this->User->getLoginData($login, $password);
            if (empty($userdata)) {
                $this->Session->setFlash(__('Login ou senha incorretos!'), 'admin/message/error');
                $this->redirect( $this->referer() );
                return;
            } else {
                $next = $this->Authentica->getPreviousUrl();
                $this->Authentica->login($userdata);
                $groups = $this->Authentica->getGroup();
                $this->Session->setFlash('Login efetuado com sucesso', 'admin/message/success');
                if( $user['User']['id'] != 1 )
                    $this->redirect(array('controller' => 'profiles'));
                else
                    $this->redirect(array('action' => 'index', 'controller' => 'dashboards', 'admin' => true));
            }
        }
    }

    function logoutSite() {
        $group = $this->Authentica->getGroup();
        if ($this->Authentica->isLogged()) {
            $this->Authentica->logout();
            $this->Session->setFlash(__('Você saiu do sistema!'), 'admin/message/success');
            //$this->redirect( $this->referer() );
            //$this->redirect($group['logout_redirect']);
            $this->redirect("/login");

        }
        if (!empty($group['logout_redirect'])) {
            //$this->redirect($group['logout_redirect']);
            $this->redirect("/login");
        } else {
            //$this->redirect(array('action' => 'login', 'controller' => 'users', 'admin' => false));
           // $this->redirect( $this->referer() );
            $this->redirect("/login");
        }
    }

    function logout() {
        $group = $this->Authentica->getGroup();
        if ($this->Authentica->isLogged()) {
            $this->Authentica->logout();
            $this->Session->setFlash(__('Você saiu do sistema!'), 'admin/message/success');
            $this->redirect( $this->referer() );

        }
        if (!empty($group['logout_redirect'])) {
            $this->redirect($group['logout_redirect']);
        } else {
            //$this->redirect(array('action' => 'login', 'controller' => 'users', 'admin' => false));
            $this->redirect( $this->referer() );
        }
    }

    public function passwordRecovery() {
        $this->layout = 'login';
        if ($this->request->is('post')) {
            if (!empty($this->request->data['User']['loginrec'])) {
                $user = $this->User->find('first', array('conditions' => array('User.login' => $this->request->data['User']['loginrec'])));
            } elseif (!empty($this->request->data['Profile']['emailrec'])) {
                $user = $this->Profile->find('first', array('conditions' => array('Profile.email' => $this->request->data['Profile']['emailrec'])));
            } else {
                $this->Session->setFlash(__('Informe seu login ou email!'), 'admin/message/error');
            }
            if (empty($user)) {
                $this->Session->setFlash(__('Usuário não encontrado!'), 'admin/message/error');
            } else {
                $codeLinkPassword = $this->makePassword();
                $this->request->data['User']['password_recovery'] = $codeLinkPassword[0];
                $this->User->id = $user['User']['id'];
                if ($this->User->save($this->request->data)) {
                    $email = new CakeEmail();
                    $email->config('smtp');
                    $email->viewVars(array('data' => $this->request->data));
                    $email->template('password_recovery', 'default')->to($user['Profile']['email'])->subject('Recuperação de senha')->send();
                    $this->Session->setFlash(__('Os passos para a recuperação de senha foram enviados para o seu email'), 'admin/message/success');
                } else {
                    $this->Session->setFlash(__('Não foi possível recuperar a senha, entre em contato com o administrador!'), 'admin/message/error');
                }
            }
        }
    }

    public function newPassword($id = null) {
        $this->layout = 'login';
        $user = $this->User->find('first', array('conditions' => array('User.password_recovery' => $id)));
        if (empty($user)) {
            $this->Session->setFlash(__('Usuário não encontrado!'), 'admin/message/error');
            $this->redirect(array('action' => 'login', 'controller' => 'users', 'admin' => false));
        } else {
            if (!empty($this->request->data)) {
                $this->User->id = $user['User']['id'];
                $senha = $this->makePassword($this->request->data['User']['senha'], $this->request->data['User']['senha2']);
                if (strlen($this->request->data['User']['senha']) < 6 || strlen($this->request->data['User']['senha']) > 20) {
                    $this->Session->setFlash(__('A nova senha deve conter de 6 a 20 caracteres!'), 'admin/message/error');
                } else {
                    if (is_array($senha)) {
                        $email = new CakeEmail();
                        $email->config('smtp');
                        $this->request->data['User']['password'] = $senha[0];
                        $this->request->data['User']['password_recovery'] = '';
                        $this->request->data['Profile']['email'] = $user['Profile']['email'];
                        $this->request->data['User']['stringSenha'] = $senha[1];
                        $email->viewVars(array('data' => $this->request->data));
                        if ($this->User->save($this->request->data)) {
                            $email->template('modificada', 'default')->to($user['Profile']['email'])->subject('Senha modificada')->send();
                            $this->Session->setFlash(__('Senha modificada com sucesso!'), 'admin/message/success');
                            $this->redirect(array('action' => 'login', 'controller' => 'users', 'admin' => false));
                        } else {
                            $this->Session->setFlash(__('Sua senha não pode ser modificada, entre em contato com o administrador'), 'admin/message/error');
                        }
                    } else {
                        $this->Session->setFlash(__('As senhas informadas são diferentes.'), 'admin/message/error');
                    }
                }
            }
        }
    }

    public function resetPassword($id = null) {
        
    }

    public function admin_passwordChange() {
        if ($this->Authentica->isLogged()) {
            if ($this->request->is('post') || $this->request->is('put')) {
                $user = $this->User->read(null, $this->Authentica->getUserId());
                $atual = $this->makePassword($this->request->data['User']['atual']);
                if ($atual[0] == $user['User']['password']) {
                    if (strlen($this->request->data['User']['password']) < 6 || strlen($this->request->data['User']['password']) > 20) {
                        $this->Session->setFlash(__('A nova senha deve conter de 6 a 20 caracteres!'), 'admin/message/error');
                    } else {
                        $senha = $this->makePassword($this->request->data['User']['password'], $this->request->data['User']['password2']);
                        if (is_array($senha)) {
                            $this->request->data['User']['password'] = $senha[0];
                            if ($this->User->save($this->request->data)) {
                                $this->Session->setFlash(__('Senha modificada com sucesso!'), 'admin/message/success');
                            } else {
                                $this->Session->setFlash(__('Sua senha não pode ser modificada, entre em contato com o administrador'), 'admin/message/error');
                            }
                        } else {
                            $this->Session->setFlash(__('As senhas informadas são diferentes'), 'admin/message/error');
                        }
                    }
                } else {
                    $this->Session->setFlash(__('Senha atual incorreta!'), 'admin/message/error');
                }
            } else {
                $this->request->data = $this->User->read(null, $this->Authentica->getUserId());
                $this->request->data['User']['password'] = '';
                $this->request->data['User']['password2'] = '';
            }
        }
    }

    // Method para resetar a senha do usuário
    public function resetarsenha( $idUser = null, $code = null ) {

        if ( $idUser AND $code ) {

            $user = $this->User->read( null, $idUser );
            if ( !empty($user) ) {

                $resetedPassword = strtoupper(substr(md5(time()), 0, 8));                
                $newPassword = $this->makePassword( $resetedPassword, $resetedPassword );                
                $data = array(
                        'nome' => $user['User']['name'],
                        'newpassword' => $resetedPassword,
                    );

                $user['User']['password'] = $newPassword[0];
                $this->User->save( $user );

                $email = new CakeEmail();
                $email->config('smtp');
                $email->viewVars(array('data' => $data));
                $email->template('password_recovery_ste_newpassword', 'default')->to($user['Profile']['email'])->subject('Recuperação de senha')->send();
                $this->Session->setFlash(__('Foi enviado um email com sua nova senha para: '.$user['Profile']['email']), 'admin/message/success');
                 $this->redirect( '/' );
            } else {
                $this->Session->setFlash(__('Desculpa, houve algum erro no sistema, por favor, tente novamente.'), 'admin/message/error');
                $this->redirect( $this->referer()  );
            }

            $this->redirect( $this->referer()  );
        }
        if ( $this->request->data ) {
            $userEmail = $this->request->data['User']['login'];

            if(!filter_var($userEmail, FILTER_VALIDATE_EMAIL)){ 
                $this->Session->setFlash(__('O E-Mail informado é invalido, verifique seu E-Mail e tente novamente.'), 'admin/message/error');
                $this->redirect( $this->referer()  );          
            } 
            
            $userConsult = $this->User->find( 'first', array( 'conditions' => array( 'User.login' => $userEmail ) ) );
            
            if ( !empty($userConsult) ) {               

                $data = array(
                        'nome' => $userConsult['User']['name'],
                        'email' => $userConsult['Profile']['email'],
                        'url' => $userConsult['User']['id'].'/'.md5($userConsult['User']['id'].$userConsult['User']['name'].$userConsult['Profile']['email']),
                    );

                $email = new CakeEmail();
                $email->config('smtp');
                $email->viewVars(array('data' => $data));
                $email->template('password_recovery_site', 'default')->to($userConsult['Profile']['email'])->subject('Recuperação de senha')->send();
                $this->Session->setFlash(__('Foi enviado um email com um link para gerar sua nova senha para: '.$userConsult['Profile']['email']), 'admin/message/success');
                $this->redirect( '/' );
            } else {
                $this->Session->setFlash(__('Nenhum usuário cadastrado com esse E-Mail!'), 'admin/message/error');
                $this->redirect( $this->referer()  );
            }
        } else {
            $this->Session->setFlash(__('Desculpa, mais houve algum erro, por favor, tente novamente.'), 'admin/message/error');
            $this->redirect( $this->referer()  );
        }

        

        $this->Session->setFlash(__('Desculpa, mais houve algum erro, por favor, tente novamente.'), 'admin/message/error');
        $this->redirect( $this->referer()  );
        exit();
    }

    public function index($flag = null) {
        $this->layout = 'site';
        $this->set('page', '');
        $id = $this->Session->read('Logged.User.id');
        $user = $this->User->read( null, $id );
        
        $count = $this->Newsletter->find('count', array(
           'conditions' => array( 'Newsletter.email' => $user['Profile']['email'] )
        ));

        $this->atualizaProfile($id);

        $this->set(compact('user'));
        $this->set('edit', ($flag == 1));
        $this->set('newsletter', ($count == 0)?'':'checked');
    }

}
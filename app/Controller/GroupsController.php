<?php

App::uses('AppController', 'Controller');

/**
 * Groups Controller
 *
 * @property Group $Group
 */
class GroupsController extends AppController {

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->Group->recursive = 0;
        $this->set('groups', $this->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Group->id = $id;
        if (!$this->Group->exists()) {
            throw new NotFoundException(__('Invalid group'));
        }
        $this->set('group', $this->Group->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Group->create();
            if ($this->Group->save($this->request->data)) {
                $this->Session->setFlash('Adicionado com sucesso.', 'admin/message/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Não foi possível salvar, tente novamente mais tarde.', 'admin/message/error');
            }
        }
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Group->id = $id;
        if (!$this->Group->exists()) {
            throw new NotFoundException(__('Invalid group'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            $rules = array();
            if (isset($this->request->data['Group']['rules'])) {
                foreach ($this->request->data['Group']['rules'] as $value) {
                    $temp = explode(':', $value);
                    $rules[$temp[0]][] = $temp['1'];
                }
            }
            $this->request->data['Group']['rules'] = json_encode($rules);
            //pre($this->request->data);
            if ($this->Group->save($this->request->data)) {
                $this->Session->setFlash(__('The group has been saved'), 'admin/message/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The group could not be saved. Please, try again.'), 'admin/message/error');
            }
        } else {
            $this->request->data = $this->Group->read(null, $id);
        }
        $this->set('getControllerActions', $this->Authentica->getControllerActions());
    }

    /**
     * admin_delete method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Group->id = $id;
        if (!$this->Group->exists()) {
            throw new NotFoundException(__('Invalid group'));
        }
        if ($this->Group->delete()) {
            $this->Session->setFlash('Deletado com sucesso.', 'admin/message/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('Não foi possível deletar.', 'admin/message/error');
        $this->redirect(array('action' => 'index'));
    }

}

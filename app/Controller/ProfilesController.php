<?php
App::uses('AppController', 'Controller', 'TransactionsController');
/**
 * Profiles Controller
 *
 * @property Profile $Profile
 */
class ProfilesController extends AppController {

    public $uses = array('Profile', 'Product');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Profile->recursive = 0;
        if( !empty($this->request->query['q']) ){
            $this->paginate = array(
                'conditions' => array(
                    'or' => array(
                        'Profile.full_name LIKE' => '%'.$this->request->query['q'].'%',
                        'Profile.corporate_name LIKE' => '%'.$this->request->query['q'].'%'
                    )
                ),
                'limit' => 20,
                'order' => array(
                    'Profile.full_name' => 'asc'
                )
            );
        }
		$this->set('profiles', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Profile->id = $id;
		if (!$this->Profile->exists()) {
			throw new NotFoundException(__('Invalid profile'));
		}
		$this->set('profile', $this->Profile->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Profile->create();
			if ($this->Profile->save($this->request->data)) {
				$this->Session->setFlash(__('The profile has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The profile could not be saved. Please, try again.'), 'error');
			}
		}
		$users = $this->Profile->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Profile->id = $id;
		if (!$this->Profile->exists()) {
			throw new NotFoundException(__('Invalid profile'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Profile->save($this->request->data)) {
				$this->Session->setFlash(__('The profile has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The profile could not be saved. Please, try again.'), 'error');
			}
		} else {
			$this->request->data = $this->Profile->read(null, $id);
		}
		$users = $this->Profile->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Profile->id = $id;
		if (!$this->Profile->exists()) {
			throw new NotFoundException(__('Invalid profile'));
		}
		if ($this->Profile->delete()) {
			$this->Session->setFlash(__('Profile deleted'), 'sucess');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Profile was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}

	//------------------------------------------------------------------------------------

	public function index() {
        if( $this->Session->read('Logged.User.id') == 1 ){
        	$this->redirect(array('action' => 'index', 'controller' => 'dashboards', 'admin' => true));
        }else if( $this->Session->read('Logged.User.id') != null ){
            $this->layout = 'site';
            $this->set('page', 'pg-profiles');
        }else{
            $this->redirect("/login");
        }
    }

}

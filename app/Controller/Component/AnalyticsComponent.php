<?php

class AnalyticsComponent extends Component {

    function graficos($config) {
        if (file_exists(APP . 'webroot/analytics.xml')) {
            $xml = simplexml_load_file(APP . 'webroot/analytics.xml');
            if ($xml->data < date('d-m-Y')) {
                if($this->gerarXml($this->dadosAnalytics($config))){
                    return $this->convertXmlObjToArr(simplexml_load_file(APP . 'webroot/analytics.xml'));
                }else{
                    return false;
                }
            } else {
                return $this->convertXmlObjToArr($xml);
            }
        } else {
            if ($this->dadosAnalytics($config)) {
                $this->gerarXml($this->dadosAnalytics($config));
                return $this->convertXmlObjToArr(simplexml_load_file(APP . 'webroot/analytics.xml'));
            } else {
                return false;
            }
        }
    }

    function convertXmlObjToArr($xml) {
        $arr = array();
        foreach ($xml as $element) {
            $tag = $element->getName();
            $e = get_object_vars($element);
            if (!empty($e)) {
                $arr[$tag] = $element instanceof SimpleXMLElement ? $this->convertXmlObjToArr($element) : $e;
            } else {
                $arr[$tag] = trim($element);
            }
        }
        return $arr;
    }

    function arrayToXml($dadosAnalytics, &$xml) {
        foreach ($dadosAnalytics as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml->addChild("$key");
                    $this->arrayToXml($value, $subnode);
                } else {
                    $this->arrayToXml($value, $xml);
                }
            } else {
                $xml->addChild("$key", "$value");
            }
        }
    }

    function gerarXml($dadosAnalytics) {
        if (!empty($dadosAnalytics)) {
            $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><dadosanalytics><data>" . date('d-m-Y') . "</data></dadosanalytics>");
            $this->arrayToXml($dadosAnalytics, $xml);
            $xml->asXML(APP . "webroot/analytics.xml");
            chmod(APP . "webroot/analytics.xml", 0777);
            return true;
        }else{
            return false;
        }
    }

    function dadosAnalytics($config) {

        $dataInicioMesAnterior = date('Y-m-d', mktime(0, 0, 0, date("m") - 1, 1, date("Y")));
        $dataFimMesAnterior = mktime(0, 0, 0, date("m"), 1, date("Y"));
        $dataFimMesAnterior = date('Y-m-d', $dataFimMesAnterior - 1);
        $dataInicioMesAtual = date('Y-m-d', mktime(0, 0, 0, date("m"), 1, date("Y")));
        $dataFimMesAtual = date('Y-m-d');

        $acesso = explode(',', $config);
        try {
            $ga = new gapi($acesso[0], $acesso[1]);
            $id = $acesso[2];

            $ga->requestReportData($id, 'month', array('pageviews', 'visits', 'uniquePageviews', 'exitRate', 'visitors', 'percentNewVisits', 'entranceBounceRate', 'pageviewsPerVisit', 'avgTimeOnSite'), null, null, $dataInicioMesAnterior, $dataFimMesAnterior);
            foreach ($ga->getResults() as $dados) {

                $mes = $this->mes($dados[1]['month']);

                $analytics['anterior']['mes'] = $mes;
                $analytics['anterior']['visits'] = $dados[0]['visits'];
                $analytics['anterior']['pageviews'] = $dados[0]['pageviews'];
                $analytics['anterior']['visitors'] = $dados[0]['visitors'];
                $analytics['anterior']['pageviewsPerVisit'] = $dados[0]['pageviewsPerVisit'];
                $analytics['anterior']['entranceBounceRate'] = $dados[0]['entranceBounceRate'];
                $analytics['anterior']['avgTimeOnSite'] = $dados[0]['avgTimeOnSite'];
                $analytics['anterior']['percentNewVisits'] = $dados[0]['percentNewVisits'];
            }

            /*             * ********************* */
            $ga->requestReportData($id, 'country', array('visits'), null, null, $dataInicioMesAnterior, $dataFimMesAnterior);
            //$analyticsWorld[] = $ga->getResults();
            $key = 0;
            foreach ($ga->getResults() as $dados) {

                $analyticsWorld['country' . $key]['visits'] = $dados[0]['visits'];
                $analyticsWorld['country' . $key]['country'] = $dados[1]['country'];
                $key++;
            }
            /*             * ********************* */
            $ga->requestReportData($id, 'isMobile', array('visits'), null, null, $dataInicioMesAnterior, $dataFimMesAnterior);
            $key2 = 0;
            foreach ($ga->getResults() as $dados) {

                $analyticsMobile['result' . $key2]['visits'] = $dados[0]['visits'];
                $dados[1]['isMobile'] == "Yes" ? $analyticsMobile['result' . $key2]['isMobile'] = "Mobile" : $analyticsMobile['result' . $key2]['isMobile'] = "Outras Fontes";
                $key2++;
            }
            /*             * ********************* */
            $ga->requestReportData($id, 'month', array('pageviews', 'visits', 'uniquePageviews', 'exitRate', 'visitors', 'percentNewVisits', 'entranceBounceRate', 'pageviewsPerVisit', 'avgTimeOnSite'), null, null, $dataInicioMesAtual, $dataFimMesAtual);
            foreach ($ga->getResults() as $dados) {

                $mes = $this->mes($dados[1]['month']);

                $analytics['atual']['mes'] = $mes;
                $analytics['atual']['visits'] = $dados[0]['visits'];
                $analytics['atual']['pageviews'] = $dados[0]['pageviews'];
                $analytics['atual']['visitors'] = $dados[0]['visitors'];
                $analytics['atual']['pageviewsPerVisit'] = $dados[0]['pageviewsPerVisit'];
                $analytics['atual']['entranceBounceRate'] = $dados[0]['entranceBounceRate'];
                $analytics['atual']['avgTimeOnSite'] = $dados[0]['avgTimeOnSite'];
                $analytics['atual']['percentNewVisits'] = $dados[0]['percentNewVisits'];
            }

            $ga->requestReportData($id, 'nthWeek', array('pageviews', 'visits'), array('nthWeek'), null, $dataInicioMesAnterior, $dataFimMesAnterior, 1, 50);
            //pr($ga->getResults()); exit;
            $grafs = $ga->getResults();
            foreach ($grafs as $key => $graf) {
                $grafAnalitycs['week' . $key] = $graf;
            }
            $retorno = array();
            $retorno['analytics'] = $analytics;
            $retorno['grafanalytics'] = $grafAnalitycs;
            $retorno['analyticsworld'] = $analyticsWorld;
            $retorno['analyticsmobile'] = $analyticsMobile;
            return $retorno;
        } catch (Exception $e) {
            $analyticsError = $e->getMessage();
            return false;
        }
    }

    function mes($key) {
        $mes = array(
            '01' => 'Janeiro',
            '02' => 'Fevereiro',
            '03' => 'Março',
            '04' => 'Abril',
            '05' => 'Maio',
            '06' => 'Junho',
            '07' => 'Julho',
            '08' => 'Agosto',
            '09' => 'Setembro',
            '10' => 'Outubro',
            '11' => 'Novembro',
            '12' => 'Dezembro'
        );
        return $mes[$key];
    }

}

?>
<?php

App::uses('AppController', 'Controller');

/**
 * {'VARIAVEL_UCFIRST_PLURALIZE'} Controller
 *
 * @property {'VARIAVEL_UCFIRST_SINGULAR'} ${'VARIAVEL_UCFIRST_SINGULAR'}
 */
class {'VARIAVEL_UCFIRST_PLURALIZE'}Controller extends AppController {


    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->set('files', false);
        $this->{'VARIAVEL_UCFIRST_SINGULAR'}->recursive = 0;
        $this->set('{'VARIAVEL_PLURALIZE'}', $this->paginate());
    }

    /**
     * admin_view method
     *
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->{'VARIAVEL_UCFIRST_SINGULAR'}->id = $id;
        if (!$this->{'VARIAVEL_UCFIRST_SINGULAR'}->exists()) {
            throw new NotFoundException(__('Invalid {'VARIAVEL_SINGULARIZE'}'));
        }
        $this->set('{'VARIAVEL_SINGULARIZE'}', $this->{'VARIAVEL_UCFIRST_SINGULAR'}->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->{'VARIAVEL_UCFIRST_SINGULAR'}->create($this->request->data);
            if ($this->{'VARIAVEL_UCFIRST_SINGULAR'}->save($data)) {
                $this->Session->setFlash(__('The {'VARIAVEL_SINGULARIZE'} has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The {'VARIAVEL_SINGULARIZE'} could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * admin_edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->{'VARIAVEL_UCFIRST_SINGULAR'}->id = $id;
        if (!$this->{'VARIAVEL_UCFIRST_SINGULAR'}->exists()) {
            throw new NotFoundException(__('Invalid {'VARIAVEL_SINGULARIZE'}'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->{'VARIAVEL_UCFIRST_SINGULAR'}->save($this->request->data)) {
                $this->Session->setFlash(__('The {'VARIAVEL_SINGULARIZE'} has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The {'VARIAVEL_SINGULARIZE'} could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->{'VARIAVEL_UCFIRST_SINGULAR'}->read(null, $id);
        }
    }

    /**
     * admin_delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->{'VARIAVEL_UCFIRST_SINGULAR'}->id = $id;
        if (!$this->{'VARIAVEL_UCFIRST_SINGULAR'}->exists()) {
            throw new NotFoundException(__('Invalid {'VARIAVEL_SINGULARIZE'}'));
        }
        if ($this->{'VARIAVEL_UCFIRST_SINGULAR'}->delete()) {
            $this->Session->setFlash(__('{'VARIAVEL_UCFIRST_SINGULAR'} deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('{'VARIAVEL_UCFIRST_SINGULAR'} was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    
    
}

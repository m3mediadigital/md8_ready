<?php

App::uses('AppController', 'Controller');
App::uses('csvtoarray', 'Vendor');

/**
 * Newsletters Controller
 *
 * @property Newsletter $Newsletter
 */
class NewslettersController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {

		$this->Newsletter->recursive = 0;
		$this->set('newsletters', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 * @throws NotFoundException
 */
	public function admin_view($id = null) {
		$this->Newsletter->id = $id;
		if (!$this->Newsletter->exists()) {
			throw new NotFoundException(__('Invalid newsletter'));
		}
		$this->set('newsletter', $this->Newsletter->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Newsletter->create();
			if ($this->Newsletter->save($this->request->data)) {
				$this->Session->setFlash(__('The newsletter has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The newsletter could not be saved. Please, try again.'), 'error');
			}
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Newsletter->create();
			if ($this->Newsletter->save($this->request->data)) {
				$this->Session->setFlash(__('The newsletter has been saved'), 'sucess');
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('The newsletter could not be saved. Please, try again.'), 'error');
				$this->redirect($this->referer());
			}
		}
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 * @throws NotFoundException
 */
	public function admin_edit($id = null) {
		$this->Newsletter->id = $id;
		if (!$this->Newsletter->exists()) {
			throw new NotFoundException(__('Invalid newsletter'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Newsletter->save($this->request->data)) {
				$this->Session->setFlash(__('The newsletter has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The newsletter could not be saved. Please, try again.'), 'error');
			}
		} else {
			$this->request->data = $this->Newsletter->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Newsletter->id = $id;
		if (!$this->Newsletter->exists()) {
			throw new NotFoundException(__('Invalid newsletter'));
		}
		if ($this->Newsletter->delete()) {
			$this->Session->setFlash(__('Newsletter deleted'), 'sucess');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Newsletter was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}

/**
 * exportCsv method
 *
 * @return file
 */
	public function exportCsv() {
		$this->autoRender = false;
		$this->response->type('Content-Type: text/csv');
		$this->response->download(strtolower(Inflector::pluralize('Newsletter')) . '.csv');
		$this->response->body($this->Newsletter->exportCSV());
	}

}

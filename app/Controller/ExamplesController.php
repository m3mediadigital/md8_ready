<?php

App::uses('AppController', 'Controller');

/**
 * Examples Controller
 *
 * @property Example $Example
 */
class ExamplesController extends AppController {


    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->set('files', true);
        $this->Example->recursive = 0;
        $this->set('examples', $this->paginate());
    }

    /**
     * admin_view method
     *
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Example->id = $id;
        if (!$this->Example->exists()) {
            throw new NotFoundException(__('Invalid example'));
        }
        $this->set('example', $this->Example->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Example->create($this->request->data);
            if ($this->Example->save($data)) {
                $this->Session->setFlash(__('The example has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The example could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * admin_edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Example->id = $id;
        if (!$this->Example->exists()) {
            throw new NotFoundException(__('Invalid example'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Example->save($this->request->data)) {
                $this->Session->setFlash(__('The example has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The example could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Example->read(null, $id);
        }
    }

    /**
     * admin_delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Example->id = $id;
        if (!$this->Example->exists()) {
            throw new NotFoundException(__('Invalid example'));
        }
        if ($this->Example->delete()) {
            $this->Session->setFlash(__('Example deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Example was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    
    
}

<?php

App::uses('AppController', 'Controller');

/**
 * Submenus Controller
 *
 * @property Submenu $Submenu
 */
class SubmenusController extends AppController {

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->Submenu->recursive = 0;
        $this->set('submenus', $this->paginate());
    }

    /**
     * admin_view method
     *
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Submenu->id = $id;
        if (!$this->Submenu->exists()) {
            throw new NotFoundException(__('Invalid submenu'));
        }
        $this->set('submenu', $this->Submenu->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Submenu->create();
            if (isset($this->request->data['Submenu']['image']) && !empty($this->request->data['Submenu']['image']['name'])) {
                $this->request->data['Submenu']['image'] = Wideimage::save(array(
                            'file' => $this->request->data['Submenu']['image'],
                            'styles' => array(
                                'admin_list' => '29x29#',
                                'admin_edit' => '150x150#',
                                'small' => '190x160#',
                                'medium' => '650x190#',
                                'large' => '800x600>'),
                            'path' => APP . 'webroot/img/uploads/submenu'));
            } else {
                unset($this->request->data['Submenu']['image']);
            }
            
            // Aqui ficará a parte de geração de código automática. ;)
//                $data = AutomateComponent::generate($this->request->data);
            // Aqui fica o fim da parte de geração de código. =(

            if ($this->Submenu->save($this->request->data)) {
                $this->Session->setFlash(__('The submenu has been saved'), 'sucess');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The submenu could not be saved. Please, try again.'), 'error');
            }
        }
        $menus = $this->Submenu->Menu->find('list');
        $this->set(compact('menus'));
    }

    /**
     * admin_edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Submenu->id = $id;
        if (!$this->Submenu->exists()) {
            throw new NotFoundException(__('Invalid submenu'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if (isset($this->request->data['Submenu']['image']) && !empty($this->request->data['Submenu']['image']['name'])) {
                $this->request->data['Submenu']['image'] = Wideimage::save(array(
                            'file' => $this->request->data['Submenu']['image'],
                            'styles' => array(
                                'admin_list' => '29x29#',
                                'admin_edit' => '150x150#',
                                'small' => '190x160#',
                                'medium' => '650x190#',
                                'large' => '800x600>'),
                            'path' => APP . 'webroot/img/uploads/submenu'));
            } else {
                unset($this->request->data['Submenu']['image']);
            }
            if ($this->Submenu->save($this->request->data)) {
                $this->Session->setFlash(__('The submenu has been saved'), 'sucess');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The submenu could not be saved. Please, try again.'), 'error');
            }
        } else {
            $this->request->data = $this->Submenu->read(null, $id);
        }
        $menus = $this->Submenu->Menu->find('list');
        $this->set(compact('menus'));
    }

    /**
     * admin_delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Submenu->id = $id;
        if (!$this->Submenu->exists()) {
            throw new NotFoundException(__('Invalid submenu'));
        }
        if ($this->Submenu->delete()) {
            $this->Session->setFlash(__('Submenu deleted'), 'sucess');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Submenu was not deleted'), 'error');
        $this->redirect(array('action' => 'index'));
    }

}

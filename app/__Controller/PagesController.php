<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

    public $name = 'Pages';
    public $helpers = array('Html', 'Session');
    public $uses = array('Banner', 'Product', 'News', 'Frequentquestion', 'Client', 'Md8');

    public function index() {
        $this->layout = 'site';
        $this->set('banners', $this->Banner->find('all'));
        $this->set('products', $this->Product->find('all', array('limit' => 9)));
        $this->set('page', 'pg-home');
    }

    public function amd8() {
        $this->layout = 'site';
        $this->set('md8', $this->Md8->find('first'));
        $this->set('page', 'pg-amd8');
    }

    public function clientes() {
        $this->layout = 'site';
        $this->set('clientes', $this->Client->find('all'));
        $this->set('page', 'pg-clientes');
    }

    public function produtos() {
        $this->layout = 'site';

        if (!empty($this->request->data)) {
            $this->paginate = array('conditions' => array('Product.title LIKE' => '%'.$this->request->data['Page']['pesquisa'].'%'));
        } else {
            $this->paginate = array('limit' => 12);
        }

        $this->set('products', $this->paginate('Product'));
        $this->set('page', 'pg-produtos');
    }

    public function produto_id($title, $id) {
        $this->layout = 'site';

        $produto = $this->Product->findById($id);
        $photos = $this->Photo->findAllByAlbumId($produto['Album']['id']);

        $categorias = $this->Parentcategory->findAllByProductId($id, array('Parentcategory.category_id'));
        foreach ($categorias as $categoria) {
            $categorias_id[] = $categoria['Parentcategory']['category_id'];
        }

        $produtos_ids = $this->Parentcategory->find('all', array('conditions' => array('Parentcategory.category_id' => array_unique($categorias_id))));
        foreach ($produtos_ids as $produto_id) {
            $produtos_id[] = $produto_id['Parentcategory']['product_id'];
        }
        $produtos = $this->Product->find('all', array('conditions' => array('Product.id' => array_unique($produtos_id))));

        $this->set('produto', $produto);
        $this->set('products', $produtos);
        $this->set('photos', $photos);
		$this->set('page', 'pg-produtos');
    }

    public function produtosCategoria($title, $id) {
        $this->layout = 'site';

        $joins = array(
            array(
                'table' => 'parentcategories',
                'alias' => 'Parentcategory',
                'type' => 'INNER',
                'conditions' => array(
                    'Product.id = Parentcategory.product_id' 
                )
            )
        );

        $this->paginate = array('joins' => $joins, 'conditions' => array('Parentcategory.category_id' => $id), 'group' => 'Product.id');
        $this->set('products', $this->paginate('Product'));
        $this->set('page', 'pg-produtos');
        $this->render('produtos');
    }

    public function produtosSubcategoria($categoria, $subcategoria, $id) {
        $this->layout = 'site';

        $joins = array(
            array(
                'table' => 'parentcategories',
                'alias' => 'Parentcategory',
                'type' => 'INNER',
                'conditions' => array(
                    'Product.id = Parentcategory.product_id' 
                )
            )
        );

        $this->paginate = array('joins' => $joins, 'conditions' => array('Parentcategory.subcategory_id' => $id), 'group' => 'Product.id');
        $this->set('products', $this->paginate('Product'));
        $this->set('page', 'pg-produtos');
        $this->render('produtos');
    }


    public function noticias() {
        $this->layout = 'site';
        $this->paginate = array('limit' => 6);
        $this->set('noticias', $this->paginate('News'));
		$this->set('page', 'pg-noticias');
    }

    public function noticias_id($title,$id) {
        $this->layout = 'site';
        $this->set('noticia', $this->News->findById($id));
		$this->set('page', 'pg-noticias');
    }

    public function duvidasfrequentes() {
        $this->layout = 'site';

        $this->set('duvidas', $this->Frequentquestion->find('all'));
		$this->set('page', 'pg-duvidas');
    }

    public function cadastro() {
        $this->layout = 'site';
		$this->set('page', '');

        if( $this->request->data ){
            $password_confirmation = $this->request->data['User']['password_confirmation'];
            unset($this->request->data['User']['cpfcasal']);
            unset($this->request->data['User']['password_confirmation']);

            $userCPF = $this->User->find( 'first', array( 'conditions' => array( 'Profile.cpf' => $this->request->data['Profile']['cpf'] ) ) );
            $userCNPJ = $this->User->find( 'first', array( 'conditions' => array( 'Profile.cnpj' => $this->request->data['Profile']['cnpj'] ) ) );
            
            if ( $this->request->data['User']['receberNews'] >= 1 ) {   
                $newsletterConsult = $this->Newsletter->find( 'first', array( 'Newsletter.email' => $this->request->data['Profile']['email'] ) );
                if ( empty($newsletterConsult) ) {
                    
                    $this->Newsletter->save( array( 'Newsletter' => array( 
                        'name' => $this->request->data['Profile']['full_name'],
                        'email' => $this->request->data['Profile']['email']
                    ) ) );                     
                }
            }
            unset($this->request->data['User']['receberNews']);
            
            if ( $this->request->data['User']['password'] != $password_confirmation ) { 
                $this->Session->setFlash('Senha e confirmação não conferem!', 'warning'); 
            } else if ( !empty( $userCPF )  ) {
                $this->Session->setFlash('CPF já cadastrado!', 'warning');                
            } elseif ( !empty( $userCNPJ )  ) {
                $this->Session->setFlash('CNPJ já cadastrado!', 'warning');                
            } elseif (empty($this->request->data['Profile']['full_name'])) {
            $this->Session->setFlash('Por favor, informe o seu nome. O email não foi enviado.', 'error');
            } elseif (empty($this->request->data['Profile']['corporate_name'])) {
                $this->Session->setFlash('Por favor, informe Razão Social. O email não foi enviado.', 'error');
            } elseif (empty($this->request->data['Profile']['email'])) {
                $this->Session->setFlash('Por favor, informe o seu email. O email não foi enviado.', 'error');
            } elseif (!preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(\.[[:lower:]]{2,3})(\.[[:lower:]]{2})?$/", $this->request->data['Profile']['email'])) {
                $this->Session->setFlash('O email informado é inválido. O email não foi enviado.', 'error');
            } elseif (empty($this->request->data['Profile']['primary_phone'])) {
                $this->Session->setFlash('Por favor, informe um telefone. O email não foi enviado.', 'error');
            } elseif (empty($this->request->data['Profile']['cpf'])) {
                $this->Session->setFlash('Por favor, informe o seu CPF. O email não foi enviado.', 'error');
            } elseif (empty($this->request->data['Profile']['cnpj'])) {
                $this->Session->setFlash('Por favor, informe o seu CNPJ. O email não foi enviado.', 'error');
            } else {
                
                $this->request->data['User']['name'] = $this->request->data['Profile']['full_name'];
                $this->request->data['User']['login'] = $this->request->data['Profile']['email'];
                $this->request->data['User']['active'] = 1;
                $password = $this->makePassword($this->request->data['User']['password'], $this->request->data['User']['password']);
                $this->request->data['User']['password'] = $password[0];
                // ID do grupo 
                $this->request->data['User']['group_id'] = 2;

                if ( $this->User->saveAll( $this->request->data ) ) {
                    $this->Session->setFlash('Cadastro efetuado com suceso!', 'sucess');                    
                } else {
                    $this->Session->setFlash('Não foi possivel efetuar o cadastro!', 'error');
                }

            }

           
        }

    }
    
    public function contato() {
        $this->layout = 'site';

        if (!empty($this->request->data)) {
            if (empty($this->request->data['Page']['cpfcasal'])) {
                if (empty($this->request->data['Page']['nome'])) {
                    $this->Session->setFlash('Por favor, informe o seu nome. O email não foi enviado.', 'error');
                } elseif (empty($this->request->data['Page']['email'])) {
                    $this->Session->setFlash('Por favor, informe o seu email. O email não foi enviado.', 'error');
                } elseif (!preg_match("/^([[:alnum:]_.-]){3,}@([[:lower:][:digit:]_.-]{3,})(\.[[:lower:]]{2,3})(\.[[:lower:]]{2})?$/", $this->request->data['Page']['email'])) {
                    $this->Session->setFlash('O email informado é inválido. O email não foi enviado.', 'error');
                } elseif (empty($this->request->data['Page']['telefone'])) {
                    $this->Session->setFlash('Por favor, informe seu telefone. O email não foi enviado.', 'error');
                } elseif (empty($this->request->data['Page']['comentario'])) {
                    $this->Session->setFlash('Por favor, escreva a mensagem. O email não foi enviado.', 'error');
                } else {
                    $email = new CakeEmail();
                    $email->config('smtp');
                    $email->viewVars(array('data' => $this->request->data['Page']));
                    if ($email->template('contato', 'default')->to($this->Session->read('Settings.email_contato.value'))->replyTo($this->request->data['Page']['email'])->subject('Formulário de contato')->send()) {
                        $this->Session->setFlash('Email enviado com sucesso!', 'sucess');
                        $this->redirect($this->referer());
                    } else {
                        $this->Session->setFlash('Seu e-mail não pode ser enviado, Por favor, Tente novamente', 'error');
                    }
                }
            }
        }
        
		$this->set('page', 'pg-contato');
    }
    
    public function denied() {
        $this->layout = 'site';
    }

    public function error() {
        $this->layout = 'site';
    }

    public function teste() {
        $this->layout = 'bootstrap';
    }


    function makePassword($pass = null, $pass2 = null, $qtd = 8) {
        if (empty($pass)) {
            $caracteresAceitos = 'abcdxywzABCDZYWZ0123456789';
            $max = strlen($caracteresAceitos) - 1;
            $pass = null;
            for ($i = 0; $i < $qtd; $i++) {
                $pass .= $caracteresAceitos{mt_rand(0, $max)};
            }
            return (array($this->User->hash($pass), $pass));
        } else {
            if ($pass2 === null) {
                return (array($this->User->hash($pass), $pass));
            } elseif ($pass === $pass2) {
                return (array($this->User->hash($pass), $pass));
            } else {
                return false;
            }
        }
    }

}

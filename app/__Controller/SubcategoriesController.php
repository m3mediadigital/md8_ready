<?php
App::uses('AppController', 'Controller');
/**
 * Subcategories Controller
 *
 * @property Subcategory $Subcategory
 */
class SubcategoriesController extends AppController {


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Subcategory->recursive = 0;
		$this->set('subcategories', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Subcategory->id = $id;
		if (!$this->Subcategory->exists()) {
			throw new NotFoundException(__('Invalid subcategory'));
		}
		$this->set('subcategory', $this->Subcategory->read(null, $id));		
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add( $id = null ) {
		if ($this->request->is('post')) {
			
			$this->Subcategory->create();
			if ($this->Subcategory->save($this->request->data)) {
				$this->Session->setFlash(__('The subcategory has been saved'), 'sucess');
				$this->redirect(array( 'controller' => 'categories', 'action' => 'index'));
			} else {
				$this->Session->setFlash(__('The subcategory could not be saved. Please, try again.'), 'error');
			}
		}
		$categories = $this->Subcategory->Category->find('list');
		$subcategories = $this->Subcategory->find( 'list', array( 'fields' => array( 'id', 'subcategory' ), 'conditions' => array( 'Subcategory.category_id' => $id ) ) );
		
		$this->set(compact('categories', 'id', 'subcategories'));
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {

		$this->Subcategory->id = $id;
		if (!$this->Subcategory->exists()) {
			throw new NotFoundException(__('Invalid subcategory'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Subcategory->save($this->request->data)) {
				$this->Session->setFlash(__('The subcategory has been saved'), 'sucess');
				$this->redirect(array( 'controller' => 'categories', 'action' => 'index'));
			} else {
				$this->Session->setFlash(__('The subcategory could not be saved. Please, try again.'), 'error');
			}
		} else {
			$this->request->data = $this->Subcategory->read(null, $id);			
		}
		$categories = $this->Subcategory->Category->find('list');
		$this->set(compact('categories'));
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		
		$this->Subcategory->id = $id;
			if (!$this->Subcategory->exists()) {
			throw new NotFoundException(__('Invalid subcategory'));
		}
		if ($this->Subcategory->delete()) {
			$this->Session->setFlash(__('Subcategory deleted'), 'sucess');
			$this->redirect( $this->referer() );
		}
		$this->Session->setFlash(__('Subcategory was not deleted'), 'error');
		$this->redirect( $this->referer() );
	}
}

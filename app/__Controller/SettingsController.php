<?php

App::uses('AppController', 'Controller');

/**
 * Settings Controller
 *
 * @property Setting $Setting
 */
class SettingsController extends AppController {

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        if ($this->request->is('post')) {
            //pre($this->request->data);
            if ($this->request->data['Setting']['action'] == 'ativar') {
                foreach ($this->request->data['Setting']['ids'] as $key => $value) {
                    if ($value != 0) {
                        $this->Setting->id = $value;
                        $this->request->data['Setting']['active'] = 1;
                        $this->request->data['Setting']['id'] = $value;
                        $this->Setting->save($this->request->data);
                        $this->Session->setFlash(__('Os itens selecionados foram ativados com sucesso'), 'sucess');
                        
                    }
                }
                $this->redirect(array('action' => 'index'));
            }
            if ($this->request->data['Setting']['action'] == 'desativar') {
                foreach ($this->request->data['Setting']['ids'] as $key => $value) {
                    if ($value != 0) {
                        $this->Setting->id = $value;
                        $this->request->data['Setting']['active'] = 0;
                        $this->request->data['Setting']['id'] = $value;
                        $this->Setting->save($this->request->data);
                        $this->Session->setFlash(__('Os itens selecionados foram desativados com sucesso'), 'sucess');
                    }
                }
                $this->redirect(array('action' => 'index'));
            }
            if ($this->request->data['Setting']['action'] == 'excluir') {
                foreach ($this->request->data['Setting']['ids'] as $key => $value) {
                    if ($value != 0) {
                        $this->Setting->id = $value;
                        $this->Setting->delete();
                        $this->Session->setFlash(__('Os itens selecionados foram excluídos com sucesso'), 'sucess');
                    }
                }
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->Setting->recursive = 0;
        $this->set('settings', $this->paginate());
    }

    /**
     * admin_view method
     *
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Setting->id = $id;
        if (!$this->Setting->exists()) {
            throw new NotFoundException(__('Invalid setting'));
        }
        $this->set('setting', $this->Setting->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Setting->create();
            if ($this->Setting->save($this->request->data)) {
                $this->Session->delete('Settings');
                $this->Session->setFlash(__('The setting has been saved'), 'sucess');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The setting could not be saved. Please, try again.'), 'error');
            }
        }
    }

    /**
     * admin_edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Setting->id = $id;
        if (!$this->Setting->exists()) {
            throw new NotFoundException(__('Invalid setting'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Setting->save($this->request->data)) {
                $this->Session->delete('Settings');
                $this->Session->setFlash(__('The setting has been saved'), 'sucess');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The setting could not be saved. Please, try again.'), 'error');
            }
        } else {
            $this->request->data = $this->Setting->read(null, $id);
        }
    }

    /**
     * admin_delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Setting->id = $id;
        if (!$this->Setting->exists()) {
            throw new NotFoundException(__('Invalid setting'));
        }
        if ($this->Setting->delete()) {
            $this->Session->setFlash(__('Setting deleted'), 'sucess');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Setting was not deleted'), 'error');
        $this->redirect(array('action' => 'index'));
    }

}

<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('{'VARIAVEL_UCFIRST_PLURALIZE'}'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png'), array('action' => 'add'), array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Add'), array('action' => 'add')); ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <table>
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" class="checkall"/>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('id'); ?>
                    </th>
                    {'VARIAVELS_COLUMNS_INDEX_UPPER'}
                    <th class="actions">
                        Ações
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach (${'VARIAVEL_PLURALIZE'} as ${'VARIAVEL_SINGULARIZE'}):
                    $class = null;
                    if ($i++ % 2 == 0) {
                        $class = ' class="altrow"';
                    }
                    ?>
                    <tr>
                        <td>
                            <?= $this->Form->input('{'VARIAVEL_UCFIRST_SINGULAR'}.id.' . ${'VARIAVEL_SINGULARIZE'}['{'VARIAVEL_UCFIRST_SINGULAR'}']['id'], array('value' => ${'VARIAVEL_SINGULARIZE'}['{'VARIAVEL_UCFIRST_SINGULAR'}']['id'], 'type' => 'checkbox', 'label' => false, 'div' => false)) ?>
                        </td>
                        <td>
                            <?php echo $this->Html->link(${'VARIAVEL_SINGULARIZE'}['{'VARIAVEL_UCFIRST_SINGULAR'}']['id'], array('action' => 'edit', ${'VARIAVEL_SINGULARIZE'}['{'VARIAVEL_UCFIRST_SINGULAR'}']['id'])); ?>
                        </td>
                        {'VARIAVELS_COLUMNS_INDEX_DOWN'}
                        <td class="actions">
                            <ul>
                                <?php if($files): ?>
                                    <li>
                                        <?php echo $this->Html->link('Anexar Arquivos', array('controller' => 'photos', 'action' => 'anexar', ${'VARIAVEL_SINGULARIZE'}['{'VARIAVEL_UCFIRST_SINGULAR'}']['id'], '?' => array('tabela' => '{'VARIAVEL_PLURALIZE'}')), array('escape' => false, 'class' => 'bt-anexar')); ?>
                                    </li>
                                <?php endif; ?>
                                <li>
                                    <?php echo $this->Html->link('Ver', array('action' => 'view', ${'VARIAVEL_SINGULARIZE'}['{'VARIAVEL_UCFIRST_SINGULAR'}']['id']), array('escape' => false, 'class' => 'bt-ver')); ?>
                                </li>
                                <li>
                                    <?php echo $this->Html->link('Editar', array('action' => 'edit', ${'VARIAVEL_SINGULARIZE'}['{'VARIAVEL_UCFIRST_SINGULAR'}']['id']), array('escape' => false, 'class' => 'bt-editar')); ?>
                                </li>
                                <li>
                                    <?php echo $this->Form->postLink('Remover', array('action' => 'delete', ${'VARIAVEL_SINGULARIZE'}['{'VARIAVEL_UCFIRST_SINGULAR'}']['id']), array('escape' => false, 'class' => 'bt-deletar'), __('Are you sure you want to delete # %s?', ${'VARIAVEL_SINGULARIZE'}['{'VARIAVEL_UCFIRST_SINGULAR'}']['id'])); ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}'))); ?></p>
        <div class="acoesMultiplas">
            <button class="bt-deletar">Remover selecionados</button>
            <button class="bt-ativar">Ativar selecionados</button>
            <button class="bt-desativar">Desativar selecionados</button>
        </div>
        <div class="pagination">
            <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape' => false), null, array('class' => 'prev disabled')); ?>
            <?php echo $this->Paginator->numbers(array('separator' => ' ')); ?>
            <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape' => false), null, array('class' => 'next disabled')); ?>
        </div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png'), array('action' => 'add'), array('escape' => false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action' => 'add')); ?>
        </li>
    </ul>
</div>
<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('{'VARIAVEL_UCFIRST_PLURALIZE'}'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('{'VARIAVEL_UCFIRST_SINGULAR'}.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('{'VARIAVEL_UCFIRST_SINGULAR'}.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('{'VARIAVEL_UCFIRST_SINGULAR'}.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('{'VARIAVEL_UCFIRST_SINGULAR'}.id'))); ?>
        </li>
    </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		
                        <td><h4><?php echo __('Id'); ?></h4></td>
                        <td><?php echo ${'VARIAVEL_SINGULARIZE'}['{'VARIAVEL_UCFIRST_SINGULAR'}']['id']; ?></td>
                     </tr>
                    {'VARIAVELS_COLUMNS_VIEW'}
                 </tbody>
             </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('{'VARIAVEL_UCFIRST_SINGULAR'}.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('{'VARIAVEL_UCFIRST_SINGULAR'}.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('{'VARIAVEL_UCFIRST_SINGULAR'}.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('{'VARIAVEL_UCFIRST_SINGULAR'}.id'))); ?>
        </li>
    </ul>
</div>
<?php

App::uses('AppModel', 'Model');

class {'VARIAVEL_UCFIRST_SINGULAR'} extends AppModel {

    var $actsAs = array(
        'MeioUpload.MeioUpload' => array(
            'image' => array(
                'dir' => 'uploads',
                'create_directory' => true,
                'default' => false
            )
        )
    );

    public $hasMany = array(
        'TableFiles' => array(
            'className' => 'TableFiles',
            'foreignKey' => 'table_id',
            'dependent' => false,
            'conditions' => array(
                'TableFiles.table_title = "{'VARIAVEL_PLURALIZE'}"'
            ),
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    
}

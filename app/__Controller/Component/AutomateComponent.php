<?php

class AutomateComponent extends Component {

    public static function generate($data) {
        $abs_path = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR;
        $view_path = $abs_path . 'View' . DIRECTORY_SEPARATOR;
        $model_path = $abs_path . 'Model' . DIRECTORY_SEPARATOR;
        $controller_path = $abs_path . 'Controller' . DIRECTORY_SEPARATOR;
        $automate_docs = $controller_path . 'Component' . DIRECTORY_SEPARATOR . 'AutomateDocs' . DIRECTORY_SEPARATOR;
        
        $controller = $data['Submenu']['controller'];
        
        $controller_pluralize = Inflector::pluralize(strtolower($controller));
        $controller_singular = Inflector::singularize(strtolower($controller));
        $controller_ucfirst_pluralize = Inflector::pluralize(ucfirst($controller));
        $controller_ucfirst_singular = Inflector::singularize(ucfirst($controller));
        
        $edit_image = "";
        $colunas = $data['Submenu']['columns'];
        $colunas = explode("\n", $colunas);
        $colunas_add_edit = array();
        $colunas_view = array();
        $colunas_index_upper = array();
        $colunas_index_down = array();
        foreach ($colunas as $key => $coluna) {
            $coluna = strtolower(trim($coluna));
            if ($coluna != 'id') {
                if ($coluna != 'photo' && $coluna != 'foto' && $coluna != 'image' && $coluna != 'imagem') {
                    if ($coluna != 'active' && $coluna != 'ativo') {
                        $colunas_add_edit[$key] = 'echo $this->Form->input("'.trim($coluna).'");';
                        $colunas_view[$key] = '<tr><td><h4><?php echo __("'.ucfirst(trim($coluna)).'"); ?></h4></td>
                            <td><?php echo $'.$controller_singular.'["'.$controller_ucfirst_singular.'"]["'.trim($coluna).'"]; ?></td></tr>';
                        if ($coluna != 'description' && $coluna != 'descricao' && $coluna != 'text' && $coluna != 'texto') {
                            $colunas_index_upper[$key] = '<th><?php echo $this->Paginator->sort("'.trim($coluna).'"); ?></th>';
                            $colunas_index_down[$key] = '<td><?php echo $this->Html->link($'.$controller_singular.'
                                ["'.$controller_ucfirst_singular.'"]["'.trim($coluna).'"], array("action" => "edit", $'.$controller_singular.'
                                ["'.$controller_ucfirst_singular.'"]["'.trim($coluna).'"])); ?></td>';
                        }
                    } else {
                        $colunas_add_edit[$key] = 'echo $this->Form->input("'.trim($coluna).'", array("type" => "checkbox"));';
                        $colunas_view[$key] = '<tr><td><h4><?php echo __("'.ucfirst(trim($coluna)).'"); ?></h4></td>
                            <td><?php echo $'.$controller_singular.'["'.$controller_ucfirst_singular.'"]["'.trim($coluna).'"]; ?></td></tr>';
                        $colunas_index_upper[$key] = '<th><?php echo $this->Paginator->sort("'.trim($coluna).'"); ?></th>';
                        $colunas_index_down[$key] = '<td><?php if ($'.$controller_singular.'["'.$controller_ucfirst_singular.'"]
                                ["'.trim($coluna).'"] == 1) { echo $this->Html->image("/coreadmin/img/icons/ativar.png", array(
                                "title" => "Ativo", "class" => "ListImage")); } else { echo 
                                $this->Html->image("/coreadmin/img/icons/desativar.png", array("title" => "Desativado", 
                                "class" => "ListImage")); } ?></td>';
                    }
                } else {
                    $colunas_add_edit[$key] = 'echo $this->Form->input("'.trim($coluna).'", array("type" => "file"));';
                    $edit_image = '<?php if (!empty($this->data["'.$controller_ucfirst_singular.'"]["'.trim($coluna).'"])): ?>
                        <div class="input"><label>Foto Atual</label><div class="input">
                        <?= $this->Html->link($this->PhpThumb->thumbnail("uploads/" . $this->data["'.$controller_ucfirst_singular.'"]
                        ["'.trim($coluna).'"], array("w" => 300, "h" => 250, "zc" => 1)), $this->PhpThumb->url("uploads/" . 
                        $this->data["'.$controller_ucfirst_singular.'"]["'.trim($coluna).'"], array("w" => 640, "h" => 480, 
                        "zc" => 1)), array("escape" => false, "class" => "fancybox")) ?></div></div><?php endif; ?>';
                    $colunas_view[$key] = '<tr><td><h4><?php echo __("'.ucfirst(trim($coluna)).'"); ?></h4></td>
                        <td><?php echo $this->Html->link($this->PhpThumb->thumbnail("uploads/" . $'.$controller_singular.'
                            ["'.$controller_ucfirst_singular.'"]["'.trim($coluna).'"], array("w" => 300, "h" => 250, "zc" => 1)), 
                                "/uploads/" . $'.$controller_singular.'["'.$controller_ucfirst_singular.'"]["'.trim($coluna).'"], 
                                array("escape" => false, "class" => "fancybox")); ?></td></tr>';
                }
            }
        }
        $colunas_add_edit = implode("\n", $colunas_add_edit);
        $colunas_view = implode("\n", $colunas_view);
        $colunas_index_upper = implode("\n", $colunas_index_upper);
        $colunas_index_down = implode("\n", $colunas_index_down);

        $search = array("{'VARIAVEL_PLURALIZE'}", "{'VARIAVEL_SINGULARIZE'}", "{'VARIAVEL_UCFIRST_PLURALIZE'}", 
            "{'VARIAVEL_UCFIRST_SINGULAR'}", "{'VARIAVELS_COLUMNS_ADD_EDIT'}", "{'VARIAVELS_COLUMNS_VIEW'}",
            "{'VARIAVELS_COLUMNS_INDEX_UPPER'}", "{'VARIAVELS_COLUMNS_INDEX_DOWN'}", "{'VARIAVELS_EDIT_IMAGE'}");
        $replace = array($controller_pluralize, $controller_singular, $controller_ucfirst_pluralize, 
            $controller_ucfirst_singular, $colunas_add_edit, $colunas_view, $colunas_index_upper, $colunas_index_down,
            $edit_image);
        
        
        if ($data['Submenu']['file'] == 1) {
            $model_model = file_get_contents($automate_docs . 'model.txt');
            $model_model = str_replace($search, $replace, $model_model);
            if (!file_exists($model_path . $controller_ucfirst_singular . '.php')) {
                file_put_contents($model_path . $controller_ucfirst_singular . '.php', $model_model);
            }
            $controller_model = file_get_contents($automate_docs . 'controller_file.txt');
        } else {
            $controller_model = file_get_contents($automate_docs . 'controller.txt');
        }
        
        $controller_model = str_replace($search, $replace, $controller_model);
        if (!file_exists($controller_path . $controller_ucfirst_pluralize . 'Controller.php')) {
            file_put_contents($controller_path . $controller_ucfirst_pluralize . 'Controller.php', $controller_model);
        }
        
        if(!file_exists($view_path . $controller_ucfirst_pluralize)) {
            mkdir($view_path . $controller_ucfirst_pluralize, 775);
        }
        
        $view_add_model = file_get_contents($automate_docs . 'add.txt');
        $view_add_model = str_replace($search, $replace, $view_add_model);
        if(!file_exists($view_path . $controller_ucfirst_pluralize . DIRECTORY_SEPARATOR . 'admin_add.ctp')) {
            file_put_contents($view_path . $controller_ucfirst_pluralize . DIRECTORY_SEPARATOR . 'admin_add.ctp', $view_add_model);
        }
        
        
        $view_edit_model = file_get_contents($automate_docs . 'edit.txt');
        $view_edit_model = str_replace($search, $replace, $view_edit_model);
        if(!file_exists($view_path . $controller_ucfirst_pluralize . DIRECTORY_SEPARATOR . 'admin_edit.ctp')) {
            file_put_contents($view_path . $controller_ucfirst_pluralize . DIRECTORY_SEPARATOR . 'admin_edit.ctp', $view_edit_model);
        }
        
        $view_index_model = file_get_contents($automate_docs . 'index.txt');
        $view_index_model = str_replace($search, $replace, $view_index_model);
        if(!file_exists($view_path . $controller_ucfirst_pluralize . DIRECTORY_SEPARATOR . 'admin_index.ctp')) {
            file_put_contents($view_path . $controller_ucfirst_pluralize . DIRECTORY_SEPARATOR . 'admin_index.ctp', $view_index_model);
        }
        
        $view_view_model = file_get_contents($automate_docs . 'view.txt');
        $view_view_model = str_replace($search, $replace, $view_view_model);
        if(!file_exists($view_path . $controller_ucfirst_pluralize . DIRECTORY_SEPARATOR . 'admin_view.ctp')) {
            file_put_contents($view_path . $controller_ucfirst_pluralize . DIRECTORY_SEPARATOR . 'admin_view.ctp', $view_view_model);
        }
        
        unset($data['Submenu']['file']);
        unset($data['Submenu']['columns']);
        
        return $data;
    }
}

?>
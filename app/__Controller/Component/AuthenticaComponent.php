<?php

class AuthenticaComponent extends Component {

    var $components = array('Session', 'RequestHandler');
    var $login = array('controller' => 'users', 'action' => 'login', 'admin' => false);
    var $timeout = '600000';
    var $denied = array('action' => 'denied', 'controller' => 'pages');

    function initialize($controller) {
        
    }

    function __construct(ComponentCollection $collection, $settings = array()) {
        parent::__construct($collection, $settings);
    }

    function startup(&$controller = null) {
        
    }

    function beforeFilter($controller) {
        $path = $controller->request->params;
        if (in_array(ucfirst($path['controller']) . 'Controller', $this->getControllers())) {
            if ($path['controller'] != $this->login['controller'] && $path['action'] != $this->login['action']) {
                $this->setPreviousUrl($path);
            }
        }

        $tm = $this->timeout;
        if ($tm && $this->isLogged()) {
            $ts = $this->Session->read('Logged.timestamp');
            if ((time() - $ts) > $tm) {
                $this->logout();
                $this->Session->setFlash(__('Sua sessão expirou, faça login novamente!'), 'warning');
                $controller->redirect($this->login);
            }
            $this->setTimestamp();
        }
        if (!$this->isAllowed($path)) {
            if ($this->isLogged()) {
                $controller->redirect($this->denied);
            } else {
                //$this->Session->setFlash(sprintf(__('Efetue o login para obter acesso ao sistema')), 'warning');
                $controller->redirect($this->login);
            }
            $this->_flashmessage = '';
        }
    }

    /**
     * API functions
     */
    function setPreviousUrl($url) {
        $redirect = array('controller' => $url['controller'], 'action' => $url['action'], 'plugin' => $url['plugin']);
        if (isset($url['admin'])) {
            $redirect = array_merge($redirect, array('admin' => true));
        }
        if (count($url['pass']) > 0) {
            $redirect = array_merge($redirect, $url['pass']);
        }
        $this->Session->write('Authentica.previousUrl', $redirect);
    }

    function getPreviousUrl() {
        return $this->Session->read('Authentica.previousUrl');
    }

    function isLogged() {
        return ($this->getUserId() !== null);
    }

    function getLogin() {
        return $this->Session->read('Logged.User.login');
    }

    function getUserId() {
        return $this->Session->read('Logged.User.id');
    }

    function getUserEmail() {
        return $this->Session->read('Logged.Profile.email');
    }

    function getGroup() {
        return $this->Session->read('Logged.Group');
    }

    function getGroupId() {
        return $this->Session->read('Logged.User.group_id');
    }

    function getNome() {
        return $this->Session->read('Logged.User.name');
    }

    function setTimestamp() {
        $ts = $this->Session->write('Logged.timestamp', time());
    }

    function login($userdata) {

        $sessao = Array();
        $sessao['User'] = array(
            'id' => $userdata['User']['id'],
            'login' => $userdata['User']['login'],
            'last_login' => $userdata['User']['last_login'],
            'group_id' => $userdata['User']['group_id'],
            'name' => $userdata['User']['name']
        );
        $sessao['Group'] = $userdata['Group'];
        $this->Session->write('Logged', $sessao);
        $this->setTimestamp();
    }

    function logout() {
        $this->Session->delete('Logged');
        $this->Session->delete('Authentica');
    }

    function getRules($id) {
        App::import("Model", "Group");
        $group = new Group;
        $regra = $group->read(null, $id);
        $regras = json_decode($regra['Group']['rules']);
        $rules = array();
        if (!empty($regras)) {
            foreach ($regras as $contr => $acts) {
                foreach ($acts as $act) {
                    $rules[] = $contr . ':' . $act;
                }
            }
        }
        return $rules;
    }

    function isAllowed($url) {
        //pre($this->Session->read());
        if (!isset($url['admin'])) {
            return true;
        }
        if ($this->isLogged()) {
            $rulesGrupo = $this->getRules($this->getGroupId());
        }
        if ($this->isLogged()) {
            if ($this->getGroupId() == 1) {
                return true;
            }
            if (in_array(ucfirst($url['controller']) . ':' . $url['action'], $rulesGrupo)) {
                return true;
            }
        }
        return false;
    }

    /**
     * getControllerActions method
     *
     * @return Array
     */
    public function getControllerActions() {
        $controllerClass = App::objects('controller');
        $parentActions = array();
        foreach (get_class_methods('AppController') as $valor) {
            $pos = strripos($valor, 'admin_');
            if ($pos === false) {
                $parentActions[] = $valor;
            }
        }
        foreach ($controllerClass as $controller) {
            $actions = array();
            if ($controller != 'AppController') {
                $className = str_replace('Controller', '', $controller);
                App::import('Controller', $className);
                $this->$className = new $controller;
                foreach (get_class_methods($this->$className) as $valor) {
                    $pos = strripos($valor, 'admin_');
                    if ($pos !== false) {
                        $actions[] = $valor;
                    }
                }
                $controllers[$className] = array_diff($actions, $parentActions);
            }
        }
        return $controllers;
    }

    /**
     * getControllers method
     *
     * @return Array
     */
    private function getControllers() {
        return App::objects('controller');
    }

}

?>
<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    var $helpers = array('Form', 'Time', 'Html', 'Session', 'Js', 'Text', 'Number', 'M3', 'PhpThumb', 'FilterResults.FilterForm', 'ExtensionHtml');
    var $uses = array('Setting', 'Menu', 'Submenu', 'Album', 'User', 'Group', 'Profile', 'Photo', 'TableFiles', 'Crop', 'Example','Subcategory','Category','Parentcategory','Newsletter');
    var $components = array('Automate', 'Session', 'RequestHandler', 'Authentica', 'Analytics');
    var $counter = 0;

    function beforeFilter() {
        $this->set('itensmenu', $this->Menu->find('all', array('order' => 'Menu.title asc')));
        $this->set('categories', $this->Category->find('all'));
        $this->authentica();
        $this->settings();
        if ($this->Session->read('Settings.site_title.active') == 1) {
            $this->set('title_for_layout', $this->Session->read('Settings.site_title.value'));
        }
        if ($this->Session->read('Settings.base_url.active') == 1) {
            $this->set('base_url', $this->Session->read('Settings.base_url.value'));
        }


        if ( $this->Session->read('Logged.Group.id') ){
            $groupUserRules = $this->Group->read( null, $this->Session->read('Logged.Group.id') );
            $adminLevel = $this->Session->read('Logged.Group.id');
            $this->set( compact( 'groupUserRules','adminLevel' ) );
        }
    }

    private function authentica() {
        $this->Authentica->beforeFilter($this);
    }

    private function settings() {
        if (!$this->Session->read('Settings')) {
            $settings = $this->Setting->find('all');
            foreach ($settings as $setting) {
                $this->Session->write('Settings.' . $setting['Setting']['name'] . '.value', $setting['Setting']['value']);
                $this->Session->write('Settings.' . $setting['Setting']['name'] . '.active', $setting['Setting']['active']);
            }
        }
    }

    function admin_adicionarAlbum($id) {
        $objName = ucfirst(Inflector::singularize($this->params['controller']));
        $verificacao = $this->$objName->read(null, $id);
        if (!isset($verificacao[$objName]['album_id'])) {
            $this->Album->create(true);
            $this->request->data['Album']['title'] = date('d/m/Y');
            if ($this->Album->save($this->request->data)) {
                $this->request->data[$objName]['id'] = $id;
                $this->request->data[$objName]['album_id'] = $this->Album->id;
                $this->$objName->save($this->request->data);
                $this->redirect(array('controller' => 'photos', 'action' => 'add', $this->Album->id));
            } else {
                $this->Session->setFlash(__('Não foi possível criar um album, tente novamente', true), 'error');
                $this->redirect(array('controller' => $this->params['controller'], 'action' => 'index'));
            }
        } else {
            $this->redirect(array('controller' => 'photos', 'action' => 'add', $verificacao[$objName]['album_id']));
        }
    }

    function beforeRender() {
        
    }

    function convertDate($data, $hora = false) {
        if (strpos($data, '-')) {
            $data2 = explode(' ', $data);
            $d = explode("-", $data2[0]);
            $retorno = $d[2] . '/' . $d[1] . '/' . $d[0];
            if ($hora) {
                $retorno .= ' ' . $data2[1];
            }
            return $retorno;
        } else if (strpos($data, '/')) {
            $data2 = explode(' ', $data);
            $d = explode("/", $data2[0]);
            $retorno = $d[2] . '-' . $d[1] . '-' . $d[0];
            if ($hora) {
                $retorno .= ' ' . $data2[1];
            }
            return $retorno;
        }
    }

    function admin_changeState($id, $model) {
        $this->autoRender = false;
        $obj = $this->$model->read(null, $id);
        $this->$model->id = $id;
        if ($obj[$model]['active'] == 1) {
            $this->$model->saveField('active', 0);
            return 0;
        } else {
            $this->$model->saveField('active', 1);
            return 1;
        }
    }

/**
 * admin_allActionsListData method
 * recebe um post da view admin_index dos registros marcados com checkbox para desativar/ativar/deletar em massa
 * @return void
 * @author wescley matos
 **/
	public function admin_allActionsListData() {
		$modelName = key($this->request->data);
        $action = $this->request->data[$modelName]['action'];
        $arrayIds = $this->request->data[$modelName]['id'];

        switch ($action) {
            case 'delete':
                foreach ($arrayIds as $key => $id) {
                    if($id == 0) {
                        unset($arrayIds[$key]);
                        continue;
                    }
                    $this->$modelName->delete($id);
                }
                $this->Session->setFlash(__('Registro(s) deletado(s) com sucesso.'), 'sucess');
                break;

            case 'activate':
                foreach ($arrayIds as $key => $id) {
                    if($id == 0) {
                        unset($arrayIds[$key]);
                        continue;
                    }
                    $result = $this->$modelName->read(array('id', 'active'), $id);
                    if ($result[$modelName]['active'] == 0) {
                        $this->$modelName->set('active', 1);
                        $this->$modelName->save();
                        $this->Session->setFlash(__('Registro(s) ativado(s) com sucesso.'), 'sucess');
                    }
                }
                break;

            case 'deactivate':
                foreach ($arrayIds as $key => $id) {
                    if($id == 0) {
                        unset($arrayIds[$key]);
                        continue;
                    }
                    $result = $this->$modelName->read(array('id', 'active'), $id);
                    if ($result[$modelName]['active'] == 1) {
                        $this->$modelName->set('active', 0);
                        $this->$modelName->save();
						$this->Session->setFlash(__('Registro(s) desativado(s) com sucesso.'), 'sucess');
					}
				}
				break;
		}
        $this->redirect($this->referer());
	}

}

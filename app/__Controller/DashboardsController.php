<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppController', 'Controller');
App::import('Vendor', 'gapi/gapi');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class DashboardsController extends AppController {

	public $name = 'Dashboards';

	public $helpers = array('Html', 'Session');

	public $uses = array();

	private $__mes = array(
		'01' => 'Janeiro',
		'02' => 'Fevereiro',
		'03' => 'Março',
		'04' => 'Abril',
		'05' => 'Maio',
		'06' => 'Junho',
		'07' => 'Julho',
		'08' => 'Agosto',
		'09' => 'Setembro',
		'10' => 'Outubro',
		'11' => 'Novembro',
		'12' => 'Dezembro'
	);

	public function admin_index() {
		
	}

	public function admin_analytics() {
		$dataInicioMesAnterior = date('Y-m-d', mktime(0, 0, 0, date("m") - 1, 1, date("Y")));
		$dataFimMesAnterior = mktime(0, 0, 0, date("m"), 1, date("Y"));
		$dataFimMesAnterior = date('Y-m-d', $dataFimMesAnterior - 1);
		$dataInicioMesAtual = date('Y-m-d', mktime(0, 0, 0, date("m"), 1, date("Y")));
		$dataFimMesAtual = date('Y-m-d');

		try{
			$ga = new gapi($this->Session->read('Settings.email_analytics.value'), $this->Session->read('Settings.senha_analytics.value'));
			$id = $this->Session->read('Settings.id_analytics.value');

			$ga->requestReportData($id, 'month', array('pageviews', 'visits', 'uniquePageviews', 'exitRate', 'visitors', 'percentNewVisits', 'entranceBounceRate', 'pageviewsPerVisit', 'avgTimeOnSite'), null, null, $dataInicioMesAnterior, $dataFimMesAnterior);
			foreach ($ga->getResults() as $dados) {
				$mes = $this->__mes[$dados[1]['month']];
				$analytics['anterior']['mes'] = $mes;
				$analytics['anterior']['visits'] = $dados[0]['visits'];
				$analytics['anterior']['pageviews'] = $dados[0]['pageviews'];
				$analytics['anterior']['visitors'] = $dados[0]['visitors'];
				$analytics['anterior']['pageviewsPerVisit'] = $dados[0]['pageviews'];
				$analytics['anterior']['entranceBounceRate'] = $dados[0]['entranceBounceRate'];
				$analytics['anterior']['avgTimeOnSite'] = $dados[0]['avgTimeOnSite'];
				$analytics['anterior']['percentNewVisits'] = $dados[0]['percentNewVisits'];
			}

			/************************/
			$ga->requestReportData($id, 'country', array('visits'), null, null, $dataInicioMesAnterior, $dataFimMesAnterior);

			$key = 0;
			foreach ($ga->getResults() as $dados) {
				$analyticsWorld[$key]['visits'] = $dados[0]['visits'];
				$analyticsWorld[$key]['country'] = $dados[1]['country'];
				$key++;
			}
			/************************/
			$ga->requestReportData($id, 'isMobile', array('visits'), null, null, $dataInicioMesAnterior, $dataFimMesAnterior);
			$key2 = 0;
			foreach ($ga->getResults() as $dados) {
				$analyticsMobile[$key2]['visits'] = $dados[0]['visits'];
				$dados[1]['isMobile'] == "Yes" ? $analyticsMobile[$key2]['isMobile'] = "Mobile" : $analyticsMobile[$key2]['isMobile'] = "Outras Fontes";
				$key2++;
			}
			/************************/
			$ga->requestReportData($id, 'month', array('pageviews', 'visits', 'uniquePageviews', 'exitRate', 'visitors', 'percentNewVisits', 'entranceBounceRate', 'pageviewsPerVisit', 'avgTimeOnSite'), null, null, $dataInicioMesAtual, $dataFimMesAtual);
			foreach ($ga->getResults() as $dados) {
				$mes = $this->__mes[$dados[1]['month']];

				$analytics['atual']['mes'] = $mes;
				$analytics['atual']['visits'] = $dados[0]['visits'];
				$analytics['atual']['pageviews'] = $dados[0]['pageviews'];
				$analytics['atual']['visitors'] = $dados[0]['visitors'];
				$analytics['atual']['pageviewsPerVisit'] = $dados[0]['pageviews'];
				$analytics['atual']['entranceBounceRate'] = $dados[0]['entranceBounceRate'];
				$analytics['atual']['avgTimeOnSite'] = $dados[0]['avgTimeOnSite'];
				$analytics['atual']['percentNewVisits'] = $dados[0]['percentNewVisits'];
			}

			$ga->requestReportData($id, 'nthWeek', array('pageviews', 'visits'), array('nthWeek'), null, $dataInicioMesAnterior, $dataFimMesAnterior, 1, 50);
			$grafAnalitycs[] = $ga->getResults();
			$this->set("grafAnalitycs", $grafAnalitycs);
			$this->set("analyticsWorld", $analyticsWorld);
			$this->set("analyticsMobile", $analyticsMobile);
			$this->set("analytics", $analytics);

		} catch(Exception $e) {
			$analyticsError = $e->getMessage();
			$this->Session->setFlash( "Não foi possível conectar ao Google Analitycs. Dados atualmente indisponíveis.");
			$this->set("analytics", 0);
			$this->set("grafAnalitycs", 0);
		}

		$this->set('itensmenu', $this->Menu->find('all'));
	}
      
}
<?php
App::uses('AppController', 'Controller');
/**
 * Accommodations Controller
 *
 * @property Accommodation $Accommodation
 */
class AccommodationsController extends AppController {



/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Accommodation->recursive = 0;
		$this->set('accommodations', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Accommodation->id = $id;
		if (!$this->Accommodation->exists()) {
			throw new NotFoundException(__('Invalid accommodation'));
		}
		$this->set('accommodation', $this->Accommodation->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Accommodation->create();
			if ($this->Accommodation->save($this->request->data)) {
				$this->Session->setFlash(__('The accommodation has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The accommodation could not be saved. Please, try again.'), 'error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Accommodation->id = $id;
		if (!$this->Accommodation->exists()) {
			throw new NotFoundException(__('Invalid accommodation'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Accommodation->save($this->request->data)) {
				$this->Session->setFlash(__('The accommodation has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The accommodation could not be saved. Please, try again.'), 'error');
			}
		} else {
			$this->request->data = $this->Accommodation->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Accommodation->id = $id;
		if (!$this->Accommodation->exists()) {
			throw new NotFoundException(__('Invalid accommodation'));
		}
		if ($this->Accommodation->delete()) {
			$this->Session->setFlash(__('Accommodation deleted'), 'sucess');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Accommodation was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}
}

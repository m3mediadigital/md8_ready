<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Category $Category
 */
class CategoriesController extends AppController {


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Category->recursive = 0;
		$this->set('categories', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		$this->set('category', $this->Category->read(null, $id));
		$subcategories = $this->Subcategory->find( 'list', array( 'fields' => array( 'id', 'subcategory' ), 'conditions' => array( 'Subcategory.category_id' => $id ) ) );
		$this->set(compact('subcategories'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Category->create();
			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'), 'error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'), 'error');
			}
		} else {
			$this->request->data = $this->Category->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {

		$catgory = $this->Category->read( null, $id );

		if ( !empty($catgory['Product']) ) {
			$this->Session->setFlash(__('Ha um(s) produto(s) relacionado(s) á esta categoria, è necessario remover esta realação para remover á mesma!'), 'warning');
			$this->redirect(array('action' => 'index'));			
		} else if ( !empty($catgory['Subcategory']) ) {
			$this->Session->setFlash(__('Ha uma(s) sub categoria(s) relacionada(s) á esta categoria, è necessario remover esta realação para remover á mesma!'), 'warning');
			$this->redirect(array('action' => 'index'));			
		} else {

			$this->Category->id = $id;		
			if ($this->Category->delete()) {
				$this->Session->setFlash(__('Category deleted'), 'sucess');
				$this->redirect(array('action' => 'index'));
			}
			$this->Session->setFlash(__('Category was not deleted'), 'error');
			$this->redirect(array('action' => 'index'));
			
		}

		
	}
}

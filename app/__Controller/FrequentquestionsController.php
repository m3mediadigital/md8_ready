<?php
App::uses('AppController', 'Controller');
/**
 * Frequentquestions Controller
 *
 * @property Frequentquestion $Frequentquestion
 */
class FrequentquestionsController extends AppController {


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Frequentquestion->recursive = 0;
		$this->set('frequentquestions', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Frequentquestion->id = $id;
		if (!$this->Frequentquestion->exists()) {
			throw new NotFoundException(__('Invalid frequentquestion'));
		}
		$this->set('frequentquestion', $this->Frequentquestion->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Frequentquestion->create();
			if ($this->Frequentquestion->save($this->request->data)) {
				$this->Session->setFlash(__('The frequentquestion has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The frequentquestion could not be saved. Please, try again.'), 'error');
			}
		}
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Frequentquestion->id = $id;
		if (!$this->Frequentquestion->exists()) {
			throw new NotFoundException(__('Invalid frequentquestion'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Frequentquestion->save($this->request->data)) {
				$this->Session->setFlash(__('The frequentquestion has been saved'), 'sucess');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The frequentquestion could not be saved. Please, try again.'), 'error');
			}
		} else {
			$this->request->data = $this->Frequentquestion->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Frequentquestion->id = $id;
		if (!$this->Frequentquestion->exists()) {
			throw new NotFoundException(__('Invalid frequentquestion'));
		}
		if ($this->Frequentquestion->delete()) {
			$this->Session->setFlash(__('Frequentquestion deleted'), 'sucess');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Frequentquestion was not deleted'), 'error');
		$this->redirect(array('action' => 'index'));
	}
}

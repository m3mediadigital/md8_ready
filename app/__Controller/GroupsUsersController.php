<?php

App::uses('AppController', 'Controller');

/**
 * GroupsUsers Controller
 *
 * @property GroupsUser $GroupsUser
 */
class GroupsUsersController extends AppController {

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->GroupsUser->recursive = 0;
        $this->set('groupsUsers', $this->paginate());
    }

    /**
     * admin_view method
     *
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->GroupsUser->id = $id;
        if (!$this->GroupsUser->exists()) {
            throw new NotFoundException(__('Invalid groups user'));
        }
        $this->set('groupsUser', $this->GroupsUser->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->GroupsUser->create();
            if ($this->GroupsUser->save($this->request->data)) {
                $this->Session->setFlash(__('The groups user has been saved'), 'sucess');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The groups user could not be saved. Please, try again.'), 'error');
            }
        }
        $users = $this->GroupsUser->User->find('list');
        $groups = $this->GroupsUser->Group->find('list');
        $this->set(compact('users', 'groups'));
    }

    /**
     * admin_edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->GroupsUser->id = $id;
        if (!$this->GroupsUser->exists()) {
            throw new NotFoundException(__('Invalid groups user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->GroupsUser->save($this->request->data)) {
                $this->Session->setFlash(__('The groups user has been saved'), 'sucess');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The groups user could not be saved. Please, try again.'), 'error');
            }
        } else {
            $this->request->data = $this->GroupsUser->read(null, $id);
        }
        $users = $this->GroupsUser->User->find('list');
        $groups = $this->GroupsUser->Group->find('list');
        $this->set(compact('users', 'groups'));
    }

    /**
     * admin_delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->GroupsUser->id = $id;
        if (!$this->GroupsUser->exists()) {
            throw new NotFoundException(__('Invalid groups user'));
        }
        if ($this->GroupsUser->delete()) {
            $this->Session->setFlash(__('Groups user deleted'), 'sucess');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Groups user was not deleted'), 'error');
        $this->redirect(array('action' => 'index'));
    }

}

jQuery(function($){
    
    $('.galleryUp').click(function(e) {
        e.preventDefault();
        var count = $('div.insideGallery ul li').length;
        
        if (count > 15) {
            for (i = 0; i < 15; i++) {
                  $('div.insideGallery ul li.erased').last().prependTo($('div.insideGallery ul')).addClass('nonerased').removeClass('erased');
                  $('div.insideGallery ul li.nonerased').last().removeClass('nonerased').addClass('erased');
            }
        }
    });
    
    $('.galleryDown').click(function(e) {
        e.preventDefault();
        var count = $('div.insideGallery ul li').length;
        
        if (count > 15) {
            for (i = 0; i < 15; i++) {
                  $('div.insideGallery ul li.erased').first().addClass('nonerased').removeClass('erased');
                  $('div.insideGallery ul li.nonerased').first().appendTo($('div.insideGallery ul')).removeClass('nonerased').addClass('erased');
            }
        }
    });
    
    $('.deleteButtonRow').click(function(e) {
       e.preventDefault();
       var id = $(this).attr('rel');
       $('input[value='+id+']').attr('checked', 'checked');
       $('.bt-deletar.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only').trigger('click');
    });
    
    
    $('[placeholder]').focus(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
            input.val('');
            input.removeClass('placeholder');
        }
    }).blur(function() {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.addClass('placeholder');
            input.val(input.attr('placeholder'));
        }
    }).blur();
    

    // Jquery UI com draggable e droppable para anexação e 'desanexação' de arquivos
    $( ".draggable" ).draggable({ revert: "invalid", scope: "items", helper: "clone" }); // Draggable
    
     $('#droppable, #droppable2').droppable({
        scope: "items",
        drop: function(e, ui) {
            var $drop = $(this);
            var classe = $(this).attr('id');
            var data = new Date();
            var horario = data.getTime();
            var baseUrl = ui.draggable.attr("baseUrl")+'&type='+classe+'&rel='+horario;
            ui.draggable.append('<div class="loading" id="'+horario+'"></div>');
            $(window).load(baseUrl);
            $(ui.draggable).draggable().prependTo($drop);
            
            if (classe == 'droppable') {
                $('div.insideGallery ul li.erased').first().removeClass('erased').addClass('nonerased');
            } else {
                $('div.insideGallery ul li.nonerased').last().addClass('erased').removeClass('nonerased');
                $('div.insideGallery ul li').first().addClass('nonerased');
            }
        }
     });
     // Fim da Anexação! HA!


    $('#tableaction').change(function(){
        $('#formList').submit();
    });

    $('#fileupload').fileupload({
        autoUpload: true,
        progressall: function (e, data) {
            $(this).find('.fileupload-progressbar div').css(
                'width',
                parseInt(data.loaded / data.total * 100, 10) + '%'
                );
            if(data.loaded == data.total){
                setTimeout(function(){
                    location.reload(true);
                }, 1500);
            }
        }
    });
    $('.color').ColorPicker({
        color: '#0000ff',
        onShow: function (colpkr) {
            $(colpkr).fadeIn(500);
            return false;
        },
        onHide: function (colpkr) {
            $(colpkr).fadeOut(500);
            return false;
        },
        onChange: function (hsb, hex, rgb) {
            $('.color').css('backgroundColor', '#' + hex);
            $('.color').val('#' + hex);
        }
    });
    
    $("a.fancybox").fancybox({
        'titleShow'     : false,
        'transitionIn'	: 'elastic',
        'transitionOut'	: 'elastic',
        'easingIn'      : 'easeOutBack',
        'easingOut'     : 'easeInBack',
        'speedIn'	:	600, 
        'speedOut'	:	500
    });
    
    $(".money").maskMoney({
        showSymbol:false, 
        thousands:'', 
        decimal:'.', 
        symbolStay: true
    });
    
    $('.cpf').mask('999.999.999-99');
    $('.cep').mask('99.999-999');
    $('.data').mask('99/99/9999');
    $('.phone').mask('(99) 9999-9999');
    
    $('#settings').css('marginRight',-250);
    var toggled = false; 
    $('#settings a.settingbutton').click(function(){
        if(toggled){
            $('#settings').animate({
                marginRight:-$('#settings').width()
            },500);
        }else{
            $('#settings').animate({
                marginRight:0
            },500);
        }
        toggled = !toggled; 
        return false
    });
    
    $(".pagination span[class!='next'][class!='prev']").button({
        
        })
    $( ".bt-deletar" ).button({
        icons: {
            primary: "ui-icon-trash"
        },
        text: false
    })
    $( ".bt-alerta" ).button({
        icons: {
            primary: "ui-icon-alert"
        },
        text: false
    })
    $( ".bt-ativar" ).button({
        icons: {
            primary: "ui-icon-check"
        },
        text: false
    })
    $( ".bt-desativar" ).button({
        icons: {
            primary: "ui-icon-closethick"
        },
        text: false
    })
    $( ".bt-ver" ).button({
        icons: {
            primary: "ui-icon-search"
        },
        text: false
    })
    $( ".bt-recortar" ).button({
        icons: {
            primary: "ui-icon-scissors"
        },
        text: false
    })
    $( ".bt-anexar" ).button({
        icons: {
            primary: "ui-icon-disk"
        },
        text: false
    })
    $( ".bt-editar" ).button({
        icons: {
            primary: "ui-icon-pencil"
        },
        text: false
    })
    $( ".bt-esquerda" ).button({
        icons: {
            primary: "ui-icon-carat-1-w"
        },
        text: false
    })
    $( ".bt-direita" ).button({
        icons: {
            primary: "ui-icon-carat-1-e"
        },
        text: false
    })
    $( ".bt-foto" ).button({
        icons: {
            primary: "ui-icon-image"
        },
        text: false
    })
    $( ".bt-download" ).button({
        icons: {
            primary: "ui-icon-arrowthickstop-1-s"
        },
        text: false
    })
    $( ".bt-boletim" ).button({
        icons: {
            primary: "ui-icon-note"
        },
        text: false
    })
    
    $('textarea[class!=None]').addClass('ckeditor');
    
    $("select").chosen();
    
    /**
     * Slide toggle for blocs
     * */
    $('.bloc .title').append('<a href="#" class="toggle"></a>');
    $('.bloc .title .tabs').parent().find('.toggle').remove(); 
    $('.bloc .title .toggle').click(function(){
        $(this).toggleClass('hide').parent().parent().find('.content').slideToggle(300);
        return false; 
    });
    
    /**
     * Tooltips on every links with a title not empty
     * http://tooltipsy.com/
     * */
    //$("a[title]").tooltipsy();
    
    /**
     * Animated Scroll for anchos
     * */
    $('a[href^="#"][href!="#"]').click(function() {
        cible=$(this).attr('href');
        if(cible=="#"){
            return false;
        }
        scrollTo(cible);
        return false;
    });

    
    /**
     * iPhone Checkboxes on every input with "iphone" class
     * http://awardwinningfjords.com/2009/06/16/iphone-style-checkboxes.html
     * Triggers error on IE... Disabled waiting for a new version
     * */
    if(!$.browser.msie){
        $('.iphone').iphoneStyle({
            checkedLabel: 'YES', 
            uncheckedLabel: 'NO'
        });
    }
    
      
    /**
     * Jquery UI 
     * Automate jQuery UI insertion (no need to add more code)(and unfirm)
     * input.datepicker become a datepicker
     * input.range become a slider (value is inserted in the input) 
    **/
    $(".input input:checkbox[class != none], input:radio, input:file").uniform();
    $('.rules.input input[name = checkAll]').change(function(){
        $(this).parent().parent().find('input').attr('checked', $(this).is(':checked')); 
    })
   
    $(".datepicker").datepicker({
        dateFormat: 'dd/mm/yy',
                
        dayNames: [
        'Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'
        ],
        dayNamesMin: [
        'D','S','T','Q','Q','S','S','D'
        ],
        dayNamesShort: [
        'Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'
        ],
        monthNames: [
        'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro',
        'Outubro','Novembro','Dezembro'
        ],
        monthNamesShort: [
        'Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set',
        'Out','Nov','Dez'
        ],
        nextText: 'Próximo',
        prevText: 'Anterior'

    });
    
    $('.range').each(function(){
        var cls = $(this).attr('class'); 
        var matches = cls.split(/([a-zA-Z]+)\-([0-9]+)/g);
        var options = {
            animate : true
        };
        var elem = $(this).parent(); 
        elem.append('<div class="uirange"></div>'); 
        for (i in matches) {
            i = i*1; 
            if(matches[i] == 'max'){
                options.max = matches[i+1] * 1
            }
            if(matches[i] == 'min'){
                options.min = matches[i+1] * 1
            }
        }
        options.slide = function(event,ui){
            elem.find('span:first').empty().append(ui.value);
            elem.find('input:first').val(ui.value); 
        }
        elem.find('span:first').empty().append(elem.find('input:first').val());
        options.range = 'min';
        options.value = elem.find('input:first').val();
        elem.find('.uirange').slider(options);
        $(this).hide(); 
    });

   
    /**
     * Autohide errors when an input with error is focused
     * */
    $('.input.error input,.input textarea,.input select').focus(function(){
        $(this).parent().removeClass('error'); 
        $(this).parent().find('.error-message').fadeTo(500,0).slideUp(); 
        $(this).unbind('focus'); 
    });
    
    /**
     * Hide notification when close button is pressed
    **/
    $('.notif .close').click(function(){
        $(this).parent().fadeTo(500,0).slideUp(); 
        return false; 
    });
    
    /**
     * Tabs 
     */
    /*
    var anchor = window.location.hash;  // On rÃ©cup l'ancre dans l'url http://......#ancre
    $('.tabs').each(function(){
        var current = null;             // Permet de connaitre l'Ã©lÃ©ment courant
        var id = $(this).attr('id');    // ID de ma barre d'onglet
        // Si on a une ancre
        if(anchor != '' && $(this).find('a[href="'+anchor+'"]').length > 0){
            current = anchor;
        // Si on a une valeur de cookie
        }else if($.cookie('tab'+id) && $(this).find('a[href="'+$.cookie('tab'+id)+'"]').length > 0){
            current = $.cookie('tab'+id);
        // Sinon current = premier lien
        }else{
            current = $(this).find('a:first').attr('href');
        }
        
        $(this).find('a[href="'+current+'"]').addClass('active');   // On ajoute la classe active sur le lien qui correspond
        $(current).siblings().hide();                               // On masque les Ã©lÃ©ments
        $(this).find('a').click(function(){
           var link = $(this).attr('href'); 
           // On a cliquÃ© sur l'Ã©lÃ©ment dÃ©ja active
           if(link == current){
               return false;
           }else{
               // On ajoute la class active sur l'onglet courant et on la supprime des autres onglets
               $(this).addClass('active').siblings().removeClass('active'); 
               $(link).show().siblings().hide();    // On masque/affiche les div suivant les cas
               current = link;                      // On change la valeur de l'onglet courant
               //$.cookie('tab'+id,current);          // On stocke l'onglet courant dans les cookie
           }
        });
    });
    */
   
    $('.tabs a').click(function(){
        $('.tabs a').removeClass('active');
        $(this).addClass('active');
        $('.tabContent').removeClass('active');
        //alert($(this).attr('rel'));
        $('.tabContent[rel='+$(this).attr('rel')+']').addClass('active');
    });
    
    /**
     * CheckAll, if the checkbox with checkall class is checked/unchecked all checkbox would be checked
     * */
    $('#content .checkall').change(function(){
        $(this).parents('table:first').find('input').attr('checked', $(this).is(':checked')); 
    });
    
    /** 
     * Sidebar menus
     * Slidetoggle for menu list
     * */
    var currentMenu = null; 
    $('#sidebar>ul>li').each(function(){
        if($(this).find('li').length == 0){
            $(this).addClass('nosubmenu');
        }
    })
    $('#sidebar>ul>li:not([class*="current"])>ul').hide();
    $('#sidebar>ul>li:not([class*="nosubmenu"])>a').click(function(){
        e = $(this).parent();
        $('#sidebar>ul>li.current').removeClass('current').find('ul:first').slideUp();
        e.addClass('current').find('ul:first').slideDown();  
    });

    var htmlCollapse = $('#menucollapse').html(); 
    if($.cookie('isCollapsed') === 'true'){
        $('body').addClass('collapsed'); 
        $('#menucollapse').html('&#9654;');
    } 
    $('#menucollapse').click(function(){
        var body = $('body'); 
        body.toggleClass('collapsed');
        isCollapsed = body.hasClass('collapsed');
        if(isCollapsed){
            $(this).html('&#9654;');
        }else{
            $(this).html(htmlCollapse); 
        }
        $.cookie('isCollapsed',isCollapsed); 
        return false; 
    });


    /**
     * Fake Placeholder
     * User labels as placeholder for the next input
     * */
    $('.placeholder,#content.login .input').each(function(){
        var label = $(this).find('label:first');
        var input = $(this).find('input:first,textarea:first'); 
        if(input.val() != ''){
            label.stop().hide(); 
        }
        input.focus(function(){
            if($(this).val() == ''){
                label.stop().fadeTo(300,0.2);
            }
            $(this).parent().removeClass('error').find('.error-message').fadeOut(); 
        });
        input.blur(function(){
            if($(this).val() == ''){
                label.stop().fadeTo(300,1);  
            }
        });
        input.keypress(function(){
            label.stop().hide(); 
        });
        input.keyup(function(){
            if($(this).val() == ''){
                label.stop().fadeTo(300,0.2); 
            }
        });
        input.bind('cut copy paste', function(e) {
            label.stop().hide(); 
        });
    }); 
    
    $('.close').click(function(){
        $(this).parent().fadeTo(300,0).slideUp();
    });
    
    /** 
     * When window is resized
     * */
    $(window).resize(function(){
        /**
          * All "center" class block are centered
          * used for float left centering 
          * */
        $('.center').each(function(){
            $(this).css('display','inline'); 
            var width = $(this).width(); 
            if(parseInt($(this).height()) < 100){
                $(this).css({
                    width:'auto'
                }); 
            }else{
                $(this).css({
                    width:width
                }); 
            }
            $(this).css('display','block'); 
        }); 
         
        /**
          * Calendar sizing (all TD with same height
          * */
        $('.calendar td').height($('.calendar td[class!="padding"]').width());
    });
    
    $(window).trigger('resize'); 

    function scrollTo(cible){
        if($(cible).length>=1){
            hauteur=$(cible).offset().top;
        }else{
            return false;
        }
        hauteur -= (windowH()-$(cible).height())/2;
        $('html,body').animate({
            scrollTop: hauteur
        }, 1000,'easeOutQuint');
        return false;
    }    

    function windowH(){
        if (window.innerHeight) return window.innerHeight  ;
        else{
            return $(window).height();
        }
    }

});


/**
 * JQuery Easing for smooooooooth animation
 */
jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing,
{
    def: 'easeOutQuad',
    swing: function (x, t, b, c, d) {
        //alert(jQuery.easing.default);
        return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
    },
    easeInQuad: function (x, t, b, c, d) {
        return c*(t/=d)*t + b;
    },
    easeOutQuad: function (x, t, b, c, d) {
        return -c *(t/=d)*(t-2) + b;
    },
    easeInOutQuad: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t + b;
        return -c/2 * ((--t)*(t-2) - 1) + b;
    },
    easeInCubic: function (x, t, b, c, d) {
        return c*(t/=d)*t*t + b;
    },
    easeOutCubic: function (x, t, b, c, d) {
        return c*((t=t/d-1)*t*t + 1) + b;
    },
    easeInOutCubic: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t + b;
        return c/2*((t-=2)*t*t + 2) + b;
    },
    easeInQuart: function (x, t, b, c, d) {
        return c*(t/=d)*t*t*t + b;
    },
    easeOutQuart: function (x, t, b, c, d) {
        return -c * ((t=t/d-1)*t*t*t - 1) + b;
    },
    easeInOutQuart: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
        return -c/2 * ((t-=2)*t*t*t - 2) + b;
    },
    easeInQuint: function (x, t, b, c, d) {
        return c*(t/=d)*t*t*t*t + b;
    },
    easeOutQuint: function (x, t, b, c, d) {
        return c*((t=t/d-1)*t*t*t*t + 1) + b;
    },
    easeInOutQuint: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
        return c/2*((t-=2)*t*t*t*t + 2) + b;
    },
    easeInSine: function (x, t, b, c, d) {
        return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
    },
    easeOutSine: function (x, t, b, c, d) {
        return c * Math.sin(t/d * (Math.PI/2)) + b;
    },
    easeInOutSine: function (x, t, b, c, d) {
        return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
    },
    easeInExpo: function (x, t, b, c, d) {
        return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
    },
    easeOutExpo: function (x, t, b, c, d) {
        return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
    },
    easeInOutExpo: function (x, t, b, c, d) {
        if (t==0) return b;
        if (t==d) return b+c;
        if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
        return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
    },
    easeInCirc: function (x, t, b, c, d) {
        return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
    },
    easeOutCirc: function (x, t, b, c, d) {
        return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
    },
    easeInOutCirc: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
        return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
    },
    easeInElastic: function (x, t, b, c, d) {
        var s=1.70158;
        var p=0;
        var a=c;
        if (t==0) return b;
        if ((t/=d)==1) return b+c;
        if (!p) p=d*.3;
        if (a < Math.abs(c)) {
            a=c;
            var s=p/4;
        }
        else var s = p/(2*Math.PI) * Math.asin (c/a);
        return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
    },
    easeOutElastic: function (x, t, b, c, d) {
        var s=1.70158;
        var p=0;
        var a=c;
        if (t==0) return b;
        if ((t/=d)==1) return b+c;
        if (!p) p=d*.3;
        if (a < Math.abs(c)) {
            a=c;
            var s=p/4;
        }
        else var s = p/(2*Math.PI) * Math.asin (c/a);
        return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
    },
    easeInOutElastic: function (x, t, b, c, d) {
        var s=1.70158;
        var p=0;
        var a=c;
        if (t==0) return b;
        if ((t/=d/2)==2) return b+c;
        if (!p) p=d*(.3*1.5);
        if (a < Math.abs(c)) {
            a=c;
            var s=p/4;
        }
        else var s = p/(2*Math.PI) * Math.asin (c/a);
        if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
        return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
    },
    easeInBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c*(t/=d)*t*((s+1)*t - s) + b;
    },
    easeOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
    },
    easeInOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
        return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
    },
    easeInBounce: function (x, t, b, c, d) {
        return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
    },
    easeOutBounce: function (x, t, b, c, d) {
        if ((t/=d) < (1/2.75)) {
            return c*(7.5625*t*t) + b;
        } else if (t < (2/2.75)) {
            return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
        } else if (t < (2.5/2.75)) {
            return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
        } else {
            return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
        }
    },
    easeInOutBounce: function (x, t, b, c, d) {
        if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
        return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
    }
});
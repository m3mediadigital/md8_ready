$(document).ready(function(){
	
	$('.menuWS ul li a').hover(function(){
		$('ul', $(this).parent()).css( 'display', 'block' );
		$('.menuWS ul li ul').hover(function(){
			$('ul', $(this).parent()).css( 'display', 'block' );
		})
	})
	$('.menuWS ul li a').mouseout(function(){
		$('ul', $(this).parent()).css( 'display', 'none' );
		$('.menuWS ul li ul').hover(function(){
			$('ul', $(this).parent()).css( 'display', 'none' );
		})
	})
		
    $("a[rel^='prettyPhoto']").prettyPhoto();
        
    $(".datepicker").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: [
        'Domingo','Segunda','TerÃ§a','Quarta','Quinta','Sexta','SÃ¡bado','Domingo'
        ],
        dayNamesMin: [
        'D','S','T','Q','Q','S','S','D'
        ],
        dayNamesShort: [
        'Dom','Seg','Ter','Qua','Qui','Sex','SÃ¡b','Dom'
        ],
        monthNames: [
        'Janeiro','Fevereiro','MarÃ§o','Abril','Maio','Junho','Julho','Agosto','Setembro', 'Outubro','Novembro','Dezembro'
        ],
        monthNamesShort: [
        'Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set', 'Out','Nov','Dez'
        ],
        nextText: 'PrÃ³ximo',
        prevText: 'Anterior'
    });
	
	$(".phone").mask("(99) 9999-9999");

	function clickCloseMessage() {
	  $(".notif").slideUp('fast');
	  return false;
	}
    $('.notif').click(function(){
        clickCloseMessage();
        return false;
    });
    if($('.notif').size()) {
    	setTimeout(clickCloseMessage, 10000);
    }

    $('.Apagar').click(function(){
        if(this.value == this.defaultValue){
            this.value = '';
        }
    });
    $('.Apagar').blur(function(){
        if (this.value == ''){
            this.value = this.defaultValue;
        }
    });
	
	// SLIDER HOME
	$('.slider').cycle({ 
        fx:    'fade', 
        speed: 1000,
        timeout: 6000,
        prev: '.slideLeft',
        next: '.slideRight'
    });	
	
	// HOVER DO CARRINHO
	var timeout;
	$('#openCarrinho').click(function(){
	  var $this = $(this);
	  timeout = window.setTimeout(function(){
		  $this.mousedown();
	  }, 300);
	}, function(){
	  window.clearTimeout(timeout);
	});
	$('#openCarrinho').mousedown(function(){
		window.clearTimeout(timeout);
		var form = $('#compras');
		if(form.is(':visible'))
		{
			  $('#compras').hide(0);
			  $(this).removeClass('active');
		}
		else
		{
			  $(this).addClass('active');
			  $('#compras').show(0);
		}
		return false;
	});
	$(document).mousedown(function(){
		var form = $('#compras');
		if(form.is(':visible'))
		{
			  $('#compras').hide(0);
			  $('#openCarrinho').removeClass('active');
		}
	});
	$('#compras').mousedown(function(e){
		e.stopPropagation();
	});

	// HOVER DA BUSCA
	var timeout;
	$('#openBusca').click(function(){
	  var $this = $(this);
	  timeout = window.setTimeout(function(){
		  $this.mousedown();
	  }, 300);
	}, function(){
	  window.clearTimeout(timeout);
	});
	$('#openBusca').mousedown(function(){
		window.clearTimeout(timeout);
		var form = $('#busca');
		if(form.is(':visible'))
		{
			  $('#busca').hide(0);
			  $(this).removeClass('active');
		}
		else
		{
			  $(this).addClass('active');
			  $('#busca').show(0);
		}
		return false;
	});
	$(document).mousedown(function(){
		var form = $('#busca');
		if(form.is(':visible'))
		{
			  $('#busca').hide(0);
			  $('#openBusca').removeClass('active');
		}
	});
	$('#busca').mousedown(function(e){
		e.stopPropagation();
	});

	// TABS
	(function() {

		var $tabsNav    = $('.tabs-nav'),
			$tabsNavLis = $tabsNav.children('li'),
			$tabContent = $('.tab-content');

		$tabsNav.each(function() {
			var $this = $(this);

			$this.next().children('.tab-content').stop(true,true).hide()
												 .first().show();

			$this.children('li').first().addClass('active').stop(true,true).show();
		});

		$tabsNavLis.on('click', function(e) {
			var $this = $(this);

			$this.siblings().removeClass('active').end()
				 .addClass('active');
			
			$this.parent().next().children('.tab-content').stop(true,true).hide()
														  .siblings( $this.find('a').attr('href') ).fadeIn();

			e.preventDefault();
		});

	})();

	//	ACCORDION - MENU CATEGORIAS
	/*
	(function() {

		var $container = $('#sidebar ul ul'),
			$trigger   = $('#sidebar ul li');

		$container.hide();
		$trigger.next().hide();

		var fullWidth = $container.outerWidth(true);
		$trigger.css('width', fullWidth);
		$container.css('width', fullWidth);
		
		$trigger.on('hover', function(e) {
			if( $(this).next().is(':hidden') ) {
				$trigger.removeClass('active').next().slideUp(1000);
				$(this).toggleClass('active').next().slideDown(1000);
			}
			e.preventDefault();
		});

	})();

	//	ACCORDION - DÃšVIDAS FREQUENTES
	(function() {

		var $container = $('.duviva p'),
			$trigger   = $('.duviva h2');

		$container.hide();
		$trigger.first().addClass('active').next().hide();

		var fullWidth = $container.outerWidth(true);
		$trigger.css('width', fullWidth);
		$container.css('width', fullWidth);
		
		$trigger.on('click', function(e) {
			if( $(this).next().is(':hidden') ) {
				$trigger.removeClass('active').next().slideUp(300);
				$(this).toggleClass('active').next().slideDown(300);
			}
			e.preventDefault();
		});

		// Resize
		$(window).on('resize', function() {
			fullWidth = $container.outerWidth(true)
			$trigger.css('width', $trigger.parent().width() );
			$container.css('width', $container.parent().width() );
		});

	})();
	*/

	// LAZYLOAD
	$('img').lazyload({
	  effect : 'fadeIn'
	});


	$(".formPedido").submit(function() {          
            if ( this.elements[2].value == '' ) {
                alert("Por favor, informe a quantidade para a compra.");
                return false;
            };        
        });

	// Pegaa o soma total e, valortotal e coloca na i (valorcarrinho)
	var valortotalElement = document.getElementById("valortotal");
    var valorcarrinhoElement = document.getElementById("valorcarrinho");
	if( valortotalElement && valorcarrinhoElement )
	    valorcarrinhoElement.innerHTML = valortotalElement.innerHTML;
	
});

// Mascara CPF e CNPJ
// Modo de Uso
// <input type='text' name='cpfcnpj' onkeypress='mascaraMutuario(this,cpfCnpj)' onblur='clearTimeout()'>
function mascaraMutuario(o,f){
    v_obj=o
    v_fun=f
    setTimeout('execmascara()',1)
}
 
function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}
 
function cep(v){

    // Remove tudo o que nÃ£o Ã© dÃ­gito
    v=v.replace(/\D/g,"");

    // Por partes...
    v=v.replace(/(\d{2})(\d)/,"$1.$2");
    v=v.replace(/(\d{2})\.(\d{3})(\d)/,"$1.$2-$3");

    // De uma vez...
    //v=v.replace(/(\d{2})(\d{3})(\d{3})/,"$1.$2-$3");

    return v;

}
 
function cpfCnpj(v){
 
    //Remove tudo o que nÃ£o Ã© dÃ­gito
    v=v.replace(/\D/g,"")
 
    if ( v.length <= 14) { //CPF
 		// 444.555.666-77
        v=v.replace(/(\d{3})(\d)/,"$1.$2") 

        v=v.replace(/(\d{3})\.(\d{3})(\d)/,"$1.$2.$3")
 
        v=v.replace(/(\d{3})\.(\d{3})\.(\d{3})(\d)/,"$1.$2.$3-$4")

 
    } 

    if (v.length > 14) { //CNPJ
    	//Remove tudo o que nÃ£o Ã© dÃ­gito
    	v=v.replace(/\D/g,"")

 		// 01.947.770/0001-09
        v=v.replace(/^(\d{2})(\d)/,"$1.$2")

        v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3")
        
        v=v.replace(/^(\d{2})\.(\d{3})\.(\d{3})(\d)/,"$1.$2.$3/$4")

        v=v.replace(/^(\d{2})\.(\d{3})\.(\d{3})\/(\d{4})(\d)/,"$1.$2.$3/$4-$5")       
 
    }
 
    return v
 
}
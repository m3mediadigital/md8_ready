<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	include_once( "../web/config.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/SQL.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Format.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Functions.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Vars.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Chat/get.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Footprints/get.php" ) ;

	$image_dir = realpath( "$CONF[DOCUMENT_ROOT]/pics/icons/pixels" ) ; $image_path = "" ;
	$deptid = Util_Format_Sanatize( Util_Format_GetVar( "deptid" ), "ln" ) ;
	$onpage = rawurldecode( Util_Format_Sanatize( Util_Format_GetVar( "onpage" ), "url" ) ) ;
	$title = rawurldecode( Util_Format_Sanatize( Util_Format_GetVar( "title" ), "title" ) ) ;
	$refer = rawurldecode( Util_Format_Sanatize( Util_Format_GetVar( "r" ), "url" ) ) ;
	$resolution = Util_Format_Sanatize( Util_Format_GetVar( "resolution" ), "ln" ) ;
	$c = Util_Format_Sanatize( Util_Format_GetVar( "c" ), "ln" ) ;

	$ip = Util_Format_GetIP() ;
	//$ip = "167.153.240.51" ;
	$marketid = $skey = $excluded = 0 ;
	if ( preg_match( "/$ip/", $VALS["TRAFFIC_EXCLUDE_IPS"] ) )
		$excluded = 1 ;
	preg_match( "/plk(=|%3D)(.*)-m/", $onpage, $matches ) ;
	if ( isset( $matches[2] ) )
		LIST( $pi, $marketid, $skey ) = explode( "-", $matches[2] ) ;
	if ( !isset( $CONF["foot_log"] ) )
		$CONF["foot_log"] = "on" ;

	$agent = isset( $_SERVER["HTTP_USER_AGENT"] ) ? $_SERVER["HTTP_USER_AGENT"] : "&nbsp;" ;
	$mobile = Util_Mobile_Detect() ;
	if ( $mobile ) { $os = 5 ; }
	else if ( preg_match( "/Windows/i", $agent ) ) { $os = 1 ; }
	else if ( preg_match( "/Mac/i", $agent ) ) { $os = 2 ; }
	else { $os = 4 ; }

	if ( preg_match( "/MSIE/i", $agent ) ) { $browser = 1 ; }
	else if ( preg_match( "/Firefox/i", $agent ) ) { $browser = 2 ; }
	else if ( preg_match( "/Chrome/i", $agent ) ) { $browser = 3 ; }
	else if ( preg_match( "/Safari/i", $agent ) ) { $browser = 4 ; }
	else { $browser = 6 ; }

	if ( preg_match( "/(statichtmlapp.com)/", $onpage ) && !$title )
		$title = "Facebook Page" ;
	else if ( !$title )
		$title = "- no title -" ;

	$footprintinfo = Footprints_get_IPFootprints_U( $dbh, $ip ) ;
	// brute bug fix for situations where the web/chat_initiate/$ip.txt is removed
	// another layer of check so the widget is not closed
	$chatinfo = Chat_get_RequestIPInfo( $dbh, $ip, 1 ) ;

	if ( $excluded )
		$image_path = "$image_dir/4x4.gif" ;
	else if ( !$c && !isset( $footprintinfo["ip"] ) )
	{
		// first time situation, put in refer and footprint
		include_once( "$CONF[DOCUMENT_ROOT]/API/Footprints/put.php" ) ;
		include_once( "$CONF[DOCUMENT_ROOT]/API/IPs/put.php" ) ;
		include_once( "$CONF[DOCUMENT_ROOT]/API/IPs/Util.php" ) ;

		$country = $region = $city = "" ;
		$latitude = $longitude = 0 ;
		if ( $geoip )
		{
			include_once( "$CONF[DOCUMENT_ROOT]/API/GeoIP/get.php" ) ;

			LIST( $ip_num, $network ) = UtilIPs_IP2Long( $ip ) ;
			$geoinfo = GeoIP_get_GeoInfo( $dbh, $ip_num, $network ) ;
			if ( isset( $geoinfo["latitude"] ) )
			{
				$country = $geoinfo["country"] ;
				$region = $geoinfo["region"] ;
				$city = $geoinfo["city"] ;
				$latitude = $geoinfo["latitude"] ;
				$longitude = $geoinfo["longitude"] ;
			}
		}

		IPs_put_IP( $dbh, $ip, $deptid, 1, 0, 0, 0, 1, 1, time(), $onpage ) ;

		Footprints_put_Print_U( $dbh, $deptid, $os, $browser, $resolution, $ip, $onpage, $title, $marketid, $refer, $country, $region, $city, $latitude, $longitude ) ;
		if ( $CONF["foot_log"] == "on" )
			Footprints_put_Print( $dbh, $deptid, $os, $browser, $ip, $onpage, $title ) ;

		// if $refer exists always put it into DB on first visit for logging
		if ( $refer )
		{
			if ( !isset( $_COOKIE["phplive_refer"] ) )
				setcookie( "phplive_refer", $refer, time()+60*60*24*180 ) ;
			Footprints_put_Refer( $dbh, $ip, $marketid, $refer ) ;
		}

		$image_path = "$image_dir/1x1.gif" ;
	}
	else if ( !$c && isset( $footprintinfo["ip"] ) )
	{
		// went to new page, first call on that new page only
		include_once( "$CONF[DOCUMENT_ROOT]/API/Footprints/put.php" ) ;
		include_once( "$CONF[DOCUMENT_ROOT]/API/Footprints/update.php" ) ;
		include_once( "$CONF[DOCUMENT_ROOT]/API/IPs/put.php" ) ;

		IPs_put_IP( $dbh, $ip, $deptid, 1, 0, 0, 0, 1, 1, time(), $onpage ) ;

		if ( $CONF["foot_log"] == "on" )
			Footprints_put_Print( $dbh, $deptid, $os, $browser, $ip, $onpage, $title ) ;

		$nrows = Footprints_update_Footprint_UOnpage( $dbh, $ip, $onpage, $title ) ;
		if ( $nrows )
			$image_path = "$image_dir/1x1.gif" ;
		else
			$image_path = "$image_dir/4x4.gif" ;
	}
	else if ( $c > $VARS_JS_FOOTPRINT_MAX_CYCLE )
	{
		// stop the ajax from calling again because the visitor has left the
		// browser open or been at the page for a very long time... no need to use up resources
		$image_path = "$image_dir/4x4.gif" ;
	}
	else
	{
		// repeat calling, just update footprint unique
		include_once( "$CONF[DOCUMENT_ROOT]/API/Footprints/update.php" ) ;
		include_once( "$CONF[DOCUMENT_ROOT]/API/IPs/put.php" ) ;

		IPs_put_IP( $dbh, $ip, $deptid, 0, 0, 0, 0, 1, 0, 0, $onpage ) ;

		Footprints_update_FootprintUniqueValue( $dbh, $ip, "updated", time() ) ;
		$image_path = "$image_dir/1x1.gif" ;
	}

	// override previous images if initiate flag
	if ( file_exists( "$CONF[TYPE_IO_DIR]/$ip.txt" ) || ( isset( $chatinfo["ip"] ) && !$chatinfo["status"] ) )
		$image_path = "$image_dir/2x2.gif" ;

	if ( isset( $dbh ) && isset( $dbh['con'] ) )
		database_mysql_close( $dbh ) ;

	Header( "Content-type: image/GIF" ) ;
	readfile( $image_path ) ;
?>
<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	/*
	// status json route: -1 no request, 0 same op route, 1 request accepted, 2 new op route, 10 leave a message
	*/
	include_once( "../web/config.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Format.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Vars.php" ) ;

	$action = Util_Format_Sanatize( Util_Format_GetVar( "action" ), "ln" ) ;

	if ( $action == "routing" )
	{
		include_once( "$CONF[DOCUMENT_ROOT]/API/SQL.php" ) ;
		include_once( "$CONF[DOCUMENT_ROOT]/API/Chat/get.php" ) ;

		$ces = Util_Format_Sanatize( Util_Format_GetVar( "ces" ), "ln" ) ;
		$deptid = Util_Format_Sanatize( Util_Format_GetVar( "deptid" ), "ln" ) ;
		$rtype = Util_Format_Sanatize( Util_Format_GetVar( "rtype" ), "ln" ) ;
		$rtime = Util_Format_Sanatize( Util_Format_GetVar( "rtime" ), "ln" ) ;

		$requestinfo = Chat_get_RequestCesInfo( $dbh, $ces ) ;
		if ( !isset( $requestinfo["requestID"] ) )
			$json_data = "json_data = { \"status\": 10 };" ;
		else
		{
			if ( $requestinfo["status"] )
			{
				include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/get.php" ) ;
				include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/put.php" ) ;

				$opinfo = Ops_get_OpInfoByID( $dbh, $requestinfo["opID"] ) ;
				Ops_put_OpReqStat( $dbh, $requestinfo["deptID"], $opinfo["opID"], "taken", 1 ) ;

				$json_data = "json_data = { \"status\": 1, \"status_request\": $requestinfo[status], \"requestid\": $requestinfo[requestID], \"initiated\": $requestinfo[initiated], \"name\": \"$opinfo[name]\", \"deptid\": $deptid, \"opid\": $opinfo[opID], \"email\": \"$opinfo[email]\", \"pic\": \"$opinfo[pic]\" };" ;
			}
			else
			{
				// vupdated is used for routing UNTIL chat is accepted then it is used
				// for visitor's callback updated time
				$rupdated = $requestinfo["vupdated"] + $rtime ;
				if ( time() <= $rupdated )
					$json_data = "json_data = { \"status\": 0 };" ;
				else
				{
					include_once( "$CONF[DOCUMENT_ROOT]/API/Chat/update.php" ) ;
					include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/get.php" ) ;
					include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/put.php" ) ;

					Ops_put_OpReqStat( $dbh, $deptid, $requestinfo["opID"], "declined", 1 ) ;
					$opinfo_next = Ops_get_NextRequestOp( $dbh, $deptid, $rtype, $requestinfo["rstring"] ) ;
					if ( isset( $opinfo_next["opID"] ) )
					{
						if ( $opinfo_next["sms"] == 1 )
						{
							include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Email.php" ) ;

							$question = ( strlen( $requestinfo["question"] ) > 100 ) ? substr( $requestinfo["question"], 0, 100 ) . "..." : $requestinfo["question"] ;
							$question = preg_replace( "/<br>/", " ", $question ) ;
							Util_Email_SendEmail( $opinfo_next["name"], $opinfo_next["email"], $requestinfo["vname"], base64_decode( $opinfo_next["smsnum"] ), "Chat Request", $question, "sms" ) ;
						}

						Chat_update_RouteChat( $dbh, $requestinfo["requestID"], $requestinfo["ces"], $opinfo_next["opID"], $opinfo_next["sms"],  " $requestinfo[rstring] AND p_operators.opID <> $opinfo_next[opID] " ) ;

						// don't log trasfer chats on total stats of requests
						if ( $requestinfo["status"] != 2 )
							Ops_put_OpReqStat( $dbh, $deptid, $opinfo_next["opID"], "requests", 1 ) ;
						$json_data = "json_data = { \"status\": 2 };" ;
					}
					else
					{
						include_once( "$CONF[DOCUMENT_ROOT]/API/Chat/put.php" ) ;
						include_once( "$CONF[DOCUMENT_ROOT]/API/Chat/update.php" ) ;
						include_once( "$CONF[DOCUMENT_ROOT]/API/Chat/remove.php" ) ;

						// leave a message
						// on stats db the leave a message is not op specific, just use the current opID to track
						// requests that went to leave a messge
						Ops_put_OpReqStat( $dbh, $deptid, $requestinfo["opID"], "message", 1 ) ;
						Chat_remove_RequestByCes( $dbh, $requestinfo["ces"] ) ;
						$json_data = "json_data = { \"status\": 10 };" ;
					}
				}
			}
		}
	}

	if ( isset( $dbh ) && isset( $dbh['con'] ) )
		database_mysql_close( $dbh ) ;

	$json_data = preg_replace( "/\r\n/", "", $json_data ) ;
	$json_data = preg_replace( "/\t/", "", $json_data ) ;
	print "$json_data" ;
	exit ;
?>
<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	include_once( "../web/config.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Format.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/SQL.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Vals.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/get.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/update.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Footprints/remove.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/IPs/remove.php" ) ;

	$image_dir = "$CONF[DOCUMENT_ROOT]/web" ; $image_path = $image_type = "" ;
	$deptid = Util_Format_Sanatize( Util_Format_GetVar( "d" ), "ln" ) ;
	$onpage = Util_Format_Sanatize( Util_Format_GetVar( "p" ), "url" ) ;
	$title = rawurldecode( Util_Format_Sanatize( Util_Format_GetVar( "title" ), "title" ) ) ;
	$refer = rawurldecode( Util_Format_Sanatize( Util_Format_GetVar( "r" ), "url" ) ) ;
	$resolution = Util_Format_Sanatize( Util_Format_GetVar( "resolution" ), "ln" ) ;

	$ip = Util_Format_GetIP() ;

	if ( $onpage )
	{
		preg_match( "/plk(=|%3D)(.*)-m/", $onpage, $matches ) ;
		if ( isset( $matches[2] ) )
		{
			include_once( "$CONF[DOCUMENT_ROOT]/API/Marketing/get.php" ) ;
			include_once( "$CONF[DOCUMENT_ROOT]/API/Marketing/update.php" ) ;
			include_once( "$CONF[DOCUMENT_ROOT]/API/Marketing/put.php" ) ;

			LIST( $pi, $marketid, $skey ) = explode( "-", $matches[2] ) ;
			$marketinfo = Marketing_get_MarketingByID( $dbh, $marketid ) ;

			if ( $marketinfo["skey"] == $skey )
			{
				$clickinfo = Marketing_get_ClickInfo( $dbh, $marketid ) ;
				if ( isset( $clickinfo["marketID"] ) )
					Marketing_update_MarketClickValue( $dbh, $marketid, "clicks", $clickinfo["clicks"]+1 ) ;
				else
					Marketing_put_Click( $dbh, $marketid, 1 ) ;
			}
		}
		else
			$marketid = 0 ;
	}

	// basic cleaning of DB
	Footprints_remove_Expired_U( $dbh ) ;
	Ops_update_IdleOps( $dbh ) ;

	// to save on resources and not for every image load on heavy traffic sites, only
	// clean so often as the past logs are not crucial to remove right away, but an
	// auto clean system should be in place
	if ( time() % 2 )
	{
		Footprints_remove_Expired_Footprints( $dbh ) ;
		IPs_remove_Expired_IPs( $dbh ) ;
	}

	if ( preg_match( "/$ip/", $VALS["CHAT_SPAM_IPS"] ) )
		$total_ops = 0 ;
	else
		$total_ops = Ops_get_AnyOpsOnline( $dbh, $deptid ) ;

	if ( $total_ops )
		$prefix = "icon_online" ;
	else
		$prefix = "icon_offline" ;
	
	if ( file_exists( realpath( "$image_dir/$prefix"."_$deptid.GIF" ) ) )
	{
		$image_type = "GIF" ;
		$image_path = "$image_dir/$prefix"."_$deptid.GIF" ;
	}
	else if ( file_exists( realpath( "$image_dir/$prefix"."_$deptid.JPEG" ) ) )
	{
		$image_type = "JPEG" ;
		$image_path = "$image_dir/$prefix"."_$deptid.JPEG";
	}
	else if ( file_exists( realpath( "$image_dir/$prefix"."_$deptid.PNG" ) ) )
	{
		$image_type = "PNG" ;
		$image_path = "$image_dir/$prefix"."_$deptid.PNG" ;
	}
	else if ( file_exists( realpath( "$image_dir/$CONF[$prefix]" ) ) && $CONF[$prefix] )
	{
		$image_type = preg_replace( "/(.*?)\./", "", $CONF[$prefix] ) ;
		$image_path = "$image_dir/$CONF[$prefix]" ;
	}
	else
	{
		$image_type = "PNG" ;
		$image_path = "$CONF[DOCUMENT_ROOT]/pics/icons/$prefix.gif" ;
	}

	if ( isset( $dbh ) && isset( $dbh['con'] ) )
		database_mysql_close( $dbh ) ;

	Header( "Content-type: image/$image_type" ) ;
	readfile( $image_path ) ;
?>
<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	include_once( "../web/config.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Format.php" ) ;

	$action = Util_Format_Sanatize( Util_Format_GetVar( "action" ), "ln" ) ;

	// todo: $opinfo [mod Jake: 82]
	if ( !isset( $_COOKIE["phplive_opID"] ) )
		$json_data = "json_data = { \"status\": -1 };" ;
	else if ( $action == "sms_send" )
	{
		include_once( "$CONF[DOCUMENT_ROOT]/API/SQL.php" ) ;
		include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/get.php" ) ;
	
		$phonenum = Util_Format_Sanatize( Util_Format_GetVar( "phonenum" ), "n" ) ;
		$carrier = Util_Format_Sanatize( Util_Format_GetVar( "carrier" ), "ln" ) ;

		$opinfo = Ops_get_OpInfoByID( $dbh, $_COOKIE["phplive_opID"] ) ;
		if ( !$opinfo["sms"] )
			$json_data = "json_data = { \"status\": 0, \"error\": \"SMS is not enabled for this account.\" }; " ;
		else if ( $opinfo["sms"] < time()-60 )
		{
			include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Email.php" ) ;
			include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/update.php" ) ;

			$now = time() ;
			$smsnum = "$phonenum@$carrier" ;
			Ops_update_OpValue( $dbh, $_COOKIE["phplive_opID"], "sms", $now ) ;
			Ops_update_OpValue( $dbh, $_COOKIE["phplive_opID"], "smsnum", base64_encode( $smsnum ) ) ;

			Util_Email_SendEmail( $opinfo["name"], $opinfo["email"], "Mobile", $smsnum, "Verification", "Verification Code: $now", "sms" ) ;
			$json_data = "json_data = { \"status\": 1 }; " ;
		}
		else
		{
			$time_left = $opinfo["sms"] - ( time()-60 ) ;
			$json_data = "json_data = { \"status\": 0, \"error\": \"Please try again in $time_left seconds.\" }; " ;
		}
	}
	else if ( $action == "sms_verify" )
	{
		include_once( "$CONF[DOCUMENT_ROOT]/API/SQL.php" ) ;
		include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/get.php" ) ;
		include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/update.php" ) ;
	
		$code = Util_Format_Sanatize( Util_Format_GetVar( "code" ), "n" ) ;

		$opinfo = Ops_get_OpInfoByID( $dbh, $_COOKIE["phplive_opID"] ) ;
		if ( !$opinfo["sms"] )
			$json_data = "json_data = { \"status\": 0, \"error\": \"SMS is not enabled for this account.\" }; " ;
		else if ( ( $code == 1 ) || ( $code == 2 ) )
		{
			Ops_update_OpValue( $dbh, $_COOKIE["phplive_opID"], "sms", $code ) ;
			$json_data = "json_data = { \"status\": 1 };" ;
		}
		else if ( ( $opinfo["sms"] == 1 ) || ( $opinfo["sms"] == 2 ) )
			$json_data = "json_data = { \"status\": 1, \"error\": \"Your mobile has already been verified.\" }; " ;
		else if ( $opinfo["sms"] == $code )
		{
			Ops_update_OpValue( $dbh, $_COOKIE["phplive_opID"], "sms", 2 ) ;
			$json_data = "json_data = { \"status\": 1 }; " ;
		}
		else
			$json_data = "json_data = { \"status\": 0, \"error\": \"Verification code is invalid.\" }; " ;
	}
	else if ( $action == "dn_toggle" )
	{
		include_once( "$CONF[DOCUMENT_ROOT]/API/SQL.php" ) ;
		include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/get.php" ) ;
		include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/update.php" ) ;
	
		$dn = Util_Format_Sanatize( Util_Format_GetVar( "dn" ), "n" ) ;

		Ops_update_OpValue( $dbh, $_COOKIE["phplive_opID"], "dn", $dn ) ;
		$json_data = "json_data = { \"status\": 1 };" ;
	}
	else
		$json_data = "json_data = { \"status\": 0 };" ;

	if ( isset( $dbh ) && isset( $dbh['con'] ) )
		database_mysql_close( $dbh ) ;

	$json_data = preg_replace( "/\r\n/", "", $json_data ) ;
	$json_data = preg_replace( "/\t/", "", $json_data ) ;
	print "$json_data" ;
	exit ;
?>

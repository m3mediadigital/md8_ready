<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	if ( !file_exists( "./web/VERSION.php" ) ) { touch( "./web/VERSION.php" ) ; }  // patch 4.2.105 adjustment
	include_once( "./web/config.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Format.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Error.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Security.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Upload.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/SQL.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Depts/get.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/get.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Chat/get.php" ) ;
	include_once( "$CONF[DOCUMENT_ROOT]/API/Marketing/get.php" ) ;
	/* AUTO PATCH */
	if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/$patch_v" ) )
	{
		$query = ( isset( $_SERVER["QUERY_STRING"] ) ) ? $_SERVER["QUERY_STRING"] : "" ;
		HEADER( "location: patch.php?from=chat&".$query ) ;
		exit ;
	}

	$ces = Util_Security_GenSetupSes() ;
	$onpage = Util_Format_Sanatize( Util_Format_GetVar( "onpage" ), "url" ) ; $onpage = ( $onpage ) ? $onpage : "" ;
	$title = Util_Format_Sanatize( Util_Format_GetVar( "title" ), "title" ) ; $title = ( $title ) ? $title : "" ;
	$deptid = Util_Format_Sanatize( Util_Format_GetVar( "d" ), "n" ) ;
	$theme = Util_Format_Sanatize( Util_Format_GetVar( "theme" ), "ln" ) ;
	$widget = Util_Format_Sanatize( Util_Format_GetVar( "widget" ), "n" ) ;
	$js_name = Util_Format_Sanatize( Util_Format_GetVar( "js_name" ), "ln" ) ;
	$js_email = Util_Format_Sanatize( Util_Format_GetVar( "js_email" ), "e" ) ;
	$custom = Util_Format_Sanatize( Util_Format_GetVar( "custom" ), "ln" ) ;
	$mobile = Util_Mobile_Detect() ;

	$marquee_test = Util_Format_Sanatize( Util_Format_GetVar( "marquee_test" ), "js" ) ;

	$agent = isset( $_SERVER["HTTP_USER_AGENT"] ) ? $_SERVER["HTTP_USER_AGENT"] : "&nbsp;" ;
	$ip = Util_Format_GetIP() ;
	LIST( $os, $browser ) = Util_Format_GetOS( $agent ) ;

	$cookie = ( !isset( $CONF["cookie"] ) || ( $CONF["cookie"] == "on" ) ) ? 1 : 0 ;

	$temp_vname = ( !$js_name && ( isset( $_COOKIE["phplive_vname"] ) && $cookie ) ) ? $_COOKIE["phplive_vname"] : $js_name ;
	$temp_vemail = ( !$js_email && ( isset( $_COOKIE["phplive_vemail"] ) && $cookie ) ) ? $_COOKIE["phplive_vemail"] : $js_email ;
	$vname = ( $temp_vname ) ? $temp_vname : "" ;
	$vemail = ( $temp_vemail ) ? $temp_vemail : "" ;

	$dept_offline = "" ; // for js eval for offline messages
	$dept_settings = "" ; // for js eval for dept settings

	if ( preg_match( "/$ip/", $VALS["CHAT_SPAM_IPS"] ) )
		$spam_exist = 1 ;
	else
		$spam_exist = 0 ;

	if ( file_exists( "$CONF[TYPE_IO_DIR]/$ip.txt" ) )
	{
		include_once( "$CONF[DOCUMENT_ROOT]/API/IPs/update.php" ) ;

		$initiate_array = ( isset( $CONF["auto_initiate"] ) && $CONF["auto_initiate"] ) ? unserialize( html_entity_decode( $CONF["auto_initiate"] ) ) : Array() ;

		$auto_initiate_reset = 0 ;
		if ( isset( $initiate_array["reset"] ) )
			$auto_initiate_reset = $initiate_array["reset"] ;

		$reset = 60*60*24*$auto_initiate_reset ;
		IPs_update_IpValue( $dbh, $ip, "i_initiate", time() + $reset ) ;

		unlink( "$CONF[TYPE_IO_DIR]/$ip.txt" ) ;
	}

	if ( $widget )
	{
		$requestinfo = Chat_get_RequestIPInfo( $dbh, $ip, 1 ) ;
		$deptid = ( isset( $requestinfo["deptID"] ) ) ? $requestinfo["deptID"] : $deptid ;
		// reset widget if auto invitation was not located
		$widget = ( isset( $requestinfo["deptID"] ) ) ? 1 : 0 ;
	}

	$total_ops = 0 ; $dept_online = Array() ; $departments = Array() ;
	if ( $deptid )
	{
		$deptinfo = Depts_get_DeptInfo( $dbh, $deptid ) ;
		$departments[0] = $deptinfo ;
		if ( !isset( $deptinfo["deptID"] ) )
		{
			$query = $_SERVER["QUERY_STRING"] ;
			$query = preg_replace( "/(d=(.*?)(&|$))/", "d=0&", $query ) ;
			HEADER( "location: phplive.php?$query" ) ; exit ;
		}

		$total = ( $widget ) ? 1 : Ops_get_AnyOpsOnline( $dbh, $deptinfo["deptID"] ) ;
		$total_ops += $total ;
		$dept_online[$deptinfo["deptID"]] = $total ;
		$dept_offline .= "dept_offline[$deptinfo[deptID]] = '".preg_replace( "/'/", "&lsquo;", $deptinfo["msg_offline"] )."' ; " ;
		$dept_settings .= " dept_settings[$deptinfo[deptID]] = Array( $deptinfo[remail], $deptinfo[temail] ) ; " ;
		
		if ( $deptinfo["lang"] )
			$CONF["lang"] = $deptinfo["lang"] ;
	}
	else
	{
		$departments_pre = Depts_get_AllDepts( $dbh ) ;
		for ( $c = 0; $c < count( $departments_pre ); ++$c )
		{
			$department = $departments_pre[$c] ;
			if ( $department["visible"] ) { $departments[] = $department ; }
		}
		
		for ( $c = 0; $c < count( $departments ); ++$c )
		{
			$department = $departments[$c] ;
			if ( $spam_exist )
				$total = 0 ;
			else
				$total = Ops_get_AnyOpsOnline( $dbh, $department["deptID"] ) ;
			$total_ops += $total ;

			$dept_online[$department["deptID"]] = $total ;
			$dept_offline .= "dept_offline[$department[deptID]] = '".preg_replace( "/'/", "&lsquo;", $department["msg_offline"] )."' ; " ;
			$dept_settings .= " dept_settings[$department[deptID]] = Array( $department[remail], $department[temail] ) ; " ;
		}

		if ( count( $departments ) == 1 )
			$deptid = $departments[0]["deptID"] ;
	}

	include_once( "./setup/KEY.php" ) ;
	$upload_dir = "$CONF[DOCUMENT_ROOT]/web" ;

	if ( $marquee_test )
		$marquees = Array( Array( "snapshot" => "", "message" => "$marquee_test" ) ) ;
	else
		$marquees = Marketing_get_DeptMarquees( $dbh, $deptid ) ;
	$marquee_string = "" ;
	for ( $c = 0; $c < count( $marquees ); ++$c )
	{
		$marquee = $marquees[$c] ;
		$snapshot = preg_replace( "/'/", "&#39;", preg_replace( "/\"/", "&quot;", $marquee["snapshot"] ) ) ;
		$message = preg_replace( "/'/", "&#39;", preg_replace( "/\"/", "", $marquee["message"] ) ) ;

		$marquee_string .= "marquees[$c] = '$snapshot' ; marquees_messages[$c] = '$message' ; " ;
	}
	if ( !count( $marquees ) )
		$$marquee_string = "marquees[0] = '' ; marquees_messages[0] = '' ; " ;

	include_once( "./lang_packs/$CONF[lang].php" ) ;
?>
<?php include_once( "./inc_doctype.php" ) ?>
<!--
********************************************************************
* PHP Live! (c) OSI Codes Inc.
* www.phplivesupport.com
********************************************************************
-->
<head>
<title> <?php echo $LANG["CHAT_WELCOME"] ?> </title>

<meta name="description" content="PHP Live! Support <?php echo $VERSION ?>">
<meta name="keywords" content="powered by: PHP Live!  www.phplivesupport.com">
<meta name="robots" content="all,index,follow">
<meta name="phplive" content="version: <?php echo $VERSION ?>, KEY: <?php echo $KEY ?>">
<meta http-equiv="content-type" content="text/html; CHARSET=<?php echo $LANG["CHARSET"] ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width">

<link rel="Stylesheet" href="./themes/<?php echo ( $theme ) ? $theme : $CONF["THEME"] ; ?>/style.css?<?php echo $VERSION ?>">
<script type="text/javascript" src="./js/global.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="./js/global_chat.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="./js/setup.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="./js/framework.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="./js/framework_cnt.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="./js/jquery.tools.min.js?<?php echo $VERSION ?>"></script>

<script type="text/javascript">
<!--
	var marquees = marquees_messages = new Array() ;
	var marquee_index = 0 ;
	var widget = <?php echo $widget ?> ;
	var mobile = <?php echo $mobile ?> ;

	var win_width = screen.width ;
	var win_height = screen.height ;

	var dept_offline = new Object ;
	var dept_settings = new Object ;

	var js_email = "<?php echo $js_email ?>" ;

	$(document).ready(function()
	{
		$('#win_dim').val( win_width + " x " + win_height ) ;
		init_divs_pre() ;
		eval( "<?php echo preg_replace( "/\"/", "&quot;", $dept_offline ) ; ?>" ) ;
		eval( "<?php echo $dept_settings ?>" ) ;

		init_marquees( "<?php echo $marquee_string ?>" ) ;

		$('#chat_button_start').html( "<?php echo $LANG["CHAT_BTN_START_CHAT"] ?>" ).unbind('click').bind('click', function() {
			start_chat() ;
		}) ;

		<?php echo ( isset( $requestinfo["ces"] ) ) ? "start_chat() ; " : "select_dept( $deptid ) ; $('body').show() ; " ?>
		<?php echo ( $deptid ) ? "select_dept( $deptid ) ;" : "$('#div_vdeptids').show() ;" ; ?>
		
		flashembed( "flash_result", {
			src: "./media/expressInstall.swf",
			version: [8, 0],
			expressInstall: "./media/expressInstall.swf",
			onFail: function() {
				<?php if ( !$mobile ): ?>$('#flash_detect').show() ;<?php endif ; ?>
			}
		});
	});
	$(window).resize(function() {
		if ( !mobile ) { init_divs_pre() ; }
	});

	function init_divs_pre()
	{
		var browser_height = $(window).height() ; var browser_width = $(window).width() ;
		var body_height = browser_height - $('#chat_footer').height() - 45 ;
		var body_width = browser_width - 42 ;
		var logo_width = body_width - 10 ;
		var deptid_width = logo_width + 5 ;
		var input_width = Math.floor( logo_width/2 ) - 15 ;

		$('#chat_logo').css({'width': logo_width}) ;
		$('#chat_body').css({'height': body_height, 'width': body_width}) ;
		$('#vdeptid').css({'width': deptid_width}) ;
		$('#vname').css({'width': input_width }) ;
		$('#vemail').css({'width': input_width }) ;
		$('#vemail_null').css({'width': input_width }) ;
		$('#vquestion').css({'width': input_width }) ;
	}

	function select_dept( thevalue )
	{
		$('#deptid').val( thevalue ) ;
		if ( ( $('#vdeptid option:selected').attr( "class" ) == "offline" ) )
		{
			$('#chat_text_header_sub').html( dept_offline[thevalue] ) ;
			$('#div_vemail_visible').show() ;
			$('#div_vemail_hidden').hide() ;
			if ( parseInt( js_email ) ) { $('#vemail').val( js_email ).attr( "disabled", true ) ; }
			$('#div_email_trans').hide() ;
			$('#chat_button_start').html( "<?php echo $LANG["CHAT_BTN_EMAIL"] ?>" ).unbind('click').bind('click', function() {
				send_email() ;
			});
		}
		else
		{
			$('#chat_text_header_sub').html( "<?php echo preg_replace( "/\"/", "&quot;", $LANG["CHAT_WELCOME_SUBTEXT"] ) ?>" ) ;
			if ( thevalue && ( typeof( dept_settings[thevalue] ) != "undefined" ) )
			{
				if ( dept_settings[thevalue][0] && dept_settings[thevalue][1] ) { $('#div_email_trans').show() ; }
				else { $('#div_email_trans').hide() ; }

				if ( dept_settings[thevalue][0] )
				{
					$('#div_vemail_hidden').hide() ;
					$('#div_vemail_visible').show() ;
					if ( parseInt( js_email ) ) { $('#vemail').val( js_email ).attr( "disabled", true ) ; }
				}
				else { $('#div_vemail_visible').hide() ; $('#div_vemail_hidden').show() ; }
			}
			else
			{
				$('#div_email_trans').hide() ;
				$('#div_vemail_visible').hide() ;
				$('#div_vemail_hidden').show() ;
			}

			$('#chat_button_start').html( "<?php echo $LANG["CHAT_BTN_START_CHAT"] ?>" ).unbind('click').bind('click', function() {
				start_chat() ;
			});
		}
	}

	function check_form( theflag )
	{
		if ( widget )
			return true ;

		var deptid = parseInt( $('#deptid').val() ) ;

		if ( !deptid ){
			do_alert( 0, "<?php echo $LANG["CHAT_JS_BLANK_DEPT"] ?>" ) ;
			return false ;
		}
		else if ( !$('#vname').val() ){
			do_alert( 0, "<?php echo $LANG["CHAT_JS_BLANK_NAME"] ?>" ) ;
			return false ;
		}
		else if ( !$('#vemail').val() ){
			if ( dept_settings[deptid][0] || theflag )
			{
				do_alert( 0, "<?php echo $LANG["CHAT_JS_BLANK_EMAIL"] ?>" ) ;
				return false ;
			}
		}
		else if ( !$('#vquestion').val() ){
			do_alert( 0, "<?php echo $LANG["CHAT_JS_BLANK_QUESTION"] ?>" ) ;
			return false ;
		}
		else if ( !check_email( $('#vemail').val() ) ){
			if ( dept_settings[deptid][0] || theflag )
			{
				do_alert( 0, "<?php echo $LANG["CHAT_JS_INVALID_EMAIL"] ?>" ) ;
				return false ;
			}
		}
		
		return true ;
	}

	function start_chat()
	{
		if ( check_form(0) )
		{
			<?php if ( file_exists( "$CONF[DOCUMENT_ROOT]/addons/inc_lang.php" ) ): ?>
			$('#pre_chat_form').hide() ;
			$('#pre_chat_lang').show() ;
			<?php else: ?>
			$('#theform').submit() ;
			<?php endif ; ?>
		}
	}

	function send_email()
	{
		if( check_form(1) )
		{
			var json_data = new Object ;
			var unique = unixtime() ;
			var deptid = $('#deptid').val() ;
			var vname = $('#vname').val() ;
			var vemail = $('#vemail').val() ;
			var vsubject = encodeURIComponent( "<?php echo $LANG["CHAT_JS_LEAVE_MSG"] ?>" ) ;
			var vquestion =  encodeURIComponent( $('#vquestion').val() ) ;
			var onpage =  encodeURIComponent( "<?php echo $onpage ?>" ) ;

			$('#chat_button_start').blur() ;
			$.post("./phplive_m.php", { action: "send_email", deptid: deptid, vname: vname, vemail: vemail, vsubject: vsubject, vquestion: vquestion, onpage: onpage, unique: unique },  function(data){
				eval( data ) ;

				if ( json_data.status )
				{
					$('#chat_button_start').attr( "disabled", true ) ;
					$('#chat_button_start').html( "<img src=\"./themes/default/alert_good.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\"> <?php echo $LANG["CHAT_JS_EMAIL_SENT"] ?>" ) ;
					setTimeout( function(){ window.close() }, 1000 ) ;
				}
				else
				{
					alert( json_data.error ) ;
					$('#chat_button_start').attr( "disabled", false ) ;
					$('#chat_button_start').html( "<?php echo $LANG["CHAT_BTN_EMAIL"] ?>" ) ;
				}
			});
		}
	}

//-->
</script>
</head>
<body style="display: none; overflow: hidden;">
<div id="chat_canvas" style="min-height: 100%; width: 100%;"></div>
<div style="position: absolute; top: 2px; padding: 10px; z-Index: 2;">
	<div id="chat_body" style="padding: 10px;">

		<div id="chat_logo" style="width: 100%; height: 45px; background: url( <?php echo Util_Upload_GetLogo( ".", $deptid ) ?> ) no-repeat;"></div>
		<div id="chat_text_header" style="margin-top: 10px;"><?php echo $LANG["CHAT_WELCOME"] ?></div>
		<div id="chat_text_header_sub" style="margin-top: 5px;"><?php echo $LANG["CHAT_WELCOME_SUBTEXT"] ?></div>

		<form method="POST" action="phplive_.php?submit&<?php echo time() ?>" id="theform">
		<input type="hidden" name="deptid" id="deptid" value="<?php echo ( isset( $requestinfo["deptID"] ) ) ? $requestinfo["deptID"] : $deptid ; ?>">
		<input type="hidden" name="ces" value="<?php echo ( isset( $requestinfo["ces"] ) ) ? $requestinfo["ces"] : $ces ; ?>">
		<input type="hidden" name="onpage" value="<?php echo ( isset( $requestinfo["ces"] ) ) ? $requestinfo["onpage"] : $onpage ; ?>">
		<input type="hidden" name="title" value="<?php echo ( isset( $requestinfo["ces"] ) ) ? $requestinfo["title"] :$title ; ?>">
		<input type="hidden" name="win_dim" id="win_dim" value="">
		<input type="hidden" name="widget" value="<?php echo $widget ?>">
		<input type="hidden" name="theme" value="<?php echo $theme ?>">
		<input type="hidden" name="custom" value="<?php echo $custom ?>">
		<?php if ( $js_name || $js_email ): ?><input type="hidden" id="auto_pop" name="auto_pop" value="1"><?php endif ; ?>
		<?php if ( $js_name ): ?><input type="hidden" name="vname" value="<?php echo $vname ?>"><?php endif ; ?>
		<?php if ( $js_email ): ?><input type="hidden" name="vemail" value="<?php echo $vemail ?>"><?php endif ; ?>
		<div id="pre_chat_form" style="margin-top: 10px;">
			<table cellspacing=0 cellpadding=0 border=0>
			<tr>
				<td colspan=2>
					<div id="div_vdeptids" style="display: none;">
						<?php echo $LANG["TXT_DEPARTMENT"] ?><br>
						<select id="vdeptid" onChange="select_dept(this.value)"><option value=0><?php echo $LANG["CHAT_SELECT_DEPT"] ?></option>
						<?php
							$selected = "" ;
							for ( $c = 0; $c < count( $departments ); ++$c )
							{
								$department = $departments[$c] ;
								$class = "offline" ; $text = $LANG["TXT_OFFLINE"] ;
								if ( $dept_online[$department["deptID"]] ) { $class = "online" ; $text = $LANG["TXT_ONLINE"] ; }
								if ( count( $departments ) == 1 ) { $selected = "selected" ; }
								print "<option class=\"$class\" value=\"$department[deptID]\" $selected>$department[name] - $text</option>" ;
							}
						?>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div style="margin-top: 5px;">
					<?php echo $LANG["TXT_NAME"] ?><br>
					<input type="input" class="input_text" id="vname" name="vname" maxlength="30" value="<?php echo isset( $requestinfo["vname"] ) ? preg_replace( "/<v>/", "", $requestinfo["vname"] ) : $vname ; ?>" onKeyPress="return noquotestags(event)" <?php echo ( $js_name ) ? "disabled" : "" ?>>
					</div>
				</td>
				<td>
					<div style="margin-top: 5px; margin-left: 23px;">
						<div id="div_vemail_visible" style="display: none;">
							<?php echo $LANG["TXT_EMAIL"] ?><br>
							<input type="input" class="input_text" id="vemail" name="vemail" maxlength="160" value="<?php echo isset( $requestinfo["vemail"] ) ? $requestinfo["vemail"] : $vemail ; ?>" onKeyPress="return justemails(event)" <?php echo ( $js_email ) ? "disabled" : "" ?>>
						</div>
						<!-- have to place an invisible text input div for formatting issues -->
						<div id="div_vemail_hidden" style="display: none;">&nbsp;<br><input type="input" class="input_text" id="vemail_null" name="vemail_null" maxlength="160" style="opacity:0.0; filter:alpha(opacity=0);"></div>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div style="margin-top: 5px;">
					<?php echo $LANG["TXT_QUESTION"] ?><br>
					<textarea class="input_text" id="vquestion" name="vquestion" rows="5" wrap="virtual" style="resize: none;"><?php echo isset( $requestinfo["question"] ) ? $requestinfo["question"] : "" ; ?></textarea>
					</div>
				</td>
				<td valign="top">
					<div id="chat_btn" style="margin-top: 5px; margin-left: 23px;">
						<div id="div_email_trans"><input type="checkbox" class="input_text" name="etrans" id="etrans" value=1> <?php echo $LANG["CHAT_AUTO_EMAIL_TRANS"] ?></div>
						<div style="margin-top: 20px;">
							<button id="chat_button_start" class="input_button" type="button" style="<?php echo ( $mobile ) ? "" : "width: 140px; height: 45px; font-size: 14px; font-weight: bold;" ?> padding: 6px;"><?php echo $LANG["CHAT_BTN_START_CHAT"] ?></button>
						</div>
						<div id="chat_text_powered" style="margin-top: 10px; font-size: 10px;"><?php if ( isset( $CONF["KEY"] ) && ( $CONF["KEY"] == md5($KEY."-c615") ) ): ?><?php else: ?>&nbsp;<br><a href="http://www.phplivesupport.com/?key=<?php echo $KEY ?>&plk=pi-5-ykq-m" target="_blank">PHP Live!</a> powered<?php endif ; ?></div>
					</div>
				</td>
			</tr>
		</table>
		</div>

		<?php if ( file_exists( "$CONF[DOCUMENT_ROOT]/addons/inc_lang.php" ) ): ?>
		<div id="pre_chat_lang" style="display: none; margin-top: 15px;">
			<iframe src="./addons/inc_lang.php" style="width:100%; height: 120px; border: 0px;" scrolling="no" frameBorder="0"></iframe>
			<div style="margin-top: 10px;">
				<button class="input_button" type="button" style="width: 140px; height: 45px; padding: 6px; font-size: 14px; font-weight: bold;" onClick="$('#theform').submit();"><?php echo $LANG["CHAT_BTN_START_CHAT"] ?></button>
			</div>
		</div>
		<?php endif ; ?>
		</form>

	</div>
</div>

<?php if ( !$mobile ): ?>
<div id="chat_footer" style="position: relative; width: 100%; margin-top: -28px; height: 28px; padding-top: 7px; padding-left: 15px; z-Index: 10;"></div>
<?php endif ; ?>

<?php include_once( "./inc_flash.php" ) ; ?>

</body>
</html>
<?php database_mysql_close( $dbh ) ; ?>

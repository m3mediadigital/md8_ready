<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	if ( !file_exists( "./web/VERSION.php" ) ) { touch( "./web/VERSION.php" ) ; } // patch 4.2.105 adjustment
	if ( !file_exists( "./web/config.php" ) ){ HEADER("location: ./setup/install.php") ; exit ; }
	include_once( "./web/config.php" ) ;
	include_once( "./API/SQL.php" ) ;
	include_once( "./API/Util_Format.php" ) ;
	include_once( "./API/Util_Error.php" ) ;
	/* AUTO PATCH */
	$query = ( isset( $_SERVER["QUERY_STRING"] ) ) ? $_SERVER["QUERY_STRING"] : "" ;
	if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/$patch_v" ) )
	{
		HEADER( "location: patch.php?from=index&".$query ) ;
		exit ;
	}
	include_once( "./lang_packs/$CONF[lang].php" ) ;

	/////////////////////////////////////////////
	// intercept for previous version conditions
	if ( defined( "LANG_CHAT_WELCOME" ) )
		ErrorHandler ( 611, "Update your language pack ($CONF[lang]).", $PHPLIVE_FULLURL, 0, Array() ) ;
	// end intercept
	/////////////////////////////////////////////

	$action = Util_Format_Sanatize( Util_Format_GetVar( "action" ), "ln" ) ;
	$login = Util_Format_Sanatize( Util_Format_GetVar( "phplive_login" ), "ln" ) ;
	$from = Util_Format_Sanatize( Util_Format_GetVar( "from" ), "ln" ) ;
	$auto = Util_Format_Sanatize( Util_Format_GetVar( "auto" ), "ln" ) ;
	$wp = ( Util_Format_Sanatize( Util_Format_GetVar( "wp" ), "ln" ) ) ? Util_Format_Sanatize( Util_Format_GetVar( "wp" ), "ln" ) : 0 ;
	$menu = ( Util_Format_Sanatize( Util_Format_GetVar( "menu" ), "ln" ) == "sa" ) ? "sa" : "operator" ;
	$wpress = Util_Format_Sanatize( Util_Format_GetVar( "wpress" ), "ln" ) ;
	$mobile = Util_Mobile_Detect() ;

	$error = $reload = $ses = ""  ;
	if ( !isset( $CONF["screen"] ) ) { $CONF["screen"] = "same" ; }
	if ( $wp || $wpress || ( $query == "op" ) ) { $CONF["screen"] = "separate" ; }

	if ( isset( $_COOKIE["phplive_token"] ) && $_COOKIE["phplive_token"] )
		$token = Util_Format_Sanatize( $_COOKIE["phplive_token"], "ln" ) ;
	else
	{
		$token = Util_Format_RandomString( 10 ) ;
		setcookie( "phplive_token", $token, time()+(60*60*24*30) ) ;
	}

	if ( $action == "submit" )
	{
		$menu = Util_Format_Sanatize( Util_Format_GetVar( "menu" ), "ln" ) ;
		$password = Util_Format_Sanatize( Util_Format_GetVar( "phplive_password" ), "" ) ;

		if ( $menu == "sa" )
		{
			include_once( "./API/Util_Security.php" ) ;
			include_once( "./API/Ops/get_ext.php" ) ;
			include_once( "./API/Ops/update_ext.php" ) ;
			include_once( "./API/Footprints/get_ext.php" ) ;

			$admininfo = Ops_ext_get_AdminInfoByLogin( $dbh, $login ) ;
			if ( isset( $admininfo["adminID"] ) && ( $password == md5($admininfo["password"].$token) ) )
			{
				$ses = Util_Security_GenSetupSes() ;
				Ops_ext_update_AdminValue( $dbh, $admininfo["adminID"], "lastactive", time() ) ;
				Ops_ext_update_AdminValue( $dbh, $admininfo["adminID"], "ses", $ses ) ;
				setcookie( "phplive_adminID", $admininfo['adminID'], -1 ) ;

				$sdate = mktime( 0, 0, 1, date( "m", time() ), date( "j", time() )-1, date( "Y", time() ) ) ;
				database_mysql_close( $dbh ) ;

				HEADER( "location: setup/?ses=$ses" ) ;
				exit ;
			}
			else
				$error = 1 ;
		}
		else
		{
			include_once( "./API/Util_Security.php" ) ;
			include_once( "./API/Ops/get.php" ) ;
			include_once( "./API/Ops/get_ext.php" ) ;
			include_once( "./API/Ops/update.php" ) ;

			$remember = Util_Format_Sanatize( Util_Format_GetVar( "remember" ), "ln" ) ;
			$opinfo = Ops_ext_get_OpInfoByLogin( $dbh, $login ) ;
			if ( isset( $opinfo["opID"] ) && ( $password == md5($opinfo["password"].$token) ) )
			{
				// only one instance of console window per browser type since system uses cookies
				if ( isset( $_COOKIE["phplive_opID"] ) && ( $_COOKIE["phplive_opID"] != $opinfo['opID'] ) )
				{
					include_once( "./API/Ops/get.php" ) ;

					$opinfo_ = Ops_get_OpInfoByID( $dbh, $_COOKIE["phplive_opID"] ) ;
					if ( $opinfo_["lastactive"] > time()-25 )
					{
						Ops_update_OpValue( $dbh, $_COOKIE["phplive_opID"], "status", 0 ) ;
						Ops_update_OpValue( $dbh, $_COOKIE["phplive_opID"], "ses", "expired" ) ;
						$reload = 1 ;
					}
				}
				else
				{
					$ses = Util_Security_GenSetupSes() ;
					Ops_update_OpValue( $dbh, $opinfo["opID"], "lastactive", time() ) ;
					Ops_update_OpValue( $dbh, $opinfo["opID"], "ses", $ses ) ;
				}

				if ( $remember )
					setcookie( "phplive_adminLogin", $opinfo['login'], time()+(60*60*24*1095) ) ;
				else
					setcookie( "phplive_adminLogin", "", time()-1 ) ;

				setcookie( "phplive_opID", $opinfo['opID'], -1 ) ;
			}
			else
				$error = 1 ;
		}
	}
	else if ( $action == "logout" )
	{
		include_once( "./API/Ops/get.php" ) ;

		if ( $menu == "sa" )
		{
			include_once( "./API/SQL.php" ) ;
			include_once( "./API/Ops/update_ext.php" ) ;

			if ( isset( $_COOKIE["phplive_adminID"] ) ) { Ops_ext_update_AdminValue( $dbh, $_COOKIE["phplive_adminID"], "ses", "" ) ; setcookie( "phplive_adminID", FALSE ) ; }
		}
		else
		{
			include_once( "./API/SQL.php" ) ;
			include_once( "./API/Ops/update.php" ) ;

			if ( isset( $_COOKIE["phplive_opID"] ) ) { Ops_update_putOpStatus( $dbh, $_COOKIE["phplive_opID"], 0 ) ; Ops_update_OpValue( $dbh, $_COOKIE["phplive_opID"], "status", 0 ) ; Ops_update_OpValue( $dbh, $_COOKIE["phplive_opID"], "ses", "" ) ; setcookie( "phplive_opID", FALSE ) ; }
		}

		// main one included at chat_actions_op.php - put it here as well as placing
		// at action=logout does not work for too fast query.
		if ( !Ops_get_AnyOpsOnline( $dbh, 0 ) )
		{
			$initiate_dir = $CONF["TYPE_IO_DIR"] ; 
			$dh = dir( $initiate_dir ) ; 
			while( $file = $dh->read() ) { 
				if ( $file != "." && $file != ".." )
					unlink( "$initiate_dir/$file" ) ; 
			} 
			$dh->close() ;
		}
	}

	if ( !$login && isset( $_COOKIE["phplive_adminLogin"] ) && $_COOKIE["phplive_adminLogin"] )
		$login = $_COOKIE["phplive_adminLogin"] ;
?>
<?php include_once( "./inc_doctype.php" ) ?>
<!--
********************************************************************
* PHP Live! (c) OSI Codes Inc.
* www.phplivesupport.com
********************************************************************
-->
<head>
<title> PHP Live! Support <?php echo $VERSION ?> </title>

<meta name="author" content="osicodesinc">
<meta name="description" content="PHP Live! Support <?php echo $VERSION ?>">
<meta name="keywords" content="powered by: PHP Live!  www.phplivesupport.com">
<meta name="robots" content="all,index,follow">
<meta http-equiv="content-type" content="text/html; CHARSET=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="Stylesheet" href="./css/base_setup.css?<?php echo $VERSION ?>">
<script type="text/javascript" src="./js/global.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="./js/global_chat.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="./js/setup.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="./js/framework.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="./js/framework_cnt.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="./js/jquery.tools.min.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="./js/jquery.md5.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="./js/winapp.js?<?php echo $VERSION ?>"></script>

<script type="text/javascript">
<!--
	var loaded = 1 ;
	var base_url = "." ;
	var widget = 0 ;
	var screen_ = ( typeof( phplive_wp ) != "undefined" ) ? "separate" : "<?php echo $CONF["screen"] ?>" ;

	$(document).ready(function()
	{
		$("body").show() ;
		init_menu() ;

		<?php if ( $error ): ?>do_alert( 0, 'Invalid login or password.' ) ;<?php endif ; ?>

		toggle_menu( "<?php echo $menu ?>" ) ;

		<?php if ( !preg_match( "/(submit)|(logout)/", $action ) && !$error ): ?>
		flashembed( "flash_result", {
			src: "./media/expressInstall.swf",
			version: [8, 0],
			expressInstall: "./media/expressInstall.swf",
			onFail: function() {
				<?php if ( !$mobile ): ?>$('#flash_detect').show() ;<?php endif ; ?>
			}
		});
		<?php endif ; ?>


		<?php
			if ( ( $action == "submit" ) && ( $menu == "operator" ) && !$error )
			{
				if ( $reload )
				{
					print "$('#div_reload').show() ;" ;
					print "setTimeout( function(){ $('#theform').submit() ; }, 15000 ) ;" ;
				}
				else
				{
					print "$('#div_sound').show() ; play_sound( \"login_op\", \"new_request_$opinfo[sound1]\" ) ;" ;
					if ( $wp || $auto )
						print "setTimeout( function(){ location.href='ops/operator.php?ses=$ses&wp=$wp' ; }, 4000 ) ;" ;
					else
						print "setTimeout( function(){ location.href='ops/?ses=$ses' ; }, 4000 ) ;" ;
				}
			}
		?>


		if ( <?php echo $wp ?> && ( "<?php echo $action ?>" != "logout" ) )
			window.external.wp_save_history( location.href.replace( /index.php.+/i, "winapp.php" ) ) ;
		else if ( <?php echo $wp ?> && ( "<?php echo $action ?>" == "logout" ) )
			wp_total_visitors( 0 ) ;
	});

	function toggle_menu( themenu )
	{
		$('#popout').hide() ;

		if ( themenu == "sa" )
		{
			$('#login_remember').hide() ;
			$('#body_sub_title').html( "<img src=\"./pics/icons/user_key.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Setup Login" ) ;
			$('#btn_login').val( "Login as Setup" ) ;

			$('#phplive_login').val( "" ) ;
			if ( screen_ == "same" ) { $('#menu_operator').show() ; }

			if ( typeof( phplive_wp ) != "undefined" )
				$('#popout').show() ;
		}
		else
		{
			$('#login_remember').show() ;
			$('#body_sub_title').html( "<img src=\"./pics/icons/user_key.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Operator Login" ) ;
			$('#btn_login').val( "Login as Operator" ) ;

			$('#phplive_login').val( "<?php echo ( $login ) ? $login : "" ?>" ) ;
			if ( screen_ == "same" ) { $('#menu_sa').show() ; }
		}

		$('#menu_sa').removeClass("menu_focus").addClass("menu") ;
		$('#menu_operator').removeClass("menu_focus").addClass("menu") ;

		$('#menu_'+themenu).removeClass("menu").addClass("menu_focus").show() ;
		$('#menu').val( themenu ) ;
	}

	function do_login()
	{
		if ( $('#phplive_login').val() == "" )
			do_alert( 0, "Blank login is invalid." ) ;
		else if ( $('#phplive_password').val() == "" )
			do_alert( 0, "Blank password is invalid." ) ;
		else
		{
			var md5_password = $.md5( $.md5($('#phplive_password').val())+'<?php echo $token ?>' ) ;
			$('#phplive_password').val( md5_password ) ;
			$('#theform').submit() ;
		}
	}

	function input_text_listen( e )
	{
		var key = -1 ;
		var shift ;

		key = e.keyCode ;
		shift = e.shiftKey ;

		if ( !shift && ( ( key == 13 ) || ( key == 10 ) ) )
			do_login() ;
	}
//-->
</script>
</head>
<body style="display: none; overflow: hidden;">

<div id="body" style="padding-bottom: 60px;">
	<div id="body_wrapper" style="z-Index: 5;"></div>
	<div style="width: 100%; z-Index: 10;">
		<div style="width: 480px; height: 180px; margin: 0 auto;">
			<div id="body_sub_title"></div>

			<form method="POST" action="index.php?submit" id="theform">
			<input type="hidden" name="action" value="submit">
			<input type="hidden" name="auto" value="<?php echo $auto ?>">
			<input type="hidden" name="wp" value="<?php echo $wp ?>">
			<input type="hidden" name="menu" id="menu" value="">
			<input type="hidden" name="wpress" id="wpress" value="<?php echo $wpress ?>">
			<table cellspacing=0 cellpadding=5 border=0 style="margin-top: 25px;">
			<tr>
				<td>Login</td>
				<td> <input type="text" class="input" name="phplive_login" id="phplive_login" size="15" maxlength="15" value="<?php echo ( $login ) ? $login : "" ?>" onKeyPress="return noquotestags(event)" onKeyup="input_text_listen(event);"></td>
				<td>Password</td>
				<td> <input type="password" class="input" name="phplive_password" id="phplive_password" size="15" maxlength="35" value="<?php echo ( isset( $password ) && $reload ) ? $password : "" ; ?>" onKeyPress="return noquotes(event)" onKeyup="input_text_listen(event);"></td>
			</tr>
			<tr>
				<td></td>
				<td colspan=3><div id="login_remember" style="display: none;"><input type="checkbox" name="remember" id="remember" value=1 <?php echo ( isset( $_COOKIE["phplive_adminLogin"] ) && $_COOKIE["phplive_adminLogin"] ) ? "checked" :  "" ; ?>> remember login</div></td>
			</tr>
			<tr>
				<td></td><td colspan=3><div style="margin-top: 10px;"><input type="button" id="btn_login" value="Login as Operator" onClick="do_login()"> <?php if ( ( $CONF["screen"] == "separate" ) && ( $menu == "sa" ) ): ?> or <a href="./" target="new">login as operator</a> <?php endif ; ?></div></td>
			</tr>
			<tr><td></td><td colspan=3><div id="popout" style="margin-top: 35px; display: none;">&bull; you can also access the setup in a <a href="./setup/" target="snew">new window</a></div></td></tr>
			</table>
			</form>
		
		</div>

		<div style="width: 100%;">
			<div style="width: 480px; margin: 0 auto; margin-top: 60px; font-size: 10px; background: url( ./pics/bg_footer.gif ) repeat-x; border-left: 1px solid #B1B4B5; border-right: 1px solid #B1B4B5; border-bottom: 1px solid #B1B4B5; color: #606060; -moz-border-radius: 5px; border-radius: 5px;">
				<div style="float: left; padding: 3px; padding-top: 7px;"><a href="http://www.phplivesupport.com/?plk=osicodes-5-ykq-m&key=<?php echo $KEY ?>" target="new">PHP Live!</a> &copy; OSI Codes Inc. </div>
				<div style="float: left; width: 2px; height: 26px; background: url( ./pics/h_divider.gif ) no-repeat;"></div>
				<div style="float: left; padding: 3px; padding-top: 7px;"><a href="http://www.phplivesupport.com/help_desk.php?&plk=osicodes-5-ykq-m&key=<?php echo $KEY ?>" target="new">Help Desk</a></div>
				<div style="float: left; width: 2px; height: 26px; background: url( ./pics/h_divider.gif ) no-repeat;"></div>
				<div style="clear: both;"></div>
			</div>
		</div>
	</div>
</div>
<div style="position: absolute; top: 0px; left: 0px; width: 100%; z-index: 13;">
	<div style="width: 480px; margin: 0 auto; background: url( ./pics/bg_trans.png ) repeat; border-bottom-left-radius: 5px 5px; -moz-border-radius-bottomleft: 5px 5px; border-bottom-right-radius: 5px 5px; -moz-border-radius-bottomright: 5px 5px;">
		<div id="menu_wrapper">
			<div id="menu_operator" class="menu" style="display: none;" onClick="toggle_menu('operator')">Operator Login</div>
			<div id="menu_sa" class="menu" style="display: none;" onClick="toggle_menu('sa')">Setup Login</div>
			<div style="clear: both;"></div>
		</div>
	</div>
</div>
<div style="position: absolute; top: 14px; left: 0px; width: 100%; z-index: 12;">
	<div style="width: 480px; margin: 0 auto; padding-top: 31px;">
		<div style="font-size: 10px; font-weight: bold; color: #CBD1D5; text-align: right;"><span style="font-size: 16px; text-shadow: #5A6787 -1px -1px;">PHP Live! v.<?php echo $VERSION ?></span></div>
	</div>
</div>

<div style="position: absolute; background: url( ./pics/bg_fade_bottom.png ) repeat-x; background-position: bottom; top: 0px; left: 0px; width: 100%; height: 60px; z-index: 11;"></div>

<div id="div_sound" style="display: none; position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; background: url( ./pics/bg_trans.png ) repeat; overflow: hidden; z-index: 20;">
	<div style="position: relative; width: 460px; margin: 0 auto; top: 130px; text-align: center;">
		<div class="info_box" style="font-size: 14px; font-weight: bold;">
			<img src="pics/icons/vcard.png" width="16" height="16" border="0" alt=""> Authentication success.  Logging in...
			<div id="sounds" style="width: 1px; height: 1px; overflow: hidden; opacity:0.0; filter:alpha(opacity=0);">
				<div id="div_sounds_login_op"></div>
			</div>
		</div>
	</div>
</div>

<div id="div_reload" style="display: none; position: absolute; top: 0px; left: 0px; width: 100%; height: 2000px; background: url( ./pics/bg_trans.png ) repeat; overflow: hidden; z-index: 20;">
	<div style="position: relative; width: 460px; margin: 0 auto; top: 130px; text-align: center;">
		<div class="info_box" style="font-size: 14px; font-weight: bold;">
			<img src="pics/loading_bar.gif" width="16" height="16" border="0" alt=""> Multiple sessions detected.  Just a moment...
		</div>
	</div>
</div>

<?php include_once( "./inc_flash.php" ) ; ?>

<!-- [winapp=4] -->

</body>
</html>
<?php
	if ( isset( $dbh ) && isset( $dbh['con'] ) )
		database_mysql_close( $dbh ) ;
?>
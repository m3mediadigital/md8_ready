<?php
	if ( defined( 'API_Chat_put' ) ) { return ; }
	define( 'API_Chat_put', true ) ;

	/****************************************************************/
	FUNCTION Chat_put_Request( &$dbh,
					$deptid,
					$opid,
					$status,
					$initiate,
					$op2op,
					$etrans,
					$os,
					$browser,
					$ces,
					$resolution,
					$vname,
					$vemail,
					$ip,
					$agent,
					$onpage,
					$title,
					$question,
					$marketid,
					$refer,
					$custom,
					$auto_pop = 0 )
	{
		if ( ( $deptid == "" ) || ( $opid == "" ) || ( $os == "" ) || ( $browser == "" )
			|| ( $ces == "" ) || ( $vname == "" ) || ( $ip == "" )
			|| ( $agent == "" ) || ( $question == "" ) )
			return false ;

		global $CONF ;
		global $VARS_SMS_BUFFER ;
		global $opinfo_next ;
		if ( !defined( 'API_Chat_get' ) )
			include_once( "$CONF[DOCUMENT_ROOT]/API/Chat/get.php" ) ;

		$now = ( isset( $opinfo_next["sms"] ) && ( $opinfo_next["sms"] == 1 ) ) ? time() + $VARS_SMS_BUFFER : time() ;
		$hostname = gethostbyaddr( $ip ) ;
		$onpage = strip_tags( $onpage ) ;

		LIST( $deptid, $opid, $status, $initiate, $op2op, $etrans, $os, $browser, $ces, $resolution, $vname, $vemail, $ip, $hostname, $agent, $onpage, $title, $question, $marketid, $refer, $custom, $auto_pop ) = database_mysql_quote( $deptid, $opid, $status, $initiate, $op2op, $etrans, $os, $browser, $ces, $resolution, $vname, $vemail, $ip, $hostname, $agent, $onpage, $title, $question, $marketid, $refer, $custom, $auto_pop ) ;

		$requestinfo = Chat_get_RequestCesInfo( $dbh, $ces ) ;

		if ( isset( $requestinfo["requestID"] ) )
		{
			if ( $requestinfo["initiated"] )
			{
				if ( !defined( 'API_Chat_update' ) )
					include_once( "$CONF[DOCUMENT_ROOT]/API/Chat/update.php" ) ;

				Chat_update_RequestValue( $dbh, $requestinfo["requestID"], "status", 1 ) ;
				Chat_update_RequestValue( $dbh, $requestinfo["requestID"], "auto_pop", $auto_pop ) ;
			}

			return $requestinfo["requestID"] ;
		}
		else
		{
			$vupdated = time() ;
			$rstring = "AND p_operators.opID <> $opid" ;

			// todo: perhaps put total requests during activation of chat and not here
			$requests = Chat_get_IPTotalRequests( $dbh, $ip ) ;
			if ( !$requests )
				$requests = 1 ;

			$query = "INSERT INTO p_requests VALUES ( 0, $now, 0, $now, $vupdated, $status, $auto_pop, $initiate, $etrans, $deptid, $opid, $op2op, $marketid, $os, $browser, $requests, '$ces', '$resolution', '$vname', '$vemail', '$ip', '$hostname', '$agent', '$onpage', '$title', '$rstring', '$refer', '$custom', '$question' )" ;
			database_mysql_query( $dbh, $query ) ;

			if ( $dbh[ 'ok' ] )
			{
				if ( $initiate )
					touch( "$CONF[TYPE_IO_DIR]/$ip.txt" ) ;

				$id = database_mysql_insertid ( $dbh ) ;
				return $id ;
			}

			return false ;
		}
	}

	/****************************************************************/
	FUNCTION Chat_put_ReqLog( &$dbh,
					$requestid )
	{
		if ( $requestid == "" )
			return false ;

		LIST( $requestid ) = database_mysql_quote( $requestid ) ;

		$query = "INSERT INTO p_req_log ( ces, created, ended, status, initiated, etrans, deptID, opID, op2op, marketID, os, browser, resolution, vname, vemail, ip, hostname, agent, onpage, title, custom, question ) SELECT ces, created, 0, status, initiated, etrans, deptID, opID, op2op, marketID, os, browser, resolution, vname, vemail, ip, hostname, agent, onpage, title, custom, question FROM p_requests WHERE p_requests.requestID = $requestid" ;
		database_mysql_query( $dbh, $query ) ;

		if ( $dbh[ 'ok' ] )
			return true ;

		return false ;
	}

	/****************************************************************/
	FUNCTION Chat_put_Transcript( &$dbh,
					$ces,
					$status,
					$etrans,
					$created,
					$ended,
					$deptid,
					$opid,
					$initiated,
					$op2op,
					$rating,
					$fsize,
					$vname,
					$vemail,
					$ip,
					$question,
					$formatted,
					$plain )
	{
		if ( ( $ces == "" ) || ( $deptid == "" ) || ( $opid == "" ) || ( $fsize == "" )
			|| ( $ended == "" ) || ( $vname == "" ) || ( $ip == "" )
			|| ( $question == "" ) || ( $formatted == "" ) || ( $plain == "" ) )
			return false ;

		global $CONF ;
		if ( !defined( 'API_Ops_get' ) )
			include_once( "$CONF[DOCUMENT_ROOT]/API/Ops/get.php" ) ;
		if ( !defined( 'API_Depts_get' ) )
			include_once( "$CONF[DOCUMENT_ROOT]/API/Depts/get.php" ) ;
		if ( !defined( 'API_Util_Email' ) )
			include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Email.php" ) ;

		$success = 1 ;

		LIST( $ces, $status, $created, $ended, $deptid, $opid, $initiated, $op2op, $rating, $fsize, $vname, $vemail, $ip, $question, $formatted, $plain ) = database_mysql_quote( $ces, $status, $created, $ended, $deptid, $opid, $initiated, $op2op, $rating, $fsize, $vname, $vemail, $ip, $question, $formatted, $plain ) ;

		$query = "SELECT * FROM p_transcripts WHERE ces = '$ces' LIMIT 1" ;
		database_mysql_query( $dbh, $query ) ;
		$transcript = database_mysql_fetchrow( $dbh ) ;

		if ( !isset( $transcript["ces"] ) )
		{
			$query = "INSERT INTO p_transcripts VALUES ( '$ces', $created, $ended, $deptid, $opid, $initiated, $op2op, $rating, $fsize, '$vname', '$vemail', '$ip', '$question', '$formatted', '$plain' )" ;
			database_mysql_query( $dbh, $query ) ;
		}
		else if ( $created == "null" )
			$formatted = $transcript["formatted"] ;
		else
			$formatted = false ;

		if ( $dbh[ 'ok' ] && $formatted )
		{
			// todo: enhance it for formatted style
			$deptinfo = Depts_get_DeptInfo( $dbh, $deptid ) ;

			if ( $status && ( $deptinfo["temail"] || $deptinfo["temaild"] ) )
			{
				$opinfo = Ops_get_OpInfoByID( $dbh, $opid ) ;
				$vname = preg_replace( "/<v>/", "", $vname ) ;

				$lang = $CONF["lang"] ;
				if ( $deptinfo["lang"] )
					$lang = $deptinfo["lang"] ;
				include( "$CONF[DOCUMENT_ROOT]/lang_packs/$lang.php" ) ;

				$subject_visitor = $LANG["TRANSCRIPT_SUBJECT"]." $opinfo[name]" ;
				$subject_department = $LANG["TRANSCRIPT_SUBJECT"]." $vname" ;

				$message_trans = preg_replace( "/%%visitor%%/", $vname, $deptinfo["msg_email"] ) ;
				$message_trans = preg_replace( "/%%operator%%/", $opinfo["name"], $message_trans ) ;
				$message_trans = preg_replace( "/%%op_email%%/", $opinfo["email"], $message_trans ) ;
				$message_trans = preg_replace( "/%%transcript%%/", preg_replace( "/\\$/", "-dollar-", stripslashes( $formatted ) ), $message_trans ) ;

				if ( $etrans )
				{
					if ( !Util_Email_SendEmail( $opinfo["name"], $opinfo["email"], $vname, $vemail, $subject_visitor, $message_trans, "trans" ) )
						$success = 0 ;
				}
				
				if ( $deptinfo["temaild"] )
				{
					if ( !Util_Email_SendEmail( $opinfo["name"], $opinfo["email"], $deptinfo["name"], $deptinfo["email"], $subject_department, "---\r\n$subject_department <$vemail>\r\n--\r\n$message_trans", "trans" ) )
						$success = 0 ;
				}

				if ( $success )
					return true ;
				else
					return false ;
			}
			else
				return true ;
		}

		return false ;
	}

?>
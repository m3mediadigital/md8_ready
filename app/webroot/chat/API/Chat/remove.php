<?php
	if ( defined( 'API_Chat_remove' ) ) { return ; }
	define( 'API_Chat_remove', true ) ;

	/****************************************************************/
	FUNCTION Chat_remove_Request( &$dbh,
						$requestid )
	{
		if ( $requestid == "" )
			return false ;

		LIST( $requestid ) = database_mysql_quote( $requestid ) ;

		$query = "DELETE FROM p_requests WHERE requestID = $requestid" ;
		database_mysql_query( $dbh, $query ) ;

		return true ;
	}

	/****************************************************************/
	FUNCTION Chat_remove_RequestByCes( &$dbh,
						$ces )
	{
		if ( $ces == "" )
			return false ;

		LIST( $ces ) = database_mysql_quote( $ces ) ;

		$query = "DELETE FROM p_requests WHERE ces = '$ces'" ;
		database_mysql_query( $dbh, $query ) ;

		return true ;
	}

	/****************************************************************/
	FUNCTION Chat_remove_OpDept( &$dbh,
						$opid,
						$deptid )
	{
		if ( ( $opid == "" ) || ( $deptid == "" ) )
			return false ;

		LIST( $opid, $deptid ) = database_mysql_quote( $opid, $deptid ) ;

		$query = "SELECT * FROM p_dept_ops WHERE deptID = $deptid AND opID = $opid" ;
		database_mysql_query( $dbh, $query ) ;
		$data = database_mysql_fetchrow( $dbh ) ;

		$query = "DELETE FROM p_dept_ops WHERE deptID = $deptid AND opID = $opid" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "UPDATE p_dept_ops SET display = display-1 WHERE deptID = $deptid AND display >= $data[display]" ;
		database_mysql_query( $dbh, $query ) ;

		return true ;
	}

	/****************************************************************/
	FUNCTION Chat_remove_ExpiredOp2OpRequests( &$dbh )
	{
		global $VARS_EXPIRED_OP2OP ;
		$expired_op2op = time() - $VARS_EXPIRED_OP2OP ;

		// only time it is expired is when both operators are no longer online.  otherwise don't
		// expire it because a console is still open and they may be reading the transcript.  allow
		// the operator to clean close the chat.
		$query = "DELETE FROM p_requests WHERE updated < $expired_op2op AND vupdated < $expired_op2op AND op2op <> 0" ;
		database_mysql_query( $dbh, $query ) ;

		return true ;
	}

	/****************************************************************/
	FUNCTION Chat_remove_OldRequests( &$dbh )
	{
		global $CONF ;

		global $VARS_EXPIRED_REQS ;
		$expired = time() - $VARS_EXPIRED_REQS ;
		$now = time() ;

		// cycle it so data is put in transcript .txt file for warning BEFORE delete
		// set it AFTER delete so it sets it on next pass to delete
		$query = "SELECT * FROM p_requests WHERE ( created < $expired AND ( updated < $expired OR vupdated < $expired ) ) AND op2op = 0" ;
		database_mysql_query( $dbh, $query ) ;
		if ( $dbh[ 'ok' ] )
		{
			if ( database_mysql_nresults( $dbh ) )
			{
				if ( !defined( 'API_Chat_Util' ) )
					include_once( "$CONF[DOCUMENT_ROOT]/API/Chat/Util.php" ) ;
				if ( !defined( 'API_Chat_put' ) )
					include_once( "$CONF[DOCUMENT_ROOT]/API/Chat/put.php" ) ;
				if ( !defined( 'API_Chat_get' ) )
					include_once( "$CONF[DOCUMENT_ROOT]/API/Chat/get.php" ) ;
				if ( !defined( 'API_Chat_update' ) )
					include_once( "$CONF[DOCUMENT_ROOT]/API/Chat/update.php" ) ;
				if ( !defined( 'API_Depts_get' ) )
					include_once( "$CONF[DOCUMENT_ROOT]/API/Depts/get.php" ) ;

				$lang = $CONF["lang"] ; $prev_deptid = 1111111111 ; // start things off
				$expired_requests = Array() ;
				while( $data = database_mysql_fetchrow( $dbh ) )
					$expired_requests[] = $data ;

				for ( $c = 0; $c < count( $expired_requests ); ++$c )
				{
					$request = $expired_requests[$c] ;
					$ces = $request["ces"] ;
					$ip = $request["ip"] ;
					$deptid = $request["deptID"] ;
					$trans_file = "$ces.txt" ;

					if ( $prev_deptid != $deptid )
					{
						$prev_deptid = $deptid ;
						$deptinfo = Depts_get_DeptInfo( $dbh, $deptid ) ;
						if ( $deptinfo["lang"] )
							$lang = $deptinfo["lang"] ;
						include( "$CONF[DOCUMENT_ROOT]/lang_packs/$lang.php" ) ;
					}

					$query = "UPDATE p_footprints_u SET chatting = 0 WHERE ip = '$ip'" ;
					database_mysql_query( $dbh, $query ) ;

					if ( file_exists( "$CONF[CHAT_IO_DIR]/$trans_file" ) )
					{
						UtilChat_AppendToChatfile( $trans_file, "<div class='cl'><disconnected><d6>".$LANG["CHAT_NOTIFY_DISCONNECT"]."</div>" ) ;

						$output = UtilChat_ExportChat( $trans_file ) ;
						if ( isset( $output[0] ) )
						{
							LIST( $formatted, $plain ) = $output ;

							$fsize = strlen( $formatted ) ;
							$requestinfo = Chat_get_RequestHistCesInfo( $dbh, $ces ) ;
							$ip = $requestinfo["ip"] ;
							
							// should be deleted but check to make sure
							if ( file_exists( "$CONF[TYPE_IO_DIR]/$ip.txt" ) )
								unlink( "$CONF[TYPE_IO_DIR]/$ip.txt" ) ;

							if ( !$requestinfo["ended"] )
								Chat_update_RequestLogValue( $dbh, $ces, "ended", $now ) ;

							if ( $requestinfo["status"] )
							{
								if ( Chat_put_Transcript( $dbh, $ces, $requestinfo["status"], $requestinfo["etrans"], $requestinfo["created"], $now, $requestinfo["deptID"], $requestinfo["opID"], $requestinfo["initiated"], $requestinfo["op2op"], 0, $fsize, $requestinfo["vname"],	$requestinfo["vemail"], $requestinfo["ip"], $requestinfo["question"], $formatted, $plain ) )
									Chat_remove_RequestByCes( $dbh, $ces ) ;
							}
						}
						UtilChat_RemoveChatfile( $trans_file ) ;
					}
				}

				$query = "DELETE FROM p_requests WHERE ( created < $expired AND ( updated < $expired OR vupdated < $expired ) ) AND op2op = 0" ;
				database_mysql_query( $dbh, $query ) ;
			}
		}

		return true ;
	}

	/****************************************************************/
	FUNCTION Chat_remove_Transcript( &$dbh,
						$ces,
						$created )
	{
		if ( ( $ces == "" ) || ( $created == "" ) )
			return false ;

		LIST( $ces, $created ) = database_mysql_quote( $ces, $created ) ;

		$query = "DELETE FROM p_transcripts WHERE ces = '$ces' AND created = $created" ;
		database_mysql_query( $dbh, $query ) ;
		
		if ( $dbh[ 'ok' ] )
		{
			$query = "DELETE FROM p_req_log WHERE ces = '$ces'" ;
			database_mysql_query( $dbh, $query ) ;

			return true ;
		}
		else
			return false ;
	}

?>

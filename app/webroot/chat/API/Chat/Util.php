<?php
	if ( defined( 'API_Chat_Util' ) ) { return ; }
	define( 'API_Chat_Util', true ) ;

	/*****************************************************************/
	FUNCTION UtilChat_AppendToChatfile( $chatfile,
							$string )
	{
		if ( ( $chatfile == "" ) || ( $string == "" ) )
			return false ;

		global $CONF ;
		$string .= "<>" ; // add new line marker

		if ( preg_match( "/^[56]/", phpversion() ) )
			file_put_contents( "$CONF[CHAT_IO_DIR]/$chatfile", $string, FILE_APPEND ) ;
		else
		{
			$fp = fopen( "$CONF[CHAT_IO_DIR]/$chatfile", "a" ) ;
			fwrite( $fp, $string, strlen( $string ) ) ;
			fclose( $fp ) ;
		}

		return true ;
	}

	/*****************************************************************/
	FUNCTION UtilChat_RemoveChatfile( $chatfile )
	{
		if ( $chatfile == "" )
			return false ;

		global $CONF ;

		if ( file_exists( "$CONF[CHAT_IO_DIR]/$chatfile" ) )
			unlink( "$CONF[CHAT_IO_DIR]/$chatfile" ) ;
		return true ;
	}

	/*****************************************************************/
	FUNCTION UtilChat_ExportChat( $chatfile )
	{
		if ( $chatfile == "" )
			return false ;

		global $CONF ;

		$output = Array() ;
		if ( file_exists( "$CONF[CHAT_IO_DIR]/$chatfile" ) )
		{
			$trans_raw = file_get_contents( "$CONF[CHAT_IO_DIR]/$chatfile" ) ;
			$output[] = $trans_raw ;
			$output[] = preg_replace( "/<(.*?)>/", "", preg_replace( "/<>/", "\r\n", $trans_raw ) ) ;
		}
		return $output ;
	}

	/*****************************************************************/
	FUNCTION UtilChat_WriteIsWriting( $theces, $theflag, $thewname, $thername )
	{
		if ( ( $theces == "" ) || ( $thewname == "" ) || ( $thername == "" ) )
			return false ;

		global $CONF ;

		$thewname = rawurldecode( $thewname ) ;
		$typing_file = "$theces.$thewname.txt" ;
		if ( file_exists( "$CONF[CHAT_IO_DIR]/$theces.txt" ) )
		{
			if ( $theflag )
			{
				if ( !file_exists( "$CONF[TYPE_IO_DIR]/$typing_file" ) )
					touch( "$CONF[TYPE_IO_DIR]/$typing_file" ) ;
			}
			else
			{
				if ( file_exists( "$CONF[TYPE_IO_DIR]/$typing_file" ) )
					unlink( "$CONF[TYPE_IO_DIR]/$typing_file" ) ;
			}

			return true ;
		}
		else
			return false ;
	}

	/*****************************************************************/
	FUNCTION UtilChat_CheckIsWriting( $theces, $thewname, $thername )
	{
		if ( ( $theces == "" ) || ( $thewname == "" ) || ( $thername == "" ) )
			return 0 ;

		global $CONF ;

		$typing_file = "$theces.$thername.txt" ;
		if ( file_exists( "$CONF[TYPE_IO_DIR]/$typing_file" ) )
			return 1 ;

		return 0 ;
	}

?>
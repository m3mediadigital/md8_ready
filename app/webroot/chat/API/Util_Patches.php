<?php
	//
	// Auto patching of PHP Live! system
	//

	/* auto patch of versions and needed modifications */
	if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/1" ) )
	{
		$query = "ALTER TABLE p_operators CHANGE signal signall TINYINT( 4 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;
		$query = "ALTER TABLE p_footprints_u ADD resolution VARCHAR( 15 ) NOT NULL AFTER browser" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.1.1" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/1" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/2" ) )
	{
		$query = "ALTER TABLE p_requests ADD etrans TINYINT( 1 ) NOT NULL AFTER status" ;
		database_mysql_query( $dbh, $query ) ;
		$query = "ALTER TABLE p_req_log ADD etrans TINYINT( 1 ) NOT NULL AFTER status" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.1.2" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/2" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/3" ) )
	{
		$query = "ALTER TABLE p_requests ADD tupdated INT NOT NULL AFTER created" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.1.3" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/3" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/4" ) )
	{
		$query = "ALTER TABLE p_vars ADD sm_fb TEXT NOT NULL, ADD sm_tw TEXT NOT NULL, ADD sm_yt TEXT NOT NULL, ADD sm_li TEXT NOT NULL " ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.1.4" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/4" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/5" ) )
	{
		$query = "ALTER TABLE p_requests ADD initiated TINYINT( 1 ) NOT NULL AFTER status" ;
		database_mysql_query( $dbh, $query ) ;
		$query = "ALTER TABLE p_req_log ADD initiated TINYINT( 1 ) NOT NULL AFTER status" ;
		database_mysql_query( $dbh, $query ) ;
		$query = "ALTER TABLE p_footprints_u ADD chatting TINYINT( 1 ) NOT NULL AFTER marketID" ;
		database_mysql_query( $dbh, $query ) ;
		$query = "ALTER TABLE p_reqstats ADD initiated_taken SMALLINT UNSIGNED NOT NULL AFTER initiated" ;
		database_mysql_query( $dbh, $query ) ;
		$query = "ALTER TABLE p_transcripts ADD initiated TINYINT( 1 ) NOT NULL AFTER opID" ;
		database_mysql_query( $dbh, $query ) ;
		$query = "ALTER TABLE p_footprints_u ADD agent VARCHAR( 200 ) NOT NULL AFTER hostname" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.1.54" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/5" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/6" ) )
	{
		// attempt to drop table to reset
		$query = "DROP TABLE IF EXISTS p_sm" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "CREATE TABLE p_sm ( deptID INT( 10 ) UNSIGNED NOT NULL , sm LONGTEXT NOT NULL , PRIMARY KEY ( deptID ) )" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "SELECT * FROM p_vars LIMIT 1" ;
		database_mysql_query( $dbh, $query ) ;
		$data = database_mysql_fetchrow( $dbh ) ;

		if ( isset( $data["sm_fb"] ) || isset( $data["sm_tw"] ) || isset( $data["sm_yt"] ) || isset( $data["sm_li"] ) )
		{
			$sm_fb = ( isset( $data["sm_fb"] ) ) ? $data["sm_fb"] : "" ;
			$sm_tw = ( isset( $data["sm_tw"] ) ) ? $data["sm_tw"] : "" ;
			$sm_yt = ( isset( $data["sm_yt"] ) ) ? $data["sm_yt"] : "" ;
			$sm_li = ( isset( $data["sm_li"] ) ) ? $data["sm_li"] : "" ;

			$sm_string = "$sm_fb-sm-$sm_tw-sm-$sm_yt-sm-$sm_li" ;
			$query = "INSERT INTO p_sm VALUES( 0, '$sm_string' )" ;
			database_mysql_query( $dbh, $query ) ;
		}

		$query = "ALTER TABLE p_vars DROP sm_fb, DROP sm_tw, DROP sm_yt, DROP sm_li" ;
		database_mysql_query( $dbh, $query ) ;

		// patching from very beginning to now needs to include lang var
		if ( !isset( $CONF["lang"] ) )
		{
			$CONF["lang"] = "english" ;

			$conf_vars = "\$CONF = Array() ;\n" ;
			foreach( $CONF as $key => $value )
			{
				if ( ( $key == "DOCUMENT_ROOT" ) && preg_match( "/WIN/i", PHP_OS ) )
					$conf_vars .= "\$CONF['$key'] = \"".addslashes( $CONF[$key] )."\" ;\n" ;
				else
					$conf_vars .= "\$CONF['$key'] = \"".$CONF[$key]."\" ;\n" ;
			}

			$conf_string = "< php\n	$conf_vars" ;
			$conf_string .= "	if ( phpversion() >= \"5.1.0\" ){ date_default_timezone_set( \$CONF[\"TIMEZONE\"] ) ; }\n" ;
			$conf_string .= "	include_once( \"\$CONF[DOCUMENT_ROOT]/API/Util_Vars.php\" ) ;\n?>" ;
			$conf_string = preg_replace( "/< php/", "<?php", $conf_string ) ;

			if ( $fp = fopen( realpath( "$CONF[DOCUMENT_ROOT]/web/config.php" ), "w" ) )
			{
				fwrite( $fp, $conf_string, strlen( $conf_string ) ) ;
				fclose( $fp ) ;
			}
		}

		Util_Vals_WriteVersion( "4.1.55" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/6" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/7" ) )
	{ Util_Vals_WriteVersion( "4.1.56" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/7" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/8" ) )
	{ Util_Vals_WriteVersion( "4.1.57" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/8" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/9" ) )
	{
		$query = "ALTER TABLE p_departments ADD lang VARCHAR( 15 ) NOT NULL AFTER rtime" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "UPDATE p_departments SET lang = '$CONF[lang]'" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.1.58" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/9" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/10" ) )
	{ Util_Vals_WriteVersion( "4.1.59" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/10" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/11" ) )
	{ Util_Vals_WriteVersion( "4.1.60" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/11" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/12" ) )
	{ Util_Vals_WriteVersion( "4.1.61" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/12" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/13" ) )
	{ Util_Vals_WriteVersion( "4.1.62" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/13" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/14" ) )
	{
		$query = "ALTER TABLE p_departments ADD temail TINYINT NOT NULL AFTER texpire" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_reqstats ADD rateit SMALLINT UNSIGNED NOT NULL , ADD ratings SMALLINT UNSIGNED NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_operators ADD ces VARCHAR( 32 ) NOT NULL AFTER ses, ADD rating TINYINT NOT NULL AFTER ces" ;
		database_mysql_query( $dbh, $query ) ;

		if ( $dbh["error"] == "None" )
		{
			$query = "UPDATE p_departments SET temail = 1" ;
			database_mysql_query( $dbh, $query ) ;

			$query = "UPDATE p_departments SET msg_email = 'Hello %%visitor%%,\r\n\r\nThank you for taking the time to chat with us.  Below is the complete transcript for your reference:\r\n\r\n%%transcript%%\r\n\r\nThank you,\r\n%%operator%%\r\n%%op_email%%\r\n'" ;
			database_mysql_query( $dbh, $query ) ;

			$dates = Array() ;
			$query = "SELECT * FROM p_reqstats ORDER BY sdate ASC" ;
			database_mysql_query( $dbh, $query ) ;
			while ( $data = database_mysql_fetchrow( $dbh ) )
				$dates[] = $data ;

			for( $c = 0; $c < count( $dates ); ++$c )
			{
				$date = $dates[$c] ;
				$deptid = $date["deptID"] ;
				$opid = $date["opID"] ;

				$stat_start = mktime( 0, 0, 1, date( "m", $date["sdate"] ), date( "j", $date["sdate"] ), date( "Y", $date["sdate"] ) ) ;
				$stat_end = mktime( 0, 0, 1, date( "m", $date["sdate"] ), date( "j", $date["sdate"] )+1, date( "Y", $date["sdate"] ) ) ; ;

				$ratings = Array() ;
				$query = "SELECT count(*) AS rateit, SUM(rating) AS ratings FROM p_transcripts WHERE deptID = $deptid AND opID = $opid AND opID <> 0 AND created >= $stat_start AND created < $stat_end AND rating <> 0" ;
				database_mysql_query( $dbh, $query ) ;
				while ( $data = database_mysql_fetchrow( $dbh ) )
					$ratings[] = $data ;

				for ( $c2 = 0; $c2 < count( $ratings ); ++$c2 )
				{
					$rating = $ratings[$c2] ;
					$query = "UPDATE p_reqstats SET rateit = $rating[rateit], ratings = $rating[ratings] WHERE sdate = $date[sdate] AND deptID = $deptid AND opID = $opid" ;
					database_mysql_query( $dbh, $query ) ;
				}
			}
		}

		Util_Vals_WriteVersion( "4.1.7" ) ;
		if ( file_exists( "$CONF[CHAT_IO_DIR]/TIMESTAMP" ) )
			unlink( "$CONF[CHAT_IO_DIR]/TIMESTAMP" ) ;

		touch( "$CONF[DOCUMENT_ROOT]/web/patches/14" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/15" ) )
	{ Util_Vals_WriteVersion( "4.1.72" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/15" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/16" ) )
	{
		$query = "ALTER TABLE p_footprints_u DROP INDEX created" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_footprints_u ADD INDEX ( created , deptID )" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_footprints DROP INDEX mdfive" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_footprints ADD INDEX ( mdfive )" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_departments ADD temaild TINYINT NOT NULL AFTER temail" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_ips DROP INDEX created" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_ips ADD INDEX ( created )" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_ips ADD i_footprints INT UNSIGNED NOT NULL AFTER t_initiate , ADD i_timestamp INT UNSIGNED NOT NULL AFTER i_footprints , ADD i_initiate INT UNSIGNED NOT NULL AFTER i_timestamp" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_dept_ops DROP INDEX display" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_dept_ops ADD INDEX ( display , visible , status )" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_dept_ops ADD status TINYINT NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "TRUNCATE TABLE p_footstats" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_footstats DROP INDEX sdate" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_footstats ADD mdfive VARCHAR( 32 ) NOT NULL AFTER sdate" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_footstats ADD PRIMARY KEY ( sdate , mdfive )" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_refer DROP INDEX created" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_refer ADD INDEX ( created )" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "TRUNCATE TABLE p_referstats" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_referstats DROP INDEX sdate" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_referstats ADD mdfive VARCHAR( 32 ) NOT NULL AFTER sdate" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_referstats ADD PRIMARY KEY ( sdate , mdfive )" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.1.8" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/16" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/17" ) )
	{ Util_Vals_WriteVersion( "4.1.81" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/17" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/18" ) )
	{ Util_Vals_WriteVersion( "4.1.82" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/18" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/19" ) )
	{ Util_Vals_WriteVersion( "4.1.83" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/19" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/20" ) )
	{ Util_Vals_WriteVersion( "4.1.84" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/20" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/21" ) )
	{
		$query = "ALTER TABLE p_requests CHANGE agent agent VARCHAR( 255 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "DROP TABLE IF EXISTS p_geo_bloc" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "CREATE TABLE p_geo_bloc ( startIpNum int(10) unsigned NOT NULL, endIpNum int(10) unsigned NOT NULL, locId int(10) unsigned NOT NULL, network mediumint(6) unsigned NOT NULL, PRIMARY KEY (endIpNum), KEY network (network) )" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "DROP TABLE IF EXISTS p_geo_loc" ;
		database_mysql_query( $dbh, $query ) ;

		if ( $mysql_old )
			$query = "CREATE TABLE p_geo_loc ( locId int(10) unsigned NOT NULL, country char(2) NOT NULL, region char(42) NOT NULL, city varchar(50) DEFAULT NULL, latitude float DEFAULT NULL, longitude float DEFAULT NULL, PRIMARY KEY (locId) )" ;
		else
			$query = "CREATE TABLE p_geo_loc ( locId int(10) unsigned NOT NULL, country char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, region char(42) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, city varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL, latitude float DEFAULT NULL, longitude float DEFAULT NULL, PRIMARY KEY (locId) )" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_footprints_u ADD country CHAR( 2 ) NOT NULL, ADD region CHAR( 42 ) NOT NULL, ADD city CHAR( 50 ) NOT NULL, ADD latitude FLOAT NOT NULL, ADD longitude FLOAT NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.2" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/21" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/22" ) )
	{ Util_Vals_WriteVersion( "4.2.1" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/22" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/23" ) )
	{ Util_Vals_WriteVersion( "4.2.2" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/23" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/24" ) )
	{ Util_Vals_WriteVersion( "4.2.3" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/24" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/25" ) )
	{ Util_Vals_WriteVersion( "4.2.4" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/25" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/26" ) )
	{ Util_Vals_WriteVersion( "4.2.5" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/26" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/27" ) )
	{ Util_Vals_WriteVersion( "4.2.6" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/27" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/28" ) )
	{ Util_Vals_WriteVersion( "4.2.7" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/28" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/29" ) )
	{ Util_Vals_WriteVersion( "4.2.8" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/29" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/30" ) )
	{ Util_Vals_WriteVersion( "4.2.9" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/30" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/31" ) )
	{ Util_Vals_WriteVersion( "4.2.11" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/31" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/32" ) )
	{ Util_Vals_WriteVersion( "4.2.12" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/32" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/33" ) )
	{ Util_Vals_WriteVersion( "4.2.13" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/33" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/34" ) )
	{ Util_Vals_WriteVersion( "4.2.14" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/34" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/35" ) )
	{
		$query = "ALTER TABLE p_vars ADD position TINYINT( 1 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.2.15" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/35" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/36" ) )
	{ Util_Vals_WriteVersion( "4.2.16" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/36" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/37" ) )
	{ Util_Vals_WriteVersion( "4.2.17" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/37" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/38" ) )
	{ Util_Vals_WriteVersion( "4.2.18" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/38" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/39" ) )
	{ Util_Vals_WriteVersion( "4.2.19" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/39" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/40" ) )
	{ Util_Vals_WriteVersion( "4.2.91" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/40" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/41" ) )
	{ Util_Vals_WriteVersion( "4.2.92" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/41" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/42" ) )
	{ Util_Vals_WriteVersion( "4.2.93" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/42" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/43" ) )
	{ Util_Vals_WriteVersion( "4.2.94" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/43" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/44" ) )
	{ Util_Vals_WriteVersion( "4.2.95" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/44" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/45" ) )
	{ Util_Vals_WriteVersion( "4.2.96" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/45" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/46" ) )
	{ Util_Vals_WriteVersion( "4.2.97" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/46" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/47" ) )
	{
		$query = "SELECT * FROM p_sm" ;
		database_mysql_query( $dbh, $query ) ;
		if ( isset( $dbh["result"] ) && $dbh["result"] )
		{
			$socials = Array() ;
			while ( $data = database_mysql_fetchrow( $dbh ) )
				$socials[] = $data ;

			if ( $mysql_old )
				$query = "CREATE TABLE p_socials ( deptID int(10) unsigned NOT NULL, status tinyint(1) NOT NULL, social varchar(15) NOT NULL, tooltip varchar(55) NOT NULL, url varchar(255) NOT NULL, UNIQUE KEY deptID (deptID,social) )" ;
			else
				$query = "CREATE TABLE p_socials ( deptID int(10) unsigned NOT NULL, status tinyint(1) NOT NULL, social varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, tooltip varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, url varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, UNIQUE KEY deptID (deptID,social) )" ;
			database_mysql_query( $dbh, $query ) ;

			for ( $c = 0; $c < count( $socials ); ++$c )
			{
				$sm = $socials[$c] ;

				$deptid = $sm["deptID"] ;
				$sm_fb_array = $sm_tw_array = $sm_yt_array = $sm_li_array = Array() ;
				LIST( $sm_fb, $sm_tw, $sm_yt, $sm_li ) = explode( "-sm-", $sm["sm"] ) ;
				$sm_fb_array = unserialize( $sm_fb ) ;
				$sm_tw_array = unserialize( $sm_tw ) ;
				$sm_yt_array = unserialize( $sm_yt ) ;
				$sm_li_array = unserialize( $sm_li ) ;

				LIST( $tooltip, $url ) = database_mysql_quote( $sm_fb_array["tooltip"], $sm_fb_array["url"] ) ;
				if ( $url )
				{
					$query = "INSERT INTO p_socials VALUES( $deptid, $sm_fb_array[status], 'facebook', '$tooltip', '$url')" ;
					database_mysql_query( $dbh, $query ) ;
				}

				LIST( $tooltip, $url ) = database_mysql_quote( $sm_tw_array["tooltip"], $sm_tw_array["url"] ) ;
				if ( $url )
				{
					$query = "INSERT INTO p_socials VALUES( $deptid, $sm_tw_array[status], 'twitter', '$tooltip', '$url')" ;
					database_mysql_query( $dbh, $query ) ;
				}

				LIST( $tooltip, $url ) = database_mysql_quote( $sm_yt_array["tooltip"], $sm_yt_array["url"] ) ;
				if ( $url )
				{
					$query = "INSERT INTO p_socials VALUES( $deptid, $sm_yt_array[status], 'youtube', '$tooltip', '$url')" ;
					database_mysql_query( $dbh, $query ) ;
				}

				LIST( $tooltip, $url ) = database_mysql_quote( $sm_li_array["tooltip"], $sm_li_array["url"] ) ;
				if ( $url )
				{
					$query = "INSERT INTO p_socials VALUES( $deptid, $sm_li_array[status], 'linkedin', '$tooltip', '$url')" ;
					database_mysql_query( $dbh, $query ) ;
				}
			}

			$query = "DROP TABLE p_sm" ;
			database_mysql_query( $dbh, $query ) ;
		}

		Util_Vals_WriteVersion( "4.2.98" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/47" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/48" ) )
	{ Util_Vals_WriteVersion( "4.2.99" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/48" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/49" ) )
	{ Util_Vals_WriteVersion( "4.2.99-1" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/49" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/50" ) )
	{
		$query = "ALTER TABLE p_operators ADD sound VARCHAR( 15 ) NOT NULL AFTER theme" ;
		database_mysql_query( $dbh, $query ) ;
		if ( $dbh["error"] == "None" )
		{
			$query = "UPDATE p_operators SET sound = 'default'" ;
			database_mysql_query( $dbh, $query ) ;
		}

		Util_Vals_WriteVersion( "4.2.99-2" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/50" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/51" ) )
	{
		$charset = ( $mysql_old ) ? "" : "CHARACTER SET utf8 COLLATE utf8_general_ci" ;

		$query = "ALTER TABLE p_req_log CHANGE agent agent VARCHAR( 255 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_canned CHANGE title title VARCHAR( 80 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_canned CHANGE message message MEDIUMTEXT $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_departments CHANGE name name VARCHAR( 80 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_departments CHANGE msg_greet msg_greet TEXT $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_departments CHANGE msg_offline msg_offline TEXT $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_departments CHANGE msg_email msg_email TEXT $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_external CHANGE name name VARCHAR( 40 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_footprints CHANGE title title VARCHAR( 150 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_footprints_u CHANGE title title VARCHAR( 150 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_marketing CHANGE name name VARCHAR( 80 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_marquees CHANGE snapshot snapshot VARCHAR( 80 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_marquees CHANGE message message VARCHAR( 255 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_operators CHANGE name name VARCHAR( 80 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_requests CHANGE vname vname VARCHAR( 80 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_requests CHANGE title title VARCHAR( 150 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;
		
		$query = "ALTER TABLE p_requests CHANGE question question TEXT $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_req_log CHANGE vname vname VARCHAR( 40 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_req_log CHANGE title title VARCHAR( 150 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_req_log CHANGE question question TEXT $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_socials CHANGE tooltip tooltip VARCHAR( 80 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_transcripts CHANGE vname vname VARCHAR( 80 ) $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_transcripts CHANGE question question TEXT $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_transcripts CHANGE formatted formatted TEXT $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_transcripts CHANGE plain plain TEXT $charset NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.2.99-3" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/51" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/52" ) )
	{ Util_Vals_WriteVersion( "4.2.99-4" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/52" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/53" ) )
	{
		$query = "ALTER TABLE p_requests ADD auto_pop TINYINT NOT NULL AFTER status" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_footprints CHANGE ip ip VARCHAR( 45 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_footprints_u CHANGE ip ip VARCHAR( 45 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_ips CHANGE ip ip VARCHAR( 45 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_refer CHANGE ip ip VARCHAR( 45 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_requests CHANGE ip ip VARCHAR( 45 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_req_log CHANGE ip ip VARCHAR( 45 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_transcripts CHANGE ip ip VARCHAR( 45 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_footprints_u CHANGE agent agent VARCHAR( 255 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_operators ADD viewip TINYINT NOT NULL AFTER traffic" ;
		database_mysql_query( $dbh, $query ) ;
		if ( $dbh['error'] == "None" )
		{
			$query = "UPDATE p_operators SET viewip = 1" ;
			database_mysql_query( $dbh, $query ) ;
		}

		$query = "ALTER TABLE p_departments ADD remail TINYINT NOT NULL AFTER texpire" ;
		database_mysql_query( $dbh, $query ) ;
		if ( $dbh['error'] == "None" )
		{
			$query = "UPDATE p_departments SET remail = 1" ;
			database_mysql_query( $dbh, $query ) ;
		}

		Util_Vals_WriteVersion( "4.2.99-5" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/53" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/54" ) )
	{ Util_Vals_WriteVersion( "4.2.99-6" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/54" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/55" ) )
	{ Util_Vals_WriteVersion( "4.2.99-7" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/55" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/56" ) )
	{
		$query = "ALTER TABLE p_operators ADD sms INT UNSIGNED NOT NULL AFTER rating, ADD smsnum VARCHAR( 65 ) NOT NULL AFTER sms" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.2.99-8" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/56" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/57" ) )
	{ Util_Vals_WriteVersion( "4.2.99-9" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/57" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/58" ) )
	{ Util_Vals_WriteVersion( "4.2.100" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/58" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/59" ) )
	{
		if ( !isset( $CONF["SALT"] ) )
			Util_Vals_WriteToConfFile( "SALT", Util_Format_RandomString( 10 ) ) ;

		$query = "ALTER TABLE p_operators DROP INDEX lastactive" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_operators DROP INDEX status" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_operators ADD INDEX ( status )" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_requests DROP INDEX updated" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_operators ADD dn TINYINT( 1 ) UNSIGNED NOT NULL AFTER viewip" ;
		database_mysql_query( $dbh, $query ) ;
		if ( $dbh['error'] == "None" )
		{
			$query = "UPDATE p_operators SET dn = 1" ;
			database_mysql_query( $dbh, $query ) ;
		}

		$query = "ALTER TABLE p_operators CHANGE viewip viewip TINYINT( 1 ) UNSIGNED NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_operators CHANGE traffic traffic TINYINT( 1 ) UNSIGNED NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_operators CHANGE op2op op2op TINYINT( 1 ) UNSIGNED NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_operators CHANGE rate rate TINYINT( 1 ) UNSIGNED NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.2.101" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/59" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/60" ) )
	{
		$query = "ALTER TABLE p_req_log DROP INDEX created" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_req_log DROP INDEX deptID" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_req_log ADD INDEX ( created, deptID )" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.2.102" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/60" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/61" ) )
	{ Util_Vals_WriteVersion( "4.2.103" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/61" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/62" ) )
	{ Util_Vals_WriteVersion( "4.2.104" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/62" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/63" ) )
	{
		$query = "ALTER TABLE p_operators ADD sound2 VARCHAR( 15 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;
		if ( $dbh["error"] == "None" )
		{
			$query = "UPDATE p_operators SET sound2 = 'default'" ;
			database_mysql_query( $dbh, $query ) ;
		}

		$query = "ALTER TABLE p_operators CHANGE sound sound1 VARCHAR( 15 ) NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "TRUNCATE TABLE p_reqstats" ;
		database_mysql_query( $dbh, $query ) ;

		$query = "ALTER TABLE p_reqstats ADD requests_ INT( 10 ) UNSIGNED NOT NULL AFTER requests" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.2.105" ) ;

		if ( file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/VERSION.php" ) )
			unlink( "$CONF[DOCUMENT_ROOT]/web/patches/VERSION.php" ) ;

		touch( "$CONF[DOCUMENT_ROOT]/web/patches/63" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/64" ) )
	{ Util_Vals_WriteVersion( "4.2.106" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/64" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/65" ) )
	{ Util_Vals_WriteVersion( "4.2.107" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/65" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/66" ) )
	{ Util_Vals_WriteVersion( "4.2.108" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/66" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/67" ) )
	{
		if ( $mysql_old )
		{
			$query = "ALTER TABLE p_requests ADD custom VARCHAR( 255 ) NOT NULL AFTER refer" ;
			database_mysql_query( $dbh, $query ) ;

			$query = "ALTER TABLE p_req_log ADD custom VARCHAR( 255 ) NOT NULL AFTER title" ;
			database_mysql_query( $dbh, $query ) ;
		}
		else
		{
			$query = "ALTER TABLE p_requests ADD custom VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER refer" ;
			database_mysql_query( $dbh, $query ) ;

			$query = "ALTER TABLE p_req_log ADD custom VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER title" ;
			database_mysql_query( $dbh, $query ) ;
		}

		$query = "ALTER TABLE p_departments DROP img_offline, DROP img_online" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.2.109" ) ;
		touch( "$CONF[DOCUMENT_ROOT]/web/patches/67" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/68" ) )
	{ Util_Vals_WriteVersion( "4.2.110" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/68" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/69" ) )
	{ Util_Vals_WriteVersion( "4.2.111" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/69" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/70" ) )
	{ Util_Vals_WriteVersion( "4.2.112" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/70" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/71" ) )
	{
		$query = "SELECT * FROM p_vars LIMIT 1" ;
		database_mysql_query( $dbh, $query ) ;
		$data = database_mysql_fetchrow( $dbh ) ;

		if ( !isset( $data["code"] ) )
		{
			$query = "INSERT INTO p_vars VALUES( 0, 1 )" ;
			database_mysql_query( $dbh, $query ) ;
		}

		$query = "ALTER TABLE p_vars ADD ts_clean INT UNSIGNED NOT NULL" ;
		database_mysql_query( $dbh, $query ) ;

		Util_Vals_WriteVersion( "4.2.113" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/71" ) ;
	}
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/72" ) )
	{ Util_Vals_WriteVersion( "4.2.114" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/72" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/73" ) )
	{ Util_Vals_WriteVersion( "4.2.115" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/73" ) ; }
	else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/74" ) )
	{ Util_Vals_WriteVersion( "4.2.116" ) ; touch( "$CONF[DOCUMENT_ROOT]/web/patches/74" ) ; }
	/* end auto patch area */
?>
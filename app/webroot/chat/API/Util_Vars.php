<?php
	/************** DO NOT MODIFY */
	if ( defined( 'API_Util_Vars' ) ) { return ; }	
	define( 'API_Util_Vars', true ) ;
	$PHPLIVE_HOST = isset( $_SERVER["HTTP_HOST"] ) ? $_SERVER["HTTP_HOST"] : "unknown_host" ;
	$PHPLIVE_URI = isset( $_SERVER["REQUEST_URI"] ) ? $_SERVER["REQUEST_URI"] : "unknown uri" ;
	$PHPLIVE_FULLURL = "$PHPLIVE_HOST/$PHPLIVE_URI" ;

	include_once( "$CONF[DOCUMENT_ROOT]/web/vals.php" ) ;
	if ( preg_match( "/patch\.php/", $PHPLIVE_URI ) ) { $VERSION = "PATCH-".time() ; }
	else { include_once( "$CONF[DOCUMENT_ROOT]/web/VERSION.php" ) ; }
	include_once( "$CONF[DOCUMENT_ROOT]/setup/KEY.php" ) ;

	/************** DO NOT MODIFY */
	// To change a variable, create a new file API/Util_Extra.php and place
	// the variable changes there as this file will be overridden with each update
	// - detailed at the bottom -
	$patch_v = 74 ; // DO NOT MODIFY or system may skip patches or produce an error
	$geo_v = 1.5 ; // DO NOT MODIFY or geo data import may produce an error

	$geoip = ( isset( $CONF["geo"] ) && $CONF["geo"] ) ? 1 : 0 ;
	$geomap = ( isset( $CONF["geo"] ) && ( strlen( $CONF["geo"] ) == 39 ) ) ? $CONF["geo"] : 0 ;

	$CONF["CHAT_IO_DIR"] = "$CONF[DOCUMENT_ROOT]/web/chat_sessions" ;
	$CONF["TYPE_IO_DIR"] = "$CONF[DOCUMENT_ROOT]/web/chat_initiate" ;

	$VARS_RTYPE = Array( 1=>"Ordered", 2=>"Round-robin", 3=>"Simultaneous" ) ;
	$VARS_BROWSER = Array( 1=>"IE", 2=>"Firefox", 3=>"Chrome", 4=>"Safari", 5=>"Opera", 6=>"Other" ) ;
	$VARS_OS = Array( 1=>"Windows", 2=>"Mac", 3=>"Unix", 4=>"Other", 5=>"Mobile" ) ;


	// the chat function variables

	$VARS_JS_ROUTING = 3 ; // seconds
	$VARS_JS_REQUESTING = 2 ; // seconds (operator.php & p_engine.php) -- used for chatting() interval
	$VARS_JS_FOOTPRINT = 10 ; // seconds
	$VARS_JS_ICON_CHECK = 25 ; // seconds -- longer status check is less load on server (15+ recommended)
	$VARS_FOOTPRINT_EXPIRE = $VARS_JS_FOOTPRINT * 3 ; // unique footprint expire time -> $VARS_CYCLE_CLEAN
	$VARS_FOOTPRINT_LOG_EXPIRE = 60 ; // remove footprint data older than x days
	$VARS_REFER_LOG_EXPIRE = 60 ; // remove refer data older than x days
	$VARS_IP_LOG_EXPIRE = 90 ; // remove IP data older than x days (default 90)
	$VARS_JS_FOOTPRINT_MAX_CYCLE = ( $VARS_JS_FOOTPRINT * 6 ) * 20 ; // 10 * 6 in every minute * 20 minutes
	$VARS_JS_RATING_FETCH = 15 ; // check for operator rating every x seconds (15-25 recommended)

	$VARS_OP_DC = $VARS_JS_REQUESTING * 4 ; // 4 cycle fails should be plenty

	$VARS_CYCLE_VUPDATE = 4 ;

	$VARS_CYCLE_CLEAN = $VARS_JS_REQUESTING + 5 ; // offset the cycle by few seconds
	$VARS_CYCLE_RESET = $VARS_JS_REQUESTING + 3 ; // offset the cycle by few seconds
	$VARS_EXPIRED_OPS = $VARS_CYCLE_CLEAN * 8 ; // relies on $VARS_CYCLE_CLEAN for cycles
	// max routing time times operators (should pickup by 5 ops) todo: incorporate # of ops in future version
	$VARS_EXPIRED_REQS = $VARS_EXPIRED_OPS * 3 ;
	$VARS_EXPIRED_OP2OP = $VARS_CYCLE_CLEAN * 3 ;

	$VARS_TRANSFER_BACK = 45 ; // transfer chat back to original operator after x seconds

	$VARS_SMS_BUFFER = 20 ; // seconds added to the normal routing time.  provides few extra time to return to computer

	/*****************************************************************************/
	/* To change a variable, create a new file API/Util_Extra.php and place
	// the variable changes there as this file will be overridden with each update
	// example:
	//	to change the variable $VARS_TRANSFER_BACK, simply place the same variable in the API/Util_extra.php
	//	with your new value.  if we introduce a new variable in this file on future versions, your changes
	//	will not be replace to the default values.
	*/
	if ( file_exists( "$CONF[DOCUMENT_ROOT]/API/Util_Extra.php" ) )
		include_once( "$CONF[DOCUMENT_ROOT]/API/Util_Extra.php" ) ;
?>
<?php
	if ( defined( 'API_Depts_put' ) ) { return ; }
	define( 'API_Depts_put', true ) ;

	/****************************************************************/
	FUNCTION Depts_put_Department( &$dbh,
					$deptid,
					$name,
					$email,
					$visible,
					$queue,
					$rtype,
					$rtime,
					$tshare,
					$texpire,
					$lang )
	{
		if ( ( $name == "" ) || ( $email == "" )  || ( $rtime == "" ) || ( $lang == "" ) )
			return false ;

		global $CONF ;
		global $LANG ;
		global $mysql_old ;

		LIST( $name ) = database_mysql_quote( $name ) ;

		$query = "SELECT * FROM p_departments WHERE deptID = $deptid" ;
		database_mysql_query( $dbh, $query ) ;
		$department = database_mysql_fetchrow( $dbh ) ;

		if ( isset( $department["deptID"] ) )
		{
			$temail = $department["temail"] ;
			$temaild = $department["temaild"] ;

			$msg_greet = $department["msg_greet"] ;
			$msg_offline = $department["msg_offline"] ;
			$msg_transcript = $department["msg_email"] ;
		}
		else
		{
			$temail = 1 ;
			$temaild = 0 ;

			$msg_greet = $LANG["CHAT_NOTIFY_LOOKING_FOR_OP"] ;
			$msg_offline = $LANG["CHAT_NOTIFY_OP_NOT_FOUND"] ;
			$msg_transcript = "Hello %%visitor%%,\r\n\r\nThank you for taking the time to chat with us.  Below is the complete transcript for your reference:\r\n\r\n%%transcript%%\r\n\r\nThank you,\r\n%%operator%%\r\n%%op_email%%\r\n" ;
		}

		LIST( $deptid, $email, $visible, $queue, $rtype, $rtime, $tshare, $texpire, $temail, $lang, $connecting, $noops, $transcript ) = database_mysql_quote( $deptid, $email, $visible, $queue, $rtype, $rtime, $tshare, $texpire, $temail, $lang, $msg_greet, $msg_offline, $msg_transcript ) ;

		if ( $mysql_old )
			$query = "REPLACE INTO p_departments VALUES ( $deptid, $visible, $queue, $tshare, $texpire, 1, $temail, $temaild, $rtype, $rtime, '$lang', '$name', '$email', '$connecting', '$noops', '$transcript' )" ;
		else
			$query = "INSERT INTO p_departments VALUES ( $deptid, $visible, $queue, $tshare, $texpire, 1, $temail, $temaild, $rtype, $rtime, '$lang', '$name', '$email', '$connecting', '$noops', '$transcript' ) ON DUPLICATE KEY UPDATE name = '$name', email = '$email', visible = $visible, queue = $queue, rtype = $rtype, rtime = $rtime, tshare = $tshare, texpire = $texpire, lang = '$lang'" ;
		database_mysql_query( $dbh, $query ) ;

		if ( $dbh[ 'ok' ] )
		{
			$id = ( $deptid ) ? $deptid : database_mysql_insertid ( $dbh ) ;

			$query = "UPDATE p_dept_ops SET visible = $visible WHERE deptID = $deptid" ;
			database_mysql_query( $dbh, $query ) ;
			return $id ;
		}

		return false ;
	}

	/****************************************************************/
	FUNCTION Depts_put_OpDept( &$dbh,
					$userid,
					$deptid )
	{
		if ( ( $userid == "" ) || ( $deptid == "" ) )
			return false ;

		LIST( $userid, $deptid ) = database_mysql_quote( $userid, $deptid ) ;

		$query = "SELECT count(*) AS total FROM chat_op_dept WHERE deptID = $deptid" ;
		database_mysql_query( $dbh, $query ) ;
		$data = database_mysql_fetchrow( $dbh ) ;

		$query = "INSERT INTO chat_op_dept VALUES ( $userid, $deptid, $data[total] )" ;
		database_mysql_query( $dbh, $query ) ;

		return true ;
	}

?>
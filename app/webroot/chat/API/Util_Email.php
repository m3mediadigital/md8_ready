<?php
	if ( defined( 'API_Util_Email' ) ) { return ; }	
	define( 'API_Util_Email', true ) ;

	$ERROR_EMAIL = "" ;
	function eMailErrorHandler($errno, $errstr) { global $ERROR_EMAIL; $ERROR_EMAIL = "Server Error: ".preg_replace( "/\"/", "\\\"", $errstr ) ; }
	function Util_Email_SendEmail( $from_name, $from_email, $to_name, $to_email, $subject, $message, $extra )
	{
		set_error_handler('eMailErrorHandler') ;

		$to_name = preg_replace( "/<v>/", "", $to_name ) ;
		global $CONF ;
		global $ERROR_EMAIL ;

		if ( $extra == "trans" )
		{
			$message = preg_replace( "/<>/", "\r\n\r\n", $message ) ;
			$message = preg_replace( "/<disconnected><d(\d)>(.*?)<\/div>/", "\r\n$2\r\n=== ===\r\n", $message ) ;
			$message = preg_replace( "/<div class='ca'><i>(.*?)<\/i><\/div>/", "===\r\n$1\r\n===", $message ) ;
			$message = preg_replace( "/<div class='co'><b>(.*?)<timestamp_(\d+)_co>:<\/b> /", "\r\n$1:\r\n", $message ) ;
			$message = preg_replace( "/<div class='cv'><b><v>(.*?)<timestamp_(\d+)_cv>:<\/b> /", "\r\n$1:\r\n", $message ) ;
			$message = preg_replace( "/<(.*?)>/", "", $message ) ;
			$message = stripslashes( preg_replace( "/-dollar-/", "\$", $message ) ) ;
		}

		if ( $extra == "sms" )
		{
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type: text/plain; charset=UTF-8" . "\r\n";
			$headers .= "From: $from_name <$from_email>" . "\r\n";

			if ( preg_match( "/(russian)/", $CONF["lang"] ) && !preg_match( "/Verification Code:/", $message ) )
				$message = "You have a new chat request." ;
		}
		else
		{
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type: text/plain; charset=UTF-8" . "\r\n";
			$headers .= "From: ".'=?UTF-8?B?'.base64_encode( $from_name ).'?='." <$from_email>" . "\r\n";
			$subject = '=?UTF-8?B?'.base64_encode( $subject ).'?=' ;
		}

		// SMTP
		//ini_set( SMTP, "localhost" ) ;

		if ( $to_email )
		{
			if ( mail( $to_email, $subject, $message, $headers ) ) { set_error_handler( "ErrorHandler" ) ; return true ; }
			else if ( $ERROR_EMAIL ) { set_error_handler( "ErrorHandler" ) ; return $ERROR_EMAIL ; }
			else{ return "Email error: 1109" ; }
		}
		else
			return true ;
	}
?>
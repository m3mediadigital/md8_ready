<?php
	if ( defined( 'API_Ops_put' ) ) { return ; }
	define( 'API_Ops_put', true ) ;

	/****************************************************************/
	FUNCTION Ops_put_Op( &$dbh,
					$opid,
					$status,
					$rate,
					$sms,
					$op2op,
					$traffic,
					$viewip,
					$login,
					$password,
					$name,
					$email )
	{
		if ( ( $login == "" ) || ( $name == "" ) || ( $email == "" ) )
			return false ;

		LIST( $login ) = database_mysql_quote( $login ) ;

		$query = "SELECT * FROM p_operators WHERE opID = $opid" ;
		database_mysql_query( $dbh, $query ) ;
		$operator = database_mysql_fetchrow( $dbh ) ;

		$operator_ = Ops_ext_get_OpInfoByLogin( $dbh, $login ) ;
		if ( $login == $operator_["login"] )
		{
			if ( $operator["opID"] != $operator_["opID"] )
				return false ;
		}

		if ( isset( $operator["opID"] ) )
		{
			if ( $password == "php-live-support" )
				$password = $operator["password"] ;
			else
				$password = md5( $password ) ;

			if ( $sms && !$operator["sms"] )
				$sms = time()-60 ;
			else if ( $sms )
				$sms = $operator["sms"] ;
		}
		else
		{
			$sms = ( $sms ) ? time()-60 : 0 ;
			$password = md5( $password ) ;
		}

		LIST( $opid, $status, $rate, $sms, $op2op, $traffic, $viewip, $password, $name, $email ) = database_mysql_quote( $opid, $status, $rate, $sms, $op2op, $traffic, $viewip, $password, $name, $email ) ;

		if ( isset( $operator["opID"] ) )
			$query = "UPDATE p_operators SET rate = $rate, op2op = $op2op, traffic = $traffic, viewip = $viewip, sms = $sms, login = '$login', password = '$password', name = '$name', email = '$email' WHERE opID = $opid" ;
		else
			$query = "REPLACE INTO p_operators VALUES ( $opid, 0, 0, 0, 0, $rate, $op2op, $traffic, $viewip, 1, '', '', 0, $sms, '', '$login', '$password', '$name', '$email', '', 'default', 'default', 'default' )" ;

		database_mysql_query( $dbh, $query ) ;

		if ( $dbh[ 'ok' ] )
		{
			$id = ( $opid ) ? $opid : database_mysql_insertid ( $dbh ) ;
			return $id ;
		}

		return false ;
	}

	/****************************************************************/
	FUNCTION Ops_put_OpDept( &$dbh,
					$opid,
					$deptid,
					$visible,
					$status )
	{
		if ( ( $opid == "" ) || ( $deptid == "" ) )
			return false ;

		LIST( $opid, $deptid, $visible, $status ) = database_mysql_quote( $opid, $deptid, $visible, $status ) ;

		$query = "SELECT count(*) AS total FROM p_dept_ops WHERE deptID = $deptid" ;
		database_mysql_query( $dbh, $query ) ;
		$data = database_mysql_fetchrow( $dbh ) ;
		$display = $data["total"] + 1 ; // add 1 because it starts at ZERO

		$query = "INSERT INTO p_dept_ops VALUES ( $deptid, $opid, $display, $visible, $status )" ;
		database_mysql_query( $dbh, $query ) ;

		return true ;
	}

	/****************************************************************/
	FUNCTION Ops_put_OpReqStat( &$dbh,
					$deptid,
					$opid,
					$tbl_name,
					$incro )
	{
		if ( ( $deptid == "" ) || ( $opid == "" ) || ( $incro == "" ) )
			return false ;

		LIST( $deptid, $opid, $tbl_name, $incro ) = database_mysql_quote( $deptid, $opid, $tbl_name, $incro ) ;

		$sdate = mktime( 0, 0, 1, date("m"), date("j"), date("Y") ) ;

		$query = "SELECT * FROM p_reqstats WHERE sdate = $sdate AND deptID = $deptid AND opID = $opid" ;
		database_mysql_query( $dbh, $query ) ;
		$data = database_mysql_fetchrow( $dbh ) ;
		
		if ( !isset( $data["sdate"] ) )
		{
			$query = "INSERT INTO p_reqstats VALUES ( $sdate, $deptid, $opid, 0, 0, 0, 0, 0, 0, 0, 0, 0 )" ;
			database_mysql_query( $dbh, $query ) ;
		}

		$value = $data[$tbl_name] + $incro ;
		$query = "UPDATE p_reqstats SET $tbl_name = $value WHERE sdate = $sdate AND deptID = $deptid AND opID = $opid" ;
		database_mysql_query( $dbh, $query ) ;

		if ( $dbh[ 'ok' ] )
			return true ;

		return false ;
	}

?>
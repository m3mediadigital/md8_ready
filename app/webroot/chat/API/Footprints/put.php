<?php
	if ( defined( 'API_Footprints_put' ) ) { return ; }
	define( 'API_Footprints_put', true ) ;

	/****************************************************************/
	FUNCTION Footprints_put_Print( &$dbh,
					$deptid,
					$os,
					$browser,
					$ip,
					$onpage,
					$title )
	{
		if ( ( $deptid == "" ) || ( $os == "" ) || ( $browser == "" )
			|| ( $ip == "" ) || ( $onpage == "" ) )
			return false ;

		$now = time() ;
		$today = mktime( 0, 0, 1, date( "m", time() ), date( "j", time() ), date( "Y", time() ) ) ;
		$url_mdfive = md5( $onpage ) ;

		LIST( $deptid, $os, $browser, $ip, $onpage, $title, $url_mdfive ) = database_mysql_quote( $deptid, $os, $browser, $ip, $onpage, $title, $url_mdfive ) ;

		$query = "INSERT INTO p_footprints VALUES ( $now, '$ip', $os, $browser, '$url_mdfive', '$onpage', '$title' )" ;
		database_mysql_query( $dbh, $query ) ;

		if ( $dbh[ 'ok' ] )
		{
			$query = "SELECT * FROM p_footstats WHERE sdate = $today AND mdfive = '$url_mdfive'" ;
			database_mysql_query( $dbh, $query ) ;

			if ( $dbh[ 'ok' ] )
			{
				$data = database_mysql_fetchrow( $dbh ) ;

				if ( isset( $data["sdate"] ) )
				{
					$total = $data["total"] + 1 ;
					$query = "UPDATE p_footstats SET total = $total WHERE sdate = $today AND mdfive = '$url_mdfive'" ;
				}
				else
					$query = "INSERT INTO p_footstats VALUES ( $today, '$url_mdfive', 1, '$onpage' )" ;

				database_mysql_query( $dbh, $query ) ;

				return true ;
			}
		}

		return false ;
	}

	/****************************************************************/
	FUNCTION Footprints_put_Print_U( &$dbh,
					$deptid,
					$os,
					$browser,
					$resolution,
					$ip,
					$onpage,
					$title,
					$marketid,
					$refer,
					$country = "",
					$region = "",
					$city = "",
					$latitude = 0,
					$longitude = 0 )
	{
		if ( ( $deptid == "" ) || ( $os == "" ) || ( $browser == "" )
			|| ( $ip == "" ) || ( $onpage == "" ) )
			return false ;

		// todo: set own md5 instead of using ip (track multiple visitors from one ip - but rare incidents)
		$now = time() ;
		$hostname = gethostbyaddr( $ip ) ;
		$agent = isset( $_SERVER["HTTP_USER_AGENT"] ) ? $_SERVER["HTTP_USER_AGENT"] : "&nbsp;" ;

		LIST( $deptid, $os, $browser, $resolution, $ip, $hostname, $agent, $onpage, $title, $marketid, $refer, $country, $region, $city, $latitude, $longitude ) = database_mysql_quote( $deptid, $os, $browser, $resolution, $ip, $hostname, $agent, $onpage, $title, $marketid, $refer, $country, $region, $city, $latitude, $longitude ) ;

		$query = "INSERT INTO p_footprints_u VALUES ( $now, $now, $deptid, $marketid, 0, $os, $browser, '$resolution', '$ip', '$hostname', '$agent', '$onpage', '$title', '$refer', '$country', '$region', '$city', $latitude, $longitude )" ;
		database_mysql_query( $dbh, $query ) ;

		if ( $dbh[ 'ok' ] )
			return true ;

		return false ;
	}

	/****************************************************************/
	FUNCTION Footprints_put_Refer( &$dbh,
					$ip,
					$marketid,
					$refer )
	{
		if ( $ip == "" )
			return false ;

		$now = time() ;
		$today = mktime( 0, 0, 1, date( "m", time() ), date( "j", time() ), date( "Y", time() ) ) ;
		$url_mdfive = md5( $refer ) ;

		LIST( $ip, $marketid, $url_mdfive, $refer ) = database_mysql_quote( $ip, $marketid, $url_mdfive, $refer ) ;

		$query = "INSERT INTO p_refer VALUES ( '$ip', $now, $marketid, '$url_mdfive', '$refer' )" ;
		database_mysql_query( $dbh, $query ) ;

		if ( $dbh[ 'ok' ] )
		{
			$query = "SELECT * FROM p_referstats WHERE sdate = $today AND mdfive = '$url_mdfive'" ;
			database_mysql_query( $dbh, $query ) ;

			if ( $dbh[ 'ok' ] )
			{
				$data = database_mysql_fetchrow( $dbh ) ;

				if ( isset( $data["sdate"] ) )
				{
					$total = $data["total"] + 1 ;
					$query = "UPDATE p_referstats SET total = $total WHERE sdate = $today AND mdfive = '$url_mdfive'" ;
				}
				else
					$query = "INSERT INTO p_referstats VALUES ( $today, '$url_mdfive', 1, '$refer' )" ;

				database_mysql_query( $dbh, $query ) ;

				return true ;
			}
		}

		return false ;
	}

?>
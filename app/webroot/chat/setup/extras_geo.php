<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	/****************************************/
	// STANDARD header for Setup
	if ( !file_exists( "../web/config.php" ) ){ HEADER("location: install.php") ; exit ; }
	include_once( "../web/config.php" ) ;
	include_once( "../API/Util_Format.php" ) ;
	include_once( "../API/Util_Error.php" ) ;
	include_once( "../API/SQL.php" ) ;
	include_once( "../API/Util_Security.php" ) ;
	$ses = Util_Format_Sanatize( Util_Format_GetVar( "ses" ), "ln" ) ;
	if ( !$admininfo = Util_Security_AuthSetup( $dbh, $ses ) ){ ErrorHandler ( 608, "Invalid setup session or session has expired.", $PHPLIVE_FULLURL, 0, Array() ) ; }
	// STANDARD header end
	/****************************************/

	$error = "" ; $dev = 0 ;
	$geo_dir = "$CONF[DOCUMENT_ROOT]/addons/geo_data" ;

	include_once( "../API/Util_Vals.php" ) ;

	$action = Util_Format_Sanatize( Util_Format_GetVar( "action" ), "ln" ) ;
	$apikey = Util_Format_Sanatize( Util_Format_GetVar( "apikey" ), "ln" ) ;
	
	if ( $action == "import_geo_files" )
	{
		$json_data = $next_file = $error = "" ;

		if ( !is_dir( $geo_dir ) )
			$json_data = "json_data = { \"status\": 0, \"error\": \"Directory addons/geo_data/ does not exist.  Please refer to step 4.\" };" ;
		else if ( !file_exists( "$CONF[DOCUMENT_ROOT]/addons/geo_data/VERSION.php" ) )
			$json_data = "json_data = { \"status\": 0, \"error\": \"Please download the latest GeoIP data and start again from step 1.\" };" ;
		else
		{
			include_once( "$CONF[DOCUMENT_ROOT]/addons/geo_data/VERSION.php" ) ;

			if ( !isset( $VERSION_GEO ) || ( $VERSION_GEO != $geo_v ) )
				$json_data = "json_data = { \"status\": 0, \"error\": \"GeoIP data is not compatible.  Please start again from step 1.\" };" ;
			else
			{
				$index = Util_Format_Sanatize( Util_Format_GetVar( "index" ), "ln" ) * 1 ;
				$index_next = $index + 1 ;

				// if first pass, empty out the database for the new inserts
				if ( !$index )
				{
					if ( !$dev )
					{
						$query = "TRUNCATE TABLE p_geo_bloc" ;
						database_mysql_query( $dbh, $query ) ;

						$query = "TRUNCATE TABLE p_geo_loc" ;
						database_mysql_query( $dbh, $query ) ;
					}

					$error = ( Util_Vals_WriteToConfFile( "geo", "" ) ) ? "" : "Could not write to config file." ;
				}

				if ( !$error )
				{
					$files = Array() ; // add empty so index starts at 1
					$dh = opendir( $geo_dir ) ;
					while( false !== ( $file = readdir( $dh ) ) )
					{
						if ( $file != "." && $file != ".." && $file != "README.txt" && $file != "VERSION.php" )
							$files[] = $file ;
					}
					closedir( $dh ) ;

					asort( $files ) ;
					for( $c = 0; $c < count( $files ); ++$c )
					{
						$file = $files[$c] ;

						if ( $c == $index )
						{
							if ( !$dev )
								import_file( $dbh, "$CONF[DOCUMENT_ROOT]/addons/geo_data/$file" ) ;
							$percent = floor( $c/count($files) * 100 ) ;

							$json_data = "json_data = { \"status\": 0, \"index\": $index_next, \"percent\": $percent };" ;
						}
					}

					if ( !$json_data )
					{
						Util_Vals_WriteToConfFile( "geo", "1" ) ;
						$json_data = "json_data = { \"status\": 1 };" ;
					}
				}
				else
					$json_data = "json_data = { \"status\": 0, \"error\": \"$error\" };" ;
			}
		}

		if ( isset( $dbh ) && isset( $dbh['con'] ) )
			database_mysql_close( $dbh ) ;

		print "$json_data" ;
		exit ;
	}
	else if ( $action == "update" )
	{
		if ( strlen( $apikey ) == 39 )
			$error = ( Util_Vals_WriteToConfFile( "geo", "$apikey" ) ) ? "" : "Could not write to config file." ;
	}
	else if ( $action == "check_dir" )
	{
		if ( !is_dir( $geo_dir ) )
			$json_data = "json_data = { \"status\": 1 };" ;
		else
			$json_data = "json_data = { \"status\": 0 };" ;

		print "$json_data" ;
		exit ;
	}
	else if ( $action == "clear" )
	{
		if ( !$dev )
		{
			$query = "TRUNCATE TABLE p_geo_bloc" ;
			database_mysql_query( $dbh, $query ) ;

			$query = "TRUNCATE TABLE p_geo_loc" ;
			database_mysql_query( $dbh, $query ) ;
		}

		Util_Vals_WriteToConfFile( "geo", "" ) ;
		HEADER( "location: ./extras_geo.php?ses=$ses&action=cleared" ) ;
		exit ;
	}

	function import_file( $dbh, $thefile )
	{
		$queries = Array() ;

		$fh = fopen( $thefile, "rb" ) ;
		while( !feof( $fh ) )
		{
			$query = preg_replace( "/[`;]/", "", rtrim( fgets( $fh ) ) ) ;
			if ( $query )
				$queries[] = $query ;
		}
		fclose( $fh ) ;

		for ( $c = 0; $c < count( $queries ); ++$c )
		{
			$query = $queries[$c] ;
			database_mysql_query( $dbh, $query ) ;
		}
	}
?>
<?php include_once( "../inc_doctype.php" ) ?>
<head>
<title> PHP Live! Support <?php echo $VERSION ?> </title>

<meta name="description" content="PHP Live! Support <?php echo $VERSION ?>">
<meta name="keywords" content="powered by: PHP Live!  www.phplivesupport.com">
<meta name="robots" content="all,index,follow">
<meta http-equiv="content-type" content="text/html; CHARSET=utf-8"> 

<link rel="Stylesheet" href="../css/base_setup.css?<?php echo $VERSION ?>">
<script type="text/javascript" src="../js/global.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/setup.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework_cnt.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/tooltip.js?<?php echo $VERSION ?>"></script>

<script type="text/javascript">
<!--
	$(document).ready(function()
	{
		$("body").show() ;
		$('#body_sub_title').html( "<img src=\"../pics/icons/chest.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Settings" ) ;

		init_menu() ;
		toggle_menu_setup( "extras" ) ;

		if ( "<?php echo $error ?>" )
			do_alert( 0, "<?php echo $error ?>" ) ;
		else if ( "<?php echo $action ?>" == "update" )
			do_alert( 1, "Update Success" ) ;
		else if ( "<?php echo $action ?>" == "cleared" )
			do_alert( 1, "GeoIP addon has been reset." ) ;
	});

	function confirm_import()
	{
		if ( confirm( "This process may take up to 45 minutes. Continue?" ) )
			check_geo_files(0, 0) ;
	}

	function check_geo_files( theindex, thepercent )
	{
		var unique = unixtime() ;
		var json_data = new Object ;

		$(window).scrollTop(0) ;
		$('#div_import').show() ;
		$('body').css({'overflow': 'hidden'}) ;
		$('#data_percent').html( thepercent ) ;

		$.get("./extras_geo.php", { action: "import_geo_files", index: theindex, ses: "<?php echo $ses ?>", unique: unique },  function(data){
			eval( data ) ;

			if ( !json_data.status && ( typeof( json_data.error ) == "undefined" ) )
			{
				check_geo_files( theindex+1, json_data.percent ) ;
			}
			else if ( json_data.status == 1 )
				location.href = "extras_geo.php?ses=<?php echo $ses ?>" ;
			else
			{
				$('#div_import').hide() ;
				$('body').css({'overflow': 'visible'}) ;
				do_alert(0, json_data.error) ;
			}
		});
	}

	function submit_key()
	{
		var apikey = $('#apikey').val() ;

		if ( !apikey )
			do_alert( 0, "Blank API Key is invalid." ) ;
		else if ( apikey.length != 39 )
			do_alert( 0, "API Key format is invalid." ) ;
		else
			location.href = "extras_geo.php?ses=<?php echo $ses ?>&action=update&apikey="+apikey ;
	}

	function reset_addon( theflag )
	{
		if ( theflag )
		{
			$('#div_reset_btn').hide() ;
			$('#div_reset').show() ;
		}
		else
		{
			$('#div_reset').hide() ;
			$('#div_reset_btn').show() ;
		}
	}

	function reset_addon_doit()
	{
		var unique = unixtime() ;
		var json_data = new Object ;

		if ( confirm( "Disable GeoIP addon?" ) )
		{
			$.get("./extras_geo.php", { action: "check_dir", ses: "<?php echo $ses ?>", unique: unique },  function(data){
				eval( data ) ;

				if ( json_data.status )
					location.href = "./extras_geo.php?ses=<?php echo $ses ?>&action=clear" ;
				else
				{
					do_alert( 0, "Error: The geo_data/ directory has not been deleted." ) ;
					$('#div_error').show() ;
				}
			});
		}
	}

//-->
</script>
</head>
<body style="display: none;">

<?php include_once( "./inc_header.php" ) ?>

		<div class="op_submenu_wrapper">
			<div class="op_submenu" onClick="location.href='extras.php?ses=<?php echo $ses ?>'" id="menu_external">External URLs</div>
			<div class="op_submenu_focus" onClick="location.href='extras_geo.php?ses=<?php echo $ses ?>'" id="menu_geoip">GeoIP and GeoMap</div>
			<div class="op_submenu" onClick="location.href='extras.php?ses=<?php echo $ses ?>&jump=apis'" id="menu_apis">Dev APIs</div>
			<div style="clear: both"></div>
		</div>

		<form method="POST" action="extras_geo.php?submit" enctype="multipart/form-data">
		<input type="hidden" name="action" value="update">
		<input type="hidden" name="ses" value="<?php echo $ses ?>">

		<div style="text-align: justify;">
			<div class="info_info" style="margin-top: 15px;">
			<?php if ( !isset( $CONF["geo"] ) || !$CONF["geo"] ): ?>
			GeoIP is the identification of the real-world geographic location of an IP address.  Enabling this feature will display a country flag, the region and city of an IP address throughout various areas of the PHP Live! system.
			<?php else: ?>
			<big><b>Congratulations!  GeoIP is enabled.</b></big>
			<div>Throughout various areas of the PHP Live! system, you'll now notice a map icon with the apporoximate region and city for each visitor IP address.

			<div id="div_reset_btn" style="margin-top: 15px;"><a href="JavaScript:void(0)" onClick="reset_addon(1)">Disable and Reset the GeoIP Addon</a></div>

			<div id="div_reset" style="display: none; margin-top: 15px;">
				<div style="margin-top: 25px;">
					<div><img src="../pics/icons/warning.png" width="16" height="16" border="0" alt=""> This action will disable the GeoIP addon and clear the GeoIP database tables.</div>
					<div style="margin-top: 5px;"><b>First</b>, you will need to manually delete the <code>geo_data/</code> directory from the <code>phplive/addons/</code> folder.  After you have deleted the <code>geo_data/</code> directory, press the button below to continue.  To reactivate the GeoIP and GeoMaps feature, you will need to follow the GeoIP data import process again.</div>
				</div>
				<div style="margin-top: 25px; display: none;" id="div_error"><div class="info_error">To clear out the previous GeoIP data files, delete the geo_data/ directory from the phplive/addons/ folder before proceeding.</div></div>
				<div style="margin-top: 25px;"><input type="button" value="Continue and Disable the GeoIP Addon" onClick="reset_addon_doit()"> or <a href="JavaScript:void(0)" onClick="reset_addon(0)">cancel</a></div>
			</div>

			<?php endif ; ?>
			</div></div>

			<div id="div_geoip" style="margin-top: 25px;">
				<table cellspacing=0 cellpadding=2 border=0 style="">
				<tr>
					<td valign="top">
						<?php if ( !isset( $CONF["geo"] ) || !$CONF["geo"] ): ?>
						<div>
							<div style=""><span class="info_title_steps">Step 1.</span> You'll want to download the GeoIP Addon Data file from the OSI Codes Inc. server.  Login to the OSI Codes Inc. client area and click the "Downloads" top menu.  From the "Downloads" area, you'll see the <code>GeoIP Addon</code> file (37 Megs GZ compressed).  Click the "download" button to download the file to your computer.</div>
							<div style="margin-top: 15px;"><a href="http://www.phplivesupport.com/r.php?r=login" target="new">Login and Download the GeoIP Addon</a></div>

							<div style="margin-top: 35px;"><span class="info_title_steps">Step 2.</span> Locate the downloaded GeoIP Data file on your computer and double click the file.  You'll want to have <a href="http://www.phplivesupport.com/r.php?r=winzip" target="new">WinZip</a> or other decompression software installed on your computer to be able to decompress the file.</div>

							<div style="margin-top: 35px;"><span class="info_title_steps">Step 3.</span> After double clicking the data file, you'll notice there is another compressed file, geo_data.tar which can be further extracted by double clicking the file.  You should now see a geo_data/ folder.  This folder contains all the GeoIP data.  Do a final extraction of this folder to a location on your computer:</div>
							<div style="margin-top: 15px;"><img src="../pics/setup/geo_data_2.gif" width="306" height="272" border="0" alt="" style="border: 1px solid #CDD3D8; -moz-border-radius: 5px; border-radius: 5px; margin-right: 35px;"> <img src="../pics/setup/geo_data_3.gif" width="306" height="272" border="0" alt="" style="border: 1px solid #CDD3D8; -moz-border-radius: 5px; border-radius: 5px;"></div>

							<div style="margin-top: 35px;"><span class="info_title_steps">Step 4.</span> Upload the entire geo_data/ folder to your server and place it inside the directory your_PHPLive_folder/addons/ .  The uploading of the geo_data/ directory may take up to 30 minutes as there are over 250 Megs of data:</div>
							<div style="margin-top: 15px; font-size: 16px; font-family: courier new;">FTP the geo_data/ folder to your_PHPLive_folder/addons/</div>

							<div style="margin-top: 35px;"><span class="info_title_steps">Step 5.</span> There should now be a folder geo_data/ inside your PHP Live! addons:</div>
							<div style="margin-top: 15px; font-size: 16px; font-family: courier new;">your_PHPLive_folder/addons/geo_data/</div>

							<div style="margin-top: 35px;"><span class="info_title_steps">Step 6.</span> Once the upload is complete and you have verified its proper location (Step 5), click the button below to begin the import process.  Please note, data import may take up to 45 minutes:</div>
							<div class="info_warning" style="margin-top: 15px; margin-bottom: 15px;"><b><img src="../pics/icons/alert.png" width="16" height="16" border="0" alt=""> IMPORTANT:</b> This process will insert thousands of GeoIP data into the database and it may take up to 45 minutes.  To avoid any errors, we recommend doing this after normal business hours or when the server activity is minimal.  Once this process has been started, it should not be interrupted.</div>
							<div style="margin-top: 15px;"><button type="button" onClick="confirm_import()">Import GeoIP Data</button></div>
						</div>
						<?php else: ?>

						<div style="padding: 10px;" class="info_box">(Optional) Expand the GeoIP features by signing up for Google Maps API &nbsp;
							<?php if ( isset( $CONF["geo"] ) && strlen( $CONF["geo"] ) == 39 ): ?>
							<span class="info_good"><img src="../pics/icons/thumb_up.png" width="16" height="16" border="0" alt=""> Google Maps is enabled.</span>
							<?php else: ?>
							<span>Update your Google Maps API Key below to activate.</span>
							<?php endif ; ?>
						</div>

						<div id="google_key" style="margin-top: 25px;">
							<div style=""><span class="info_title_steps">Step 1.</span> Due to the <a href="http://www.phplivesupport.com/r.php?r=googleapiterms" target="new">Google Maps API Terms of Service</a>, you'll want to signup for a Google API Key so that the integration is linked to your account only, binding the agreement to your account.  By signing up for an API Key, it also enables the reporting features on the Google Code Console.  To begin, login to the Google Code Console:</div>
							<div style="margin-top: 15px;"><a href="http://www.phplivesupport.com/r.php?r=googleapi" target="new">Click here to Login to Google Code Console</a></div>

							<div style="margin-top: 35px;"><span class="info_title_steps">Step 2.</span> Once logged in and Google Code Console activated, click on the "Services" left menu link.</div>
							<div style="margin-top: 15px;"><img src="../pics/setup/api_menu.gif" width="255" height="192" border="0" alt="" style="border: 1px solid #CDD3D8; -moz-border-radius: 5px; border-radius: 5px; margin-right: 35px;"></div>

							<div style="margin-top: 35px;"><span class="info_title_steps">Step 3.</span> On the "Services" page, scroll down to the "Google Maps API v3" and enable this service:</div>
							<div style="margin-top: 15px;"><img src="../pics/setup/api_off.gif" width="327" height="192" border="0" alt="" style="border: 1px solid #CDD3D8; -moz-border-radius: 5px; border-radius: 5px; margin-right: 35px;"> <img src="../pics/setup/api_on.gif" width="327" height="192" border="0" alt="" style="border: 1px solid #CDD3D8; -moz-border-radius: 5px; border-radius: 5px;"></div>

							<div style="margin-top: 35px;"><span class="info_title_steps">Step 4.</span> The API Console menu will now change and will look similar to the following image.  Click on the "API Access" link:</div>
							<div style="margin-top: 15px;"><img src="../pics/setup/api_menu_on.gif" width="255" height="192" border="0" alt="" style="border: 1px solid #CDD3D8; -moz-border-radius: 5px; border-radius: 5px; margin-right: 35px;"></div>

							<div style="margin-top: 35px;"><span class="info_title_steps">Step 5.</span> On the "API Access" page, you'll see a box detailing the API credentials.  Copy the "Simple API Access Key" and provide the key below to activate Google Maps:</div>
							<div style="margin-top: 15px;">
								<div style="float: left; margin-right: 35px;"><img src="../pics/setup/api_key.gif" width="327" height="192" border="0" alt="" style="border: 1px solid #CDD3D8; -moz-border-radius: 5px; border-radius: 5px;"></div>
								<div class="info_neutral" style="float: left; padding: 25px; width: 520px; height: 142px;">
									<div style="font-size: 14px; font-weight: bold; -moz-border-radius: 5px; border-radius: 5px;">
										Input your API Key:
										<?php if ( isset( $CONF["geo"] ) && strlen( $CONF["geo"] ) == 39 ): ?>
										<span style="font-size: 12px;"><span class="info_good"><img src="../pics/icons/thumb_up.png" width="16" height="16" border="0" alt=""> Google Maps is enabled.</span></span>
										<?php endif ; ?>
									</div>
									<div style="margin-top: 15px;">IMPORTANT: Please ensure the API Key is correct so that proper report can be generated on the <a href="https://code.google.com/apis/console/#project:350425761210:stats" target="new">Google Code Console</a>.</div>
									<div style="margin-top: 15px;"><input type="text" id="apikey" name="apikey" class="input" size="60" maxlength="55" value="<?php echo ( isset( $CONF["geo"] ) && strlen( $CONF["geo"] ) == 39 ) ? $CONF["geo"] : "" ; ?>"></div>
									<div style="margin-top: 15px;"><input type="button" value="Update Google API Key" onClick="submit_key()"></div>
								</div>
								<div style="clear: both;"></div>
							</div>
							
						</div>

						<?php endif ; ?>
					</td>
				</tr>
				</table>
			</div>

		</div>
		</form>

<?php include_once( "./inc_footer.php" ) ?>

<div id="div_import" style="display: none; position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; background: url( ../pics/bg_trans.png ) repeat; overflow: hidden; z-index: 20;">
	<div style="position: relative; width: 900px; margin: 0 auto; top: 130px; text-align: center;">
		<div class="info_box" style="font-size: 14px; font-weight: bold;">
			<img src="../pics/loading_bar.gif" width="16" height="16" border="0" alt=""> Importing GeoIP data.  Do not stop this process.  Importing may take up to 45 minutes. <span id="data_percent"></span>% complete...
		</div>
	</div>
</div>

</body>
</html>

<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	/****************************************/
	// STANDARD header for Setup
	if ( !file_exists( "../web/config.php" ) ){ HEADER("location: install.php") ; exit ; }
	if ( !file_exists( "../web/VERSION.php" ) ) { touch( "../web/VERSION.php" ) ; }  // patch 4.2.105 adjustment
	include_once( "../web/config.php" ) ;
	include_once( "../API/Util_Format.php" ) ;
	include_once( "../API/Util_Error.php" ) ;
	include_once( "../API/SQL.php" ) ;
	include_once( "../API/Util_Security.php" ) ;
	$ses = Util_Format_Sanatize( Util_Format_GetVar( "ses" ), "ln" ) ;
	if ( !$admininfo = Util_Security_AuthSetup( $dbh, $ses ) ){ ErrorHandler ( 608, "Invalid setup session or session has expired.", $PHPLIVE_FULLURL, 0, Array() ) ; }
	// STANDARD header end
	/****************************************/
	/* AUTO PATCH */
	if ( !file_exists( "$CONF[DOCUMENT_ROOT]/web/patches/$patch_v" ) )
	{
		$query = ( isset( $_SERVER["QUERY_STRING"] ) ) ? $_SERVER["QUERY_STRING"] : "" ;
		HEADER( "location: ../patch.php?from=setup&".$query ) ;
		exit ;
	}

	include_once( "../API/Ops/get.php" ) ;
	include_once( "../API/Ops/update.php" ) ;
	include_once( "../API/Chat/get.php" ) ;
	include_once( "../API/Chat/get_ext.php" ) ;

	$error = "" ;

	$operators = Ops_get_AllOps( $dbh ) ;
	$operators_hash = Array() ;
	for ( $c = 0; $c < count( $operators ); ++$c )
	{
		$operator = $operators[$c] ;
		$operators_hash[$operator["opID"]] = $operator["name"] ;
	}

	Ops_update_IdleOps( $dbh ) ;
	$transcripts = Chat_ext_get_RefinedTranscripts( $dbh, "0", "0", "", 0, 15 ) ;
?>
<?php include_once( "../inc_doctype.php" ) ?>
<head>
<title> PHP Live! Support <?php echo $VERSION ?> </title>

<meta name="description" content="PHP Live! Support <?php echo $VERSION ?>">
<meta name="keywords" content="powered by: PHP Live!  www.phplivesupport.com">
<meta name="robots" content="all,index,follow">
<meta http-equiv="content-type" content="text/html; CHARSET=utf-8"> 

<link rel="Stylesheet" href="../css/base_setup.css?<?php echo $VERSION ?>">
<script type="text/javascript" src="../js/global.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/setup.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework_cnt.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/tooltip.js?<?php echo $VERSION ?>"></script>

<script type="text/javascript">
<!--
	$(document).ready(function()
	{
		$("body").show() ;
		$('#body_sub_title').html( "<img src=\"../pics/icons/asterisk.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Home" ) ;

		init_menu() ;
		toggle_menu_setup( "home" ) ;

		var height = $('#td_right').height() - 45 ;
		$('#home_menu').css({'height': height}) ;
		height = $('#home_box_start').height() ;
		$('#home_ops').css({'height': height}) ;

		var help_tooltips = $( 'body' ).find('.help_tooltip' ) ;
		help_tooltips.tooltip({
			event: "mouseover",
			track: true,
			delay: 0,
			showURL: false,	
			showBody: "- ",
			fade: 0,
			positionLeft: false,
			extraClass: "stat"
		});
	});

	function launch_tools_op_status()
	{
		var url = "tools_op_status.php?ses=<?php echo $ses ?>" ;
		window.open( url, "Operators", "scrollbars=yes,menubar=no,resizable=1,location=no,width=270,height=220,status=0" ) ;
	}

	function open_transcript( theces, theopname )
	{
		var url = "../ops/op_trans_view.php?ses=<?php echo $ses ?>&ces="+theces+"&id=<?php echo $admininfo["adminID"] ?>&auth=setup&"+unixtime() ;
		newwin = window.open( url, theces, "scrollbars=yes,menubar=no,resizable=1,location=no,width=650,height=450,status=0" ) ;

		if ( newwin )
			newwin.focus() ;
	}

	function remote_disconnect( theopid )
	{
		if ( confirm( "Remote disconnect operator console?" ) )
		{
			$('#remote_disconnect_notice').center().show() ;

			$.ajax({
				type: "GET",
				url: "../ajax/setup_actions.php",
				data: "ses=<?php echo $ses ?>&action=remote_disconnect&opid="+theopid+"&"+unixtime(),
				success: function(data){
					eval( data ) ;

					if ( json_data.status )
					{
						// 15 seconds of wait since console checks every 10 seconds
						setTimeout( function(){ location.href = 'index.php?ses=<?php echo $ses ?>' ; }, 15000 ) ;
					}
					else
					{
						$('#remote_disconnect_notice').hide() ;
						do_alert( 0, "Could not remote disconnect console.  Please try again." ) ;
					}
				}
			});
		}
	}

	function show_div( thediv )
	{
		var divs = Array( "operator", "setup" ) ;

		for ( c = 0; c < divs.length; ++c )
		{
			$('#login_'+divs[c]).hide() ;
			$('#menu_url_'+divs[c]).removeClass('op_submenu_focus').addClass('op_submenu') ;
		}

		$('#login_'+thediv).show() ;
		$('#menu_url_'+thediv).removeClass('op_submenu').addClass('op_submenu_focus') ;
	}
//-->
</script>
</head>
<body style="display: none;">

<?php include_once( "./inc_header.php" ) ?>
			<div class="op_submenu_wrapper">

				<table cellspacing=0 cellpadding=0 border=0 width="100%">
				<tr>
					<td valign="top">
						<div class="home_box" style="color: #7C89A9; background: #FBFBFB; border: 1px solid #CACACC; margin-left: 0px;">
							<div class="home_box_header">Complete list of setup options:</div>

							<div id="home_menu" style="margin-top: 10px; font-weight: bold; height: 440px; overflow: auto;">
								<div class="home_box_li_blank" style="margin-top: 0xp;">
									<div><img src="../pics/icons/persons.png" width="14" height="14" border="0" alt=""> <a href="depts.php?ses=<?php echo $ses ?>">Departments</a></div>
								</div>
								<div class="home_box_li_blank" style="">
									<div><img src="../pics/icons/person.png" width="14" height="14" border="0" alt=""> <a href="ops.php?ses=<?php echo $ses ?>">Operators</a></div>
									<div style="padding-left: 19px; margin-top: 5px; font-decoration: normal; font-weight: normal;">
										<div class="home_box_menu_li"> <a href="ops.php?ses=<?php echo $ses ?>">Create/Edit Operators</a></div>
										<div class="home_box_menu_li"> <a href="ops.php?ses=<?php echo $ses ?>&jump=assign">Assign Operator to Department</a></div>
										<div class="home_box_menu_li"> <a href="ops_reports.php?ses=<?php echo $ses ?>">Online Activity</a></div>
										<div class="home_box_menu_li"> <a href="ops.php?ses=<?php echo $ses ?>&jump=monitor">Online Status Monitor</a></div>
										<div class="home_box_menu_li"> <a href="ops.php?ses=<?php echo $ses ?>&jump=online">Go <span style="color: #3EB538; font-weight: bold;">ONLINE</span></a></div>
									</div>
								</div>
								<div class="home_box_li_blank" style="">
									<div><img src="../pics/icons/image.png" width="14" height="14" border="0" alt=""> <a href="icons.php?ses=<?php echo $ses ?>">Chat Icons</a></div>
								</div>
								<div class="home_box_li_blank" style="">
									<div><img src="../pics/icons/page_white_code.png" width="14" height="14" border="0" alt=""> <a href="code.php?ses=<?php echo $ses ?>">HTML Code</a></div>
									<div style="padding-left: 19px; margin-top: 5px; font-decoration: normal; font-weight: normal;">
										<div class="home_box_menu_li"> <a href="code.php?ses=<?php echo $ses ?>&jump=auto">Automatic Initiate Chat</a></div>
									</div>
								</div>
								<div class="home_box_li_blank" style="">
									<div><img src="../pics/icons/view.png" width="14" height="14" border="0" alt=""> <a href="transcripts.php?ses=<?php echo $ses ?>">Transcripts</a></div>
								</div>
								<div class="home_box_li_blank" style="">
									<div><img src="../pics/icons/book.png" width="14" height="14" border="0" alt=""> <a href="reports_chat.php?ses=<?php echo $ses ?>">Report: Chats</a></div>
									<div style="padding-left: 19px; margin-top: 5px; font-decoration: normal; font-weight: normal;">
										<div class="home_box_menu_li"> <a href="reports_chat.php?ses=<?php echo $ses ?>">Chat Reports</a></div>
										<div class="home_box_menu_li"> <a href="reports_chat_active.php?ses=<?php echo $ses ?>">Active Chats</a></div>
									</div>
								</div>
								<div class="home_box_li_blank" style="">
									<div><img src="../pics/icons/book.png" width="14" height="14" border="0" alt=""> <a href="reports_traffic.php?ses=<?php echo $ses ?>">Report: Traffic</a></div>
									<div style="padding-left: 19px; margin-top: 5px; font-decoration: normal; font-weight: normal;">
										<div class="home_box_menu_li"> <a href="reports_traffic.php?ses=<?php echo $ses ?>">Footprints</a></div>
										<div class="home_box_menu_li"> <a href="reports_refer.php?ses=<?php echo $ses ?>">Refer URLs</a></div>
										<div class="home_box_menu_li"> <a href="reports_traffic.php?ses=<?php echo $ses ?>&jump=foot_settings">Settings</a></div>
									</div>
								</div>
								<div class="home_box_li_blank" style="">
									<div><img src="../pics/icons/graph.png" width="14" height="14" border="0" alt=""> <a href="marketing.php?ses=<?php echo $ses ?>">Marketing</a></div>
									<div style="padding-left: 19px; margin-top: 5px; font-decoration: normal; font-weight: normal;">
										<div class="home_box_menu_li"> <a href="marketing.php?ses=<?php echo $ses ?>">Social Media</a></div>
										<div class="home_box_menu_li"> <a href="marketing_click.php?ses=<?php echo $ses ?>">Click Tracking</a></div>
										<div class="home_box_menu_li"> <a href="reports_marketing.php?ses=<?php echo $ses ?>">Report: Clicks</a></div>
										<div class="home_box_menu_li"> <a href="marketing_marquee.php?ses=<?php echo $ses ?>">Chat Footer Marquee</a></div>
									</div>
								</div>
								<div class="home_box_li_blank" style="">
									<div><img src="../pics/icons/switch.png" width="14" height="14" border="0" alt=""> <a href="extras.php?ses=<?php echo $ses ?>">Extras</a></div>
									<div style="padding-left: 19px; margin-top: 5px; font-decoration: normal; font-weight: normal;">
										<div class="home_box_menu_li"> <a href="extras.php?ses=<?php echo $ses ?>">External URLs</a></div>
										<div class="home_box_menu_li"> <a href="extras_geo.php?ses=<?php echo $ses ?>">GeoIP & GeoMap</a></div>
										<div class="home_box_menu_li"> <a href="extras.php?ses=<?php echo $ses ?>&jump=apis">Dev APIs</a></div>
									</div>
								</div>
								<div class="home_box_li_blank" style="">
									<div><img src="../pics/icons/chest.png" width="14" height="14" border="0" alt=""> <a href="settings.php?ses=<?php echo $ses ?>">Settings</a></div>
									<div style="padding-left: 19px; margin-top: 5px; font-decoration: normal; font-weight: normal;">
										<div class="home_box_menu_li"> <a href="settings.php?ses=<?php echo $ses ?>">Upload Logo</a></div>
										<div class="home_box_menu_li"> <a href="settings.php?ses=<?php echo $ses ?>&jump=themes">Themes</a></div>
										<div class="home_box_menu_li"> <a href="settings.php?ses=<?php echo $ses ?>&jump=time">Time Zone</a></div>
										<div class="home_box_menu_li"> <a href="settings.php?ses=<?php echo $ses ?>&jump=eips">Excluded IPs</a></div>
										<div class="home_box_menu_li"> <a href="settings.php?ses=<?php echo $ses ?>&jump=sips">Spam IPs</a></div>
										<div class="home_box_menu_li"> <a href="settings.php?ses=<?php echo $ses ?>&jump=cookie">Cookies</a></div>
										<div class="home_box_menu_li"> <a href="settings.php?ses=<?php echo $ses ?>&jump=screen">Login Screen</a></div>
										<div class="home_box_menu_li"> <a href="settings.php?ses=<?php echo $ses ?>&jump=profile">Setup Profile</a></div>
									</div>
								</div>
							</div>

						</div>
					</td>
					<td id="td_right" valign="top">

						<div class="home_box home_box_header">Hi <?php echo $admininfo["login"] ?></div>

						<table cellspacing=0 cellpadding=0 border=0 width="100%">
						<tr>
							<td valign="top" width="50%">
								<div class="home_box" style="background: url( ../pics/icons/setup_box.gif ) no-repeat #F2F2F2; background-position: bottom right; border: 1px solid #D6D6D6;" id="home_box_start">
									<div class="home_box_header" id="td_dept_header" style="background: #FBF7B4; border: 1px solid #FFE221; color: #806732;">Basic things to do:</div>
									<div style="margin-top: 10px; font-weight: bold;">
										<div class="home_box_li_blank" style="margin-top: 0xp;"><img src="../pics/icons/add.png" width="14" height="14" border="0" alt=""> <a href="depts.php?ses=<?php echo $ses ?>">Manage Departments</a></div>
										<div class="home_box_li_blank"><img src="../pics/icons/add.png" width="14" height="14" border="0" alt=""> <a href="ops.php?ses=<?php echo $ses ?>">Manage Operators</a></div>
										<div class="home_box_li_blank"><img src="../pics/icons/persons.png" width="14" height="14" border="0" alt=""> <a href="ops.php?ses=<?php echo $ses ?>&jump=assign">Assign Operators</a></div>
										<div class="home_box_li_blank"><img src="../pics/icons/page_white_code.png" width="14" height="14" border="0" alt=""> <a href="code.php?ses=<?php echo $ses ?>">Copy/Paste HTML Code</a></div>
										<div class="home_box_li_blank"><img src="../pics/icons/user_key.png" width="14" height="14" border="0" alt=""> <a href="ops.php?ses=<?php echo $ses ?>&jump=online">Go <span style="color: #3EB538; font-weight: bold;">ONLINE</span></a></div>
									</div>
								</div>
							</td>
							<td valign="top" width="50%">
								<div class="home_box" style="border: 0px solid transparent; margin-right: 0px;">
									<div class="home_box_header"><img src="../pics/icons/agent.png" width="16" height="16" border="0" alt=""> <a href="ops.php?ses=<?php echo $ses ?>">Operators</a> &nbsp; &raquo; &nbsp; <a href="JavaScript:void(0)" onClick="launch_tools_op_status()" style="font-weight: normal; font-size: 12px;">launch status monitor</a></div>
									<div style="margin-top: 10px; height: 120px; overflow: auto; overflow-x: hidden;">
										<table cellspacing=0 cellpadding=0 border=0 width="100%">
										<tr>
											<td width="120"><div id="td_dept_header">Operator</div></td>
											<td width="80"><div id="td_dept_header">Status</div></td>
											<td><div id="td_dept_header">Chats</div></td>
										</tr>
										<?php
											for ( $c = 0; $c < count( $operators ); ++$c )
											{
												$operator = $operators[$c] ;
												$status = ( $operator["status"] ) ? "<b>Online</b>" : "Offline" ;
												$status_img = ( $operator["status"] ) ? "online_green.png" : "online_grey.png" ;
												$tr_style = ( $operator["status"] ) ? "background: #AFFF9F;" : "" ;
												$style = ( $operator["status"] ) ? "cursor: pointer" : "" ;
												$js = ( $operator["status"] ) ? "onClick='remote_disconnect($operator[opID])'" : "" ;

												$requests = Chat_get_OpTotalRequests( $dbh, $operator["opID"] ) ;

												print "<tr style=\"$tr_style\"><td class=\"td_dept_td_td\">$operator[name]</td><td class=\"td_dept_td_td\" id=\"op_status_$operator[opID]\"><img src=\"../pics/icons/$status_img\" width=\"12\" height=\"12\" border=0 title=\"$operator[name] - $status\" class=\"help_tooltip\" style=\" $style\" $js></td><td class=\"td_dept_td_td\"><a href=\"reports_chat_active.php?ses=$ses\">$requests</a></td></tr>" ;
											}
											if ( $c == 0 )
												print "<tr><td colspan=7 class=\"td_dept_td\">blank results</td></tr>" ;
										?>
										</table>
									</div>
									<div style="margin-top: 25px;" class="info_neutral">
										<table cellspacing=0 cellpadding=0 border=0 width="100%">
										<tr>
											<td width="33%" align="center"><a href="./transcripts.php?ses=<?php echo $ses ?>">Transcripts</a></td>
											<td width="33%" align="center"><a href="./reports_chat.php?ses=<?php echo $ses ?>">Chat Reports</a></td>
											<td width="33%" align="center"><a href="./reports_chat_active.php?ses=<?php echo $ses ?>">Active Chats</a></td>
										</tr>
										</table>
									</div>

								</div>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<div class="home_box" style="margin-top: 25px; background: url( ../pics/icons/setup_box_custom.gif ) no-repeat #F2F2F2; background-position: bottom right; border: 1px solid #D6D6D6;">
									<div class="home_box_header" id="td_dept_header">Customizations</div>
									<div style="margin-top: 10px; font-weight: bold;">
										<div class="home_box_li_blank" style="margin-top: 0xp;"><img src="../pics/icons/chest.png" width="14" height="14" border="0" alt=""> <a href="settings.php?ses=<?php echo $ses ?>">Upload Logo</a></div>
										<div class="home_box_li_blank"><img src="../pics/icons/image.png" width="14" height="14" border="0" alt=""> <a href="icons.php?ses=<?php echo $ses ?>">Chat Icons</a></div>
										<div class="home_box_li_blank"><img src="../pics/icons/chest.png" width="14" height="14" border="0" alt=""> <a href="settings.php?ses=<?php echo $ses ?>&jump=themes">Themes</a></div>
										<div class="home_box_li_blank"><img src="../pics/icons/page_white_code.png" width="14" height="14" border="0" alt=""> <a href="code.php?ses=<?php echo $ses ?>&jump=auto">Automatic Initiate Chat</a></div>
									</div>
								</div>
							</td>
							<td valign="top">
								<div class="home_box" style="margin-top: 25px; background: url( ../pics/icons/setup_box_market.gif ) no-repeat #F2F2F2; background-position: bottom right; border: 1px solid #D6D6D6;">
									<div class="home_box_header" id="td_dept_header">Marketing</div>
									<div style="margin-top: 10px; font-weight: bold;">
										<div class="home_box_li_blank" style="margin-top: 0xp;"><img src="../pics/icons/graph.png" width="14" height="14" border="0" alt=""> <a href="marketing.php?ses=<?php echo $ses ?>">Social Media</a></div>
										<div class="home_box_li_blank"><img src="../pics/icons/graph.png" width="14" height="14" border="0" alt=""> <a href="marketing_click.php?ses=<?php echo $ses ?>">Click Tracking</a></div>
										<div class="home_box_li_blank"><img src="../pics/icons/graph.png" width="14" height="14" border="0" alt=""> <a href="reports_marketing.php?ses=<?php echo $ses ?>">Click Report</a></div>
										<div class="home_box_li_blank"><img src="../pics/icons/graph.png" width="14" height="14" border="0" alt=""> <a href="marketing_marquee.php?ses=<?php echo $ses ?>">Footer Marquee</a></div>
									</div>
								</div>
							</td>
						</tr>
						</table>
	
					</td>
				</tr>
				</table>

			</div>
<?php include_once( "./inc_footer.php" ) ?>

<div id="remote_disconnect_notice" class="info_warning" style="display: none; position: absolute;">Disconnecting console... <img src="../pics/loading_fb.gif" width="16" height="11" border="0" alt=""></div>

</body>
</html>

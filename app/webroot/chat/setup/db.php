<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	/****************************************/
	// STANDARD header for Setup
	if ( !file_exists( "../web/config.php" ) ){ HEADER("location: install.php") ; exit ; }
	include_once( "../web/config.php" ) ;
	include_once( "../API/Util_Format.php" ) ;
	include_once( "../API/Util_Error.php" ) ;
	include_once( "../API/SQL.php" ) ;
	include_once( "../API/Util_Security.php" ) ;
	$ses = Util_Format_Sanatize( Util_Format_GetVar( "ses" ), "ln" ) ;
	if ( !$admininfo = Util_Security_AuthSetup( $dbh, $ses ) ){ ErrorHandler ( 608, "Invalid setup session or session has expired.", $PHPLIVE_FULLURL, 0, Array() ) ; }
	// STANDARD header end
	/****************************************/

	include_once( "../API/Util_Functions.php" ) ;
	include_once( "../API/Util_DB.php" ) ;

	// permission checking
	$perm_web = is_writable( "$CONF[DOCUMENT_ROOT]/web" ) ;
	$perm_conf = is_writeable( "$CONF[DOCUMENT_ROOT]/web/config.php" ) ;
	$perm_chats = is_writeable( $CONF["CHAT_IO_DIR"] ) ;
	$perm_initiate = is_writeable( $CONF["TYPE_IO_DIR"] ) ;
	$perm_patches = is_writeable( "$CONF[DOCUMENT_ROOT]/web/patches" ) ;
	$disabled_functions = ini_get( "disable_functions" ) ;
	$ini_open_basedir = ini_get("open_basedir") ;
	$ini_safe_mode = ini_get("safe_mode") ;
	$safe_mode = preg_match( "/on/i", $ini_safe_mode ) ? 1 : 0 ;

	$tables = Util_DB_GetTableNames( $dbh ) ;

	$created = date( "M j, Y", $admininfo["created"] ) ;
	$diff = time() - $admininfo["created"] ; $days_running = round( $diff/(60*60*24) ) ;
?>
<?php include_once( "../inc_doctype.php" ) ?>
<head>
<title> PHP Live! Support <?php echo $VERSION ?> </title>

<meta name="description" content="PHP Live! Support <?php echo $VERSION ?>">
<meta name="keywords" content="powered by: PHP Live!  www.phplivesupport.com">
<meta name="robots" content="all,index,follow">
<meta http-equiv="content-type" content="text/html; CHARSET=utf-8"> 

<link rel="Stylesheet" href="../css/base_setup.css?<?php echo $VERSION ?>">
<script type="text/javascript" src="../js/global.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/setup.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/tooltip.js?<?php echo $VERSION ?>"></script>

<script type="text/javascript">
<!--
	$(document).ready(function()
	{
		$('#body_sub_title').html( "<img src=\"../pics/icons/asterisk.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Home" ) ;

		init_menu() ;
		toggle_menu_setup( "" ) ;

		var help_tooltips = $( 'body' ).find('.help_tooltip' ) ;
		help_tooltips.tooltip({
			event: "mouseover",
			track: true,
			delay: 0,
			showURL: false,	
			showBody: "- ",
			fade: 0,
			positionLeft: false,
			extraClass: "stat"
		});
	});
//-->
</script>
</head>
<body>

<?php include_once( "./inc_header.php" ) ?>
			<div class="op_submenu_wrapper">
				<div style="">
					<div style="float: left; border: 0px solid transparent; width: 258px; margin-right: 25px;">
						<div class="home_box_header">PHP Live! Database Stats</div>
						<div style="margin-top: 10px; overflow: auto;">
							<table cellspacing=0 cellpadding=0 border=0>
							<tr>
								<td width="120"><div id="td_dept_header">Table</div></td>
								<td width="80"><div id="td_dept_header">Rows</div></td>
								<td><div id="td_dept_header">Status</div></td>
							</tr>
							<?php
								$approx_disc_use = 0 ;
								for( $c = 0; $c < count( $tables ); ++$c )
								{
									$analyze = Util_DB_AnalyzeTable( $dbh, $tables[$c] ) ;
									$stats = Util_DB_TableStats( $dbh, $tables[$c] ) ;

									$name = $stats["Name"] ;
									$type = $analyze["Msg_type"] ;
									$status = $analyze["Msg_text"] ;
									if ( $status = "Table is already up to date" )
										$status = "OK" ;

									$rows = $stats["Rows"] ;
									$ave_row_size = $stats["Avg_row_length"] ;
									$ave_disk = $ave_row_size * $rows ;
									$ave_size = Util_Functions_Bytes( $ave_disk ) ;
									
									$approx_disc_use += $ave_disk ;

									print "<tr class=\"help_tooltip\" title=\"- <b>Table: $name</b><br>Status: $status<br>Records: $rows rows<br>Approx. disk usage: $ave_size\" style=\"cursor: pointer;\"><td class=\"td_dept_td_td\">$name</td><td class=\"td_dept_td_td\">$rows</td><td class=\"td_dept_td_td\">$status</td></tr>" ;
								}

								$approx_disc_use = Util_Functions_Bytes( $approx_disc_use ) ;
								print "<tr><td></td><td class=\"help_tooltip\" title=\"Total Disk Usage - Approximately $approx_disc_use\" style=\"cursor: pointer;\"><b>$approx_disc_use</b></td><td></td></tr>" ;
							?>
							</table>
						</div>
					</div>
					<div style="float: left; font-size: 10px; width: 610px;">

						<div class="home_box_header">Database User Privileges</div>
						<div style="margin-top: 10px;">
						<?php
							$query = "SHOW GRANTS FOR '$CONF[SQLLOGIN]'@'$CONF[SQLHOST]'" ;
							database_mysql_query( $dbh, $query ) ;

							$output = Array() ;
							if ( $dbh[ 'ok' ] )
							{
								while ( $data = database_mysql_fetchrow( $dbh ) )
									print preg_replace( "/PASSWORD (.*?)( |$)/", "PASSWORD ************* ", $data[0] ) . "<br>" ;

								//print_r( $output ) ;
							}
							else
								print $dbh["error"] ;
						?>
						</div>

						<div class="home_box_header" style="margin-top: 25px;">Server Information</div>
						<ul style="margin-top: 10px;">
							<li> OS: <?php echo PHP_OS ?>
							<li> PHP: <?php echo phpversion() ?>
							<li> MySQL: <?php echo mysql_get_client_info() ?>
							<li> Disabled Functions: <?php print "$disabled_functions " ; ?>
							<li> Safe Mode: <?php echo "$ini_safe_mode, $safe_mode (obd: $ini_open_basedir)" ?>
						</ul>

						<div class="home_box_header" style="margin-top: 25px;">Directory and file permissions:</div>
						<ul style="margin-top: 10px;">
							<li> web/ directory: <?php echo ( $perm_web ) ? "write access OK" : "permission error" ; ?>
							<li> web/config.php: <?php echo ( $perm_conf ) ? "write access OK" : "permission error" ; ?>
							<li> web/chat_sessions: <?php echo ( $perm_chats ) ? "write access OK" : "permission error" ; ?>
							<li> web/chat_initiate: <?php echo ( $perm_initiate ) ? "write access OK" : "permission error" ; ?>
							<li> web/chat_patches: <?php echo ( $perm_patches ) ? "write access OK" : "permission error" ; ?>
						</ul>

						<div class="home_box_header" style="margin-top: 25px;">System Installed:</div>
						<ul style="margin-top: 10px;">
							<li>  <?php echo $created ?>, up <?php echo $days_running ?> days
						</ul>
					</div>
					<div style="clear:both;"></div>
				</div>

			</div>
<?php include_once( "./inc_footer.php" ) ?>

</body>
</html>

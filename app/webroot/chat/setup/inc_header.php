<?php
	$url_parts = explode( "/", $_SERVER["PHP_SELF"] ) ;
	$url_last = count( $url_parts ) - 1 ;
	$url_file = $url_parts[$url_last] ;
?>
<div id="body" style="padding-bottom: 60px;">
	<div id="body_wrapper" style="z-Index: 5;"></div>
	<div style="width: 100%; z-Index: 10;">
		<div id="body_sub">
			<div id="body_sub_options">
				<div id="body_sub_title"></div>
				<div id="popout" style="display: none; padding-left: 15px; padding-top: 8px; font-size: 10px;">&bull; open setup in a <a href="<?php $query = ( isset( $_SERVER["QUERY_STRING"] ) ) ? $_SERVER["QUERY_STRING"] : "" ; echo "./$url_file?$query" ?>" target="snew">new window</a></div>
			</div>
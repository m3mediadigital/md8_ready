<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	/****************************************/
	// STANDARD header for Setup
	if ( !file_exists( "../web/config.php" ) ){ HEADER("location: install.php") ; exit ; }
	include_once( "../web/config.php" ) ;
	include_once( "../API/Util_Format.php" ) ;
	include_once( "../API/Util_Error.php" ) ;
	include_once( "../API/SQL.php" ) ;
	include_once( "../API/Util_Security.php" ) ;
	$ses = Util_Format_Sanatize( Util_Format_GetVar( "ses" ), "ln" ) ;
	if ( !$admininfo = Util_Security_AuthSetup( $dbh, $ses ) ){ ErrorHandler ( 608, "Invalid setup session or session has expired.", $PHPLIVE_FULLURL, 0, Array() ) ; }
	// STANDARD header end
	/****************************************/

	include_once( "../API/Util_DB.php" ) ;
	include_once( "../API/Ops/get.php" ) ;
	include_once( "../API/Chat/get.php" ) ;

	$error = "" ;

	$operators = Ops_get_AllOps( $dbh ) ;
?>
<?php include_once( "../inc_doctype.php" ) ?>
<head>
<title> Online Status Monitor </title>

<meta name="description" content="PHP Live! Support <?php echo $VERSION ?>">
<meta name="keywords" content="powered by: PHP Live!  www.phplivesupport.com">
<meta name="robots" content="all,index,follow">
<meta http-equiv="content-type" content="text/html; CHARSET=utf-8"> 

<link rel="Stylesheet" href="../css/base_setup.css?<?php echo $VERSION ?>">
<script type="text/javascript" src="../js/global.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/setup.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework_cnt.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/tooltip.js?<?php echo $VERSION ?>"></script>

<script type="text/javascript">
<!--
	var refresh_counter = 25 ;
	var si_refresh ;

	$(document).ready(function()
	{
		$('#table_trs tr:nth-child(2n+3)').addClass('td_dept_td2') ;
		//do_alert( 1, "Reload Success" ) ;

		si_refresh = setInterval(function(){
			if ( refresh_counter <= 0 )
				location.href = "tools_op_status.php?ses=<?php echo $ses ?>&"+unixtime() ;
			else
			{
				$('#refresh_counter').html( pad( refresh_counter, 2 ) ) ;
				--refresh_counter ;
			}
		}, 1000) ;

		var help_tooltips = $( 'body' ).find('.help_tooltip' ) ;
		help_tooltips.tooltip({
			event: "mouseover",
			track: true,
			delay: 0,
			showURL: false,	
			showBody: "- ",
			fade: 0,
			positionLeft: false,
			extraClass: "stat"
		});
	});
	$(window).resize(function() { });

	function init_divs()
	{
		var doc_height = $(document).height() ;
		var header_height = $('#ops_list_header').outerHeight() ;
		var list_height = doc_height - header_height - 20 ;
	
		$('#ops_list').css({'height': list_height}) ;
	}

	function open_window( theurl )
	{
		window.open( theurl, "PHP Live! Setup", "scrollbars=yes,menubar=yes,resizable=1,location=yes,status=1" ) ;
	}

	function remote_disconnect( theopid )
	{
		if ( confirm( "Remote disconnect operator console?" ) )
		{
			clearInterval( si_refresh ) ;
			$('#remote_disconnect_notice').center().show() ;

			$.ajax({
				type: "GET",
				url: "../ajax/setup_actions.php",
				data: "ses=<?php echo $ses ?>&action=remote_disconnect&opid="+theopid+"&"+unixtime(),
				success: function(data){
					eval( data ) ;

					if ( json_data.status )
					{
						// 15 seconds of wait since console checks every 10 seconds
						setTimeout( function(){
							if ( window.opener != null && !window.opener.closed )
								window.opener.location.href = 'index.php?ses=<?php echo $ses ?>' ;

							location.href = 'tools_op_status.php?ses=<?php echo $ses ?>' ;
						}, 15000 ) ;
					}
					else
					{
						$('#remote_disconnect_notice').hide() ;
						do_alert( 0, "Could not remote disconnect console.  Please try again." ) ;
					}
				}
			});
		}
	}
//-->
</script>
</head>
<body>
<div id="ops_list_header" style="position: fixed; width: 100%; top: 0px; left: 0px; background: url( ../pics/bg_intro.jpg ) repeat-x; background-position: bottom center;">
	<div style="padding: 5px; font-size: 16px; font-weight: bold; color: #FFFFFF; text-shadow: #646C82 1px 1px; background: url( ../pics/bg_trans.png ) repeat;">Operator Online Status Monitor</div>
	<div style="padding: 5px; color: #FFFFFF;"> Auto reload in <span id="refresh_counter">25</span> seconds.</div>
</div>

<div id="ops_list" style="padding: 5px; padding-top: 60px;">
	<table cellspacing=0 cellpadding=0 border=0 width="100%" id="table_trs">
	<tr>
		<td width="120"><div id="td_dept_header">Operator</div></td>
		<td width="80"><div id="td_dept_header">Status</div></td>
		<td><div id="td_dept_header">Chats</div></td>
	</tr>
	<?php
		for ( $c = 0; $c < count( $operators ); ++$c )
		{
			$operator = $operators[$c] ;
			$status = ( $operator["status"] ) ? "<b>Online</b>" : "Offline" ;
			$status_img = ( $operator["status"] ) ? "online_green.png" : "online_grey.png" ;
			$tr_style = ( $operator["status"] ) ? "background: #AFFF9F;" : "" ;
			$style = ( $operator["status"] ) ? "cursor: pointer;" : "" ;
			$js = ( $operator["status"] ) ? "onClick='remote_disconnect($operator[opID])'" : "" ;

			$requests = Chat_get_OpTotalRequests( $dbh, $operator["opID"] ) ;

			print "<tr style=\"$tr_style\"><td class=\"td_dept_td_td\">$operator[name]</td><td class=\"td_dept_td_td\"><img src=\"../pics/icons/$status_img\" width=\"12\" height=\"12\" border=0 title=\"$operator[name] - $status\" class=\"help_tooltip\" style=\" $style\" $js></td><td class=\"td_dept_td_td\"><a href=\"JavaScript:void(0)\" onClick=\"open_window('$CONF[BASE_URL]/setup/reports_chat_active.php?ses=$ses')\">$requests</a></td></tr>" ;
		}
		if ( $c == 0 )
			print "<tr><td colspan=7 class=\"td_dept_td\">blank results</td></tr>" ;
	?>
	</table>
</div>

<div id="remote_disconnect_notice" class="info_warning" style="display: none; position: absolute;">Disconnecting console... <img src="../pics/loading_fb.gif" width="16" height="11" border="0" alt=""></div>

</body>
</html>

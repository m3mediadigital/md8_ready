<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	/****************************************/
	// STANDARD header for Setup
	if ( !file_exists( "../web/config.php" ) ){ HEADER("location: install.php") ; exit ; }
	include_once( "../web/config.php" ) ;
	include_once( "../API/Util_Format.php" ) ;
	include_once( "../API/Util_Error.php" ) ;
	include_once( "../API/SQL.php" ) ;
	include_once( "../API/Util_Security.php" ) ;
	$ses = Util_Format_Sanatize( Util_Format_GetVar( "ses" ), "ln" ) ;
	if ( !$admininfo = Util_Security_AuthSetup( $dbh, $ses ) ){ ErrorHandler ( 608, "Invalid setup session or session has expired.", $PHPLIVE_FULLURL, 0, Array() ) ; }
	// STANDARD header end
	/****************************************/

	$error = "" ;

	include_once( "../API/Ops/get.php" ) ;
	include_once( "../API/Ops/update.php" ) ;
	include_once( "../API/Depts/get.php" ) ;

	$action = Util_Format_Sanatize( Util_Format_GetVar( "action" ), "ln" ) ;
	$jump = Util_Format_Sanatize( Util_Format_GetVar( "jump" ), "ln" ) ;
	if ( !$jump )
		$jump = "main" ;

	if ( $action == "submit" )
	{
		include_once( "../API/Ops/put.php" ) ;
		include_once( "../API/Ops/get_ext.php" ) ;

		$opid = Util_Format_Sanatize( Util_Format_GetVar( "opid" ), "ln" ) ;
		$rate = Util_Format_Sanatize( Util_Format_GetVar( "rate" ), "ln" ) ;
		$sms = Util_Format_Sanatize( Util_Format_GetVar( "sms" ), "ln" ) ;
		$op2op = Util_Format_Sanatize( Util_Format_GetVar( "op2op" ), "ln" ) ;
		$traffic = Util_Format_Sanatize( Util_Format_GetVar( "traffic" ), "ln" ) ;
		$viewip = Util_Format_Sanatize( Util_Format_GetVar( "viewip" ), "ln" ) ;
		$login = Util_Format_Sanatize( Util_Format_GetVar( "login" ), "ln" ) ;
		$password = Util_Format_Sanatize( Util_Format_GetVar( "password" ), "" ) ;
		$name = Util_Format_Sanatize( Util_Format_GetVar( "name" ), "ln" ) ;
		$email = Util_Format_Sanatize( Util_Format_GetVar( "email" ), "e" ) ;

		$total_ops = Ops_get_TotalOps( $dbh ) ;
	
		if ( isset( $VARS_MAX_OPS ) && ( $total_ops >= $VARS_MAX_OPS ) && !$opid )
			$error = "Max allowed operators has been reached." ;
		else if ( !Ops_put_Op( $dbh, $opid, 1, $rate, $sms, $op2op, $traffic, $viewip, $login, $password, $name, $email ) )
			$error = "Operator login ($login) is in use." ;
	}
	else if ( $action == "delete" )
	{
		include_once( "../API/Ops/remove.php" ) ;

		$opid = Util_Format_Sanatize( Util_Format_GetVar( "opid" ), "ln" ) ;
		Ops_remove_Op( $dbh, $opid ) ;
	}
	else if ( $action == "submit_assign" )
	{
		include_once( "../API/Ops/put.php" ) ;

		$opids = Util_Format_GetVar( "opids" ) ;
		$deptids = Util_Format_GetVar( "deptids" ) ;

		for ( $c = 0; $c < count( $opids ); ++$c )
		{
			$opid = Util_Format_Sanatize( $opids[$c], "ln" ) ;
			for ( $c2 = 0; $c2 < count( $deptids ); ++$c2 )
			{
				$deptid = Util_Format_Sanatize( $deptids[$c2], "ln" ) ;
				$opinfo = Ops_get_OpInfoByID( $dbh, $opid ) ;
				$deptinfo = Depts_get_DeptInfo( $dbh, $deptid ) ;
				Ops_put_OpDept( $dbh, $opid, $deptid, $deptinfo["visible"], $opinfo["status"] ) ;
			}
		}
	}

	Ops_update_IdleOps( $dbh ) ;
	$operators = Ops_get_AllOps( $dbh ) ;
	$departments = Depts_get_AllDepts( $dbh ) ;
?>
<?php include_once( "../inc_doctype.php" ) ?>
<head>
<title> PHP Live! Support <?php echo $VERSION ?> </title>

<meta name="description" content="PHP Live! Support <?php echo $VERSION ?>">
<meta name="keywords" content="powered by: PHP Live!  www.phplivesupport.com">
<meta name="robots" content="all,index,follow">
<meta http-equiv="content-type" content="text/html; CHARSET=utf-8"> 

<link rel="Stylesheet" href="../css/base_setup.css?<?php echo $VERSION ?>">
<script type="text/javascript" src="../js/global.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/setup.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework_cnt.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/tooltip.js?<?php echo $VERSION ?>"></script>

<script type="text/javascript">
<!--
	$(document).ready(function()
	{
		$("body").show() ;
		$('#body_sub_title').html( "<img src=\"../pics/icons/person.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Operators" ) ;

		init_menu() ;
		toggle_menu_setup( "ops" ) ;
		init_op_dept_list() ;

		var help_tooltips = $( 'body' ).find( '.help_tooltip' ) ;
		help_tooltips.tooltip({
			event: "mouseover",
			track: true,
			delay: 0,
			showURL: false,	
			showBody: "- ",
			fade: 0,
			extraClass: "stat"
		});

		show_div( "<?php echo $jump ?>" ) ;

		<?php if ( $action && !$error ): ?>do_alert( 1, "Update Success!" ) ;<?php endif ; ?>
	});

	function init_op_dept_list() { <?php for ( $c = 0; $c < count( $departments ); ++$c ) { $department = $departments[$c] ; if ( $department["name"] != "Archive" ) { print "op_dept_moveup( $department[deptID], 0 ) ;" ; } } ?> }

	function do_edit( theopid, thename, theemail, thelogin, therate, thesms, theop2op, thetraffic, theviewip )
	{
		$( "input#opid" ).val( theopid ) ;
		$( "input#name" ).val( thename ) ;
		$( "input#email" ).val( theemail ) ;
		$( "input#login" ).val( thelogin ) ;
		$( "input#password" ).val( "php-live-support" ) ;
		$( "input#rate_"+therate ).attr( "checked", true ) ;
		$( "input#sms_"+thesms ).attr( "checked", true ) ;
		$( "input#op2op_"+theop2op ).attr( "checked", true ) ;
		$( "input#traffic_"+thetraffic ).attr( "checked", true ) ;
		$( "input#viewip_"+theviewip ).attr( "checked", true ) ;
		location.href = "#a_edit" ;
	}

	function do_delete( theopid )
	{
		if ( confirm( "Really delete this operator and all their data?" ) )
			location.href = "ops.php?ses=<?php echo $ses ?>&action=delete&opid="+theopid ;
	}

	function do_submit()
	{
		var name = encodeURIComponent( $( "input#name" ).val() ) ;
		var email = $( "input#email" ).val() ;
		var login = encodeURIComponent( $( "input#login" ).val() ) ;
		var password = encodeURIComponent( $( "input#password" ).val() ) ;

		if ( name == "" )
			do_alert( 0, "Please provide a name." ) ;
		else if ( !check_email( email ) )
			do_alert( 0, "Please provide a valid email address." ) ;
		else if ( login == "" )
			do_alert( 0, "Please provide a login." ) ;
		else if ( password == "" )
			do_alert( 0, "Please provide a password." ) ;
		else
		{
			email = encodeURIComponent( email ) ;
			$('#theform').submit() ;
		}
	}

	function show_div( thediv )
	{
		var divs = Array( "main", "assign", "report", "monitor", "online" ) ;

		for ( c = 0; c < divs.length; ++c )
		{
			$('#edit').hide() ;

			$('#ops_'+divs[c]).hide() ;
			$('#menu_ops_'+divs[c]).removeClass('op_submenu_focus').addClass('op_submenu') ;
		}

		if ( thediv == "main" )
			$('#edit').show() ;

		$('input#jump').val( thediv ) ;
		$('#ops_'+thediv).show() ;
		$('#menu_ops_'+thediv).removeClass('op_submenu').addClass('op_submenu_focus') ;
	}

	function op_dept_moveup( thedeptid, theopid )
	{
		$('#dept_ops_'+thedeptid).css({'opacity': 1, 'z-Index': -10}) ;

		$.ajax({
			type: "GET",
			url: "../ajax/setup_actions.php",
			data: "ses=<?php echo $ses ?>&action=moveup&deptid="+thedeptid+"&opid="+theopid+"&"+unixtime(),
			success: function(data){
				eval( data ) ;

				if ( json_data.ops != undefined )
				{
					var ops_string = "<table cellspacing=0 cellpadding=0 border=0 width=\"92%\">" ;
					for ( c = 0; c < json_data.ops.length; ++c )
					{
						var name = json_data.ops[c]["name"] ;
						var opid = json_data.ops[c]["opid"] ;
						var move_up = ( c ) ? "<a href=\"JavaScript:void(0)\" onClick=\"op_dept_moveup( "+thedeptid+", "+opid+" )\"><img src=\"../pics/icons/top.png\" width=\"14\" height=\"14\" border=\"0\" alt=\"move up\" title=\"move up\"></a>" : "<img src=\"../pics/space.gif\" width=\"14\" height=\"14\" border=\"0\">" ;

						ops_string += "<tr><td class=\"td_dept_td\" width=\"14\"><a href=\"JavaScript:void(0)\" onClick=\"op_dept_remove( "+thedeptid+", "+opid+" )\"><img src=\"../pics/icons/delete.png\" width=\"14\" height=\"14\" border=\"0\" alt=\"remove\" title=\"remove\"></a></td><td class=\"td_dept_td\" width=\"14\">"+move_up+"</td><td class=\"td_dept_td\">"+name+"</td></tr>" ;
					}
					if ( c == 0 )
						ops_string += "<tr><td colspan=7 class=\"td_dept_td\">blank results</td></tr>" ;
				}
				ops_string += "</table>" ;
				$('#dept_ops_'+thedeptid).html( ops_string ) ;
				setTimeout(function(){ $('#dept_ops_'+thedeptid).css({'opacity': 1, 'z-Index': 10}) ; }, 500) ;
			}
		});
	}

	function op_dept_remove( thedeptid, theopid )
	{
		$('#dept_ops_'+thedeptid).css({'opacity': 1, 'z-Index': -10}) ;

		$.ajax({
			type: "GET",
			url: "../ajax/setup_actions.php",
			data: "ses=<?php echo $ses ?>&action=op_dept_remove&deptid="+thedeptid+"&opid="+theopid+"&"+unixtime(),
			success: function(data){
				eval( data ) ;

				if ( json_data.status )
					op_dept_moveup( thedeptid, 0 ) ;
			}
		});
	}

	function remote_disconnect( theopid )
	{
		if ( confirm( "Remote disconnect operator console?" ) )
		{
			$('#remote_disconnect_notice').center().show() ;

			$.ajax({
				type: "GET",
				url: "../ajax/setup_actions.php",
				data: "ses=<?php echo $ses ?>&action=remote_disconnect&opid="+theopid+"&"+unixtime(),
				success: function(data){
					eval( data ) ;

					if ( json_data.status )
					{
						// 15 seconds of wait since console checks every 10 seconds
						setTimeout( function(){ location.href = 'ops.php?ses=<?php echo $ses ?>' ; }, 15000 ) ;
					}
					else
					{
						$('#remote_disconnect_notice').hide() ;
						do_alert( 0, "Could not remote disconnect console.  Please try again." ) ;
					}
				}
			});
		}
	}

	function launch_tools_op_status()
	{
		var url = "tools_op_status.php?ses=<?php echo $ses ?>" ;
		window.open( url, "Operators", "scrollbars=yes,menubar=no,resizable=1,location=no,width=270,height=220,status=0" ) ;
	}

//-->
</script>
</head>
<body style="display: none;">

<?php include_once( "./inc_header.php" ) ?>

		<div class="op_submenu_wrapper">
			<div class="op_submenu_focus" onClick="show_div('main')" id="menu_ops_main">Operator Main</div>
			<div class="op_submenu" onClick="show_div('assign')" id="menu_ops_assign">Assign Operators to Department</div>
			<div class="op_submenu" onClick="location.href='ops_reports.php?ses=<?php echo $ses ?>'" id="menu_ops_report">Online Activity</div>
			<div class="op_submenu" onClick="show_div('monitor')" id="menu_ops_monitor">Online Status Monitor</div>
			<div class="op_submenu" onClick="show_div('online')" id="menu_ops_online"><img src="../pics/icons/user_key.png" width="12" height="12" border="0" alt=""> Go ONLINE!</div>
			<div style="clear: both"></div>
		</div>

		<div id="ops_main">
			<div class="info_info" style="margin-top: 15px;">
				<b>NOTE:</b> If an operator forgets their password, you will need to edit their information and create a new password.
			</div>

			<div style="margin-top: 25px; max-height: 250px; overflow: auto;">
				<table cellspacing=0 cellpadding=0 border=0 width="96%">
				<tr>
					<td width="40"><div id="td_dept_header">&nbsp;</div></td>
					<td><div id="td_dept_header">Name</div></td>
					<td><div id="td_dept_header">Login</div></td>
					<td><div id="td_dept_header">Email</div></td>
					<td width="60" nowrap align="center"><div id="td_dept_header">Rate</div></td>
					<td width="90" nowrap align="center"><div id="td_dept_header">SMS Alerts</div></td>
					<td width="70" nowrap align="center"><div id="td_dept_header">Op-2-Op</div></td>
					<td width="60" nowrap align="center"><div id="td_dept_header">Traffic</div></td>
					<td width="80" nowrap align="center"><div id="td_dept_header">View IP</div></td>
					<td width="60" nowrap align="center"><div id="td_dept_header">Status</div></td>
				</tr>
				<?php
					$image_empty = "<img src=\"../pics/space.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"\">" ;
					$image_checked = "<img src=\"../pics/icons/check.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\">";
					for ( $c = 0; $c < count( $operators ); ++$c )
					{
						$operator = $operators[$c] ;

						$login = $operator["login"] ;
						$email = $operator["email"] ;
						$rate = ( $operator["rate"] ) ? $image_checked : $image_empty ;
						$sms = ( $operator["sms"] ) ? $image_checked : $image_empty ;
						$op2op = ( $operator["op2op"] ) ? $image_checked : $image_empty ;
						$traffic = ( $operator["traffic"] ) ? $image_checked : $image_empty ;
						$viewip = ( $operator["viewip"] ) ? $image_checked : $image_empty ;
						$status = ( $operator["status"] ) ? "<b>Operator is Online</b><br>Click to disconnect console." : "Offline" ;
						$status_img = ( $operator["status"] ) ? "online_green.png" : "online_grey.png" ;
						$style = ( $operator["status"] ) ? "cursor: pointer" : "" ;
						$js = ( $operator["status"] ) ? "onClick='remote_disconnect($operator[opID])'" : "" ;
						$sms_edit = ( $operator["sms"] ) ? 1 : 0 ;

						$edit_delete = "<a href=\"JavaScript:void(0)\" onClick=\"do_delete($operator[opID])\" class=\"help_tooltip nounder\" title=\"- delete operator\"><img src=\"../pics/icons/delete.png\" width=\"14\" height=\"14\" border=\"0\" alt=\"\"></a> &nbsp; <a href=\"JavaScript:void(0)\" onClick=\"do_edit( $operator[opID], '$operator[name]', '$operator[email]', '$operator[login]', $operator[rate], $sms_edit, $operator[op2op], $operator[traffic], $operator[viewip] )\" class=\"help_tooltip nounder\" title=\"- edit operator\"><img src=\"../pics/icons/edit.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\"></a>" ;

						$td1 = "td_dept_td" ; $td2 = "td_dept_td_td" ;
						if ( $c % 2 ) { $td1 = "td_dept_td2" ; $td2 = "td_dept_td_td2" ; }

						print "
						<tr>
							<td class=\"$td1\" nowrap>$edit_delete</td>
							<td class=\"$td1\">
								<div style=\"\">$operator[name]</div>
							</td>
							<td class=\"$td1\">$login</td>
							<td class=\"$td1\">$email</td>
							<td class=\"$td1\" align=\"center\">$rate</td>
							<td class=\"$td1\" align=\"center\">$sms</td>
							<td class=\"$td1\" align=\"center\">$op2op</td>
							<td class=\"$td1\" align=\"center\">$traffic</td>
							<td class=\"$td1\" align=\"center\">$viewip</td>
							<td class=\"$td1\" align=\"center\"><img src=\"../pics/icons/$status_img\" width=\"12\" height=\"12\" border=0 title=\" - $status\" class=\"help_tooltip\" style=\" $style\" $js></td>
						</tr>
						" ;
					}
					if ( $c == 0 )
						print "<tr><td colspan=7 class=\"td_dept_td\">blank results</td></tr>" ;
				?>
				</table>
			</div>
		</div>

		<div style="display: none;" id="ops_assign">

			<div class="info_info" style="margin-top: 15px;">
				Assign operators to departments.  An operator can be assigned to multiple departments.
			</div>

			<div style="margin-top: 25px;">
				<?php if ( !count( $departments ) ): ?>
				<span class="info_error"><img src="../pics/icons/warning.png" width="16" height="16" border="0" alt=""> You'll want to create a <a href="depts.php?ses=<?php echo $ses ?>" style="color: #FFFFFF;">Department</a> to continue.</span>
				<?php elseif ( !count( $operators ) ):  ?>
				<span class="info_error"><img src="../pics/icons/warning.png" width="16" height="16" border="0" alt=""> You'll want to create an <a href="JavaScript:void(0)" onClick="show_div('main')" style="color: #FFFFFF;">Operator</a> to continue.</span>

				<?php else: ?>
				<table cellspacing=0 cellpadding=0 border=0 width="100%">
				<tr>
					<td valign="top" width="100%">
						<form method="POST" action="ops.php?submit">
						<input type="hidden" name="ses" value="<?php echo $ses ?>">
						<input type="hidden" name="action" value="submit_assign">
						<input type="hidden" name="jump" value="assign">
						
						<div style="max-height: 250px; overflow: auto;">
							<table cellspacing=0 cellpadding=0 border=0 width="96%">
							<tr>
								<td width="20"><div id="td_dept_header">&nbsp;</div></td>
								<td><div id="td_dept_header">Operator Name</div></td>
								<td><div id="td_dept_header">Email</div></td>
							</tr>
							<?php
								for ( $c = 0; $c < count( $operators ); ++$c )
								{
									$operator = $operators[$c] ;

									$td1 = "td_dept_td" ; $td2 = "td_dept_td_td" ;
									if ( $c % 2 ) { $td1 = "td_dept_td2" ; $td2 = "td_dept_td_td2" ; }

									print "
									<tr>
										<td class=\"$td1\"><input type=\"checkbox\" name=\"opids[]\" value=\"$operator[opID]\"></td>
										<td class=\"$td1\" nowrap>
											<div style=\"\">$operator[name]</div>
										</td>
										<td class=\"$td1\">$operator[email]</td>
									</tr>
									" ;
								}
							?>
							</table>
						</div>
						<div class="info_info" style="margin-top: 15px;">
							<div class="info_box">Assign the above checked operator(s) to the following department(s):</div>

							<div style="margin-top: 10px;">
							<?php
								$ops_assigned = 0 ;
								for ( $c = 0; $c < count( $departments ); ++$c )
								{
									$department = $departments[$c] ;
									$ops = Depts_get_DeptOps( $dbh, $department["deptID"] ) ;
									if ( count( $ops ) )
										$ops_assigned = 1 ;

									if ( $department["name"] != "Archive" )
										print "<div class=\"info_clear\"><input type=\"checkbox\" name=\"deptids[]\" value=\"$department[deptID]\"> $department[name]</div>" ;
								}
							?>
							</div>

							<div style="margin-top: 10px;">
								<input type="submit" value="Submit">
							</div>
						</div>
						</form>
					</td>
					<td valign="top">
						<table cellspacing=0 cellpadding=0 border=0 style="width: 230px; margin-left: 15px;">
						<tr>
							<td style="">
								<div class="info_neutral" style="margin-bottom: 15px;">Operator Assignment & <span class="info_box">Defined Order</span></div>
								<?php
									for ( $c = 0; $c < count( $departments ); ++$c )
									{
										$department = $departments[$c] ;

										if ( $department["name"] != "Archive" )
										{
											print "<div id=\"td_dept_header\"><img src=\"../pics/icons/persons.png\" width=\"12\" height=\"12\" border=\"0\" alt=\"\">  $department[name]</div><div id=\"dept_ops_$department[deptID]\" style=\"min-height: 25px; max-height: 200px; overflow: auto;\"></div>" ;
										}
									}
								?>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
				<?php endif ; ?>
			</div>
		</div>


		<div style="display: none;" id="ops_monitor">
			<div class="info_info" style="margin-top: 15px;">
				Monitor the operator's online/offline status and chat activity within a tiny widget window that updates automatically.  The widget window can be left open on the desktop for quick access to online/offline status information and real-time chat sessions.
			</div>

			<div style="margin-top: 25px;">
				<button type="button" onClick="launch_tools_op_status()" style="padding: 6px;">Launch Operator Status Widget Window</button>
			</div>
		</div>

		<div style="display: none;" id="ops_online">
			<div class="info_info" style="margin-top: 15px;">
				<?php if ( !count( $departments ) ): ?>
				<span class="info_error"><img src="../pics/icons/warning.png" width="16" height="16" border="0" alt=""> You'll want to create a <a href="depts.php?ses=<?php echo $ses ?>" style="color: #FFFFFF;">Department</a> to continue.</span>
				<?php elseif ( !count( $operators ) ): ?>
				<span class="info_error"><img src="../pics/icons/warning.png" width="16" height="16" border="0" alt=""> You'll want to create an <a href="ops.php?ses=<?php echo $ses ?>" style="color: #FFFFFF;">Operator</a> to continue.</span>
				<?php elseif ( !$ops_assigned ): ?>
				<span class="info_error"><img src="../pics/icons/warning.png" width="16" height="16" border="0" alt=""> You'll want to <a href="ops.php?ses=<?php echo $ses ?>&jump=assign" style="color: #FFFFFF;">assign an operator to a department</a> to continue.</span>
				<?php else: ?>
				<div style="font-size: 16px; font-weight: bold;"><img src="../pics/icons/user_key.png" width="16" height="16" border="0" alt=""> Operator Login URL</div>
				<div style="margin-top: 5px;">Provide the following Operator Login URL to your <a href="JavaScript:void(0)" onClick="show_div('main')">operators</a>.  Once logged in, launch the operator console to go <span style="color: #3EB538; font-weight: bold;">ONLINE</span> and accept visitor chat requests.</div>

				<div style="margin-top: 15px; font-size: 18px; font-weight: bold;"><a href="<?php echo $CONF["BASE_URL"] ?>" target="new" style="" class="nounder"><?php echo $CONF["BASE_URL"] ?></a></div>

				<div style="margin-top: 25px;"><img src="../pics/icons/info.png" width="16" height="16" border="0" alt=""> Don't forget to place the online/offline chat icon <a href="./code.php?ses=<?php echo $ses ?>">HTML Code</a> onto your webpages.</div>
				<?php endif ; ?>
			</div>
		</div>

		<div id="edit" class="edit_wrapper" style="display: none; padding: 5px; margin-top: 55px;">
			<a name="a_edit"></a><div class="edit_title">Create/Edit Operator <span class="txt_red"><?php echo $error ?></span></div>
			<div style="margin-top: 10px;">
				<table cellspacing=0 cellpadding=5 border=0>
				<form method="POST" action="ops.php?submit" id="theform">
				<input type="hidden" name="ses" value="<?php echo $ses ?>">
				<input type="hidden" name="action" value="submit">
				<input type="hidden" name="jump" id="jump" value="">
				<input type="hidden" name="opid" id="opid" value="0">
				<tr>
					<td>Operator Name</td>
					<td> <input type="text" name="name" id="name" size="30" maxlength="40" value="" onKeyPress="return noquotes(event)"></td>
					<td><img src="../pics/space.gif" width="25" height=1></td>

					<td><img src="../pics/icons/help.png" width="14" height="14" border="0" class="help_tooltip" title="- <b>Visitor Rate</b><br>Allow visitors to rate this operator when chat session ends."> Visitor Rate</td>
					<td> <input type="radio" name="rate" id="rate_1" value="1" checked> Yes <input type="radio" name="rate" id="rate_0" value="0"> No</td>
				</tr>
				<tr>
					<td>Operator Email</td>
					<td> <input type="text" name="email" id="email" size="30" maxlength="160" value="" onKeyPress="return justemails(event)"></td>
					<td>&nbsp;</td>

					<td><img src="../pics/icons/help.png" width="14" height="14" border="0" class="help_tooltip" title="- <b>Operator to Operator Chat</b><br>Allow this operator to request Operator to Operator (Op-2-Op) chat with other online operators."> Op-2-Op Chat</td>
					<td> <input type="radio" name="op2op" id="op2op_1" value="1" checked> Yes <input type="radio" name="op2op" id="op2op_0" value="0"> No</td>
				</tr>
				<tr>
					<td>Login</td>
					<td> <input type="text" name="login" id="login" size="30" maxlength="160" value="" onKeyPress="return noquotestags(event)"></td>
					<td>&nbsp;</td>

					<td><img src="../pics/icons/help.png" width="14" height="14" border="0" class="help_tooltip" title="- <b>Traffic Monitor</b><br>Allow this operator to view website traffic and perform initiate chat and other proactive functions."> Traffic Monitor</td>
					<td> <input type="radio" name="traffic" id="traffic_1" value="1" checked> Yes <input type="radio" name="traffic" id="traffic_0" value="0"> No</td>
				</tr>
				<tr>
					<td>Password</td>
					<td> <input type="password" name="password" id="password" class="input" size="30" maxlength="15" value="" onKeyPress="return noquotes(event)"></td>
					<td>&nbsp;</td>

					<td><img src="../pics/icons/help.png" width="14" height="14" border="0" class="help_tooltip" title="- <b>View IP</b><br>Allow this operator to view IP addresses on the traffic monitor and during (IP and Hostname)."> View IP</td>
					<td> <input type="radio" name="viewip" id="viewip_1" value="1"> Yes <input type="radio" name="viewip" id="viewip_0" value="0" checked> No</td>
				</tr>
				<tr>
					<td colspan=3>&nbsp;</td>

					<td><img src="../pics/icons/help.png" width="14" height="14" border="0" class="help_tooltip" title="- <b>SMS Alerts</b><br>Allow this operator to activate and receive SMS chat request alerts."> SMS Alerts</td>
					<td> <input type="radio" name="sms" id="sms_1" value="1"> Yes <input type="radio" name="sms" id="sms_0" value="0" checked> No</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>

					<td colspan="2"> <div style=""><input type="button" value="Submit" onClick="do_submit()"> <input type="reset" value="Cancel" onClick="$( 'input#opid' ).val(0)"></div></td>
				</tr>
				</form>
				</table>
			</div>
		</div>
<?php include_once( "./inc_footer.php" ) ?>

<div id="remote_disconnect_notice" class="info_warning" style="display: none; position: absolute;">Disconnecting console... <img src="../pics/loading_fb.gif" width="16" height="11" border="0" alt=""></div>

</body>
</html>

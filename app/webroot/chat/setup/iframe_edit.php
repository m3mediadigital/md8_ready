<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	/****************************************/
	// STANDARD header for Setup
	if ( !file_exists( "../web/config.php" ) ){ HEADER("location: install.php") ; exit ; }
	include_once( "../web/config.php" ) ;
	include_once( "../API/Util_Format.php" ) ;
	include_once( "../API/Util_Error.php" ) ;
	include_once( "../API/SQL.php" ) ;
	include_once( "../API/Util_Security.php" ) ;
	$ses = Util_Format_Sanatize( Util_Format_GetVar( "ses" ), "ln" ) ;
	if ( !$admininfo = Util_Security_AuthSetup( $dbh, $ses ) ){ ErrorHandler ( 608, "Invalid setup session or session has expired.", $PHPLIVE_FULLURL, 0, Array() ) ; }
	// STANDARD header end
	/****************************************/

	$error = $sub = "" ;

	include_once( "../API/Ops/get.php" ) ;
	include_once( "../API/Depts/get.php" ) ;

	$page = ( Util_Format_Sanatize( Util_Format_GetVar( "page" ), "ln" ) ) ? Util_Format_Sanatize( Util_Format_GetVar( "page" ), "ln" ) : 0 ;
	$action = Util_Format_Sanatize( Util_Format_GetVar( "action" ), "ln" ) ;
	$option = Util_Format_Sanatize( Util_Format_GetVar( "option" ), "ln" ) ;
	$sub = Util_Format_Sanatize( Util_Format_GetVar( "sub" ), "ln" ) ;
	$deptid = Util_Format_Sanatize( Util_Format_GetVar( "deptid" ), "ln" ) ;

	$departments = Depts_get_AllDepts( $dbh ) ;

	if ( $action == "update" )
	{
		include_once( "../API/Depts/update.php" ) ;

		$copy_all = Util_Format_Sanatize( Util_Format_GetVar( "copy_all" ), "ln" ) ;
		$message = Util_Format_Sanatize( Util_Format_GetVar( "message" ), "" ) ;

		if ( $sub == "greeting" )
		{
			if ( $copy_all )
			{
				for( $c = 0; $c < count( $departments ); ++$c )
					Depts_update_UserDeptValue( $dbh, $departments[$c]["deptID"], "msg_greet", $message ) ;
			}
			else
				Depts_update_UserDeptValue( $dbh, $deptid, "msg_greet", $message ) ;
		}
		else if ( $sub == "offline" )
		{
			if ( $copy_all )
			{
				for( $c = 0; $c < count( $departments ); ++$c )
					Depts_update_UserDeptValue( $dbh, $departments[$c]["deptID"], "msg_offline", $message ) ;
			}
			else
				Depts_update_UserDeptValue( $dbh, $deptid, "msg_offline", $message ) ;
		}
		else if ( $sub == "temail" )
		{
			if ( $copy_all )
			{
				for( $c = 0; $c < count( $departments ); ++$c )
					Depts_update_UserDeptValue( $dbh, $departments[$c]["deptID"], "msg_email", $message ) ;
			}
			else
				Depts_update_UserDeptValue( $dbh, $deptid, "msg_email", $message ) ;
		}
		else if ( $sub == "settings" )
		{
			$remail = Util_Format_Sanatize( Util_Format_GetVar( "remail" ), "ln" ) ;
			$temail = Util_Format_Sanatize( Util_Format_GetVar( "temail" ), "ln" ) ;
			$temaild = Util_Format_Sanatize( Util_Format_GetVar( "temaild" ), "ln" ) ;

			if ( $copy_all )
			{
				for( $c = 0; $c < count( $departments ); ++$c )
					Depts_update_UserDeptValues( $dbh, $departments[$c]["deptID"], "remail", $remail, "temail", $temail, "temaild", $temaild ) ;
			}
			else
				Depts_update_UserDeptValues( $dbh, $deptid, "remail", $remail, "temail", $temail, "temaild", $temaild ) ;
		}
		else if ( $sub == "canned" )
		{
			include_once( "../API/Canned/get.php" ) ;
			include_once( "../API/Canned/put.php" ) ;

			$message = Util_Format_Sanatize( Util_Format_GetVar( "message" ), "" ) ;
			$canid = Util_Format_Sanatize( Util_Format_GetVar( "canid" ), "ln" ) ;
			$title = Util_Format_Sanatize( Util_Format_GetVar( "title" ), "ln" ) ;
			$message = Util_Format_Sanatize( Util_Format_GetVar( "message" ), "" ) ;
			$sub_deptid = Util_Format_Sanatize( Util_Format_GetVar( "sub_deptid" ), "ln" ) ;

			$caninfo = Canned_get_CanInfo( $dbh, $canid ) ;
			if ( isset( $caninfo["opID"] ) )
				$opid = $caninfo["opID"] ;
			else
				$opid = 1111111111 ;

			if ( !Canned_put_Canned( $dbh, $canid, $opid, $deptid, $title, $message ) )
				$error = "Error processing canned message." ;
			$deptid = $sub_deptid ;
		}
		$action = $sub ;
	}
	else if ( $action == "delete" )
	{
		include_once( "../API/Canned/get.php" ) ;
		include_once( "../API/Canned/remove.php" ) ;

		$canid = Util_Format_Sanatize( Util_Format_GetVar( "canid" ), "ln" ) ;

		$caninfo = Canned_get_CanInfo( $dbh, $canid ) ;
		Canned_remove_Canned( $dbh, $caninfo["opID"], $canid ) ;
		$action = $sub ;
	}
	
	$deptinfo = Depts_get_DeptInfo( $dbh, $deptid ) ;
	$deptname = $deptinfo["name"] ;

	switch ( $action )
	{
		case "greeting":
			$title = "Chat Greeting" ;
			$message = $deptinfo["msg_greet"] ;
			break ;
		case "offline":
			$title = "Offline Message" ;
			$message = $deptinfo["msg_offline"] ;
			break ;
		case "temail":
			$title = "Email Settings" ;
			$message = $deptinfo["msg_email"] ;
			break ;
		case "canned":
			$title = "Canned Responses" ;
			break ;
		case "settings":
			$title = "Settings" ;
			break ;
		default:
			$title = "Invalid" ;
	}
?>
<?php include_once( "../inc_doctype.php" ) ?>
<head>
<title> PHP Live! Support <?php echo $VERSION ?> </title>

<meta name="description" content="PHP Live! Support <?php echo $VERSION ?>">
<meta name="keywords" content="powered by: PHP Live!  www.phplivesupport.com">
<meta name="robots" content="all,index,follow">
<meta http-equiv="content-type" content="text/html; CHARSET=utf-8"> 

<link rel="Stylesheet" href="../css/base_setup.css?<?php echo $VERSION ?>">
<script type="text/javascript" src="../js/global.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/setup.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework_cnt.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/tooltip.js?<?php echo $VERSION ?>"></script>

<script type="text/javascript">
<!--
	var winname = unixtime() ;
	var option = <?php echo $option ?> ; // used to communicate with depts.php to toggle iframe

	$(document).ready(function()
	{
		$("body").show() ;
		<?php if ( $sub ): ?>parent.do_alert( 1, "Update Success!" ) ;<?php endif ; ?>

		init_tooltips() ;
	});

	function do_edit( thecanid, thetitle, thedeptid, themessage )
	{
		$( "input#canid" ).val( thecanid ) ;
		$( "input#title" ).val( thetitle.replace( /&-#39;/g, "'" ) ) ;
		$( "#deptid" ).val( thedeptid ) ;
		$( "#message" ).val( themessage.replace(/<br>/g, "\r\n").replace( /&-#39;/g, "'" ) ) ;
		
		toggle_new(0) ;
	}

	function toggle_new( theflag )
	{
		// theflag = 1 means force show, not toggle
		if ( $('#canned_box_new').is(':visible') && !theflag )
		{
			$( "input#canid" ).val( "0" ) ;
			$( "input#title" ).val( "" ) ;
			$( "#deptid" ).val( <?php echo $deptid ?> ) ;
			$( "#message" ).val( "" ) ;

			$('#canned_box_new').fadeOut("fast", function() { $('#canned_list').fadeIn("fast") ; }) ;
		}
		else
		{
			$('#canned_list').fadeOut("fast", function() { $('#canned_box_new').fadeIn("fast") ; }) ;
		}

		$('#title').focus() ;
	}

	function do_delete( thecanid )
	{
		var unique = unixtime() ;

		if ( confirm( "Really delete this canned response?" ) )
			location.href = "iframe_edit.php?ses=<?php echo $ses ?>&action=delete&sub=canned&deptid=<?php echo $deptid ?>&canid="+thecanid+"&"+unique ;
	}

	function do_submit()
	{
		var canid = $('#canid').val() ;
		var title = $('#title').val() ;
		var deptid = $('#deptid').val() ;
		var message = $('#message').val() ;

		if ( title == "" )
			parent.do_alert( 0, "Please provide a title." ) ;
		else if ( message == "" )
			parent.do_alert( 0, "Please provide a message." ) ;
		else
			$('#theform').submit() ;
	}

	function init_tooltips()
	{
		var help_tooltips = $( 'body' ).find( '.help_tooltip' ) ;
		help_tooltips.tooltip({
			event: "mouseover",
			track: true,
			delay: 0,
			showURL: false,
			showBody: "- ",
			fade: 0,
			positionLeft: false,
			extraClass: "stat"
		});
	}

//-->
</script>
</head>
<body style="display: none;">

<div id="iframe_body" style="height: 280px;">
	<?php if ( $action != "canned" ): ?>
	<form action="iframe_edit.php?submit" method="POST">
	<input type="hidden" name="ses" value="<?php echo $ses ?>">
	<input type="hidden" name="action" value="update">
	<input type="hidden" name="sub" value="<?php echo $action ?>">
	<input type="hidden" name="deptid" value="<?php echo $deptid ?>">
	<input type="hidden" name="option" value="<?php echo $option ?>">
	<div style="padding-bottom: 45px;">
		<div style="padding: 10px;">
			<div id="info_greeting" style="display: none;">
				<div class="info_title">Chat Intro Greeting</div>
				<div style="margin-top: 5px;">Edit your department chat greeting. This message is displayed to the visitor while waiting to be connected with an operator.</div>
				<div style="margin-top: 10px;"><b>%%visitor%%</b> = visitor's name</div>
			</div>
			<div id="info_offline" style="display: none;">
				<div class="info_title">Offline Message</div>
				<div style="margin-top: 5px;">Place a brief message with your department hours, offline message, or best time to reach the department.  This message will be displayed to the visitor when department agents are not available.</div>
				<div style="margin-top: 10px;"></div>
			</div>
			<div id="info_temail" style="display: none;">
				<div class="info_title">Transcript Email Message</div>
				<div style="margin-top: 5px;">If "Email Transcript" is enabled, the following template will be used to generate the outgoing email transcript message sent to the visitor.</div>
			</div>
			<div id="info_settings" style="display: none;">
				<div class="info_title">Department Email Settings</div>
				<div style="margin-top: 5px;">Update visitor and support department chat transcript email settings.</div>
			</div>
		</div>
		<div style="padding: 10px;">
			<?php if ( $action == "temail" ): ?>
			<table cellspacing=0 cellpadding=0 border=0 width="100%">
			<tr>
				<td valign="top" width="300">
					<textarea type="text" cols="50" rows="6" id="message" name="message"><?php echo preg_replace( "/\"/", "&quot;", $message ) ?></textarea>
				</td>
				<td><img src="../pics/space.gif" width="15" height=1></td>
				<td valign="center">
					<ul>
						<li style="margin-top: 5px;"> Following variables can be used to dynamically populate data:
							<ul style="margin-top: 10px;">
								<li> <span class="text_blue"><b>%%transcript%%</b></span> = the chat transcript
								<li> <span class="text_blue"><b>%%visitor%%</b></span> = visitor's name
								<li> <span class="text_blue"><b>%%operator%%</b></span> = operator name
								<li> <span class="text_blue"><b>%%op_email%%</b></span> = operator email
							</ul>
					</ul>
				</td>
			</tr>
			</table>
			<?php elseif ( $action == "settings" ): ?>
			<div style="padding-bottom: 15px;">
				<table cellspacing=0 cellpadding=0 border=0>
				<tr>
					<td><img src="../pics/icons/help.png" width="14" height="14" border="0" class="help_tooltip" title="- <b>Require Email Address</b><br>Require an email address to start a chat session?  Selecting 'No' will hide the email field from the visitor chat window."> Require Email Address to Chat</td>
					<td><div style="padding-left: 15px;"><input type="radio" name="remail" id="remail_1" value="1" checked> Yes <input type="radio" name="remail" id="remail_0" value="0"> No</div></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td><img src="../pics/icons/help.png" width="14" height="14" border="0" class="help_tooltip" title="- <b>Email Transcript to Visitor</b><br>Allow visitors an option to receive the chat transcript by email when chat session ends."> Email Transcript to Visitor</td>
					<td><div style="padding-left: 15px;"><input type="radio" name="temail" id="temail_1" value="1" checked> Yes <input type="radio" name="temail" id="temail_0" value="0"> No</div></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td><img src="../pics/icons/help.png" width="14" height="14" border="0" class="help_tooltip" title="- <b>Email a Transcript to Department</b><br>Send chat transcript copy to the department email address when chat session ends."> Email Transcript to Department</td>
					<td><div style="padding-left: 15px;"><input type="radio" name="temaild" id="temaild_1" value="1" checked> Yes <input type="radio" name="temaild" id="temaild_0" value="0"> No</div></td>
				</tr>
				</table>
			</div>
			<script language="JavaScript">
			<!--
				$( "input#remail_"+<?php echo $deptinfo["remail"] ?> ).attr( "checked", true ) ;
				$( "input#temail_"+<?php echo $deptinfo["temail"] ?> ).attr( "checked", true ) ;
				$( "input#temaild_"+<?php echo $deptinfo["temaild"] ?> ).attr( "checked", true ) ;
			//-->
			</script>
			<?php else: ?>
			<div style="padding-bottom: 15px;"><input type="text" style="width: 95%" id="message" name="message" maxlength="155" value="<?php echo preg_replace( "/\"/", "&quot;", $message ) ?>"></div>
			<?php endif ; ?>

			<?php if ( count( $departments ) > 1 ): ?>
			<div style="padding-top: 5px;"><input type="checkbox" id="copy_all" name="copy_all" value=1> also copy this update to all departments</div>
			<?php endif ; ?>
			<div style="padding-top: 5px;"><input type="submit" value="Update <?php echo $title ?>"></div>
		</div>
	</div>
	</form>
	<script type="text/javascript">$('#info_<?php echo $action ?>').show();</script>

	<?php
		else:
		include_once( "../API/Canned/get.php" ) ;

		$departments = Depts_get_AllDepts( $dbh ) ;
		$operators = Ops_get_AllOps( $dbh ) ;

		// make hash for quick refrence
		$operators_hash = Array() ;
		$operators_hash[1111111111] = "<img src=\"../pics/icons/user_key.png\" width=\"16\" height=\"16\" border=\"0\" class=\"help_tooltip\" title=\"- created by setup admin\">" ;
		for ( $c = 0; $c < count( $operators ); ++$c )
		{
			$operator = $operators[$c] ;
			$operators_hash[$operator["opID"]] = $operator["name"] ;
		}

		// make hash for quick refrence
		$dept_hash = Array() ;
		$dept_hash[1111111111] = "All Departments" ;
		for ( $c = 0; $c < count( $departments ); ++$c )
		{
			$department = $departments[$c] ;
			$dept_hash[$department["deptID"]] = $department["name"] ;
		}

		$cans = Canned_get_DeptCanned( $dbh, $deptid, $page, 100 ) ;
	?>
	<div id="canned_list" style="overflow: auto;">
		<table cellspacing=0 cellpadding=0 border=0 width="100%">
		<tr>
			<td width="18" nowrap><div id="td_dept_header">&nbsp;</div></td>
			<td width="120" nowrap><div id="td_dept_header">Title</div></td>
			<td width="80" nowrap><div id="td_dept_header">Operator</div></td>
			<td width="180"><div id="td_dept_header">Department</div></td>
			<td><div id="td_dept_header">Message</div></td>
		</tr>
		<?php
			for ( $c = 0; $c < count( $cans )-1; ++$c )
			{
				$can = $cans[$c] ;
				$title = preg_replace( "/\"/", "&quot;", preg_replace( "/'/", "&-#39;", $can["title"] ) ) ;
				$title_display = preg_replace( "/\"/", "&quot;", $can["title"] ) ;

				$op_name = $operators_hash[$can["opID"]] ;
				$dept_name = $dept_hash[$can["deptID"]] ;
				$message = preg_replace( "/\"/", "&quot;", preg_replace( "/'/", "&-#39;", preg_replace( "/(\r\n)|(\n)|(\r)/", "<br>", $can["message"] ) ) ) ;
				$message_display = preg_replace( "/\"/", "&quot;", preg_replace( "/(\r\n)|(\n)|(\r)/", "<br>", Util_Format_ConvertTags( $can["message"] ) ) ) ;
				//$message_display = preg_replace( "/url:(.*?)($| |)/", "[url link]", $message_display ) ;
				//$message_display = preg_replace( "/image:(.*?)($| |)/", "[image link]", $message_display ) ;

				$td1 = "td_dept_td" ; $td2 = "td_dept_td_td" ;
				if ( $c % 2 ) { $td1 = "td_dept_td2" ; $td2 = "td_dept_td_td2" ; }

				print "<tr><td class=\"$td1\" nowrap><img src=\"../pics/icons/delete.png\" width=\"14\" height=\"14\" onClick=\"do_delete($can[canID])\" style=\"cursor: pointer;\" class=\"help_tooltip\" title=\"- delete canned\"> &nbsp; <img src=\"../pics/icons/edit.png\" width=\"14\" height=\"14\" onClick=\"do_edit($can[canID], '$title', '$can[deptID]', '$message')\" style=\"cursor: pointer;\" class=\"help_tooltip\" title=\"- edit canned\"></td><td class=\"$td1\"><b>$title_display</b></td><td class=\"$td1\" nowrap>$op_name</td><td class=\"$td1\">$dept_name</td><td class=\"$td1\">$message_display</td></tr>" ;
			}
			if ( $c == 0 )
				print "<tr><td colspan=7 class=\"td_dept_td\">blank results</td></tr>" ;
		?>
		</table>
		<div class="chat_info_end"></div>
	</div>

	<div id="canned_box_new" style="display: none; padding: 5px; z-Index: 5;">
		<form method="POST" action="iframe_edit.php?<?php echo time() ?>" id="theform">
		<table cellspacing=0 cellpadding=0 border=0 width="100%">
		<tr>
			<td valign="top" width="380" style="padding: 5px;">
				<input type="hidden" name="ses" value="<?php echo $ses ?>">
				<input type="hidden" name="action" value="update">
				<input type="hidden" name="sub" value="<?php echo $action ?>">
				<input type="hidden" name="sub_deptid" value="<?php echo $deptid ?>">
				<input type="hidden" name="option" value="<?php echo $option ?>">
				<input type="hidden" name="canid" id="canid" value="0">
				<div>
					Title<br>
					<input type="text" name="title" id="title" class="input_text" style="width: 98%; margin-bottom: 10px;" maxlength="25">
					Department<br>
					<select name="deptid" id="deptid" style="width: 99%; margin-bottom: 10px;">
					<option value="1111111111">All Departments</option>
					<?php
						for ( $c = 0; $c < count( $departments ); ++$c )
						{
							$department = $departments[$c] ;
							$selected = ( $department["deptID"] == $deptid ) ? "selected" : "" ;

							print "<option value=\"$department[deptID]\" $selected>$department[name]</option>" ;
						}
					?>
					</select>
					Canned Message<br>
					<textarea name="message" id="message" class="input_text" rows="5" style="width: 98%; margin-bottom: 10px;" wrap="virtual"></textarea>

					<button type="button" onClick="do_submit()">Submit</button> or <a href="JavaScript:void(0)" onClick="toggle_new(0)">Cancel</a>
				</div>
			</td>
			<td><img src="../pics/space.gif" width="15" height=1></td>
			<td valign="center">
				<ul>
					<li> HTML will be converted to raw code.
					<!-- <li style="margin-top: 5px;"> To automatically execute some helpful functions, use the below commands:
						<ul>
							<li> <span class="text_blue"><b>load:</b></span>URL (automatically loads a URL)<br>
								example: <i>load:http://www.phplivesupport.com/trial.php</i>
						</ul> -->
					<li style="margin-top: 5px;"> Following variables can be used to dynamically populate data:
						<ul style="margin-top: 10px;">
							<li> <span class="text_blue"><b>%%visitor%%</b></span> = visitor's name
							<li> <span class="text_blue"><b>%%operator%%</b></span> = your name
							<li> <span class="text_blue"><b>%%op_email%%</b></span> = your email

							<li style="padding-top: 5px;"> <span class="text_blue"><b>email:</b><i>someone@somewhere.com</i> - link an email</span>
							<li> <span class="text_blue"><b>image:</b><i>http://www.someurl.com/image.gif</i> - display an image</span>
						</ul>
				</ul>
			</td>
		</tr>
		</table>
		</form>
	</div>

	<?php endif ; ?>
</div>

</body>
</html>

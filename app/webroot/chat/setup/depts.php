<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	/****************************************/
	// STANDARD header for Setup
	if ( !file_exists( "../web/config.php" ) ){ HEADER("location: install.php") ; exit ; }
	include_once( "../web/config.php" ) ;
	include_once( "../API/Util_Format.php" ) ;
	include_once( "../API/Util_Error.php" ) ;
	include_once( "../API/SQL.php" ) ;
	include_once( "../API/Util_Security.php" ) ;
	$ses = Util_Format_Sanatize( Util_Format_GetVar( "ses" ), "ln" ) ;
	if ( !$admininfo = Util_Security_AuthSetup( $dbh, $ses ) ){ ErrorHandler ( 608, "Invalid setup session or session has expired.", $PHPLIVE_FULLURL, 0, Array() ) ; }
	// STANDARD header end
	/****************************************/

	$error = "" ;

	include_once( "../API/Depts/get.php" ) ;

	$action = Util_Format_Sanatize( Util_Format_GetVar( "action" ), "ln" ) ;

	if ( $action == "submit" )
	{
		include_once( "../API/Depts/put.php" ) ;
		include_once( "../API/Ops/update.php" ) ;

		$deptid = Util_Format_Sanatize( Util_Format_GetVar( "deptid" ), "ln" ) ;
		$name = Util_Format_Sanatize( Util_Format_GetVar( "name" ), "ln" ) ;
		$email = Util_Format_Sanatize( Util_Format_GetVar( "email" ), "e" ) ;
		$visible = Util_Format_Sanatize( Util_Format_GetVar( "visible" ), "ln" ) ;
		$queue = Util_Format_Sanatize( Util_Format_GetVar( "queue" ), "ln" ) ;
		$rtype = Util_Format_Sanatize( Util_Format_GetVar( "rtype" ), "ln" ) ;
		$rtime = Util_Format_Sanatize( Util_Format_GetVar( "rtime" ), "ln" ) ;
		$tshare = Util_Format_Sanatize( Util_Format_GetVar( "tshare" ), "ln" ) ;
		$traffic = Util_Format_Sanatize( Util_Format_GetVar( "traffic" ), "ln" ) ;
		$texpire = Util_Format_Sanatize( Util_Format_GetVar( "texpire" ), "ln" ) ;
		$lang = Util_Format_Sanatize( Util_Format_GetVar( "lang" ), "ln" ) ;

		if ( file_exists( "../lang_packs/$lang.php" ) )
			include_once( "$CONF[DOCUMENT_ROOT]/lang_packs/$lang.php" ) ;
		else
			$lang = "english" ;

		$department_pre = Depts_get_DeptInfoByName( $dbh, $name ) ;
		if ( isset( $department_pre["deptID"] ) && !$deptid )
			$error = "$name is already in use." ;
		else
		{
			if ( ( $name != "Archive" ) && !Depts_put_Department( $dbh, $deptid, $name, $email, $visible, $queue, $rtype, $rtime, $tshare, $texpire, $lang ) )
				$error = "DB Error: $dbh[error]" ;
			else
			{
				include_once( "../API/Util_Vals.php" ) ;

				$departments = Depts_get_AllDepts( $dbh ) ;
				if ( count( $departments ) == 1 )
					$error = ( Util_Vals_WriteToConfFile( "lang", $lang ) ) ? "" : "Could not write to config file." ;
			}
		}
	}
	else if ( $action == "delete" )
	{
		include_once( "../API/Depts/remove.php" ) ;

		$deptid = Util_Format_Sanatize( Util_Format_GetVar( "deptid" ), "ln" ) ;
		Depts_remove_Dept( $dbh, $deptid ) ;

		$departments = Depts_get_AllDepts( $dbh ) ;
		if ( count( $departments ) == 1 )
		{
			$department = $departments[0] ;
			if ( isset( $department["lang"] ) && $department["lang"] && ( $CONF["lang"] != $department["lang"] ) )
			{
				include_once( "../API/Util_Vals.php" ) ;

				$lang = $department["lang"] ;
				$error = ( Util_Vals_WriteToConfFile( "lang", $lang ) ) ? "" : "Could not write to config file." ;
				$CONF["lang"] = $lang ;
			}
		}

		$action = "" ;
	}
	else if ( $action == "update_lang" )
	{
		include_once( "../API/Util_Vals.php" ) ;
		include_once( "../API/Depts/update.php" ) ;

		$lang = Util_Format_Sanatize( Util_Format_GetVar( "lang" ), "ln" ) ;

		$error = ( Util_Vals_WriteToConfFile( "lang", $lang ) ) ? "" : "Could not write to config file." ;
		$CONF["lang"] = $lang ;
	}

	if ( !isset( $departments ) )
		$departments = Depts_get_AllDepts( $dbh ) ;
?>
<?php include_once( "../inc_doctype.php" ) ?>
<head>
<title> PHP Live! Support <?php echo $VERSION ?> </title>

<meta name="description" content="PHP Live! Support <?php echo $VERSION ?>">
<meta name="keywords" content="powered by: PHP Live!  www.phplivesupport.com">
<meta name="robots" content="all,index,follow">
<meta http-equiv="content-type" content="text/html; CHARSET=utf-8"> 

<link rel="Stylesheet" href="../css/base_setup.css?<?php echo $VERSION ?>">
<script type="text/javascript" src="../js/global.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/setup.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework_cnt.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/tooltip.js?<?php echo $VERSION ?>"></script>

<script type="text/javascript">
<!--
	var global_deptid ;
	var lang = "<?php echo $CONF["lang"] ?>" ;

	$(document).ready(function()
	{
		$("body").show() ;
		$('#body_sub_title').html( "<img src=\"../pics/icons/persons.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Departments" ) ;

		init_menu() ;
		toggle_menu_setup( "depts" ) ;

		var help_tooltips = $( 'body' ).find('.help_tooltip' ) ;
		help_tooltips.tooltip({
			event: "mouseover",
			track: true,
			delay: 0,
			showURL: false,	
			showBody: "- ",
			fade: 0,
			positionLeft: false,
			extraClass: "stat"
		});

		<?php if ( $action && !$error ): ?>do_alert( 1, "Update Success!" ) ;<?php endif ; ?>
	});
	$(window).resize(function() { });

	function do_submit()
	{
		var name = $( "input#name" ).val() ;
		var email = $( "input#email" ).val() ;

		if ( name == "" )
			do_alert( 0, "Please provide the department name." ) ;
		else if ( !check_email( email ) )
			do_alert( 0, "Please provide a valid email address." ) ;
		else
			$('#theform').submit() ;
	}

	function do_options( theoption, thedeptid )
	{
		var unique = unixtime() ;

		for ( c = 1; c <= 5; ++c )
		{
			if ( c != theoption )
				$('#menu_'+c+"_"+thedeptid).removeClass('info_info_clear').addClass('info_menu') ;
		}

		if ( $('#iframe_edit_'+thedeptid).is(':visible') && ( $('#iframe_edit_'+thedeptid).attr( "contentWindow" ).option == theoption ) )
		{
			$('#div_new_canned_'+thedeptid).hide() ;
			$('#iframe_'+thedeptid).fadeOut("fast") ;

			$('#menu_'+theoption+"_"+thedeptid).removeClass('info_info_clear').addClass('info_menu') ;
		}
		else
		{
			$('#div_new_canned_'+thedeptid).hide() ;

			if ( theoption == 1 )
				$('#iframe_edit_'+thedeptid).attr('src', 'iframe_edit.php?ses=<?php echo $ses ?>&action=greeting&option='+theoption+'&deptid='+thedeptid+'&'+unique ) ;
			else if ( theoption == 2 )
				$('#iframe_edit_'+thedeptid).attr('src', 'iframe_edit.php?ses=<?php echo $ses ?>&action=offline&option='+theoption+'&deptid='+thedeptid+'&'+unique ) ;
			else if ( theoption == 3 )
			{
				$('#iframe_edit_'+thedeptid).attr('src', 'iframe_edit.php?ses=<?php echo $ses ?>&action=canned&option='+theoption+'&deptid='+thedeptid+'&'+unique ) ;
				$('#div_new_canned_'+thedeptid).show() ;
			}
			else if ( theoption == 4 )
				$('#iframe_edit_'+thedeptid).attr('src', 'iframe_edit.php?ses=<?php echo $ses ?>&action=temail&option='+theoption+'&deptid='+thedeptid+'&'+unique ) ;
			else if ( theoption == 5 )
				$('#iframe_edit_'+thedeptid).attr('src', 'iframe_edit.php?ses=<?php echo $ses ?>&action=settings&option='+theoption+'&deptid='+thedeptid+'&'+unique ) ;
			else
				return false ;

			$('#iframe_'+thedeptid).fadeIn("fast") ;
			$('#menu_'+theoption+"_"+thedeptid).removeClass('info_menu').addClass('info_info_clear') ;
		}
	}

	function do_edit( thedeptid, thename, theemail, thertype, thertime, thetexpire, thevisible, thequeue, thetshare, thelang )
	{
		$( "input#deptid" ).val( thedeptid ) ;
		$( "input#name" ).val( thename ) ;
		$( "input#email" ).val( theemail ) ;
		$( "select#rtime" ).val( thertime ) ;
		$( "select#texpire" ).val( thetexpire ) ;
		$( "input#rtype_"+thertype ).attr( "checked", true ) ;
		$( "input#visible_"+thevisible ).attr( "checked", true ) ;
		$( "input#queue_"+thequeue ).attr( "checked", true ) ;
		$( "input#tshare_"+thetshare ).attr( "checked", true ) ;
		$( "select#lang" ).val( thelang ) ;
		location.href = "#a_edit" ;
	}

	function do_delete( thedeptid )
	{
		if ( confirm( "Really delete this department?" ) )
			location.href = "depts.php?ses=<?php echo $ses ?>&action=delete&deptid="+thedeptid ;
	}

	function new_canned( thedeptid )
	{
		$('#iframe_edit_'+thedeptid).attr( "contentWindow" ).toggle_new(1) ;
	}

	function confirm_lang()
	{
		var thelang = $('#lang_pr').val() ;

		if ( lang != thelang )
			update_lang( thelang ) ;
		else
			do_alert( 1, "Update Success!" ) ;
	}

	function update_lang( thelang )
	{
		location.href = 'depts.php?ses=<?php echo $ses ?>&action=update_lang&deptid=0&lang='+thelang ;
	}
//-->
</script>
</head>
<body style="display: none;">

<?php include_once( "./inc_header.php" ) ?>

		<div class="op_submenu_wrapper">
			<div class="info_info">
				"Customer Support", "Sales" and "Order Status" are examples of possible department names.  An operator can be assigned to multiple departments.
			</div>

			<?php if ( count( $departments ) > 1 ): ?>
			<div style="margin-top: 25px;">

				<img src="../pics/icons/info.png" width="16" height="16" border="0" alt=""> Each department could have a different language.  To avoid conflict, set the default primary language for the visitor chat window:
				<select name="lang_pr" id="lang_pr">
				<?php
					$dir_langs = opendir( "$CONF[DOCUMENT_ROOT]/lang_packs/" ) ;

					$langs = Array() ;
					while ( $this_lang = readdir( $dir_langs ) )
						$langs[] = $this_lang ;
					closedir( $dir_langs ) ;

					for ( $c = 0; $c < count( $langs ); ++$c )
					{
						$this_lang = preg_replace( "/.php/", "", $langs[$c] ) ;

						$selected = "" ;
						if ( $CONF["lang"] == $this_lang )
							$selected = "selected" ;

						if ( preg_match( "/[a-z]/i", $this_lang ) )
							print "<option value=\"$this_lang\" $selected> ".ucfirst( $this_lang )."</option>" ;
					}
				?>
				</select>
				<button type="button" onClick="confirm_lang()">Update</button>

			</div>
			<?php endif; ?>
		</div>

		<table cellspacing=0 cellpadding=0 border=0 width="100%" style="margin-top: 25px;">
		<tr>
			<td width="40"><div id="td_dept_header">&nbsp;</div></td>
			<td><div id="td_dept_header">Department</div></td>
			<td width="260"><div id="td_dept_header">Email</div></td>
			<td width="90" nowrap><div id="td_dept_header" style="white-space: nowrap;">Routing Type</div></td>
			<td width="85" nowrap><div id="td_dept_header" style="white-space: nowrap;">Route Time</div></td>
			<!-- <td width="60" align="center"><div id="td_dept_header">Queue</div></td> -->
			<td width="62" align="center"><div id="td_dept_header" style="white-space: nowrap; cursor: pointer;" class="help_tooltip" title=" - Share saved transcripts between operators of the same department.">Share T</div></td>
			<!-- <td width="60" align="center"><div id="td_dept_header" style="white-space: nowrap; cursor: pointer;" class="help_tooltip" title=" - Allow visitors an option to receive the chat transcript by email when chat session ends.">Email T</div></td> -->
			<td width="60" align="center"><div id="td_dept_header">Visible</div></td>
			<td width="80" nowrap><div id="td_dept_header">Language</div></td>
		</tr>
		<?php
			$image_empty = "<img src=\"../pics/space.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"\">" ;
			$image_checked = "<img src=\"../pics/icons/check.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\">";
			for ( $c = 0; $c < count( $departments ); ++$c )
			{
				$department = $departments[$c] ;

				$name = $department["name"] ;
				$rtype = $VARS_RTYPE[$department["rtype"]] ;
				$rtime = "$department[rtime] sec" ;
				$visible = ( $department["visible"] ) ? $image_checked : $image_empty ;
				$queue = ( $department["queue"] ) ? $image_checked : $image_empty ;
				$tshare = ( $department["tshare"] ) ? $image_checked : $image_empty ;
				$temail = ( $department["temail"] ) ? $image_checked : $image_empty ;
				$lang = ucfirst( $department["lang"] ) ;

				$edit_delete = "<a href=\"JavaScript:void(0)\" onClick=\"do_delete($department[deptID])\" class=\"help_tooltip nounder\" title=\"- delete department\"><img src=\"../pics/icons/delete.png\" width=\"14\" height=\"14\" border=\"0\" alt=\"\"></a> &nbsp; <a href=\"JavaScript:void(0)\" onClick=\"do_edit( $department[deptID], '$name', '$department[email]', '$department[rtype]', '$department[rtime]', '$department[texpire]', '$department[visible]', '$department[queue]', '$department[tshare]', '$department[lang]' )\" class=\"help_tooltip nounder\" title=\"- edit department\"><img src=\"../pics/icons/edit.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\"></a>" ;
				$options = "
					<span class=\"info_menu\" id=\"menu_1_$department[deptID]\" style=\"padding: 5px; margin-right: 15px; cursor: pointer; border-bottom-left-radius: 0px; -moz-border-radius-bottomleft: 0px; border-bottom-right-radius: 0px; -moz-border-radius-bottomright: 0px;\" onClick=\"do_options( 1, $department[deptID] );\"><img src=\"../pics/icons/add.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"position: relative; top: 3px; left: 0px;\"> Chat Greeting</span>
					<span class=\"info_menu\" id=\"menu_2_$department[deptID]\" style=\"padding: 5px; margin-right: 15px; cursor: pointer; border-bottom-left-radius: 0px; -moz-border-radius-bottomleft: 0px; border-bottom-right-radius: 0px; -moz-border-radius-bottomright: 0px;\" onClick=\"do_options( 2, $department[deptID] );\"><img src=\"../pics/icons/add.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"position: relative; top: 3px; left: 0px;\"> Offline Message</span>
					<span class=\"info_menu\" id=\"menu_4_$department[deptID]\" style=\"padding: 5px; margin-right: 15px; cursor: pointer; border-bottom-left-radius: 0px; -moz-border-radius-bottomleft: 0px; border-bottom-right-radius: 0px; -moz-border-radius-bottomright: 0px;\" onClick=\"do_options( 4, $department[deptID] );\"><img src=\"../pics/icons/add.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"position: relative; top: 3px; left: 0px;\"> Transcript Email</span>
					<span class=\"info_menu\" id=\"menu_5_$department[deptID]\" style=\"padding: 5px; margin-right: 15px; cursor: pointer; border-bottom-left-radius: 0px; -moz-border-radius-bottomleft: 0px; border-bottom-right-radius: 0px; -moz-border-radius-bottomright: 0px;\" onClick=\"do_options( 5, $department[deptID] );\"><img src=\"../pics/icons/add.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"position: relative; top: 3px; left: 0px;\"> Email Settings</span>
					<span class=\"info_menu\" id=\"menu_3_$department[deptID]\" style=\"padding: 5px; cursor: pointer; border-bottom-left-radius: 0px; -moz-border-radius-bottomleft: 0px; border-bottom-right-radius: 0px; -moz-border-radius-bottomright: 0px;\" onClick=\"do_options( 3, $department[deptID] );\"><img src=\"../pics/icons/add.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"position: relative; top: 3px; left: 0px;\"> Canned Responses</span>
					<span id=\"div_new_canned_$department[deptID]\" class=\"info_box\" style=\"display: none; margin-left: 10px; padding-left: 25px; background: url( ../pics/icons/add.png ) no-repeat #FAFAA6; background-position: 5px 5px; cursor: pointer; border-bottom-left-radius: 0px; -moz-border-radius-bottomleft: 0px; border-bottom-right-radius: 0px; -moz-border-radius-bottomright: 0px;\" onClick=\"new_canned( $department[deptID] )\">new canned</span>" ;

				$td1 = "td_dept_td" ; $td2 = "td_dept_td_td" ;
				if ( $c % 2 ) { $td1 = "td_dept_td2" ; $td2 = "td_dept_td_td2" ; }

				print "
				<tr>
					<td class=\"$td1\" nowrap>$edit_delete</td>
					<td class=\"$td1\">
						<div><b>$name</b></div>
					</td>
					<td class=\"$td1\">$department[email]</td>
					<td class=\"$td1\">$rtype</td>
					<td class=\"$td1\">$rtime</td>
					<!-- <td class=\"$td1\" align=\"center\">$queue</td> -->
					<td class=\"$td1\" align=\"center\">$tshare</td>
					<!-- <td class=\"$td1\" align=\"center\">$temail</td> -->
					<td class=\"$td1\" align=\"center\">$visible</td>
					<td class=\"$td1\">$lang</td>
				</tr>
				<tr>
					<td class=\"$td2\">&nbsp;</td>
					<td class=\"$td2\" colspan=\"8\"><div style=\"padding: 5px; margin-bottom: 10px;\">$options</div><div id=\"iframe_$department[deptID]\" style=\"display: none;\"><iframe id=\"iframe_edit_$department[deptID]\" name=\"iframe_edit_$department[deptID]\" style=\"width: 100%; height: 300px; border: 0px;\" src=\"\" scrolling=\"auto\" frameBorder=\"0\"></iframe></div></td>
				</tr>
				" ;
			}
			if ( $c == 0 )
				print "<tr><td colspan=7 class=\"td_dept_td\">blank results</td></tr>" ;
		?>
		</table>

		<div class="edit_wrapper" style="padding: 5px; margin-top: 55px;">
			<a name="a_edit"></a><div class="edit_title">Create/Edit Department <span class="txt_red"><?php echo $error ?></span></div>
			<div style="margin-top: 10px;">
				<form method="POST" action="depts.php?submit" id="theform">
				<input type="hidden" name="ses" value="<?php echo $ses ?>">
				<input type="hidden" name="action" value="submit">
				<input type="hidden" name="deptid" id="deptid" value="0">
				<input type="hidden" name="queue" value="0">
				<input type="hidden" name="tshare" value="">
				<table cellspacing=0 cellpadding=0 border=0 width="100%">
				<tr>
					<td valign="top">
						<table cellspacing=0 cellpadding=5 border=0 width="100%">
						<tr>
							<td>Department Name</td>
							<td> <input type="text" name="name" id="name" size="30" maxlength="40" value="" onKeyPress="return noquotes(event)"></td>
						</tr>
						<tr>
							<td colspan="2"> "Leave a Message" emails will be sent to the Department Email address that is provided below.</td>
						</tr>
						<tr>
							<td>Department Email</td>
							<td> <input type="text" name="email" id="email" size="30" maxlength="160" value="" onKeyPress="return justemails(event)"></td>
						</tr>
						<tr>
							<td>Transcripts Expire</td>
							<td> delete after <select name="texpire" id="texpire" ><option value="31104000" selected>1 year</option><option value="62208000">2 years</option><option value="93312000">3 years</option><option value="0">no expire</option></select></td>
						</tr>
						<tr>
							<td>Chat Routing Type</td>
							<td>
								<div><input type="radio" name="rtype" id="rtype_1" value="1"> Defined Order: based on the <a href="ops.php?ses=<?php echo $ses ?>&jump=assign">operator defined order</a></div>
								<div style="margin-top: 3px;"><input type="radio" name="rtype" id="rtype_2" value="2" checked> Round-Robin: cycle through all operators based on last chat <b>accepted</b> time</div>
								<!-- <div style="margin-top: 3px;"><input type="radio" name="rtype" id="rtype_3" value="3"> Simultaneous: all operators receive the request at the same time</div> [in working progress] -->
							</td>
						</tr>
						<tr>
							<td>Chat Routing Time</td>
							<td> if the current operator does not accept or decline a chat within <select name="rtime" id="rtime" ><option value="5">5 seconds</option><option value="10">10 seconds</option><option value="15">15 seconds</option><option value="30">30 seconds</option><option value="45" selected>45 seconds</option><option value="60">60 seconds</option><option value="90">90 seconds</option><option value="120">2 minutes</option><option value="180">3 minutes</option></select>, the<br>request will automatically be routed to the next available operator</td>
						</tr>
						</table>
					</td>
					<td valign="top">
						<table cellspacing=0 cellpadding=5 border=0 width="100%">
						<tr>
							<td><img src="../pics/icons/help.png" width="14" height="14" border="0" class="help_tooltip" title="- <b>Visible to Public</b><br>When a visitor requests support, choose whether to display this department on the selection list."> Visible to Public</td>
							<td> <input type="radio" name="visible" id="visible_1" value="1" checked> Yes <input type="radio" name="visible" id="visible_0" value="0"> No</td>
						</tr>
						<tr>
							<td><img src="../pics/icons/help.png" width="14" height="14" border="0" class="help_tooltip" title="- <b>Op Share Transcripts</b><br>Select whether chat transcripts are shared between operators or have the records be private to the chat owner."> Share Transcripts</td>
							<td> <input type="radio" name="tshare" id="tshare_1" value="1" checked> Yes <input type="radio" name="tshare" id="tshare_0" value="0"> No</td>
						</tr>
						<tr>
							<td><img src="../pics/icons/help.png" width="14" height="14" border="0" class="help_tooltip" title="- <b>Language</b><br>Set language localization for the visitor chat window."> Language</td>
							<td>
								<select name="lang" id="lang">
								<?php
									$dir_langs = opendir( "$CONF[DOCUMENT_ROOT]/lang_packs/" ) ;

									$langs = Array() ;
									while ( $this_lang = readdir( $dir_langs ) )
										$langs[] = $this_lang ;
									closedir( $dir_langs ) ;

									for ( $c = 0; $c < count( $langs ); ++$c )
									{
										$this_lang = preg_replace( "/.php/", "", $langs[$c] ) ;

										$selected = "" ;
										if ( $CONF["lang"] == $this_lang )
											$selected = "selected" ;

										if ( preg_match( "/[a-z]/i", $this_lang ) )
											print "<option value=\"$this_lang\" $selected> ".ucfirst( $this_lang )."</option>" ;
									}
								?>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan=2 rowspan=2 valign="top" width="280">
								<div><input type="button" value="Submit" onClick="do_submit()"> <input type="reset" value="Reset" onClick="$('#deptid').val(0);$('#lang').val('<?php echo $CONF["lang"] ?>')"></div>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				</table>
				</form>
			</div>
		</div>
<?php include_once( "./inc_footer.php" ) ?>

</body>
</html>

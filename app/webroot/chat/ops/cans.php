<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	/****************************************/
	// STANDARD header for Setup
	if ( !file_exists( "../web/config.php" ) ){ HEADER("location: ../setup/install.php") ; exit ; }
	include_once( "../web/config.php" ) ;
	include_once( "../API/Util_Format.php" ) ;
	include_once( "../API/Util_Error.php" ) ;
	include_once( "../API/SQL.php" ) ;
	include_once( "../API/Util_Security.php" ) ;
	$ses = Util_Format_Sanatize( Util_Format_GetVar( "ses" ), "ln" ) ;
	if ( !$opinfo = Util_Security_AuthOp( $dbh, $ses ) ){ ErrorHandler ( 602, "Invalid operator session or session has expired.", $PHPLIVE_FULLURL, 0, Array() ) ; }
	// STANDARD header end
	/****************************************/

	include_once( "../API/Depts/get.php" ) ;
	include_once( "../API/Canned/get.php" ) ;

	$action = Util_Format_Sanatize( Util_Format_GetVar( "action" ), "ln" ) ;
	$console = Util_Format_Sanatize( Util_Format_GetVar( "console" ), "ln" ) ; $body_width = ( $console ) ? 800 : 900 ;
	$flag = Util_Format_Sanatize( Util_Format_GetVar( "flag" ), "ln" ) ;

	$menu = "cans" ;
	$error = "" ;

	if ( $action == "submit" )
	{
		include_once( "../API/Canned/put.php" ) ;

		$canid = Util_Format_Sanatize( Util_Format_GetVar( "canid" ), "ln" ) ;
		$deptid = Util_Format_Sanatize( Util_Format_GetVar( "deptid" ), "ln" ) ;
		$title = Util_Format_Sanatize( Util_Format_GetVar( "title" ), "ln" ) ;
		$message = Util_Format_Sanatize( Util_Format_GetVar( "message" ), "" ) ;

		$caninfo = Canned_get_CanInfo( $dbh, $canid ) ;
		if ( isset( $caninfo["opID"] ) )
			$opid = $caninfo["opID"] ;
		else
			$opid = $opinfo["opID"] ;

		if ( !Canned_put_Canned( $dbh, $canid, $opinfo["opID"], $deptid, $title, $message ) )
			$error = "Error processing canned message." ;
	}
	else if ( $action == "delete" )
	{
		include_once( "../API/Canned/remove.php" ) ;

		$canid = Util_Format_Sanatize( Util_Format_GetVar( "canid" ), "ln" ) ;

		$caninfo = Canned_get_CanInfo( $dbh, $canid ) ;
		if ( $caninfo["opID"] == $opinfo["opID"] )
			Canned_remove_Canned( $dbh, $opinfo["opID"], $canid ) ;
	}

	$departments = Depts_get_OpDepts( $dbh, $opinfo["opID"] ) ;
	$cans = Canned_get_OpCanned( $dbh, $opinfo["opID"], 0 ) ;

	// make hash for quick refrence
	$dept_hash = Array() ;
	$dept_hash[1111111111] = "All Departments" ;
	for ( $c = 0; $c < count( $departments ); ++$c )
	{
		$department = $departments[$c] ;
		$dept_hash[$department["deptID"]] = $department["name"] ;
	}
?>
<?php include_once( "../inc_doctype.php" ) ?>
<head>
<title> Canned Responses </title>

<meta name="description" content="PHP Live! Support <?php echo $VERSION ?>">
<meta name="keywords" content="powered by: PHP Live!  www.phplivesupport.com">
<meta name="robots" content="all,index,follow">
<meta http-equiv="content-type" content="text/html; CHARSET=utf-8"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="Stylesheet" href="../css/base_setup.css?<?php echo $VERSION ?>">
<script type="text/javascript" src="../js/global.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/setup.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework_cnt.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/tooltip.js?<?php echo $VERSION ?>"></script>

<script type="text/javascript">
<!--
	var menu ;
	var reset_url = "transcripts.php?ses=<?php echo $ses ?>" ;

	$(document).ready(function()
	{
		$("body").show() ;
		init_menu() ;
		init_tooltips() ;
		toggle_menu_op( "<?php echo $menu ?>", '<?php echo $ses ?>' ) ;

		<?php if ( ( $action == "submit" ) && !$error ): ?>parent.do_alert( 1, "Update Success!" ) ;<?php endif ; ?>
	});

	function do_edit( thecanid, thetitle, thedeptid, themessage )
	{
		$( "input#canid" ).val( thecanid ) ;
		$( "input#title" ).val( thetitle.replace( /&-#39;/g, "'" ) ) ;
		$( "#deptid" ).val( thedeptid ) ;
		$( "#message" ).val( themessage.replace(/<br>/g, "\r\n").replace( /&-#39;/g, "'" ) ) ;

		location.href = "#a_edit" ;
	}

	function do_delete( thiscanid )
	{
		if ( confirm( "Really delete this canned response?" ) )
			location.href = "cans.php?ses=<?php echo $ses ?>&action=delete&canid="+thiscanid ;
	}

	function do_submit()
	{
		var canid = $('#canid').val() ;
		var title = $('#title').val() ;
		var deptid = $('#deptid').val() ;
		var message = $('#message').val() ;

		if ( title == "" )
			do_alert( 0, "Please provide a title." ) ;
		else if ( message == "" )
			do_alert( 0, "Please provide a message." ) ;
		else
			$('#theform').submit() ;
	}

	function cancel_edit()
	{
		$( "input#canid" ).val( 0 ) ;
		$( "input#title" ).val( "" ) ;
		$( "#deptid" ).val( "" ) ;
		$( "#message" ).val( "" ) ;
	}

	function init_tooltips()
	{
		var help_tooltips = $( '#cans' ).find( '.help_tooltip' ) ;
		help_tooltips.tooltip({
			event: "mouseover",
			track: true,
			delay: 0,
			showURL: false,
			showBody: "- ",
			fade: 0,
			positionLeft: false,
			extraClass: "stat"
		});
	}

//-->
</script>
</head>
<body style="display: none;">

<div id="body" style="padding-bottom: 60px;">
	<div id="body_wrapper" style="z-Index: 5;"></div>
	<div style="width: 100%; z-Index: 10;">
		<div style="width: 900px; margin: 0 auto;">
		<div id="body_sub_title"></div>

			<div id="op_cans" class="body_main" style="display: none; margin: 0 auto; margin-top: 25px;">
				<div class="info_info">
					Create/Edit canned responses.  Your canned responses are not shared or visible by other operators.
				</div>

				<a name="a_top"></a>
				<div id="cans" style="margin-top: 35px;">
					<table cellspacing=0 cellpadding=0 border=0 width="100%" id="table_trs">
					<tr>
						<td width="18" nowrap><div id="td_dept_header">&nbsp;</div></td>
						<td width="180" nowrap><div id="td_dept_header">Title</div></td>
						<td width="180"><div id="td_dept_header">Department</div></td>
						<td><div id="td_dept_header">Message</div></td>
					</tr>
					<?php
						for ( $c = 0; $c < count( $cans ); ++$c )
						{
							$can = $cans[$c] ;
							$title = preg_replace( "/\"/", "&quot;", preg_replace( "/'/", "&-#39;", $can["title"] ) ) ;
							$title_display = preg_replace( "/\"/", "&quot;", $can["title"] ) ;

							$dept_name = $dept_hash[$can["deptID"]] ;
							$message = preg_replace( "/\"/", "&quot;", preg_replace( "/'/", "&-#39;", preg_replace( "/(\r\n)|(\n)|(\r)/", "<br>", $can["message"] ) ) ) ;
							$message_display = preg_replace( "/\"/", "&quot;", preg_replace( "/(\r\n)|(\n)|(\r)/", "<br>", Util_Format_ConvertTags( $can["message"] ) ) ) ;

							$td1 = "td_dept_td" ; $td2 = "td_dept_td_td" ;
							if ( $c % 2 ) { $td1 = "td_dept_td2" ; $td2 = "td_dept_td_td2" ; }

							$delete_image = ( $can["opID"] == $opinfo["opID"] ) ? "<img src=\"../pics/icons/delete.png\" style=\"cursor: pointer;\" onClick=\"do_delete($can[canID])\" class=\"help_tooltip\" title=\"- delete canned\" width=\"14\" height=\"14\" border=0>" : "<img src=\"../pics/space.gif\" width=\"14\" height=\"14\" border=0>" ;
							$edit_image = ( $can["opID"] == $opinfo["opID"] ) ? "<img src=\"../pics/icons/edit.png\" style=\"cursor: pointer;\" onClick=\"do_edit($can[canID], '$title', '$can[deptID]', '$message')\" class=\"help_tooltip\" title=\"- edit canned\"  width=\"14\" height=\"14\" border=0>" : "<img src=\"../pics/space.gif\" width=\"14\" height=\"14\" border=0>" ;

							print "<tr><td class=\"$td1\" nowrap>$delete_image &nbsp; $edit_image</td><td class=\"$td1\"><b>$title_display</b></td><td class=\"$td1\">$dept_name</td><td class=\"$td1\">$message_display</td></tr>" ;
						}
						if ( $c == 0 )
							print "<tr><td colspan=7 class=\"td_dept_td\">blank results</td></tr>" ;
					?>
					</table>
				</div>

				<a name="a_edit"></a><div class="edit_title" style="margin-top: 55px;">Create/Edit Canned Responses <span class="txt_red"><?php echo $error ?></span></div>
				<div style="margin-top: 10px;">
					<table cellspacing=0 cellpadding=0 border=0 width="100%">
					<tr>
						<form method="POST" action="cans.php?<?php echo time() ?>" id="theform">
						<td valign="top" style="width: 420px; padding: 5px;">
							<input type="hidden" name="ses" value="<?php echo $ses ?>">
							<input type="hidden" name="action" value="submit">
							<input type="hidden" name="canid" id="canid" value="0">
							<div>
								Title<br>
								<input type="text" name="title" id="title" class="input_text" style="width: 98%; margin-bottom: 10px;" maxlength="25">
								Department<br>
								<select name="deptid" id="deptid" style="width: 99%; margin-bottom: 10px;">
								<option value="1111111111">All Departments</option>
								<?php
									for ( $c = 0; $c < count( $departments ); ++$c )
									{
										$department = $departments[$c] ;

										print "<option value=\"$department[deptID]\">$department[name]</option>" ;
									}
								?>
								</select>
								Canned Message<br>
								<textarea name="message" id="message" class="input_text" rows="7" style="min-width: 98%; margin-bottom: 10px;" wrap="virtual"></textarea>

								<button type="button" onClick="do_submit()" class="input_button">Submit</button> or <span style="text-decoration: underline; cursor: pointer;" onClick="cancel_edit()">Cancel</span>
							</div>
						</td>
						</form>
						<td valign="center">
							<ul>
								<li> HTML will be converted to raw code.
								<li style="margin-top: 5px;"> Your created canned messages are private and are not shared.

								<li style="margin-top: 5px;"> Following variables can be used to dynamically populate data:
									<ul style="margin-top: 10px;">
										<li> <b>%%visitor%%</b> = visitor's name
										<li> <b>%%operator%%</b> = your name
										<li> <b>%%op_email%%</b> = your email

										<li style="padding-top: 10px;"> to link an email use the command <b>email:</b>
											<ul>
												<li> <i>example</i> - <b>email:</b><i>someone@somewhere.com</i>
											</ul>
										<li style="padding-top: 10px;"> to display an image use the command <b>image:</b>
											<ul>
												<li> <i>example</i> - <b>image:</b><i>http://www.someurl.com/image.gif</i>
											</ul>
									</ul>
							</ul>
						</td>
					</tr>
					</table>
				</div>
			</div>
		
		</div>

		<?php include_once( "./inc_footer.php" ) ; ?>
	</div>
</div>
<?php include_once( "./inc_menu.php" ) ; ?>

</body>
</html>
<?php database_mysql_close( $dbh ) ; ?>
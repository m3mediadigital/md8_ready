<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	/****************************************/
	// STANDARD header for Setup
	if ( !file_exists( "../web/config.php" ) ){ HEADER("location: ../setup/install.php") ; exit ; }
	include_once( "../web/config.php" ) ;
	include_once( "../API/Util_Format.php" ) ;
	include_once( "../API/Util_Error.php" ) ;
	include_once( "../API/SQL.php" ) ;
	include_once( "../API/Util_Security.php" ) ;
	$ses = Util_Format_Sanatize( Util_Format_GetVar( "ses" ), "ln" ) ;
	if ( !$opinfo = Util_Security_AuthOp( $dbh, $ses ) ){ ErrorHandler ( 602, "Invalid operator session or session has expired.", $PHPLIVE_FULLURL, 0, Array() ) ; }
	// STANDARD header end
	/****************************************/

	include_once( "../API/Util_Functions.php" ) ;
	include_once( "../API/Ops/get.php" ) ;
	include_once( "../API/Depts/get.php" ) ;
	include_once( "../API/Chat/get_ext.php" ) ;

	$action = Util_Format_Sanatize( Util_Format_GetVar( "action" ), "ln" ) ;
	$console = Util_Format_Sanatize( Util_Format_GetVar( "console" ), "ln" ) ; $body_width = ( $console ) ? 800 : 900 ;
	$page = ( Util_Format_Sanatize( Util_Format_GetVar( "page" ), "ln" ) ) ? Util_Format_Sanatize( Util_Format_GetVar( "page" ), "ln" ) : 0 ;
	$index = ( Util_Format_Sanatize( Util_Format_GetVar( "index" ), "ln" ) ) ? Util_Format_Sanatize( Util_Format_GetVar( "index" ), "ln" ) : 0 ;

	$menu = "transcripts" ;
	$error = "" ;
	$theme = "default" ;

	$departments = Depts_get_OpDepts( $dbh, $opinfo["opID"] ) ;
	$operators = Ops_get_AllOps( $dbh ) ;

	// make hash for quick refrence
	$operators_hash = Array() ;
	for ( $c = 0; $c < count( $operators ); ++$c )
	{
		$operator = $operators[$c] ;
		$operators_hash[$operator["opID"]] = $operator["name"] ;
	}

	$dept_hash = Array() ;
	for ( $c = 0; $c < count( $departments ); ++$c )
	{
		$department = $departments[$c] ;
		$dept_hash[$department["deptID"]] = $department["name"] ;
	}

	$text = Util_Format_Sanatize( Util_Format_GetVar( "text" ), "" ) ;
	$text = ( $text ) ? $text : "" ;
	$text_query = urlencode( $text ) ;
	$transcripts = Chat_ext_get_OpDeptTrans( $dbh, $opinfo["opID"], $text, $page, 20 ) ;

	$total_index = count($transcripts) - 1 ;
	$pages = Util_Functions_Page( $page, $index, 20, $transcripts[$total_index], "transcripts.php", "ses=$ses&text=$text_query&opid=$opinfo[opID]" ) ;
?>
<?php include_once( "../inc_doctype.php" ) ?>
<head>
<title> Transcripts </title>

<meta name="description" content="PHP Live! Support <?php echo $VERSION ?>">
<meta name="keywords" content="powered by: PHP Live!  www.phplivesupport.com">
<meta name="robots" content="all,index,follow">
<meta http-equiv="content-type" content="text/html; CHARSET=utf-8"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="Stylesheet" href="../css/base_setup.css?<?php echo $VERSION ?>">
<script type="text/javascript" src="../js/global.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/setup.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/framework_cnt.js?<?php echo $VERSION ?>"></script>
<script type="text/javascript" src="../js/tooltip.js?<?php echo $VERSION ?>"></script>

<script type="text/javascript">
<!--
	var menu ;
	var reset_url = "transcripts.php?ses=<?php echo $ses ?>" ;

	$(document).ready(function()
	{
		$("body").show() ;
		init_menu() ;
		init_tooltips() ;
		toggle_menu_op( "<?php echo $menu ?>", '<?php echo $ses ?>' ) ;

		$('#input_search').focus() ;
		$('#form_search').bind("submit", function() { return false ; }) ;
	});
	
	function open_transcript( theces, theopname )
	{
		var url = "./op_trans_view.php?ses=<?php echo $ses ?>&ces="+theces+"&id=<?php echo $opinfo["opID"] ?>&auth=operator&"+unixtime() ;
		newwin = window.open( url, theces, "scrollbars=yes,menubar=no,resizable=1,location=no,width=650,height=450,status=0" ) ;

		if ( newwin )
			newwin.focus() ;
	}

	function input_text_listen_search( e )
	{
		var key = -1 ;
		var shift ;

		key = e.keyCode ;
		shift = e.shiftKey ;

		if ( !shift && ( ( key == 13 ) || ( key == 10 ) ) )
			$('#btn_page_search').click() ;
	}

	function init_tooltips()
	{
		var help_tooltips = $( '#transcripts' ).find( '.help_tooltip' ) ;
		help_tooltips.tooltip({
			event: "mouseover",
			track: true,
			delay: 0,
			showURL: false,
			showBody: "- ",
			fade: 0,
			positionLeft: false,
			extraClass: "stat"
		});
	}

//-->
</script>
</head>
<body style="display: none;">

<div id="body" style="padding-bottom: 60px;">
	<div id="body_wrapper" style="z-Index: 5;"></div>
	<div style="width: 100%; z-Index: 10;">
		<div style="width: 900px; margin: 0 auto;">
		<div id="body_sub_title"></div>

			<div id="op_transcripts" class="body_main" style="display: none; margin: 0 auto; margin-top: 25px;">
				<div class="info_info">
					View chat transcripts.
				</div>

				<div id="transcripts" style="margin-top: 35px;">
					<table cellspacing=0 cellpadding=0 border=0 width="100%">
					<tr><td colspan="10"><div class="page_top_wrapper"><?php echo $pages ?></div></td>
					<tr>
						<td width="20" nowrap><div id="td_dept_header">&nbsp;</div></td>
						<td width="80" nowrap><div id="td_dept_header">Operator</div></td>
						<td width="80" nowrap><div id="td_dept_header">Visitor</div></td>
						<td width="40"><div id="td_dept_header">Rating</div></td>
						<td width="140"><div id="td_dept_header">Created</div></td>
						<td width="80"><div id="td_dept_header">Duration</div></td>
						<td width="50"><div id="td_dept_header">Size</div></td>
						<td><div id="td_dept_header">Question</div></td>
					</tr>
					<?php
						$opinfo["theme"] = "default" ;
						for ( $c = 0; $c < count( $transcripts )-1; ++$c )
						{
							$transcript = $transcripts[$c] ;

							// brute fix of rare bug
							if ( $transcript["opID"] && ( isset( $operators_hash[$transcript["op2op"]] ) || isset( $operators_hash[$transcript["opID"]] ) ) )
							{
								$operator = ( $transcript["op2op"] ) ? $operators_hash[$transcript["op2op"]] : $operators_hash[$transcript["opID"]] ;
								$created = date( "M j (g:i:s a)", $transcript["created"] ) ;
								$duration = $transcript["ended"] - $transcript["created"] ;
								$duration = ( ( $duration - 60 ) < 1 ) ? " 1 min" : Util_Format_Duration( $duration ) ;
								$question = $transcript["question"] ;
								$vname = ( $transcript["op2op"] ) ? $operators_hash[$transcript["opID"]] : $transcript["vname"] ;
								$rating = Util_Functions_Stars( $transcript["rating"] ) ;
								$initiated = ( $transcript["initiated"] ) ?  "<img src=\"../pics/icons/info_initiate.gif\" width=\"10\" height=\"10\" border=\"0\" alt=\"\" class=\"help_tooltip\" title=\"- Operator Initiated Chat\"> " : "" ;

								if ( $transcript["op2op"] )
									$question = "<div class=\"text_grey\"><img src=\"../pics/icons/agent.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"Op-2-Op Chat\" class=\"help_tooltip\" title=\"- Operator to Operator Chat\" style=\"cursor: help;\"></div>" ;

								$td1 = "td_dept_td" ; $td2 = "td_dept_td_td" ;
								if ( $c % 2 ) { $td1 = "td_dept_td2" ; $td2 = "td_dept_td_td2" ; }

								print "<tr id=\"tr_$transcript[ces]\"><td class=\"$td1\"><img src=\"../pics/icons/view.png\" style=\"cursor: pointer;\" onClick=\"open_transcript('$transcript[ces]', '$operator')\" id=\"img_$transcript[ces]\" class=\"help_tooltip\" title=\"- view transcript\"></td><td class=\"$td1\" nowrap><b><div id=\"transcript_$transcript[ces]\">$initiated$operator</div></b></td><td class=\"$td1\" nowrap>$vname</td><td class=\"$td1\" align=\"center\">$rating</td><td class=\"$td1\" nowrap>$created</td><td class=\"$td1\" nowrap>$duration</td><td class=\"$td1\">$transcript[fsize]</td><td class=\"$td1\">$question</td></tr>" ;
							}
						}
						if ( $c == 0 )
							print "<tr><td colspan=7 class=\"td_dept_td\">blank results</td></tr>" ;
					?>
					</table>
				</div>
			</div>
		
		</div>

		<?php include_once( "./inc_footer.php" ) ; ?>
	</div>
</div>
<?php include_once( "./inc_menu.php" ) ; ?>

</body>
</html>
<?php database_mysql_close( $dbh ) ; ?>
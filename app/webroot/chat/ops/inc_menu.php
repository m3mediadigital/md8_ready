<div style="position: absolute; top: 0px; left: 0px; width: 100%; z-index: 13;">
	<div style="width: <?php echo $body_width ?>px; margin: 0 auto; background: url( ../pics/bg_trans.png ) repeat; border-bottom-left-radius: 5px 5px; -moz-border-radius-bottomleft: 5px 5px; border-bottom-right-radius: 5px 5px; -moz-border-radius-bottomright: 5px 5px;">
		<div id="menu_wrapper">
			<?php if ( !$console ): ?><div id="menu_go" class="menu" onClick="<?php echo ( preg_match( "/(cans)|(transcript)|(report)/", $menu ) ) ? "location.href='./?ses=$ses'" : "toggle_menu_op('go', '$ses')" ; ?>">Go ONLINE!</div><?php endif ; ?>
			<div id="menu_themes" class="menu" onClick="<?php echo ( preg_match( "/(cans)|(transcript)|(report)/", $menu ) ) ? "location.href='./?menu=themes&console=$console&ses=$ses'" : "toggle_menu_op('themes', '$ses')" ; ?>">Themes</div>
			<div id="menu_sounds" class="menu" onClick="<?php echo ( preg_match( "/(cans)|(transcript)|(report)/", $menu ) ) ? "location.href='./?menu=sounds&console=$console&ses=$ses'" : "toggle_menu_op('sounds', '$ses')" ; ?>">Sounds</div>
			<div id="menu_dn" class="menu" onClick="<?php echo ( preg_match( "/(cans)|(transcript)|(report)/", $menu ) ) ? "location.href='./?menu=dn&console=$console&ses=$ses'" : "toggle_menu_op('dn', '$ses')" ; ?>">Desktop Notification</div>
			<?php if ( $opinfo["sms"] ): ?><div id="menu_mobile" class="menu" onClick="<?php echo ( preg_match( "/(cans)|(transcript)|(report)/", $menu ) ) ? "location.href='./?menu=mobile&console=$console&ses=$ses'" : "toggle_menu_op('mobile', '$ses')" ; ?>">SMS</div><?php endif ; ?>
			<?php if ( !$console ): ?><div id="menu_cans" class="menu" onClick="location.href='./cans.php?ses=<?php echo $ses ?>'">Canned Responses</div><?php endif ; ?>
			<?php if ( !$console ): ?><div id="menu_transcripts" class="menu" onClick="location.href='./transcripts.php?ses=<?php echo $ses ?>'">Transcripts</div><?php endif ; ?>
			<div id="menu_reports" class="menu" onClick="location.href='./reports.php?console=<?php echo $console ?>&ses=<?php echo $ses ?>'">Online Activity</div>
			<div id="menu_password" class="menu" onClick="<?php echo ( preg_match( "/(cans)|(transcript)|(report)/", $menu ) ) ? "location.href='./?menu=password&console=$console&ses=$ses'" : "toggle_menu_op('password', '$ses')" ; ?>">Password</div>
			<?php if ( file_exists( "$CONF[DOCUMENT_ROOT]/addons/inc_lang.php" ) ): ?>
			<div id="menu_language" class="menu" onClick="<?php echo ( preg_match( "/(cans)|(transcript)|(report)/", $menu ) ) ? "location.href='./?menu=language&console=$console&ses=$ses'" : "toggle_menu_op('language', '$ses')" ; ?>">Set Language</div>
			<?php endif ; ?>
			<div style="clear: both;"></div>
		</div>
	</div>
</div>
<div style="position: absolute; top: 13px; left: 0px; width: 100%; z-index: 12;">
	<div style="width: <?php echo $body_width ?>px; margin: 0 auto; padding-top: 32px;">
		<div style="font-size: 10px; font-weight: bold; color: #CBD1D5; text-align: right;"><span style="font-size: 16px; "><img src="../pics/icons/vcard.png" width="16" height="16" border="0" alt=""> <?php echo $opinfo["name"] ?></span> &nbsp; <?php echo ( isset($opinfo['opID']) && !$console ) ? "<span style=\"padding: 5px; background: #FFFFFF; color: #7C89A9; border: 1px solid #687086; -moz-border-radius: 5px; border-radius: 5px;\"><a href=\"JavaScript:void(0)\" onClick=\"logout_op('$ses')\">sign out</a></span>" : "" ; ?></div>
	</div>
</div>

<div style="position: absolute; background: url( ../pics/bg_fade_bottom.png ) repeat-x; background-position: bottom; top: 0px; left: 0px; width: 100%; height: 60px; z-index: 11;"></div>
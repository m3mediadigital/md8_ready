var phplive_wp ; if ( parent.location != window.location  ) { phplive_wp = 1 ; }
function init_menu()
{
	$( '*', 'body' ).each( function(){
		var div_name = $( this ).attr('id') ;
		var class_name = $( this ).attr('class') ;
		if ( class_name == "menu" )
		{
			$(this).hover(
				function () {
					$(this).removeClass('menu').addClass('menu_hover') ;
				}, 
				function () {
					$(this).removeClass('menu_hover').addClass('menu') ;
				}
			);
		}
		else if ( class_name == "home_box_li" )
		{
			$(this).hover(
				function () {
					$(this).removeClass('home_box_li').addClass('home_box_li_focus') ;
				}, 
				function () {
					$(this).removeClass('home_box_li_focus').addClass('home_box_li') ;
				}
			);
		}
	} );
}

function toggle_menu_op( themenu )
{
	var divs = Array( "go", "cans", "transcripts", "reports", "themes", "sounds", "password", "language", "mobile", "dn" ) ;

	for ( c = 0; c < divs.length; ++c )
	{
		$('#menu_'+divs[c]).removeClass('menu_focus').addClass('menu') ;
		$('#op_'+divs[c]).hide() ;
	}

	menu = themenu ;

	if ( themenu == "go" )
		$('#body_sub_title').html( "<img src=\"../pics/icons/lightbulb.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Go ONLINE!" ) ;
	else if ( themenu == "cans" )
		$('#body_sub_title').html( "<img src=\"../pics/icons/chats.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Canned Responses" ) ;
	else if ( themenu == "transcripts" )
		$('#body_sub_title').html( "<img src=\"../pics/icons/view.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Transcripts" ) ;
	else if ( themenu == "reports" )
		$('#body_sub_title').html( "<img src=\"../pics/icons/book.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Online Activity" ) ;
	else if ( themenu == "themes" )
		$('#body_sub_title').html( "<img src=\"../pics/icons/image.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Themes" ) ;
	else if ( themenu == "sounds" )
		$('#body_sub_title').html( "<img src=\"../pics/icons/bell_start.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Sound Alerts" ) ;
	else if ( themenu == "mobile" )
		$('#body_sub_title').html( "<img src=\"../pics/icons/mobile.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> SMS Alert" ) ;
	else if ( themenu == "dn" )
		$('#body_sub_title').html( "<img src=\"../pics/icons/notify.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Desktop Notification" ) ;
	else if ( themenu == "password" )
		$('#body_sub_title').html( "<img src=\"../pics/icons/user_key.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"\" style=\"margin-right: 5px;\"> Update Password" ) ;

	$('#menu_'+themenu).removeClass('menu').addClass('menu_focus') ;
	$('#op_'+themenu).show() ;
}

function logout_op( theses )
{
	location.href = "../index.php?action=logout&ses="+theses ;
}

function toggle_menu_setup( themenu )
{
	// place actions that should always trigger in setup here
	if ( phplive_wp )
		$('#popout').show() ;

	var divs = Array( "home", "depts", "ops", "icons", "html", "trans", "rchats", "rtraffic", "marketing", "settings", "extras" ) ;

	for ( c = 0; c < divs.length; ++c )
		$('#menu_'+divs[c]).removeClass('menu_focus').addClass('menu') ;

	$('#menu_'+themenu).removeClass('menu').addClass('menu_focus') ;
	menu = themenu ;
}

function preview_theme( thetheme )
{
	var unique = unixtime() ;

	window.open( "../phplive.php?theme="+thetheme+"&"+unique, "themed"+unique, 'scrollbars=no,resizable=yes,menubar=no,location=no,screenX=50,screenY=100,width=550,height=410' ) ;
}

<?php
	/* (c) OSI Codes Inc. */
	/* http://www.osicodesinc.com */
	/* Dev team: 615 */
	include_once( "../web/config.php" ) ;
	include_once( "../API/Util_Format.php" ) ;
	include_once( "../API/Util_Vars.php" ) ;

	$query = Util_Format_Sanatize( Util_Format_GetVar( "q" ), "" ) ;
	$mobile = Util_Mobile_Detect() ;

	$params = Array() ;
	$tok = strtok( $query, '|' ) ;
	while ( $tok !== false ) { $params[] = $tok ; $tok = strtok( '|' ) ; }

	// proto flags
	// 0 (toggle), 1 (http), 2 (https)

	$deptid = isset( $params[0] ) ? Util_Format_Sanatize( $params[0], "n" ) : 0 ;
	$btn = isset( $params[1] ) ? Util_Format_Sanatize( $params[1], "n" ) : 0 ;
	$proto = isset( $params[2] ) ? Util_Format_Sanatize( rawurldecode( $params[2] ), "n" ) : 0 ;
	$text = isset( $params[3] ) ? Util_Format_Sanatize( rawurldecode( $params[3] ), "ln" ) : "" ;

	$base_url = $CONF["BASE_URL"] ;
	if ( $proto == 1 ) { $base_url = preg_replace( "/(http:)|(https:)/", "http:", $base_url ) ; }
	else if ( $proto == 2 ) { $base_url = preg_replace( "/(http:)|(https:)/", "https:", $base_url ) ; }
	else { $base_url = preg_replace( "/(http:)|(https:)/", "", $base_url ) ; }

	$ip = Util_Format_GetIP() ;
	if ( !isset( $CONF["icon_check"] ) ) { $CONF["icon_check"] = "off" ; }

	$initiate = ( isset( $CONF["auto_initiate"] ) && $CONF["auto_initiate"] ) ? unserialize( html_entity_decode( $CONF["auto_initiate"] ) ) : Array() ;
	$widget_max = 4 ;
	$widget_slider = ( isset( $initiate["pos"] ) ) ? $initiate["pos"] : 1 ;
	// 1 left, 2 right, 3 bottom left, 4 bottom right, 5 bottom center, x center
	if ( $widget_slider == 1 )
	{ $widget_animate_show = "left: '50px'" ; $widget_animate_hide = "left: -800" ; $widget_top_left = "top: 190px; left: 50px;" ; $widget_cover_top_left = "top: 190px; left: -800px;" ; }
	else if ( $widget_slider == 2 )
	{ $widget_animate_show = "right: '50px'" ; $widget_animate_hide = "right: -800" ; $widget_top_left = "top: 190px; right: 50px;" ; $widget_cover_top_left = "top: 190px; right: -800px;" ; }
	else if ( $widget_slider == 3 )
	{ $widget_animate_show = "bottom: '50px'" ; $widget_animate_hide = "bottom: -800" ; $widget_top_left = "bottom: 50px; left: 50px;" ; $widget_cover_top_left = "bottom: -800px; left: 50px;" ; }
	else if ( $widget_slider == 4 )
	{ $widget_animate_show = "bottom: '50px'" ; $widget_animate_hide = "bottom: -800" ; $widget_top_left = "bottom: 50px; right: 50px;" ; $widget_cover_top_left = "bottom: -800px; right: 50px;" ; }
	else { $widget_animate_show = $widget_animate_hide = $widget_top_left = $widget_cover_top_left = "" ; }

	$phplive_chat_width = 550 ;
	$phplive_chat_height = 420 ;

	Header( "content-type:application/x-javascript" ) ;
?>
function phplive_unique() { var date = new Date() ; return date.getTime() ; }
if ( typeof( phplive_js ) == "undefined" )
{
	var phplive_jquery ;
	var phplive_stat_refer = encodeURIComponent( document.referrer.replace("http", "hphp") ) ;
	var phplive_stat_onpage = encodeURIComponent( location.toString().replace("http", "hphp") ) ;
	var phplive_stat_title = encodeURIComponent( document.title ) ;
	var win_width = screen.width ;
	var win_height = screen.height ;
	var phplive_initiate_widget = 0 
	var phplive_widget = "" ;
	var obj_div, obj_cover, obj_iframe ;
	var phplive_center = function(a){var b=phplive_jquery(window),c=b.scrollTop();return this.each(function(){var f=phplive_jquery(this),e=phplive_jquery.extend({against:"window",top:false,topPercentage:0.5},a),d=function(){var h,g,i;if(e.against==="window"){h=b;}else{if(e.against==="parent"){h=f.parent();c=0;}else{h=f.parents(against);c=0;}}g=((h.width())-(f.outerWidth()))*0.5;i=((h.height())-(f.outerHeight()))*e.topPercentage+c;if(e.top){i=e.top+c;}f.css({left:g,top:i});};d();b.resize(d);});} ;

	var resolution = escape( win_width + " x " + win_height ) ;

	var phplive_quirks = 0 ;
	var phplive_IE ;
	//@cc_on phplive_IE = navigator.appVersion ;

	var mode = document.compatMode,m ;
	if ( ( mode == 'BackCompat' ) && phplive_IE )
		phplive_quirks = 1 ;
}

if ( !window.phplive_init_jquery )
{
	window.phplive_init_jquery = function()
	{
		if ( typeof( phplive_jquery ) == "undefined" )
		{
			if ( typeof( window.jQuery ) == "undefined" )
			{
				var script_jquery = document.createElement('script') ;
				script_jquery.type = "text/javascript" ; script_jquery.async = true ;
				script_jquery.onload = script_jquery.onreadystatechange = function () {
					if ( ( typeof( this.readyState ) == "undefined" ) || ( this.readyState == "loaded" || this.readyState == "complete" ) )
					{
						phplive_jquery = window.jQuery.noConflict() ;
						phplive_jquery.fn.center = phplive_center ;
					}
				} ;
				script_jquery.src = "<?php echo $base_url ?>/js/framework.js?<?php echo $VERSION ?>" ;
				var script_jquery_s = document.getElementsByTagName('script')[0] ;
				script_jquery_s.parentNode.insertBefore(script_jquery, script_jquery_s) ;
			}
			else
			{
				phplive_jquery = window.jQuery ;
				phplive_jquery.fn.center = phplive_center ;
			}
		}
	}
}
var phplive_pullimg_footprint_<?php echo $deptid ?>, st_phplive_pullimg_<?php echo $deptid ?>, phplive_thec_<?php echo $deptid ?> = 0 ;
var phplive_status_image_<?php echo $deptid ?> = "<?php echo $base_url ?>/ajax/image.php?d=<?php echo $deptid ?>&r="+phplive_stat_refer+"&p="+phplive_stat_onpage+"&title="+phplive_stat_title+"&btn=<?php echo $btn ?>&resolution="+resolution+"&"+phplive_unique() ;
var phplive_request_url_<?php echo $deptid ?> = "<?php echo $base_url ?>/phplive.php?d=<?php echo $deptid ?>&btn=<?php echo $btn ?>&onpage="+phplive_stat_onpage+"&title="+phplive_stat_title ;
var phplive_pullimg_widget_<?php echo $deptid ?> ;
var phplive_interval_<?php echo $btn ?> ;

function phplive_image_refresh_<?php echo $btn ?>()
{
	<?php if ( $text || ( $CONF["icon_check"] == "off" ) ): ?>
	if ( typeof( phplive_interval_<?php echo $btn ?> ) != "undefined" )
		clearInterval( phplive_interval_<?php echo $btn ?> ) ;
	<?php else: ?>
	var chat_icon_check = "<?php echo $base_url ?>/ajax/image.php?d=<?php echo $deptid ?>&r="+phplive_stat_refer+"&p="+phplive_stat_onpage+"&title="+phplive_stat_title+"&btn=<?php echo $deptid ?>&resolution="+resolution+"&"+phplive_unique() ;
	document.getElementById("phplive_image_or_text_image_<?php echo $btn ?>").src = chat_icon_check ;
	<?php endif ; ?>
}

function phplive_silent_close( phplive_theces, theisadmin, thetimer, theunique )
{
	//
}

function phplive_footprint_tracker_<?php echo $deptid ?>()
{
	phplive_pullimg_footprint_<?php echo $deptid ?> = new Image ;
	phplive_pullimg_footprint_<?php echo $deptid ?>.onload = phplive_pullimg_actions_<?php echo $deptid ?> ;
	phplive_pullimg_footprint_<?php echo $deptid ?>.src = "<?php echo $base_url ?>/ajax/footprints.php?deptid=<?php echo $deptid ?>&r="+phplive_stat_refer+"&onpage="+phplive_stat_onpage+"&title="+phplive_stat_title+"&c="+phplive_thec_<?php echo $deptid ?>+"&resolution="+resolution+"&"+phplive_unique() ;
}

function phplive_pullimg_actions_<?php echo $deptid ?>()
{
	var thisflag = phplive_pullimg_footprint_<?php echo $deptid ?>.width ;

	if ( ( thisflag == 1 ) || ( thisflag == 2 ) )
	{
		if ( typeof( phplive_jquery ) == "undefined" )
			phplive_init_jquery() ;
		else if ( ( thisflag == 2 ) && !phplive_initiate_widget )
		{
			phplive_jquery( "body" ).append( phplive_widget ) ;

			phplive_initiate_widget = 1 ;
			obj_div = phplive_jquery( "#phplive_widget" ) ;
			obj_div_cover = phplive_jquery( "#phplive_widget_cover" ) ;
			obj_iframe = phplive_jquery( "#iframe_widget" ) ;

			obj_iframe.attr( 'src', "<?php echo $base_url ?>/widget.php?btn=<?php echo $deptid ?>&"+phplive_unique() ) ;

			if ( <?php echo $widget_slider ?> > <?php echo $widget_max ?> )
			{
				obj_div_cover.center().show() ;
				obj_div.center().fadeIn("fast") ;
			}
			else
				obj_div_cover.show().animate({ <?php echo $widget_animate_show ?> }, 2000, function() { obj_div.fadeIn("fast") ; }) ;
		}
		else if ( ( thisflag == 1 ) && phplive_initiate_widget )
		{
			phplive_initiate_widget = 0 ;
			obj_div.fadeOut( "fast" ) ;
			obj_div_cover.fadeOut("fast") ;
		}

		++phplive_thec_<?php echo $deptid ?> ;
		if ( typeof( st_phplive_pullimg_<?php echo $deptid ?> ) != "undefined" )
			clearTimeout( st_phplive_pullimg_<?php echo $deptid ?> ) ;

		st_phplive_pullimg_<?php echo $deptid ?> = setTimeout(function(){ phplive_footprint_tracker_<?php echo $deptid ?>() }, <?php echo $VARS_JS_FOOTPRINT ?> * 1000) ;
	}
	else if ( thisflag == 4 )
		clearTimeout( st_phplive_pullimg_<?php echo $deptid ?> ) ;
}

function phplive_launch_chat_<?php echo $deptid ?>(thewidget, thetheme)
{
	var winname = phplive_unique() ;
	var name = email = ""
	var custom_vars = "&custom=" ;
	if ( typeof( thetheme ) == "undefined" ){ thetheme = "" ; }

	if ( typeof( phplive_v ) != "undefined" )
	{
		for ( var key in phplive_v )
		{
			if ( key == "name" )
				name = encodeURIComponent( phplive_v["name"] ) ;
			else if ( key == "email" )
				email = encodeURIComponent( phplive_v["email"] ) ;
			else
				custom_vars += encodeURIComponent( key )+"-_-"+encodeURIComponent( phplive_v[key] )+"-cus-" ;
		}
	}

	phplive_request_url_<?php echo $deptid ?> = phplive_request_url_<?php echo $deptid ?>+"&theme="+thetheme+"&js_name="+name+"&js_email="+email+"&widget="+thewidget+custom_vars ;

	window.open( phplive_request_url_<?php echo $deptid ?>, winname, 'scrollbars=no,resizable=yes,menubar=no,location=no,screenX=50,screenY=100,width=<?php echo $phplive_chat_width ?>,height=<?php echo $phplive_chat_height ?>' ) ;
}

function phplive_write_widget()
{
	var this_position = ( phplive_quirks ) ? "absolute" : "fixed" ;
	var this_widget_width = ( phplive_quirks ) ? 270 : 250 ;
	var this_widget_height = ( phplive_quirks ) ? 180 : 160 ;

	// NOTE: we do not recommend modifying the width and height of the widget window as this will alter the
	// communication behavior based on mouse coordinates in function phplive_pullimg_actions_<?php echo $deptid ?>()
	phplive_widget = "<map name='initiate_chat_cover'><area shape='rect' coords='222,2,247,26' href='JavaScript:void(0)' onClick='phplive_widget_decline()'><area shape='rect' coords='0,26,250,160' href='JavaScript:void(0)' onClick='phplive_widget_launch()'></map><div id='phplive_widget' style='display: none; position: "+this_position+"; <?php echo $widget_top_left ?> background: url( <?php echo $base_url ?>/themes/initiate/bg_trans.png ) repeat; padding: 10px; width: "+this_widget_width+"px; height: "+this_widget_height+"px; -moz-border-radius: 5px; border-radius: 5px; z-Index: 10000;'><iframe id='iframe_widget' name='iframe_widget' style='display: none; width: 1px; height: 1px;' src='<?php echo $base_url ?>/blank.php' scrolling='no' border=0 frameborder=0 onLoad=''></iframe></div><div id='phplive_widget_cover' style='display: none; position: "+this_position+"; <?php echo $widget_cover_top_left ?> padding: 10px; z-Index: 10001;'><img src='<?php echo Util_Format_GetInitiate( $base_url, 0 ) ; ?>' width='250' height='160' border=0 usemap='#initiate_chat_cover' style='-moz-border-radius: 5px; border-radius: 5px;'></div>" ;
}

function phplive_widget_launch()
{
	if ( phplive_initiate_widget )
	{
		var obj_div = phplive_jquery( "#phplive_widget" ) ;
		var obj_div_cover = phplive_jquery( "#phplive_widget_cover" ) ;
		var obj_iframe = phplive_jquery( "#iframe_widget" ) ;

		obj_div.fadeOut( "fast" ) ;
		obj_div_cover.hide() ;
		obj_iframe.attr( 'src', "<?php echo $base_url ?>/blank.php" ) ;
		// todo: look into this further as it will always call the first instance of JS
		phplive_launch_chat_<?php echo $deptid ?>(1) ;
	}
}

function phplive_widget_decline()
{
	if ( phplive_initiate_widget )
	{
		var obj_div = phplive_jquery( "#phplive_widget" ) ;
		var obj_div_cover = phplive_jquery( "#phplive_widget_cover" ) ;
		var obj_iframe = phplive_jquery ( "#iframe_widget" ) ;

		obj_div.hide() ;

		phplive_pullimg_widget_<?php echo $deptid ?> = new Image ;
		phplive_pullimg_widget_<?php echo $deptid ?>.onload = function() {
			if ( <?php echo $widget_slider ?> > <?php echo $widget_max ?> )
				obj_div_cover.fadeOut("fast") ;
			else
				obj_div_cover.animate({ <?php echo $widget_animate_hide ?> }, 2000) ;
		};
		phplive_pullimg_widget_<?php echo $deptid ?>.src = "<?php echo $base_url ?>/ajax/chat_actions.php?action=disconnect&isop=0&widget=1&ip=<?php echo $ip ?>&"+phplive_unique() ;
		obj_iframe.attr( 'src', "<?php echo $base_url ?>/blank.php" ) ;
		phplive_initiate_widget = 0 ;
	}
}

if ( typeof( phplive_footprint_js_<?php echo $deptid ?> ) == "undefined" )
{
	phplive_footprint_js_<?php echo $deptid ?> = 1 ;
	phplive_footprint_tracker_<?php echo $deptid ?>() ;
}

if ( typeof( phplive_js ) == "undefined" )
{
	phplive_js = 1 ;
	phplive_write_widget() ;
}

/********************************************/
// the actual writing of the chat status icon or text
/********************************************/
function phplive_output_image_or_text_<?php echo $btn ?>()
{
	<?php if ( $text ): ?>
	var phplive_image_or_text_<?php echo $btn ?> = "<?php echo $text ?>" ;
	<?php else: ?>
	phplive_interval_<?php echo $btn ?> = setInterval(function(){ phplive_image_refresh_<?php echo $btn ?>() ; }, <?php echo ( $VARS_JS_ICON_CHECK * 1000 ) ?>) ;
	var phplive_image_or_text_<?php echo $btn ?> = "<img src=\""+phplive_status_image_<?php echo $deptid ?>+"\" border=0 name=\"phplive_image_or_text_image_<?php echo $btn ?>\" id=\"phplive_image_or_text_image_<?php echo $btn ?>\" title=\"\" alt=\"\">" ;
	<?php endif ; ?>

	if ( phplive_quirks )
	{
		var phplive_btn = document.getElementById('phplive_btn_<?php echo $btn ?>') ;
		if ( ( typeof( phplive_btn.style.position ) != "undefined" ) && phplive_btn.style.position )
			phplive_btn.style.position = "absolute" ;
	}

	var phplive_image_or_text_<?php echo $btn ?>_span = document.createElement("span") ;
	phplive_image_or_text_<?php echo $btn ?>_span.innerHTML = phplive_image_or_text_<?php echo $btn ?> ;
	document.getElementById("phplive_btn_<?php echo $btn ?>").appendChild( phplive_image_or_text_<?php echo $btn ?>_span ) ;
}
/********************************************/

phplive_output_image_or_text_<?php echo $btn ?>() ;
phplive_init_jquery() ;

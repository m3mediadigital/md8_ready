<?php
	/******************************************/
	// This file is depricated.  If you have not generated an HTML code
	// in the setup area that points to the new phplive_v2.js.php, please
	// do so and update your HTML codes on your webpages.  This file is
	// no longer supported as of 1/9/2012
	/******************************************/
	include_once( "../web/config.php" ) ;
	include_once( "../API/Util_Format.php" ) ;
	include_once( "../API/Util_Vars.php" ) ;

	$deptid = Util_Format_Sanatize( Util_Format_GetVar( "d" ), "n" ) ;
	$base_url = Util_Format_Sanatize( rawurldecode( Util_Format_GetVar( "base_url" ) ), "base_url" ) ;
	$text = Util_Format_Sanatize( rawurldecode( Util_Format_GetVar( "text" ) ), "ln" ) ;
	$mobile = Util_Mobile_Detect() ;

	$ip = Util_Format_GetIP() ;
	if ( !isset( $CONF["icon_check"] ) )
		$CONF["icon_check"] = "off" ;

	srand( (double)microtime() ) ; $btn = mt_rand( 1000,100000000 ) ;


	/* chat variables */
	$phplive_chat_width = 550 ; // width of the chat window
	$phplive_chat_height = 410 ; // height of the chat window
	$phplive_initiate_left = 50 ; // left position of the initiate chat image
	/* end variables */


	Header( "content-type:application/x-javascript" ) ;
?>

function phplive_unique() { var date = new Date() ; return date.getTime() ; }
if ( typeof( phplive_js ) == "undefined" )
{
	phplive_js = 1 ;
	var phplive_dom ;
	var phplive_btn = <?php echo $btn ?> ;
	var phplive_stat_refer = encodeURIComponent( document.referrer.replace("http", "hphp") ) ;
	var phplive_stat_onpage = encodeURIComponent( location.toString().replace("http", "hphp") ) ;
	var phplive_stat_title = encodeURIComponent( document.title ) ;
	var win_width = screen.width ;
	var win_height = screen.height ;
	var phplive_initiate_widget = 0 
	var phplive_support_<?php echo $btn ?> ;
	var obj_div ;
	var obj_cover ;
	var obj_iframe ;

	var resolution = escape( win_width + " x " + win_height ) ;

	var phplive_quirks = 0 ;
	var phplive_IE ;
	//@cc_on phplive_IE = navigator.appVersion ;

	javascript:(
		function()
		{
			var mode=document.compatMode,m ;
			if( mode )
			{
				if ( ( mode == 'BackCompat' ) && phplive_IE )
					phplive_quirks = 1 ;
			}
		}
	)();

}

function init_doms()
{
	// jQuery lib load
	if ( ( typeof( window.jQuery ) == "undefined" ) && ( typeof( phplive_dom ) == "undefined" ) )
	{
		var script_jquery = document.createElement('script') ;
		script_jquery.type = "text/javascript" ; script_jquery.async = true ;
		script_jquery.onload = script_jquery.onreadystatechange = function () {
			phplive_dom = window.jQuery.noConflict() ;
		} ;
		script_jquery.src = "<?php echo $base_url ?>/js/framework.js" ;
		var script_jquery_s = document.getElementsByTagName('script')[0] ;
		script_jquery_s.parentNode.insertBefore(script_jquery, script_jquery_s) ;
	}
	else
		phplive_dom = window.jQuery ;
}

var phplive_pullimg_footprint_<?php echo $btn ?>, st_phplive_pullimg_<?php echo $btn ?>, phplive_thec_<?php echo $btn ?> = 0 ;
// below image also in function phplive_image_refresh_<?php echo $btn ?>()
var phplive_status_image_<?php echo $btn ?> = "<?php echo $base_url ?>/ajax/image.php?d=<?php echo $deptid ?>&r="+phplive_stat_refer+"&p="+phplive_stat_onpage+"&title="+phplive_stat_title+"&btn=<?php echo $btn ?>&resolution="+resolution+"&"+phplive_unique() ;
var phplive_request_url_<?php echo $btn ?> = "<?php echo $base_url ?>/phplive.php?d=<?php echo $deptid ?>&btn=<?php echo $btn ?>&onpage="+phplive_stat_onpage+"&title="+phplive_stat_title ;
var phplive_widget_<?php echo $btn ?> = "" ;

<?php if ( $text ): ?>
var phplive_image_or_text_<?php echo $btn ?> = "<?php echo $text ?>" ;
<?php else: ?>
var phplive_interval_<?php echo $btn ?> = setInterval(function(){ phplive_image_refresh_<?php echo $btn ?>() ; }, <?php echo ( $VARS_JS_ICON_CHECK * 1000 ) ?>) ;
var phplive_image_or_text_<?php echo $btn ?> = "<img src=\""+phplive_status_image_<?php echo $btn ?>+"\" border=0 name=\"phplive_image_or_text_image_<?php echo $btn ?>\" id=\"phplive_image_or_text_image_<?php echo $btn ?>\">" ;
<?php endif ; ?>

function phplive_image_refresh_<?php echo $btn ?>()
{
	<?php if ( $text || ( $CONF["icon_check"] == "off" ) ): ?>
	clearInterval( phplive_interval_<?php echo $btn ?> ) ;
	<?php else: ?>
	var chat_icon_<?php echo $btn ?> = "<?php echo $base_url ?>/ajax/image.php?d=<?php echo $deptid ?>&r="+phplive_stat_refer+"&p="+phplive_stat_onpage+"&title="+phplive_stat_title+"&btn=<?php echo $btn ?>&resolution="+resolution+"&"+phplive_unique() ;

	document.getElementById("phplive_image_or_text_image_<?php echo $btn ?>").src = chat_icon_<?php echo $btn ?> ;
	<?php endif ; ?>
}

function phplive_silent_close( phplive_theces, theisadmin, thetimer, theunique )
{
	alert( unescape( phplive_theces ) ) ;
}

function phplive_footprint_tracker_<?php echo $btn ?>()
{
	phplive_pullimg_footprint_<?php echo $btn ?> = new Image ;
	phplive_pullimg_footprint_<?php echo $btn ?>.onload = phplive_pullimg_actions_<?php echo $btn ?> ;
	phplive_pullimg_footprint_<?php echo $btn ?>.src = "<?php echo $base_url ?>/ajax/footprints.php?deptid=<?php echo $deptid ?>&r="+phplive_stat_refer+"&onpage="+phplive_stat_onpage+"&title="+phplive_stat_title+"&c="+phplive_thec_<?php echo $btn ?>+"&resolution="+resolution+"&"+phplive_unique() ;
}

function phplive_pullimg_actions_<?php echo $btn ?>()
{
	var thisflag = phplive_pullimg_footprint_<?php echo $btn ?>.width ;

	if ( ( thisflag == 1 ) || ( thisflag == 2 ) )
	{
		// if phplive_dom is not registered, wait for the next cycle as the loading was too fast
		if ( ( thisflag == 2 ) && !phplive_initiate_widget && ( typeof( phplive_dom ) != "undefined" ) )
		{
			phplive_dom( "body" ).append( phplive_widget_<?php echo $btn ?> ) ;

			phplive_initiate_widget = 1 ;
			obj_div = phplive_dom( "#phplive_widget_<?php echo $btn ?>" ) ;
			obj_div_cover = phplive_dom( "#phplive_widget_cover_<?php echo $btn ?>" ) ;
			obj_iframe = phplive_dom ( "#iframe_widget_<?php echo $btn ?>" ) ;

			obj_iframe.attr( 'src', "<?php echo $base_url ?>/widget.php?btn=<?php echo $btn ?>&"+phplive_unique() ) ;
			if ( typeof( obj_div_cover.center ) == "undefined" )
			{
				obj_div = phplive_dom( "#phplive_widget_<?php echo $btn ?>" ) ;
				obj_div_cover = phplive_dom( "#phplive_widget_cover_<?php echo $btn ?>" ) ;
				obj_iframe = phplive_dom ( "#iframe_widget_<?php echo $btn ?>" ) ;
			}

			obj_div_cover.show().animate({ left: <?php echo $phplive_initiate_left ?> }, 2000, function() { obj_div.fadeIn("fast") ; }) ;
		}
		else if ( ( thisflag == 1 ) && phplive_initiate_widget )
		{
			phplive_initiate_widget = 0 ;
			obj_div.fadeOut( "fast" ) ;
			obj_div_cover.fadeOut("fast") ;
		}
		else
		{
			// brute fix - will refine to dom load check in future build
			setTimeout(function(){ init_doms() }, 2000) ;
		}

		++phplive_thec_<?php echo $btn ?> ;
		st_phplive_pullimg_<?php echo $btn ?> = setTimeout(function(){ phplive_footprint_tracker_<?php echo $btn ?>() }, <?php echo $VARS_JS_FOOTPRINT ?> * 1000) ;
	}
	else if ( thisflag == 4 )
	{
		clearTimeout( st_phplive_pullimg_<?php echo $btn ?> ) ;
		clearInterval( phplive_interval_<?php echo $btn ?> ) ;
	}
}

function phplive_launch_chat_<?php echo $btn ?>(thewidget)
{
	var winname = phplive_unique() ;
	phplive_request_url_<?php echo $btn ?> = phplive_request_url_<?php echo $btn ?>+"&widget="+thewidget ;

	phplive_support_<?php echo $btn ?> = window.open( phplive_request_url_<?php echo $btn ?>, winname, 'scrollbars=no,resizable=yes,menubar=no,location=no,screenX=50,screenY=100,width=<?php echo $phplive_chat_width ?>,height=<?php echo $phplive_chat_height ?>' ) ;
}

function phplive_write_widget_<?php echo $btn ?>()
{
	var this_position = ( phplive_quirks ) ? "absolute" : "fixed" ;
	var this_widget_width = ( phplive_quirks ) ? 270 : 250 ;
	var this_widget_height = ( phplive_quirks ) ? 180 : 160 ;
	var this_widget_top = 190 ;

	// NOTE: we do not recommend modifying the width and height of the widget window as this will alter the
	// communication behavior based on mouse coordinates in function phplive_pullimg_actions_<?php echo $btn ?>()
	phplive_widget_<?php echo $btn ?> = "<map name='initiate_chat_cover'><area shape='rect' coords='222,2,247,26' href='JavaScript:void(0)' onClick='phplive_widget_decline_<?php echo $btn ?>()'><area shape='rect' coords='0,26,250,160' href='JavaScript:void(0)' onClick='phplive_widget_launch_<?php echo $btn ?>()'></map><div id='phplive_widget_<?php echo $btn ?>' style='display: none; position: "+this_position+"; top: "+this_widget_top+"px; left: <?php echo $phplive_initiate_left ?>px; background: url( <?php echo $base_url ?>/themes/initiate/bg_trans.png ) repeat; padding: 10px; width: "+this_widget_width+"px; height: "+this_widget_height+"px; -moz-border-radius: 5px; border-radius: 5px; z-Index: 10000;'><iframe id='iframe_widget_<?php echo $btn ?>' name='iframe_widget_<?php echo $btn ?>' style='display: none; width: 1px; height: 1px;' src='<?php echo $base_url ?>/blank.php' scrolling='no' border=0 frameborder=0 onLoad=''></iframe></div><div id='phplive_widget_cover_<?php echo $btn ?>' style='display: none; position: "+this_position+"; top: "+this_widget_top+"px; left: -800px; padding: 10px; z-Index: 10001;'><img src='<?php echo Util_Format_GetInitiate( $base_url, $deptid ) ; ?>' width='250' height='160' border=0 usemap='#initiate_chat_cover' style='-moz-border-radius: 5px; border-radius: 5px;'></div>" ;
}

function phplive_widget_launch_<?php echo $btn ?>()
{
	if ( phplive_initiate_widget )
	{
		var obj_div = phplive_dom( "#phplive_widget_<?php echo $btn ?>" ) ;
		var obj_div_cover = phplive_dom( "#phplive_widget_cover_<?php echo $btn ?>" ) ;
		var obj_iframe = phplive_dom( "#iframe_widget_<?php echo $btn ?>" ) ;

		obj_div.fadeOut( "fast" ) ;
		obj_div_cover.hide() ;
		phplive_launch_chat_<?php echo $btn ?>(1) ;
	}
}

function phplive_widget_decline_<?php echo $btn ?>()
{
	if ( phplive_initiate_widget )
	{
		var obj_div = phplive_dom( "#phplive_widget_<?php echo $btn ?>" ) ;
		var obj_div_cover = phplive_dom( "#phplive_widget_cover_<?php echo $btn ?>" ) ;
		var obj_iframe = phplive_dom ( "#iframe_widget_<?php echo $btn ?>" ) ;

		obj_div.fadeOut( "fast" ) ;
		obj_div_cover.hide() ;

		phplive_pullimg_widget_<?php echo $btn ?> = new Image ;
		phplive_pullimg_widget_<?php echo $btn ?>.onload = function() {
			//
		};
		phplive_pullimg_widget_<?php echo $btn ?>.src = "<?php echo $base_url ?>/ajax/chat_actions.php?action=disconnect&isop=0&widget=1&ip=<?php echo $ip ?>&"+phplive_unique() ;
		phplive_initiate_widget = 0 ;
	}
}

var phplive_status_image_write_<?php echo $btn ?> = "<span id=\"phplive_btn_<?php echo $btn ?>\" onClick=\"phplive_launch_chat_<?php echo $btn ?>(0)\" style=\"cursor: pointer;\"></span>" ;
document.write( phplive_status_image_write_<?php echo $btn ?> ) ;

(function() {
	if ( typeof( phplive_footprint_js ) == "undefined" )
	{
		phplive_footprint_js = 1 ;
		phplive_footprint_tracker_<?php echo $btn ?>() ;
	}

	document.getElementById("phplive_btn_<?php echo $btn ?>").innerHTML = phplive_image_or_text_<?php echo $btn ?> ;
	phplive_write_widget_<?php echo $btn ?>() ;
})();

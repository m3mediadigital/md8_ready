<style type="text/css">

            .link-remover-photo { position: relative; top: -121px; right: 30px }

            .gallery-image li { float: left; }
            .gallery-image li img:hover { opacity: 0.5; }

            .how-user { font: 14px Arial; }
            .how-user i { color: red; }

</style>
<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Albums'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Album.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Album.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Album.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Album.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		<td><h4><?php echo __('Id'); ?></h4></td>
		<td>
			<?php echo $album['Album']['id']; ?></td>
</tr><tr>		<td><h4><?php echo __('Title'); ?></h4></td>
		<td>
			<?php echo $album['Album']['title']; ?></td>
</tr><tr>		<td><h4><?php echo __('Image'); ?></h4></td>
		<td><?php if(empty($album['Album']['image'])){ echo $this->Html->image('/coreadmin/img/nopicture_list.jpg'); }else{ echo $this->Html->link($this->Html->image('uploads/album/admin_list/'.$album['Album']['image']), '/img/uploads/album/large/'.$album['Album']['image'], array('escape'=>false, 'class'=>'fancybox')); }  ?></td>
</tr><tr>		<td><h4><?php echo __('Active'); ?></h4></td>
		<td>
			<?php echo $album['Album']['active']; ?></td>
</tr><tr>		<td><h4><?php echo __('Modified'); ?></h4></td>
		<td>
			<?php echo $album['Album']['modified']; ?></td>
</tr><tr>		<td><h4><?php echo __('Created'); ?></h4></td>
		<td>
			<?php echo $album['Album']['created']; ?></td>
</tr>                </tbody>
            </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Album.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Album.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Album.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Album.id'))); ?>
            </li>
            </ul>
</div>
</br>
</br>
</br>
</br>
</br>
<p>&nbsp; <br /> &nbsp;</p>
    <div class="bloc">
        <div class="title">
            <?php echo __('Imagens atuais');?>
        </div>
        <div class="content">

            <ul class="gallery" id="droppable">
            <?php if ( $actualFotos ): ?>
                <?php foreach ( $actualFotos as $photo ): ?>
                    <li>
                        <?php echo $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $photo['Photo']['image'], array('w' => 200, 'h' => 150, 'zc' => 1)), '/uploads/' . $photo['Photo']['image'], array('escape' => false, 'class' => 'fancybox')); ?>
                    </li>
                <?php endforeach; ?>
            <?php else: ?>
                <center> <h3> Nenhua imagem cadastrada cadastrada! </h3> </center>                
            <?php endif ?>
        </ul>

        </div>
    </div>
   

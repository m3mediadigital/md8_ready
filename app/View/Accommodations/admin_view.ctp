<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Accommodations'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Accommodation.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Accommodation.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Accommodation.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Accommodation.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		<td><h4><?php echo __('Id'); ?></h4></td>
		<td>
			<?php echo $accommodation['Accommodation']['id']; ?></td>
</tr><tr>		<td><h4><?php echo __('Name'); ?></h4></td>
		<td>
			<?php echo $accommodation['Accommodation']['name']; ?></td>
</tr><tr>		<td><h4><?php echo __('Text'); ?></h4></td>
		<td>
			<?php echo $accommodation['Accommodation']['text']; ?></td>
</tr><tr>		<td><h4><?php echo __('Image'); ?></h4></td>
		<td><?php if(empty($accommodation['Accommodation']['image'])){ echo $this->Html->image('/coreadmin/img/nopicture_list.jpg'); }else{ echo $this->Html->link($this->Html->image('uploads/accommodation/admin_list/'.$accommodation['Accommodation']['image']), '/img/uploads/accommodation/large/'.$accommodation['Accommodation']['image'], array('escape'=>false, 'class'=>'fancybox')); }  ?></td>
</tr><tr>		<td><h4><?php echo __('Created'); ?></h4></td>
		<td>
			<?php echo $accommodation['Accommodation']['created']; ?></td>
</tr>                </tbody>
            </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Accommodation.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Accommodation.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Accommodation.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Accommodation.id'))); ?>
            </li>
            </ul>
</div>
</br>
</br>
</br>
</br>
</br>


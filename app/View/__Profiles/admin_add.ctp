<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Profiles'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title' => 'Voltar')), array('action' => 'index'), array('escape' => false)); ?>
            <?= $this->Html->link(__('Back'), array('action' => 'index')); ?>
        </li>
    </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('Profile', array('type' => 'file')); ?>
        <?= $this->Form->input('user_id'); ?>
        <?= $this->Form->input('name'); ?>
        <?= $this->Form->input('nickname'); ?>
        <?= $this->Form->input('title'); ?>
        <?= $this->Form->input('image', array('type' => 'file')); ?>
        <?= $this->Form->input('cpf'); ?>
        <?= $this->Form->input('rg'); ?>
        <?= $this->Form->input('birth'); ?>
        <?= $this->Form->input('email'); ?>
        <?= $this->Form->input('facebook'); ?>
        <?= $this->Form->input('twitter'); ?>
        <?= $this->Form->input('msn'); ?>
        <?= $this->Form->input('skype'); ?>
        <?= $this->Form->input('site'); ?>
        <?= $this->Form->input('description'); ?>
        <?= $this->Form->input('blood'); ?>
        <?= $this->Form->end(__('Submit', true)); ?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action' => 'index'), array('escape' => false)); ?>
            <?= $this->Html->link(__('Back'), array('action' => 'index')); ?>
        </li>
    </ul>
</div>
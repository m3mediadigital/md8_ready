<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Profiles'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Profile.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Profile.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Profile.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Profile.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		<td><h4><?php echo __('Id'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['id']; ?></td>
</tr><tr>		<td><h4><?php echo __('User'); ?></h4></td>
		<td>
			<?php echo $this->Html->link($profile['User']['id'], array('controller' => 'users', 'action' => 'view', $profile['User']['id'])); ?></td>
</tr><tr>		<td><h4><?php echo __('Name'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['name']; ?></td>
</tr><tr>		<td><h4><?php echo __('Nickname'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['nickname']; ?></td>
</tr><tr>		<td><h4><?php echo __('Title'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['title']; ?></td>
</tr><tr>		<td><h4><?php echo __('Image'); ?></h4></td>
		<td><?php if(empty($profile['Profile']['image'])){ echo $this->Html->image('/coreadmin/img/nopicture_list.jpg'); }else{ echo $this->Html->link($this->Html->image('uploads/profile/admin_list/'.$profile['Profile']['image']), '/img/uploads/profile/large/'.$profile['Profile']['image'], array('escape'=>false, 'class'=>'fancybox')); }  ?></td>
</tr><tr>		<td><h4><?php echo __('Cpf'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['cpf']; ?></td>
</tr><tr>		<td><h4><?php echo __('Rg'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['rg']; ?></td>
</tr><tr>		<td><h4><?php echo __('Birth'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['birth']; ?></td>
</tr><tr>		<td><h4><?php echo __('Email'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['email']; ?></td>
</tr><tr>		<td><h4><?php echo __('Facebook'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['facebook']; ?></td>
</tr><tr>		<td><h4><?php echo __('Twitter'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['twitter']; ?></td>
</tr><tr>		<td><h4><?php echo __('Msn'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['msn']; ?></td>
</tr><tr>		<td><h4><?php echo __('Skype'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['skype']; ?></td>
</tr><tr>		<td><h4><?php echo __('Site'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['site']; ?></td>
</tr><tr>		<td><h4><?php echo __('Description'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['description']; ?></td>
</tr><tr>		<td><h4><?php echo __('Blood'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['blood']; ?></td>
</tr><tr>		<td><h4><?php echo __('Created'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['created']; ?></td>
</tr><tr>		<td><h4><?php echo __('Modified'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['modified']; ?></td>
</tr>                </tbody>
            </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Profile.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Profile.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Profile.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Profile.id'))); ?>
            </li>
            </ul>
</div>
</br>
</br>
</br>
</br>
</br>

    <h1><?php echo $this->Html->image('admin/icons/posts.png') ?> <?php echo __('Related Addresses');?></h1>
    <div class="actionsList">
        <ul>
            		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Addresses', true), array('controller' => 'addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Address', true), array('controller' => 'addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Phones', true), array('controller' => 'phones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phone', true), array('controller' => 'phones', 'action' => 'add')); ?> </li>
        </ul>
    </div>
    <div class="bloc">
        <div class="title">
            <?php echo __('Related Addresses');?>        </div>
        <div class="content">
            <table>
                <thead>
                    <tr>
                                                    <th>id</th>
                                                    <th>phone</th>
                                                    <th>provider</th>
                                                    <th>main</th>
                                                    <th>created</th>
                                                    <th>modified</th>
                                                    <th>profile_id</th>
                                                <th class="actions"><?php __('Actions');?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach ($profile['Phone'] as $address):
                            $class = null;
                            if ($i++ % 2 == 0) {
                                    $class = ' class="altrow"';
                            }
                    ?>
		<td><?php echo $address['id']; ?>&nbsp;</td>
		<td><?php echo $address['phone']; ?>&nbsp;</td>
		<td><?php echo $address['provider']; ?>&nbsp;</td>
		<td><?php echo $address['main']; ?>&nbsp;</td>

                        <?php
                            echo '<td>'.$this->Time->format('d/m/Y H:i:s', $address['created']).'</td>';
                        ?>
                        <?php
                            echo '<td>'.$this->Time->format('d/m/Y H:i:s', $address['modified']).'</td>';
                        ?>		<td><?php echo $address['profile_id']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link($this->Html->image('admin/icons/edit.png', array('title' => 'Editar')), array('action' => 'edit', $address['id']), array('escape' => false)); ?>
			<?php echo $this->Html->link($this->Html->image('admin/icons/delete.png', array('title' => 'Remover')), array('action' => 'delete', $address['id']), array('escape' => false), sprintf(__('Are you sure you want to delete # %s?', true), $profile['Profile']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>


                </tbody>
            </table>
            <div class="left input">
                <select name="data[Profile][action]" id="tableaction">
                    <option value="">Action</option>
                    <option value="ativardesativar">Ativar / Desativar</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actionsList">
        <ul>
            		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Addresses', true), array('controller' => 'addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Address', true), array('controller' => 'addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Phones', true), array('controller' => 'phones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phone', true), array('controller' => 'phones', 'action' => 'add')); ?> </li>
        </ul>
    </div>
    <h1><?php echo $this->Html->image('admin/icons/posts.png') ?> <?php echo __('Related Phones');?></h1>
    <div class="actionsList">
        <ul>
            		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Addresses', true), array('controller' => 'addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Address', true), array('controller' => 'addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Phones', true), array('controller' => 'phones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phone', true), array('controller' => 'phones', 'action' => 'add')); ?> </li>
        </ul>
    </div>
    <div class="bloc">
        <div class="title">
            <?php echo __('Related Phones');?>        </div>
        <div class="content">
            <table>
                <thead>
                    <tr>
                                                    <th>id</th>
                                                    <th>phone</th>
                                                    <th>provider</th>
                                                    <th>main</th>
                                                    <th>created</th>
                                                    <th>modified</th>
                                                    <th>profile_id</th>
                                                <th class="actions"><?php __('Actions');?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach ($profile['Phone'] as $phone):
                            $class = null;
                            if ($i++ % 2 == 0) {
                                    $class = ' class="altrow"';
                            }
                    ?>
		<td><?php echo $phone['id']; ?>&nbsp;</td>
		<td><?php echo $phone['phone']; ?>&nbsp;</td>
		<td><?php echo $phone['provider']; ?>&nbsp;</td>
		<td><?php echo $phone['main']; ?>&nbsp;</td>

                        <?php
                            echo '<td>'.$this->Time->format('d/m/Y H:i:s', $phone['created']).'</td>';
                        ?>
                        <?php
                            echo '<td>'.$this->Time->format('d/m/Y H:i:s', $phone['modified']).'</td>';
                        ?>		<td><?php echo $phone['profile_id']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link($this->Html->image('admin/icons/edit.png', array('title' => 'Editar')), array('action' => 'edit', $phone['id']), array('escape' => false)); ?>
			<?php echo $this->Html->link($this->Html->image('admin/icons/delete.png', array('title' => 'Remover')), array('action' => 'delete', $phone['id']), array('escape' => false), sprintf(__('Are you sure you want to delete # %s?', true), $profile['Profile']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>


                </tbody>
            </table>
            <div class="left input">
                <select name="data[Profile][action]" id="tableaction">
                    <option value="">Action</option>
                    <option value="ativardesativar">Ativar / Desativar</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actionsList">
        <ul>
            		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Addresses', true), array('controller' => 'addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Address', true), array('controller' => 'addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Phones', true), array('controller' => 'phones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Phone', true), array('controller' => 'phones', 'action' => 'add')); ?> </li>
        </ul>
    </div>

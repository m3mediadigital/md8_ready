<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Profiles'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title' => 'Adicionar')), array('action' => 'add'), array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Adicionar'), array('action' => 'add')); ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <table>
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" class="checkall"/>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('id'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('user_id'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('name'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('nickname'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('title'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('image'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('cpf'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('rg'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('birth'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('email'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('facebook'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('twitter'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('msn'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('skype'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('site'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('description'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('blood'); ?>
                    </th>



                    <th class="actions">
                        Ações
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($profiles as $profile):
                    $class = null;
                    if ($i++ % 2 == 0) {
                        $class = ' class="altrow"';
                    }
                    ?>
                    <tr>
                        <td>
                            <?= $this->Form->input('Profile.id.' . $profile['Profile']['id'], array('value' => $profile['Profile']['id'], 'type' => 'checkbox', 'label' => false, 'div' => false)) ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['id'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['User']['id'], array('controller' => 'users', 'action' => 'view', $profile['User']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['name'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['nickname'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['title'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <?php if (!empty($profile['Profile']['image'])): ?>
                            <td>
                                <?= $this->Html->link($this->Html->image('uploads/setting/admin_list/' . $profile['Profile']['image']), '/img/uploads/setting/large/' . $profile['Profile']['image'], array('escape' => false, 'class' => 'fancybox')) . '</td>'; ?>
                            </td>
                        <?php else: ?>
                            <td>
                                <?= $this->Html->image('/coreadmin/img/nopicture_list.jpg') ?>
                            </td>
                        <?php endif; ?>
                        <td>
                            <?= $this->Html->link($profile['Profile']['cpf'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['rg'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['birth'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['email'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['facebook'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['twitter'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['msn'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['skype'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['site'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['description'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($profile['Profile']['blood'], array('action' => 'edit', $profile['Profile']['id'])); ?>
                        </td>

                        <td class="actions">
                            <ul>
                                <li>
                                    <?php echo $this->Html->link('<button class="bt-ver">Ver</button>', array('action' => 'view', $profile['Profile']['id']), array('escape' => false)); ?>
                                </li>
                                <li>
                                    <?php echo $this->Html->link('<button class="bt-editar">Editar</button>', array('action' => 'edit', $profile['Profile']['id']), array('escape' => false)); ?>
                                </li>
                                <li>
                                    <?php echo $this->Form->postLink('<button class="bt-deletar">Remover</button>', array('action' => 'delete', $profile['Profile']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $profile['Profile']['id'])); ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}'))); ?></p>
        <?php if (count($profiles) > 0): ?>        <div class="acoesMultiplas">
                <button class="bt-deletar">Remover selecionados</button>
                <button class="bt-ativar">Ativar selecionados</button>
                <button class="bt-desativar">Desativar selecionados</button>
            </div>
        <?php endif; ?>        <div class="pagination">
        <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape' => false), null, array('class' => 'prev disabled')); ?>
        <?php echo $this->Paginator->numbers(array('separator' => ' ')); ?>
        <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape' => false), null, array('class' => 'next disabled')); ?>
        </div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title' => 'Adicionar')), array('action' => 'add'), array('escape' => false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action' => 'add')); ?>
        </li>
    </ul>
</div>
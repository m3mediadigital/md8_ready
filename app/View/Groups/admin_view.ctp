<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Groups'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Group.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Group.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Group.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Group.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		<td><h4><?php echo __('Id'); ?></h4></td>
		<td>
			<?php echo $group['Group']['id']; ?></td>
</tr><tr>		<td><h4><?php echo __('Name'); ?></h4></td>
		<td>
			<?php echo $group['Group']['name']; ?></td>
</tr><tr>		<td><h4><?php echo __('Rules'); ?></h4></td>
		<td>
			<?php echo $group['Group']['rules']; ?></td>
</tr><tr>		<td><h4><?php echo __('Active'); ?></h4></td>
		<td>
			<?php echo $group['Group']['active']; ?></td>
</tr><tr>		<td><h4><?php echo __('Login Redirect'); ?></h4></td>
		<td>
			<?php echo $group['Group']['login_redirect']; ?></td>
</tr><tr>		<td><h4><?php echo __('Logout Redirect'); ?></h4></td>
		<td>
			<?php echo $group['Group']['logout_redirect']; ?></td>
</tr><tr>		<td><h4><?php echo __('Created'); ?></h4></td>
		<td>
			<?php echo $group['Group']['created']; ?></td>
</tr><tr>		<td><h4><?php echo __('Modified'); ?></h4></td>
		<td>
			<?php echo $group['Group']['modified']; ?></td>
</tr>                </tbody>
            </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Group.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Group.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Group.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Group.id'))); ?>
            </li>
            </ul>
</div>
</br>
</br>
</br>
</br>
</br>

    <h1><?php echo $this->Html->image('admin/icons/posts.png') ?> <?php echo __('Related Users');?></h1>
    <div class="actionsList">
        <ul>
            		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
        </ul>
    </div>
    <div class="bloc">
        <div class="title">
            <?php echo __('Related Users');?>        </div>
        <div class="content">
            <table>
                <thead>
                    <tr>
                                                    <th>id</th>
                                                    <th>login</th>
                                                    <th>password</th>
                                                    <th>active</th>
                                                    <th>last_login</th>
                                                    <th>password_recovery</th>
                                                    <th>created</th>
                                                    <th>modified</th>
                                                <th class="actions"><?php __('Actions');?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach ($group['User'] as $user):
                            $class = null;
                            if ($i++ % 2 == 0) {
                                    $class = ' class="altrow"';
                            }
                    ?>
		<td><?php echo $user['id']; ?>&nbsp;</td>
		<td><?php echo $user['login']; ?>&nbsp;</td>
		<td><?php echo $user['password']; ?>&nbsp;</td>

                        <?php
                            if($user['active'] == 1){
                                echo '<td>'.$this->Html->image('admin/icons/ativo.png', array('title' => 'Ativo', 'class'=>'ListImage')).'</td>';
                            }else{
                                echo '<td>'.$this->Html->image('admin/icons/inativo.png', array('title' => 'Desativado', 'class'=>'ListImage')).'</td>';
                            }
                        ?>		<td><?php echo $user['last_login']; ?>&nbsp;</td>
		<td><?php echo $user['password_recovery']; ?>&nbsp;</td>

                        <?php
                            echo '<td>'.$this->Time->format('d/m/Y H:i:s', $user['created']).'</td>';
                        ?>
                        <?php
                            echo '<td>'.$this->Time->format('d/m/Y H:i:s', $user['modified']).'</td>';
                        ?>		<td class="actions">
			<?php echo $this->Html->link($this->Html->image('admin/icons/edit.png', array('title' => 'Editar')), array('action' => 'edit', $user['id']), array('escape' => false)); ?>
			<?php echo $this->Html->link($this->Html->image('admin/icons/delete.png', array('title' => 'Remover')), array('action' => 'delete', $user['id']), array('escape' => false), sprintf(__('Are you sure you want to delete # %s?', true), $group['Group']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>


                </tbody>
            </table>
            <div class="left input">
                <select name="data[Group][action]" id="tableaction">
                    <option value="">Action</option>
                    <option value="ativardesativar">Ativar / Desativar</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actionsList">
        <ul>
            		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
        </ul>
    </div>

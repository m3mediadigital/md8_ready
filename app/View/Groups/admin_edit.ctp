<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Groups'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title' => 'Voltar')), array('action' => 'index'), array('escape' => false)); ?>
            <?= $this->Html->link(__('Back'), array('action' => 'index')); ?>
        </li>
        <li>
            <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title' => 'Remover')), array('action' => 'delete', $this->Form->value('Group.id')), array('escape' => false), __('Are you sure you want to delete # %s?', $this->Form->value('Group.id'))); ?>
            <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Group.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Group.id'))); ?>
        </li>
    </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('Group', array('type' => 'file')); ?>
        <?= $this->Form->input('id'); ?>
        <?= $this->Form->submit('Enviar'); ?><br/>
        <?= $this->Form->input('name'); ?>
        <?= $this->Form->input('active'); ?>
        <?= $this->Form->input('login_redirect'); ?>
        <?= $this->Form->input('logout_redirect'); ?>
        <div class="rules input">
            <label for="rules">Regras</label>
            <?php $regras = json_decode($this->request->data['Group']['rules']); ?>
            <?php
            if (!empty($regras)) {
                $rules = array();
                foreach ($regras as $contr => $acts) {
                    foreach ($acts as $act) {
                        $rules[] = $contr . ':' . $act;
                    }
                }
            }
            ?>
            <?php foreach ($getControllerActions as $key => $value): ?>
                <div style="clear: both; margin-top: 0;">
                    <div style="clear: both; font-weight: bold; margin-right: 15px;">
                        <?= $this->Form->input('checkAll', array('type' => 'checkbox', 'label' => false, 'div' => false, 'value' => '', 'class' => 'none', 'hiddenField' => false, 'name' => 'checkAll')) ?><?= $key ?>:
                    </div>
                    <?php foreach ($value as $metodo): ?>
                        <div>
                            <?php if (isset($rules) && in_array($key . ':' . $metodo, $rules)): ?>
                                <?= $this->Form->input('rules', array('checked' => 'checked', 'type' => 'checkbox', 'label' => false, 'div' => false, 'value' => $key . ':' . $metodo, 'class' => 'none', 'hiddenField' => false, 'name' => 'data[Group][rules][]')) . ' ' . $metodo ?>
                            <?php else: ?>
                                <?= $this->Form->input('rules', array('type' => 'checkbox', 'label' => false, 'div' => false, 'value' => $key . ':' . $metodo, 'class' => 'none', 'hiddenField' => false, 'name' => 'data[Group][rules][]')) . ' ' . $metodo ?>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?= $this->Form->end(__('Submit', true)); ?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action' => 'index'), array('escape' => false)); ?>
            <?= $this->Html->link(__('Back'), array('action' => 'index')); ?>
        </li>
        <li>
            <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('Group.id')), array('escape' => false), __('Are you sure you want to delete # %s?', $this->Form->value('Group.id'))); ?>
            <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Group.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Group.id'))); ?>
        </li>
    </ul>
</div>
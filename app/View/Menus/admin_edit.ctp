<?php //pr($this->data['Menu'])  ?>
<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Menus'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action' => 'index'), array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action' => 'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('Menu.id')), array('escape' => false), __('Are you sure you want to delete # %s?', $this->Form->value('Menu.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Menu.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Menu.id'))); ?>
        </li>
    </ul>
</div>
<div class="bloc menus clear">
    <div class="content">
        <?php echo $this->Form->create('Menu', array('type' => 'file')); ?>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('title');
        echo $this->Form->input('image', array('type' => 'file'));
        ?>
        <?php if (empty($this->data['Menu']['image'])): ?>
        <?php else: ?>
        <?php endif; ?>
        <div class='input'>
            <label>Foto Atual</label>
            <div class='input'>
                <?= $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $this->data['Menu']['image'], array('w' => 300, 'h' => 250, 'zc' => 1)), $this->PhpThumb->url('uploads/' . $this->data['Menu']['image'], array('w' => 640, 'h' => 480, 'zc' => 1)), array('escape' => false, 'class' => 'fancybox')) ?>
            </div>
        </div>
        <?php echo $this->Form->input('active'); ?>
        <?php echo $this->Form->end(__('Submit', true)); ?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action' => 'index'), array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action' => 'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('Menu.id')), array('escape' => false), __('Are you sure you want to delete # %s?', $this->Form->value('Menu.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Menu.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Menu.id'))); ?>
        </li>
    </ul>
</div>
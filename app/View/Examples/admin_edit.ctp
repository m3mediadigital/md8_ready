<?php //pr($this->data['Example'])  ?>
<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Examples'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action' => 'index'), array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action' => 'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('Example.id')), array('escape' => false), __('Are you sure you want to delete # %s?', $this->Form->value('Example.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Example.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Example.id'))); ?>
        </li>
    </ul>
</div>
<div class="bloc examples clear">
    <div class="content">
        <?php echo $this->Form->create('Example', array('type' => 'file')); ?>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('title');
        echo $this->Form->input('image', array('type' => 'file'));
        ?>
        
        <?php echo $this->Form->end(__('Submit', true)); ?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action' => 'index'), array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action' => 'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('Example.id')), array('escape' => false), __('Are you sure you want to delete # %s?', $this->Form->value('Example.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Example.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Example.id'))); ?>
        </li>
    </ul>
</div>
<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Subcategories'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Adicionar'), array('action'=>'add')); ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <?php echo $this->Form->create('Subcategory', array('url' => array('action' => 'allActionsListData'))); ?>
        <table>
            <thead>
                <tr>
                    <th><input type="checkbox" class="checkall"/></th>
                    		<th><?php echo $this->Paginator->sort('id');?></th>
                                        		<th><?php echo $this->Paginator->sort('subcategory');?></th>
                                        		<th><?php echo $this->Paginator->sort('category_id');?></th>
                                        <th class="actions">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                    foreach ($subcategories as $subcategory):
                        $class = null;
                        if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                        }
                ?>
                <tr>
                    <td>
                        <?= $this->Form->input('Subcategory.id.'.$subcategory['Subcategory']['id'],array('value'=>$subcategory['Subcategory']['id'] ,'type'=>'checkbox','label'=>false, 'div'=>false))?>
                    </td>
		<td>
			<?= $this->Html->link($subcategory['Subcategory']['id'], array('action'=>'edit', $subcategory['Subcategory']['id'])); ?>
		</td>
		<td>
			<?= $this->Html->link($subcategory['Subcategory']['subcategory'], array('action'=>'edit', $subcategory['Subcategory']['id'])); ?>
		</td>
	<td>
		<?= $this->Html->link($subcategory['Category']['category'], array('controller' => 'categories', 'action' => 'view', $subcategory['Category']['id'])); ?>
	</td>

                    <td class="actions">
                        <ul>
      <?php if(!empty($addImages)): ?>
                            <li>
                                <?php echo $this->Html->link('Anexar Imagens', array('controller' => 'photos', 'action' => 'anexar', $subcategory['Subcategory']['id'], '?' => array('tabela' => 'subcategories')), array('escape' => false, 'class' => 'bt-anexar')); ?>
                            </li>
                            <?php endif; ?>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-03.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'view', $subcategory['Subcategory']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-04.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'edit', $subcategory['Subcategory']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-05.jpg', array('style' => 'width:29px;margin:0;padding:0;')), '#', array('escape' => false, 'class' => 'deleteButtonRow', 'rel' => $subcategory['Subcategory']['id'])); ?>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}')));?></p>
        <?php if(count($subcategories)>0): ?>        <div class="acoesMultiplas">
            <button type="submit" name="data[Subcategory][action]" value="delete" class="bt-deletar">Remover selecionados</button>

            <button name="data[Subcategory][action]" value="activate" class="bt-ativar">Ativar selecionados</button>
            <button name="data[Subcategory][action]" value="deactivate" class="bt-desativar">Desativar selecionados</button>
        </div>
        <?php endif; ?>        <div class="pagination">
            <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape'=>false), null, array('class' => 'prev disabled'));?>
            <?php echo $this->Paginator->numbers(array('separator' => ' '));?>
            <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape'=>false), null, array('class' => 'next disabled'));?>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?><div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action'=>'add')); ?>
        </li>
    </ul>
</div>
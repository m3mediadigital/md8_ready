<h1><?= $this->Html->image('/coreadmin/img/icons/chart.png') ?> Analitycs</h1>
<div class="bloc left">
    <div class="content">
        <div class="left">
            <table class="noalt">
                <thead>
                    <tr>
                        <th colspan="2" style="padding: 10px 0px 15px 0px;"><em>Mês de <?= $analytics['anterior']['mes'] ?></em></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><h4><?= $analytics['anterior']['visits']; ?></h4></td>
                        <td>Visitas</td>
                    </tr>
                    <tr>
                        <td><h4><?= round($analytics['anterior']['percentNewVisits'], 2); ?></h4>% </td>
                        <td>Novas</td>
                    </tr>
                    <tr>
                        <td><h4><?= $analytics['anterior']['visitors']; ?></h4></td>
                        <td>Exclusivos</td>
                    </tr>

                    <tr>
                        <td><h4><?= $analytics['anterior']['pageviews']; ?></h4></td>
                        <td>Pageviews</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="right">
            <table class="noalt">
                <thead>
                    <tr>
                        <th colspan="2" style="padding: 10px 0px 15px 0px;"><em>&nbsp;</em></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><h4><?= round($analytics['anterior']['pageviewsPerVisit'], 2); ?></h4></td>
                        <td>Pág/visita</td>
                    </tr>
                    <tr>
                        <td><h4><?= round($analytics['anterior']['entranceBounceRate'], 2); ?></h4>% </td>
                        <td>Rejeições</td>
                    </tr>
                    <tr>
                        <td><h4><?= round(($analytics['anterior']['avgTimeOnSite'] / 60), 2); ?></h4>min </td>
                        <td>Tempo médio</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div class="bloc right">
    <div class="content">
        <div class="left">
            <table class="noalt">
                <thead>
                    <tr>
                        <th colspan="2" style="padding: 10px 0px 15px 0px;"><em>Mês de <?= $analytics['atual']['mes'] ?></em></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><h4><?= $analytics['atual']['visits']; ?></h4></td>
                        <td>Visitas</td>
                    </tr>
                    <tr>

                        <td><h4><?= round($analytics['atual']['percentNewVisits'], 2); ?></h4>% </td>
                        <td>Novas</td>
                    </tr>
                    <tr>
                        <td><h4><?= $analytics['atual']['visitors']; ?></h4></td>
                        <td>Exclusivos</td>
                    </tr>

                    <tr>
                        <td><h4><?= $analytics['atual']['pageviews']; ?></h4></td>
                        <td>Pageviews</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="right">
            <table class="noalt">
                <thead>
                    <tr>
                        <th colspan="2" style="padding: 10px 0px 15px 0px;"><em>&nbsp;</em></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><h4><?= round($analytics['atual']['pageviewsPerVisit'], 2); ?></h4></td>
                        <td>Pág/visita</td>
                    </tr>
                    <tr>
                        <td><h4><?= round($analytics['atual']['entranceBounceRate'], 2); ?></h4>% </td>
                        <td>Rejeições</td>
                    </tr>

                    <tr>
                        <td><h4><?= round(($analytics['atual']['avgTimeOnSite'] / 60), 2); ?></h4>min </td>
                        <td>Tempo médio</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div class="cb"></div>

<div class="bloc left">
    <div class="content">
        <div id="graf_area"></div>
        <div class="cb"></div>
    </div>
</div>
<div class="bloc right">
    <div class="content">
        <div id="graf_mobile"></div>
        <div class="cb"></div>
    </div>
</div>

<div class="cb"></div>

<div class="bloc">
<div class="title">
        Acesso mundial do mês anterior
</div>
    <div class="content world">
        <div id="map_world"></div>
    </div>
</div>
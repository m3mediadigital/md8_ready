<?php //pre( $this->request->data) ?>
<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Talks'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                  
    </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('Talk', array('type'=>'file'));?>
        	<?= $this->Form->input('id');?>
	<?= $this->Form->input('transaction_id', array( 'type' => 'hidden' ));?>
	<?= $this->Form->input('name', array( 'type' => 'hidden' ));?>
	<?= $this->Form->input('talk');?>
        <?= $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), '<script type="text/javascript"> window.location.back() </script>', array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array( 'controller' => 'transactions', 'action' => 'index' ) ); ?>
        </li>
                   
    </ul>
</div>

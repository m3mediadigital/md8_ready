<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Submenus'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
            </ul>
</div>
<div class="bloc submenus clear">
    <div class="content">
<!--        <p><i>Os arquivos do submenu (Controller, Model e View) serão gerados automáticamente.</br>
        No caso das views ainda é necessário ajustar.</i></p>-->
        <?php echo $this->Form->create('Submenu', array('type'=>'file'));?>
            <?php
		echo $this->Form->input('menu_id');
		echo $this->Form->input('label');
		echo $this->Form->input('controller');
		echo $this->Form->input('action');
		echo $this->Form->input('plugin');
//		echo $this->Form->input('file', array('type' => 'checkbox', 'label' => 'O Submenu possuirá imagem?'));
            ?>
<!--            <div class="input text">
                <i style="color:red;">Separe as colunas por linha</i>
            </div>-->
            <?php
//                echo $this->Form->input('columns', array('type' => 'textarea', 'class' => 'None'));
            ?>
        <?php echo $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
        </ul>
</div>
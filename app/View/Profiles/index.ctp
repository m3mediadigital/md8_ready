
<?php echo $this->element('cliente_admin_site_base') ?>

<style type="text/css">

	table tr td{
		text-align: center;
		padding: 50px 0px 50px 0px;
	}
	table tr td.adjustLinks{
		text-align: center;
		padding: 0px;
		width: 10%;
	}

</style>

<div class="body">
	<div id="internas">

		<h1>Área do cliente</h1>

        <div class="bloc clear">
            <div class="content">


			    <table>
			    	<tr>
			    		<td class="adjustLinks" >&nbsp;</td>
			    		<td>

					    	<?= $this->Html->link(
					    			$this->Html->image('transacoes.png', array('title' => 'Transações')).'<br /><h4>Transações</h4>', 
					    			array(
					    					'controller' => 'transactions', 
					    					'action' => 'index', 
					    					'plugin' => false, 
					    					'admin' => false ),
					    			array('escape' => false)
					    	) ?>

			    		</td>
			    		<td>

					    	<?= $this->Html->link(
					    			$this->Html->image('meus-dados.png', array('title' => 'Meus Dados')).'<br /><h4>Meus Dados</h4>', 
					    			array('controller' => 'users', 
					    					'action' => 'index', 
					    					'plugin' => false, 
					    					'admin' => false ), 
					    			array('escape' => false)
					    	) ?>

			    		</td>
			    		<td class="adjustLinks" >&nbsp;</td>
			    	</tr>
			    </table>


			</div>
		</div>
		    
	</div>
</div>
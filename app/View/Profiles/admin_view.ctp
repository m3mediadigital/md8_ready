<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Profiles'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Profile.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Profile.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Profile.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Profile.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		<td><h4><?php echo __('Id'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['id']; ?></td>
</tr><tr>		<td><h4><?php echo __('User'); ?></h4></td>
		<td>
			<?php echo $this->Html->link($profile['User']['name'], array('controller' => 'users', 'action' => 'view', $profile['User']['id'])); ?></td>
</tr><tr>		<td><h4><?php echo __('Full Name'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['full_name']; ?></td>
</tr><tr>		<td><h4><?php echo __('Corporate Name'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['corporate_name']; ?></td>
</tr><tr>		<td><h4><?php echo __('Cnpj'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['cnpj']; ?></td>
</tr><tr>		<td><h4><?php echo __('Cpf'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['cpf']; ?></td>
</tr><tr>		<td><h4><?php echo __('Full Address'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['full_address']; ?></td>
</tr><tr>		<td><h4><?php echo __('Cep'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['cep']; ?></td>
</tr><tr>		<td><h4><?php echo __('City'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['city']; ?></td>
</tr><tr>		<td><h4><?php echo __('Uf'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['uf']; ?></td>
</tr><tr>		<td><h4><?php echo __('Primary Phone'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['primary_phone']; ?></td>
</tr><tr>		<td><h4><?php echo __('Secondary Phone'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['secondary_phone']; ?></td>
</tr><tr>		<td><h4><?php echo __('Fax'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['fax']; ?></td>
</tr><tr>		<td><h4><?php echo __('Email'); ?></h4></td>
		<td>
			<?php echo $profile['Profile']['email']; ?></td>
</tr>                </tbody>
            </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Profile.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Profile.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Profile.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Profile.id'))); ?>
            </li>
            </ul>
</div>
</br>
</br>
</br>
</br>
</br>


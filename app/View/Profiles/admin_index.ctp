<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Profiles'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Adicionar'), array('action'=>'add')); ?>
        </li>
    </ul>
</div>
<style type="text/css">
    .adjustSearch{
        height: 20px;
    }
</style>
<div style="margin-top: 58px; margin-left: 35px; float: right; width: 330px;">
    <form>
        <input type="text" name="q" style="width: 250px;" class="adjustSearch">
        <input type="submit" value="Buscar" class="adjustSearch">
    </form>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <?php echo $this->Form->create('Profile', array('url' => array('action' => 'allActionsListData'))); ?>
        <table>
            <thead>
                <tr>
                    <th><input type="checkbox" class="checkall"/></th>
                    		<th><?php echo $this->Paginator->sort('id');?></th>
                                        		<th><?php echo $this->Paginator->sort('full_name');?></th>
                                        		<th><?php echo $this->Paginator->sort('corporate_name');?></th>
                                        		<th><?php echo $this->Paginator->sort('cnpj');?></th>
                                        		<th><?php echo $this->Paginator->sort('cpf');?></th>
                                        		<th><?php echo $this->Paginator->sort('primary_phone');?></th>
                                        		<!-- th><?php //echo $this->Paginator->sort('email');?></th -->
                                        <th class="actions">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0; echo count( $profiles );
                    foreach ($profiles as $profile):
                        $class = null;
                        if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                        }
                ?>
                <tr>
                    <td>
                        <?= $this->Form->input('Profile.id.'.$profile['Profile']['id'],array('value'=>$profile['Profile']['id'] ,'type'=>'checkbox','label'=>false, 'div'=>false))?>
                    </td>
		<td>
			<?= $this->Html->link($profile['Profile']['id'], array('action'=>'edit', $profile['Profile']['id'])); ?>
		</td>
		<td>
			<?= $this->Html->link($profile['Profile']['full_name'], array('action'=>'edit', $profile['Profile']['id'])); ?>
		</td>
		<td>
			<?= $this->Html->link($profile['Profile']['corporate_name'], array('action'=>'edit', $profile['Profile']['id'])); ?>
		</td>
		<td>
			<?= $this->Html->link($profile['Profile']['cnpj'], array('action'=>'edit', $profile['Profile']['id'])); ?>
		</td>
		<td>
			<?= $this->Html->link($profile['Profile']['cpf'], array('action'=>'edit', $profile['Profile']['id'])); ?>
		</td>
		<td>
			<?= $this->Html->link($profile['Profile']['primary_phone'], array('action'=>'edit', $profile['Profile']['id'])); ?>
		</td>
		<!-- td>
			<?php //echo $this->Html->link($profile['Profile']['email'], array('action'=>'edit', $profile['Profile']['id'])); ?>
		</td -->

                    <td class="actions">
                        <ul>
      <?php if(!empty($addImages)): ?>
                            <li>
                                <?php echo $this->Html->link('Anexar Imagens', array('controller' => 'photos', 'action' => 'anexar', $profile['Profile']['id'], '?' => array('tabela' => 'profiles')), array('escape' => false, 'class' => 'bt-anexar')); ?>
                            </li>
                            <?php endif; ?>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-03.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'view', $profile['Profile']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-04.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'edit', $profile['Profile']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-05.jpg', array('style' => 'width:29px;margin:0;padding:0;')), '#', array('escape' => false, 'class' => 'deleteButtonRow', 'rel' => $profile['Profile']['id'])); ?>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}')));?></p>
        <?php if(count($profiles)>0): ?>        <div class="acoesMultiplas">
            <button type="submit" name="data[Profile][action]" value="delete" class="bt-deletar">Remover selecionados</button>

            <button name="data[Profile][action]" value="activate" class="bt-ativar">Ativar selecionados</button>
            <button name="data[Profile][action]" value="deactivate" class="bt-desativar">Desativar selecionados</button>
        </div>
        <?php endif; ?>        <div class="pagination">
            <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape'=>false), null, array('class' => 'prev disabled'));?>
            <?php echo $this->Paginator->numbers(array('separator' => ' '));?>
            <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape'=>false), null, array('class' => 'next disabled'));?>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?><div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action'=>'add')); ?>
        </li>
    </ul>
</div>
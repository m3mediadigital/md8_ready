<?php
ob_start();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
        <link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'/>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array('reset', '/coreadmin/css/jqueryUi', '/coreadmin/css/chosen', '/coreadmin/css/colorpicker', '/coreadmin/css/fancybox', '/coreadmin/css/fileuploadUi', '/coreadmin/css/estilo'));
        echo $this->Html->script(array('jquery', 'jqueryUi', 'https://www.google.com/jsapi', '/coreadmin/js/colorpicker', '/coreadmin/js/money', '/coreadmin/ckeditor/ckeditor', '/coreadmin/js/easing', '/coreadmin/js/mousewheel', '/coreadmin/js/fancybox', '/coreadmin/js/chosen', '/coreadmin/js/excanvas', '/coreadmin/js/iphone-checkboxes', '/coreadmin/js/tooltipsy', '/coreadmin/js/uniform', '/coreadmin/js/visualize', '/coreadmin/js/cookie/cookie', '/coreadmin/js/iframe-transport', '/coreadmin/js/fileupload', '/coreadmin/js/tmpl', '/coreadmin/js/loadimage', '/coreadmin/js/fileuploadUi', '/coreadmin/js/application', '/coreadmin/js/actions'));
        echo $scripts_for_layout;
        ?>
    </head>
    <body>
        <div id="content" class="login">
            <?php echo $content_for_layout; ?>
        </div>
        <?php //echo $this->element('sql_dump'); ?>
    </body>
</html>
<?php
ob_end_flush();
?>
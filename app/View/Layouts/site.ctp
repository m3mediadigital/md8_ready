﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>
		<meta name="keywords" content="md8, representações, embalagens, plasticas, papeis, filme stretch, stress, fitas adesivas, fitas de arquear, etiqueta, ribbon, filme, seguimentos, filme tecnico, filme para empacotamento, sacola impressa, saco impresso, saco litrografado, industrial, fechamento de caixa, sacolas, personalizada, natal, rn, rio grande do norte, paraiba, joao pessoa" />
		<meta name="description" content="MD8 - Embalagens Industriais em Natal/RN" />
		<meta name="URL" content="http://www.md8.com.br" /> 
		<meta name="revisit-after" content="5 days" />
		<meta name="reply-to" content="atendimento@m3mediadigital.com.br" / >
		<meta name="language" content="portuguese" />
		<meta name="document-rights" content="Public" / > <!-- define o documento como p�blico, de acesso a todos --> 
		<meta name="document-rating" content="General" / > <!-- informa a classifica��o geral do conte�do para as idades, sendo geral= todas as iaddes --> 
		<meta name="document-state" content="Dynamic" / ><!-- informa se o conte�do � est�tico ou din�mico --> 
		<meta http-equiv="Content-Language" content="PT-BR" / ><!-- define a linguagem predominante da p�gina --> 
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array('estilos', 'font-awesome.min', 'font-awesome-ie7.min', 'prettyPhoto', 'jqueryUi'));
        echo $this->Html->script(array('jquery', 'lazyload', 'cycle', 'jqueryUi', 'prettyPhoto', 'jquery.maskedinput-1.3.min', 'actions'));
        echo $scripts_for_layout;
        ?>
        <!-- FAVICON -->
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
        <div id="fb-root"></div>
		<script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=162396560593052";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    </head>

    <body id="<?php echo $page ?>">
<div id="container">
<?php echo $this->Session->flash(); ?>
    <div id="header-out"><div id="header-in">
        
        <?php echo $this->Html->link('Logo - MD8', array('controller' => 'pages', 'action' => 'index'), array('escape' => false, 'id' => 'logo')) ?>

        <div class="dadosCliente">
            <?php 
                $logged = $this->Session->read( 'Logged' );
                if ( empty( $logged ) ) {
                    $acessarConta = '/login';
                    $usuarioAtual = 'visitante';
                    $logged = false;
                } else {
                   // $acessarConta = '/admin';
                    $acessarConta = '/profiles';
                    $usuarioAtual = $this->Session->read( 'Logged.User.name' );
                    $logged = true;
                }
            ?>
            <p>Olá, <?= $usuarioAtual ?>!! &nbsp;
            Acesse <?php echo $this->Html->link('sua conta', $acessarConta) ?> 
            <?php if ( $logged ): ?>
                ou <?php echo $this->Html->link('desconecte da sessão', array( 'controller' => 'users', 'action' => 'logoutSite')) ?>.
            <?php else: ?>
                ou <?php echo $this->Html->link('cadastre-se', array('controller' => 'pages', 'action' => 'cadastro')) ?>.
            <?php endif ?></p>
        </div>
        <?php /* Produtos */ ?>
        <?php
            $products = $this->Session->read('Carrinho')>0? $this->Session->read('Carrinho') : array();   
            $productsQuantity = 0;
            foreach ($products as $countQuantity) {
                $productsQuantity = $productsQuantity+$countQuantity['quantity'];
            }
        ?>
        <div id="carrinho">
            <h1><i class="ico-carrinho"></i>
            <?php echo $this->Html->link('MEU CARRINHO<small>- '.$productsQuantity.' item(ns) - R$ <span id="valorcarrinho">0,00</span></small>', 'javascript:;', array('escape' => false, 'id'=>'openCarrinho')) ?></h1>
        </div>
        
        <div id="compras">
            <?php if ( !empty($products) ): ?>                
                <?php $valorTotal = 0; ?>
                <?php foreach ($products as $product): ?>
                        
                    <div class="produto">
                        <p class="nome"><?php echo $product['product']; ?></p>
                        <p class="quantidade">x<?php echo $product['quantity']; ?></p>
                        <p class="valor">R$ <?php echo number_format($product['value'], 2, ',','.'); ?></p>
                        <?php echo $this->Html->link('x', '/removerCarrinho/'.$product['id'], array('escape' => false, 'id'=>'excluir', 'class'=>'excluir')) ?>
                    </div>
                    <?php $valorTotal = ( $product['value'] * $product['quantity'] ) + $valorTotal; ?>                
                <?php endforeach ?>            
                <div class="total">                
                    <p>Total: R$ <p id="valortotal"><?php echo number_format($valorTotal, 2,',','.'); ?></p></p>
                </div>
    			<?php echo $this->Html->link('Finalizar Pedido', '/finalizarpedido', array('escape' => false, 'class'=>'bt-pedido', 'onClick' => "ga('send', 'event', 'Pedido', 'Enviar');" )) ?>
            <?php else: ?>
                <h1>Seu carrinho está vazio!</h1>    
            <?php endif ?>
        </div>
        
        <ul id="nav">
            <li><?php echo $this->Html->link('Home', array('controller' => 'pages', 'action' => 'index'), array('class' => 'notranslate menu01')) ?></li>
            <li><?php echo $this->Html->link('A MD8', array('controller' => 'pages', 'action' => 'amd8'), array('class' => 'menu02')) ?></li>
            <li><?php echo $this->Html->link('Clientes', array('controller' => 'pages', 'action' => 'clientes'), array('class' => 'menu03')) ?></li>
            <li><?php echo $this->Html->link('Produtos', array('controller' => 'pages', 'action' => 'produtos'), array('class' => 'menu04')) ?></li>
            <li><?php echo $this->Html->link('Notícias', array('controller' => 'pages', 'action' => 'noticias'), array('class' => 'menu05')) ?></li>
            <li><?php echo $this->Html->link('Dúvidas Frequentes', array('controller' => 'pages', 'action' => 'duvidasfrequentes'), array('class' => 'menu06')) ?></li>
            <li><?php echo $this->Html->link('Contato', array('controller' => 'pages', 'action' => 'contato'), array('class' => 'menu07')) ?></li>
        </ul>

        <ul id="redessociais">
            <li><?php echo $this->Html->link('Facebook - MD8', 'https://www.facebook.com/pages/MD8-embalagens-industriais/299883563365527?ref=hl', array('escape' => false, 'class' => 'facebook', 'target' => '_blank')) ?></li>
            <li><?php echo $this->Html->link('Twitter - MD8', 'http://www.twitter.com', array('escape' => false, 'class' => 'twitter', 'target' => '_blank')) ?></li>
            <li><?php echo $this->Html->link('Busca - MD8', 'javascript:;', array('escape' => false, 'class' => 'busca', 'id'=>'openBusca')) ?></li>
		</ul>

		<?php //echo $this->Session->flash(); ?>    
        <?php echo $this->Form->create('Page', array('class' => 'formBusca', 'id'=>'busca', 'url' => '/produtos')) ?>
            <?php echo $this->Form->input('pesquisa', array('label' => false, 'div' => false, 'placeholder' => 'pesquisar por...')) ?>
            <?php echo $this->Form->submit('pesquisar', array('div' => false)) ?>
        <?php echo $this->Form->end() ?>
                                           
    </div></div><!-- #header-in #header-out -->
<div id="content">

<div id="mainContent"> 
	<?php echo $content_for_layout; ?>
</div><!-- #mainContent -->
              
        <div class="clear"></div>
        </div><!-- #content -->
                
		<div id="footer-out"><div id="footer-in">
			<?php echo $this->Html->link('M3 Media Digital', 'http://www.novam3.com.br', array('escape' => false, 'id' => 'm3', 'target' => '_blank')) ?>

            <ul id="end">
            	<li><i class="ico-maps"></i> Rua Olinto Meira, 1197 | Lagoa Seca - Natal - RN</li>
            	<li><i class="ico-phone"></i> Telefone: (84) 3213-2264 / 3081-3014</li>
            	<li><i class="ico-email"></i> E-mail: <?php echo $this->Html->link('atendimento@md8.com.br', 'mailto:atendimento@md8.com.br') ?></li>
            </div><!-- #end -->
            
		</div></div><!-- #footer-in #footer-out -->
                
        </div><!-- #container -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-32465476-17', 'md8.com.br');
  ga('send', 'pageview');

</script>
	</body>
</html>
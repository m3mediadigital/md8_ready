<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Transactions'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Transaction.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Transaction.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Transaction.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Transaction.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		<td width="120"><h4><?php echo __('Id'); ?></h4></td>
		<td>
			<?php echo $transaction['Transaction']['id']; ?></td>
</tr><tr>		<td><h4><?php echo __('User'); ?></h4></td>
		<td>
			<?php echo $this->Html->link($transaction['User']['name'], array('controller' => 'users', 'action' => 'view', $transaction['User']['id'])); ?></td>
</tr><tr>		<td><h4><?php echo __('Product'); ?></h4></td>
		<td>
			<?php echo $this->Html->link($transaction['Product']['title'], array('controller' => 'products', 'action' => 'view', $transaction['Product']['id'])); ?></td>
</tr><tr>		<td><h4><?php echo __('Status'); ?></h4></td>
		<td>
			<?php echo $transaction['Status']['statu']; ?></td>
</tr><tr>		<td><h4><?php echo __('Created'); ?></h4></td>
		<td>
			<?php echo $this->Time->format( 'd/m/Y H:i:s', $transaction['Transaction']['created'] ); ?></td>
</tr>                </tbody>
            </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Transaction.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Transaction.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Transaction.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Transaction.id'))); ?>
            </li>
            </ul>
</div>

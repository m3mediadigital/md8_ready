
<?php echo $this->element('cliente_admin_site_base') ?>

<div class="body">
    <div id="internas">


            <h1>
                <?php echo "Negociação"; //__('Talks'); ?> 
                <span style='float: right;' >
                    <?= $this->Html->link('Voltar', array('controller' => 'transactions', 'action' => 'index')); ?>
                </span>
            </h1>


            <div class="bloc clear">
                <div class="content">

                    <script type="text/javascript">

                        $(document).ready(function(){
                            $('.all-talks').hide();
                            $('#form-responder').click(function(){
                                $("#new-talk").slideToggle("slow");
                            });
                            $('.talk-action').click(function(e){
                                e.preventDefault();
                                var id = $(this).attr("href").replace(/#/,"");
                                $("#talk-"+id).slideToggle("slow");
                                $("#text-talk-"+id).slideToggle("slow");
                            });
                            $("a.fancybox").bind("click", function(){
                                $("#fancybox-content div div form label br").hide();
                            });
                            var btn = "<button>"+$(".uploader:first span.action").text()+"</button>";
                            $(".uploader input").css( "cursor", "pointer" );
                            $(".uploader span.action").html( btn );
                            $(".uploader span.filename").css( "margin-top", "8px" );
                        });

                    </script>

                <div id="transaction-info">

                    <table>
                        <thead>
                            <tr> 
                                <th colspan="12"><h5><center>Informações da Negociação</center></h5></th>  
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="tb-bold" width="60">Produto:&nbsp;</td>
                                <td colspan="5"><?php echo $talks['Product']['title']; ?></td>
                                <td class="tb-bold" width="80">Quantidade:&nbsp;</td>
                                <td><?php echo $talks['Transaction']['quantity']; ?></td>
                                <td class="tb-bold" width="90">Valor unidade:&nbsp;</td>
                                <td>R$ <?php echo number_format($talks['Product']['value'], 2 , ',', '.'); ?></td>
                                <td class="tb-bold" width="80">Valor total:&nbsp;</td>
                                <?php 
                                    $totalValue = $talks['Product']['value'] * $talks['Transaction']['quantity'];
                                ?>
                                <td>R$ <?php echo number_format( $totalValue, 2 , ',', '.' ); ?></td>
                            </tr>                
                        </tbody>
                    </table>     

                </div>

                <br /><br />

                <p>
                    <?php echo $this->Html->link( $this->Html->image('anexar01.png', array( 'width' => 24 )).'Responder', '#responder', array( 'id' => 'form-responder', 'escape' => false )); ?>
                </p>

                <?php /* Nova resposta */ ?>

                <div id="new-talk" class="all-talks">
                    <?= $this->Form->create('Talk', array( 'url' => array( 'controller' => 'talks', 'action' => 'add', 1, 'admin' => true ), 'type'=>'file'));?>
                    <?= $this->Form->input('transaction_id', array( 'type' => 'hidden', 'value' => $talks['Transaction']['id'] ));?>
                    <?= $this->Form->input('name', array( 'type' => 'hidden', 'value' => $this->Session->read('Logged.User.name') ));?>
                    <?= $this->Form->input('talk');?>
                    <?= $this->Form->end( array( 'label' => 'Responder' ) );?>
                    <br />
                </div> 

                <hr /><br />     

                    <table>
                    <?php foreach ($talks['Talk'] as $talk): ?>
                        <tr>

                            <th width="100"><b>Ultima resposta: </b></th>
                            <th width="150"><?php echo $this->Time->format( 'd/m/Y H:i:s', $talk['created'] ); ?></th>
                            <th width="35"><b>Por: </b> </th>
                            <th><?php echo $talk['name']; ?></th>
                            <th><b></b> </th>
                            <th width="100" class="talks_actions">

                                <?php if( ($this->Session->read('Logged.User.name') == $talk['name']) ){ ?>

                                    <?php echo $this->Html->link( $this->Html->image('edit2.png', array('width' => 24)) , "#".$talk['id'], array( 'escape' => false, 'title' => 'Editar resposta', 'class' => 'talk-action')) ?>
                                    <?php echo $this->Html->link( $this->Html->image('upload01.png', array('width' => 30)) , '#anexar'.$talk['id'], array( 'escape' => false, 'title' => 'Anexar arquivo', 'class' => 'fancybox')) ?>   

                                    <div style="width:auto;height:auto;overflow: auto;position:relative; display:none;">                 
                                        <div id="anexar<?php echo $talk['id']; ?>">

                                                <?php /* Upload de arquivo */ ?>
                                                <?= $this->Form->create('Filestalk', array( 'url' => array( 'controller' => 'filestalks', 'action' => 'add', 'admin' => true ), 'type'=>'file', 'style' => 'width:400px; height:80px;')) ;?>
                                                <?= $this->Form->input('talk_id', array( 'type' => 'hidden', 'value' => $talk['id'] ));?>
                                                <?= $this->Form->input('user_id', array( 'type' => 'hidden', 'value' => $this->Session->read('Logged.User.id') ));?>
                                                <?= $this->Form->input('file.', array( 'label' => "Clique aqui e selecione o(s) arquivo(s) para serem anexado(s).\n<br /><br />", 'type' =>'file', 'multiple' => true ));?>
                                                <?= $this->Form->end( array( 'label' => 'Enviar' ) );?>

                                        </div>
                                    </div>

                                <?php } ?>

                            </th>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <span id="text-talk-<?= $talk['id'] ?>">
                                    <?php echo $talk['talk']; /*print_r($_SESSION);*/ ?>
                                </span>

                                <?php if( ($this->Session->read('Logged.User.name') == $talk['name']) ) { ?>
                                <div id="talk-<?= $talk['id'] ?>" class="all-talks">
                                    <?= $this->Form->create('Talk', array( 'url' => array( 'controller' => 'talks', 'action' => 'edit', $talk['id'], $talks['Transaction']['id'], 1, 'admin' => true ), 'type'=>'file'));?>
                            
                                    <?= $this->Form->input('id', array( 'type' => 'hidden', 'value' => $talk['id'] ));?>

                                    <?= $this->Form->input('transaction_id', array( 'type' => 'hidden', 'value' => $talks['Transaction']['id'] ));?>
                                    <?= $this->Form->input('name', array( 'type' => 'hidden', 'value' => $this->Session->read('Logged.User.name') ));?>
                                    <?= $this->Form->input('talk', array( 'value' => $talk['talk'] ));?>
                                    <?= $this->Form->end( array( 'label' => 'Responder' ) );?>
                                    <br />
                                </div>  
                                <?php } ?>              

                            </td>
                        </tr>
                        <tr><td colspan="6"><b>Anexo(s)</b></td></tr>
                        <tr>
                            <td colspan="6">
                               
                                <?php foreach ($talkFiles as $anexo): ?>
                                <?php // pre( $anexo ) ?>
                                    <?php if (  $anexo['Filestalk']['talk_id'] == $talk['id'] ): ?>
                                        
                                        <?php echo $this->Html->link( $anexo['Filestalk']['file'], '/download/'.$anexo['Filestalk']['file'], array( 'class' => 'button', 'title' => "Arquivo: ".$anexo['Filestalk']['file']." \nTamanho: ".filesize(APP.'webroot/uploads/'.$anexo['Filestalk']['file'])." bytes \nEnviado: ".$this->Time->format( 'd/m/Y H:i:s',$anexo['Filestalk']['created'] )." \nPor: ".$anexo['User']['name'] ) ); ?> 
                                        <?php if ( $anexo['Filestalk']['user_id'] == $this->Session->read('Logged.User.id') ): ?>
                                            
                                            <?php echo $this->Html->link( $this->Html->image('remover.png', array( 'width' => 20 )), array( 'controller' => 'filestalks', 'action' => 'delete', 'admin' => true, $anexo['Filestalk']['id'] ), array( 'class' => 'remover-anexo', 'escape' => false ), 'Deseja realmente remover este anexo ?' ); ?>

                                        <?php endif ?>                            
                                        
                                    <?php endif ?>
                                <?php endforeach ?>
                                <hr />
                            </td>
                        </tr>

                    <?php endforeach ?>
                    </table>

                <p>&nbsp;</p><br />
                <div class="clear"></div>
                </div>
            </div>

    </div>
</div>
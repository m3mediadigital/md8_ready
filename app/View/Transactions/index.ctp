
<?php echo $this->element('cliente_admin_site_base') ?>

<div class="body">
    <div id="internas">


			    	<h1>
			    		<?php echo __('Transactions'); ?> 
			    		<span style='float: right;' >
			    			<?= $this->Html->link('Voltar', array('controller' => 'profiles', 'action' => 'index')); ?>
			    		</span>
			    	</h1>

			    	<div class="bloc clear listagem">
			    		<div class="content">

			    			<table>
			    				<thead>
			    					<tr>
			    						<th width="25"><?php echo $this->Paginator->sort('id', 'Cod');?></th>
			    						<th><?php echo $this->Paginator->sort('product_id');?></th>
			    						<th><?php echo $this->Paginator->sort('created', 'Data do Pedido');?></th>
			    						<th class='actions'>Ações</th>
			    					</tr>
			    				</thead>
			    				<tbody>
			    					<?php
			    					$i = 0;
			    					foreach ($transactions as $transaction):
			    						$class = null;
			    					if ($i++ % 2 == 0) {
			    						$class = ' class="altrow"';
			    					}
			    					?>
			    					<tr>
			    						<td>
			    							<?= $transaction['Transaction']['id'] ?>
			    						</td>
			    						<td>
			    							<?= $this->Html->link($transaction['Product']['title'], "/produto".DS.Inflector::slug(strtolower($transaction['Product']['title']), '-').DS.$transaction['Product']['id']) ?>
			    						</td>
			    						<td>
			    							<?php echo $this->Time->format( 'd/m/Y H:i:s', $transaction['Transaction']['created'] ); ?>
			    						</td>

			    						<td class="actions">
			    							<ul>

			    								<li>
			    									<?php echo $this->Html->link($this->Html->image('talk_transactions2.png', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'talks', $transaction['Transaction']['id']), array('escape' => false, 'title' => "Negociação" )); ?>
			    								</li>    

			    								<li 
			    								cod="<?= $transaction['Transaction']['id'] ?>"
			    								usuario="<?= $transaction['User']['name'] ?>"
			    								produto="<?= htmlspecialchars($transaction['Product']['title']) ?>"
			    								status="<?= $transaction['Status']['statu'] ?>"
			    								criado="<?= $this->Time->format( 'd/m/Y H:i:s', $transaction['Transaction']['created'] ) ?>"
			    								>
			    								<?php echo $this->Html->link($this->Html->image('jqueryui/ico-03.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'view', $transaction['Transaction']['id'], 'admin' => true), array('escape' => false, 'class' => 'view_popup', 'title' => 'Visualização')); ?>
			    							</li>
			    						</ul>
			    					</td>
			    				</tr>
			    			<?php endforeach; ?>
			    		</tbody>
			    	</table>
			    	<p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}')));?>
			    	</p>      
			    	<div class="pagination">
			    		<?php echo $this->Paginator->prev("<button class='bt-esquerda'>Anterior</button>", array('escape'=>false), null, array('class' => 'prev disabled'));?>
			    		<?php echo $this->Paginator->numbers(array('separator' => ' '));?>
			    		<?php echo $this->Paginator->next("<button class='bt-direita'>Próxima</button>", array('escape'=>false), null, array('class' => 'next disabled'));?>
			    	</div>
			    </div>
			</div>

			<div style="display: none; padding: 10px;" id="view_transaction">
				<h1>Goodbye visitor!</h1><h3>Thanks for visiting us!</h3><br />
				Some additional text here ... lorem ipsum. 
			</div>	

			<!-- Modal -->
			<div id="dialog-message" title="Detalhes do Pedido" style="display: none;"></div>

			<script>
			$("a.view_popup").click(function(e){
				e.preventDefault();   

				var obj = $( "#dialog-message" );
				var data = $(this).parent();

				obj.dialog({
					modal: true
				});

				obj.html('\
					<table class="noalt"> \
					<tbody> \
					<tr> \
					<td width="120"><h4>Código</h4></td> \
					<td>'+ data.attr('cod') +'</td> \
					</tr> \
					<tr> \
					<td><h4><?php echo __('User'); ?></h4></td> \
					<td>'+ data.attr('usuario') +'</td> \
					</tr> \
					<tr> \
					<td><h4><?php echo __('Product'); ?></h4></td> \
					<td>'+ data.attr('produto') +'</td> \
					</tr> \
					<tr> \
					<td><h4><?php echo __('Status'); ?></h4></td> \
					<td>'+ data.attr('status') +'</tr> \
					<tr> \
					<td><h4><?php echo __('Created'); ?></h4></td> \
					<td>'+ data.attr('criado') +'</td> \
					</tr> \
					</tbody> \
					</table> \
					');

			});
			</script>		

    </div>
</div>
		
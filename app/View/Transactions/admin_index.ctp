<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Transactions'); ?></h1>
<div id="shortcutInternal" class="bloc"> 
</div>
<div class="bloc clear listagem">
    <div class="content">
        <?php echo $this->Form->create('Transaction', array('url' => array('action' => 'deleteall'))); ?>
        <table>
            <thead>
                <tr>
                    <th width="25"><input type="checkbox" class="checkall"/></th>
                    		<th width="25"><?php echo $this->Paginator->sort('id');?></th>
                        		<th><?php echo $this->Paginator->sort('user_id');?></th>
                                <th><?php echo $this->Paginator->sort('product_id');?></th>
                        		<th><?php echo $this->Paginator->sort('quantity');?></th>
                                <th><?php echo $this->Paginator->sort('status_id');?></th>
                        		<th><?php echo $this->Paginator->sort('created');?></th>
                    <th class="actions">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                    foreach ($transactions as $transaction):
                        $class = null;
                        if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                        }
                ?>
                <tr>
                    <td>
                        <?= $this->Form->input('Transaction.id.'.$transaction['Transaction']['id'],array('value'=>$transaction['Transaction']['id'] ,'type'=>'checkbox','label'=>false, 'div'=>false))?>
                    </td>
		<td>
			<?= $this->Html->link($transaction['Transaction']['id'], array('action'=>'edit', $transaction['Transaction']['id'])); ?>
		</td>
	<td>
		<?= $this->Html->link($transaction['User']['name'], array('controller' => 'users', 'action' => 'view', $transaction['User']['id'])); ?>
	</td>
	<td>
		<?= $this->Html->link($transaction['Product']['title'], array('controller' => 'products', 'action' => 'view', $transaction['Product']['id'])); ?>
	</td>
    <td>
        <?= $transaction['Transaction']['quantity']; ?>
    </td>
	<td>
		<?= $transaction['Status']['statu']; ?>
	</td>
    <td>
        <?php echo $this->Time->format( 'd/m/Y H:i:s', $transaction['Transaction']['created'] ); ?>
    </td>

                    <td class="actions">
                        <ul>
                            
                            <li>
                                <?php echo $this->Html->link($this->Html->image('talk_transactions2.png', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'talks', $transaction['Transaction']['id']), array('escape' => false, 'title' => "Negociação" )); ?>
                            </li>    

                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-03.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'view', $transaction['Transaction']['id']), array('escape' => false)); ?>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-04.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'edit', $transaction['Transaction']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-05.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array( 'action' => 'delete', $transaction['Transaction']['id'] ), array('escape' => false ), 'Deseja realmente remover esta transação ?'); ?>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}')));?></p>
        <?php if(count($transactions)>0): ?>        <div class="acoesMultiplas">
            <button type="submit" name="data[Transaction][action]" value="deleteall" class="bt-deletar">Remover selecionados</button>
        </div>
        <?php endif; ?>        <div class="pagination">
            <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape'=>false), null, array('class' => 'prev disabled'));?>
            <?php echo $this->Paginator->numbers(array('separator' => ' '));?>
            <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape'=>false), null, array('class' => 'next disabled'));?>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?><div id="shortcutInternal" class="bloc">    
</div>
<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Negociação'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>                  
    </ul>
</div>
<div class="bloc clear">
    <div class="content">

        <script type="text/javascript">

            $(document).ready(function(){
                $('#new-talk').hide();
                $('#form-responder').click(function(){
                    $("#new-talk").slideToggle("slow");
                });
            });

        </script>

    <div id="transaction-info">

        <table>
            <thead>
                <tr> 
                    <th colspan="12"><h5><center>Informações</center></h5></th>  
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tb-bold" width="60"><center>Cliente:</center></td>
                    <td><?php echo $talks['User']['name']; ?></td>
                    <td class="tb-bold" width="40">Status:</td>
                    <?php if ( $this->Session->read( 'Logged.User.id' ) == 1 ): ?>
                        <td width="40"><?php echo $this->Html->link($this->Html->image('jqueryui/ico-04.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'edit', $talks['Transaction']['id']), array('escape' => false)); ?></td>
                    <?php endif ?>
                    <td><?php echo $talks['Status']['statu']; ?></td>
                    <td class="tb-bold" width="60">Produto:</td>
                    <td><?php echo $talks['Product']['title']; ?></td>
                    <td class="tb-bold" width="80">Quantidade:</td>
                    <td><?php echo $talks['Transaction']['quantity']; ?></td>
                    <td class="tb-bold" width="90">Valor unidade:</td>
                    <td>R$ <?php echo number_format($talks['Product']['value'], 2 , ',', '.'); ?></td>
                    <td class="tb-bold" width="80">Valor total:</td>
                    <?php 
                        $totalValue = $talks['Product']['value'] * $talks['Transaction']['quantity'];
                    ?>
                    <td>R$ <?php echo number_format( $totalValue, 2 , ',', '.' ); ?></td>
                </tr>                
            </tbody>
        </table>     

    </div>
    <br /><br />
    <p><?php echo $this->Html->link( $this->Html->image('anexar01.png', array( 'width' => 24 )).'Responder', '#responder', array( 'id' => 'form-responder', 'escape' => false )); ?></p>
    <?php /* Nova resposta */ ?>
    <div id="new-talk">
        <?= $this->Form->create('Talk', array( 'url' => array( 'controller' => 'talks', 'action' => 'add' ), 'type'=>'file'));?>
        <?= $this->Form->input('transaction_id', array( 'type' => 'hidden', 'value' => $talks['Transaction']['id'] ));?>
        <?= $this->Form->input('name', array( 'type' => 'hidden', 'value' => $this->Session->read('Logged.User.name') ));?>
        <?= $this->Form->input('talk');?>
        <?= $this->Form->end( array( 'label' => 'Responder' ) );?>
    </div> 
    <p><hr /></p><br />     

        <table>
        <?php foreach ($talks['Talk'] as $talk): ?>
            <tr>

                <th width="100"><b>Ultima resposta: </b></th>
                <th width="150"><?php echo $this->Time->format( 'd/m/Y H:i:s', $talk['created'] ); ?></th>
                <th width="35"><b>Por: </b> </th>
                <th><?php echo $talk['name']; ?></th>
                <th><b></b> </th>
                <th width="100">

                    <?php if( ($this->Session->read('Logged.User.name') == $talk['name']) ){ ?>

                        <?php echo $this->Html->link( $this->Html->image('edit2.png', array('width' => 24)) , array( 'controller' => 'talks', 'action' => 'edit', $talk['id'],$talks['Transaction']['id'] ), array( 'escape' => false, 'title' => 'Editar resposta')) ?>
                        <?php echo $this->Html->link( $this->Html->image('upload01.png', array('width' => 30)) , '#anexar'.$talk['id'], array( 'escape' => false, 'title' => 'Anexar arquivo', 'class' => 'fancybox')) ?>                  

                        <div style="width:auto;height:auto;overflow: auto;position:relative; display:none;">                 
                            <div id="anexar<?php echo $talk['id']; ?>">

                                    <?php /* Upload de arquivo */ ?>
                                    <?= $this->Form->create('Filestalk', array( 'url' => array( 'controller' => 'filestalks', 'action' => 'add' ), 'type'=>'file', 'style' => 'width:400px; height:80px;')) ;?>
                                    <?= $this->Form->input('talk_id', array( 'type' => 'hidden', 'value' => $talk['id'] ));?>
                                    <?= $this->Form->input('user_id', array( 'type' => 'hidden', 'value' => $this->Session->read('Logged.User.id') ));?>
                                    <?= $this->Form->input('file.', array( 'label' => "Clique aqui e selecione o(s) arquivo(s) para serem anexado(s).\n<br /><br />", 'type' =>'file', 'multiple' => true ));?>
                                    <?= $this->Form->end( array( 'label' => 'Enviar' ) );?>

                            </div>
                        </div>   

                    <?php } ?>               

                </th>
            </tr>
            <tr>
                <td colspan="6"><?php echo $talk['talk']; ?>
                </td>
            </tr>
            <tr><td colspan="6"><b>Anexo(s)</b></td></tr>
            <tr>
                <td colspan="6">
                   
                    <?php foreach ($talkFiles as $anexo): ?>
                    <?php // pre( $anexo ) ?>
                        <?php if (  $anexo['Filestalk']['talk_id'] == $talk['id'] ): ?>
                            
                            <?php echo $this->Html->link( $anexo['Filestalk']['file'], '/download/'.$anexo['Filestalk']['file'], array( 'class' => 'button', 'title' => "Arquivo: ".$anexo['Filestalk']['file']." \nTamanho: ".filesize(APP.'webroot/uploads/'.$anexo['Filestalk']['file'])." bytes \nEnviado: ".$this->Time->format( 'd/m/Y H:i:s',$anexo['Filestalk']['created'] )." \nPor: ".$anexo['User']['name'] ) ); ?> 
                            <?php if ( $anexo['Filestalk']['user_id'] == $this->Session->read('Logged.User.id') ): ?>
                                
                                <?php echo $this->Html->link( $this->Html->image('remover.png', array( 'width' => 20 )), array( 'controller' => 'filestalks', 'action' => 'delete', $anexo['Filestalk']['id'] ), array( 'class' => 'remover-anexo', 'escape' => false ), 'Deseja realmente remover este anexo ?' ); ?>

                            <?php endif ?>                            
                            
                        <?php endif ?>
                    <?php endforeach ?>
                    <hr />
                </td>
            </tr>

        <?php endforeach ?>
        </table>

    <p>&nbsp;</p><br />
    <div class="clear"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>          
    </ul>
</div>

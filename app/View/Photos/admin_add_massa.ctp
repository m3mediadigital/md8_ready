<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Photos'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title' => 'Voltar')), 'javascript:history.back()', array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Back'), 'javascript:history.back()'); ?>
        </li>
    </ul>
</div>
<div class="bloc photos clear">
    <div class="content">
        <?php echo $this->Form->create('Photo', array('type' => 'file', 'id' => 'fileupload')); ?>
        <div class="input text">
            <span>T&iacute;tulo para as fotos</span>
            <input type="text" name="data[Photo][title]" multiple class="none">
        </div>
        <div style="clear:both;"></div>
        <div class="row fileupload-buttonbar">
            <div class="span7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="icon-plus icon-white"></i>
                    <span>Escolha as fotos...</span>
                    <div class="input file">
                        <input type="file" name="data[Photo][image]" multiple class="none">
                    </div>
                </span>
            </div>
            <!-- The global progress information -->
            <div class="span5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="bar" style="width:0%;"></div>
                </div>
                <!-- The extended global progress information -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The loading indicator is shown during file processing -->
        <br>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
        <?= $this->Form->submit('Enviar') ?>
        <?php echo $this->Form->end(); ?>
        </form>
    </div>
</div>

<?php echo $this->Form->create('Photo', array('url' => array('action' => 'allActionsListData'))); ?>
<div class="bloc">
    <div class="content">
        <p>Lista de fotos</p>
        
        <ul class="gallery">
            <?php foreach ($fotos as $foto): ?>
                <li>
                    <?= $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $foto['Photo']['image'], array('w' => 200, 'h' => 150, 'zc' => 1)), $this->PhpThumb->url('uploads/' . $foto['Photo']['image'], array('w' => 640, 'h' => 480, 'zc' => 1)), array('escape' => false, 'class' => 'fancybox')) ?>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="cb"></div>
    </div>
</div>
<?php echo $this->Form->end(); ?>

<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title' => 'Voltar')), 'javascript:history.back()', array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Back'), 'javascript:history.back()'); ?>
        </li>
    </ul>
</div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        {% if (file.error) { %}
            <td class="error" colspan="2"><span class="label label-important">{%=locale.fileupload.error%}</span> {%=locale.fileupload.errors[file.error] || file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
            <td>
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
            </td>
            <td class="start">{% if (!o.options.autoUpload) { %}
                <button class="btn btn-primary">
                    <i class="icon-upload icon-white"></i>
                    <span>{%=locale.fileupload.start%}</span>
                </button>
            {% } %}</td>
        {% } else { %}
            <td colspan="2"></td>
        {% } %}
        <td class="cancel">{% if (!i) { %}
            <button class="btn btn-warning">
                <i class="icon-ban-circle icon-white"></i>
                <span>{%=locale.fileupload.cancel%}</span>
            </button>
        {% } %}</td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        {% if (file.error) { %}
            <td class="preview"><span class="fade"></span></td>
            <td class="name"><span>{%=file.name%}</span></td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td class="" colspan="2"></td>
        {% } else { %}
            <td class="preview">{% if (file.thumbnail_url) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" rel="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
            {% } %}</td>
            <td class="name">
                <a href="{%=file.url%}" title="{%=file.name%}" rel="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
            </td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td colspan="2"></td>
        {% } %}
        <td class="delete">

        </td>
    </tr>
{% } %}
</script>
<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Photos'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
            </ul>
</div>
<div class="bloc photos clear">
    <div class="content">
        <?php echo $this->Form->create('Photo', array('type'=>'file'));?>
        	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('author');
		echo $this->Form->input('alt');
		echo $this->Form->input('image', array('type'=>'file'));
	?>
        <?php echo $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
        </ul>
</div>
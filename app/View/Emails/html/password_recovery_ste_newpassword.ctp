<tr>
    <td colspan="3">
        <h1 style="color:#585b5d; font:18px Arial; background:#e6e6e6; padding:14px 0 12px; text-indent:20px;">Recuperação de senha.</h1>
    </td>
</tr>
<tr>
    <td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:25px 20px 0 20px;">Olá <?= $data['nome'] ?>, sua senha foi resetada com sucesso!</p>
    </td>    
</tr>
<tr>
	<td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:25px 20px 0 20px;">Sua nova senha é: <b><?= $data['newpassword'] ?></b></p>
    </td>
</tr>
<br/>
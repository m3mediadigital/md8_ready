<tr>
    <td colspan="3">
        <h1 style="color:#585b5d; font:18px Arial; background:#e6e6e6; padding:14px 0 12px; text-indent:20px;">Senha de acesso modificada com sucesso</h1>
    </td>
</tr>
<tr>
    <td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:25px 20px 0 20px;">Olá, sua senha de acesso ao sistema foi modificada com sucesso, segue abaixo os novos dados de acesso.</p>
    </td>
</tr>
<tr>
    <td colspan="3">
        <h2 style="color:#585b5d; font:12px Arial; margin:25px 20px 5px 20px; font-weight: bold;">Endereço:</h2>
    </td>
</tr>
<tr>
    <td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:0 20px 0 20px;"><a href="http://localhost/episteme/admin/" target="_blank">http://localhost/episteme/admin/</a></p>
    </td>
</tr>
<tr>
    <td colspan="3">
        <h2 style="color:#585b5d; font:12px Arial; margin:25px 20px 5px 20px; font-weight: bold;">Login:</h2>
    </td>
</tr>
<tr>
    <td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:0 20px 0 20px;"><?= $data['User']['login'] ?></p>
    </td>
</tr>
<tr>
    <td colspan="3">
        <h2 style="color:#585b5d; font:12px Arial; margin:25px 20px 5px 20px; font-weight: bold;">Senha:</h2>
    </td>
</tr>
<tr>
    <td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:0 20px 0 20px;"><?= $data['User']['stringPassword'] ?></p>
    </td>
</tr>
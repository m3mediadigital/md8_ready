<tr>
    <td colspan="3">
        <h1 style="color:#585b5d; font:18px Arial; background:#e6e6e6; padding:14px 0 12px; text-indent:20px;">Recuperação de senha.</h1>
    </td>
</tr>
<tr>
    <td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:25px 20px 0 20px;">Olá <?= $data['nome'] ?>, recebemos sua solicitação para a recuperação de sua senha de acesso, para cadastrar uma nova senha ( restando a atual ) clique <a href="http://www.md8.com.br/resetpassword/<?= $data['url'] ?>">AQUI</a>.</p>
    </td>    
</tr>
<tr>
	<td colspan="3">
        <p style="color:#585b5d; font:12px Arial; margin:25px 20px 0 20px;">Caso não tenha solicitado este serviço, por favor ignore este E-Mail.</p>
    </td>
</tr>
<br/>
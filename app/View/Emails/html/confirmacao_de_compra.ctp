<tr>
    <td colspan="4">
        <h1 style="color:#585b5d; font:18px Arial; background:#e6e6e6; padding:14px 0 12px; text-indent:20px;">Confirmação de transação.</h1>
    </td>
</tr>
<tr>
    <td colspan="4">
        <p style="color:#585b5d; font:12px Arial; margin:25px 20px 0 20px;"><b>O usuário <?= $data['usuario'] ?>, iniciou uma(s) nova(s) transação!</b></p>
    </td>
</tr>
<tr>
    <td colspan="4">
        <p style="color:#585b5d; font:12px Arial; margin:25px 20px 0 20px;"><b><center>Produto(s)</center></b></p>
    </td>
</tr>
<tr>
    <td><center>Produto:</center></td>
    <td><center>Valor:</center></td>
    <td><center>Quantidade:</center></td>
    <td><center>Total:</center></td>
</tr>
<?php $totalCompra = 0; ?>
<?php foreach ($data['produtos'] as $produto): ?>    
    <tr>
        <td><center><?= $produto['product'] ?></center></td>
        <td><center><?= ($produto['value'])?$produto['value']:'0,00'; ?></center></td>
        <td><center><?= $produto['quantity'] ?></center></td>
        <?php $total = $produto['value']*$produto['quantity']; ?>
        <td><center><?= number_format($total,2,',','.') ?></center></td>
    </tr>
    <?php $totalCompra = $totalCompra+$total; ?>
<?php endforeach ?>
<tr>
    <td></td>
    <td></td>
    <td><center>Valor total:</center></td>
    <td><center><?= number_format($totalCompra,2,',','.') ?></center></td>  
</tr>

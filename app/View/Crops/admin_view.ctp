<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Recorte'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Crop.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Crop.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Crop.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Crop.id'))); ?>
        </li>
    </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		
                        <td><h4><?php echo __('Id'); ?></h4></td>
                        <td><?php echo $crop['Crop']['id']; ?></td>
                     </tr>
                     <tr>		
                        <td><h4><?php echo __('Tabela'); ?></h4></td>
                        <td><?php echo $crop['Crop']['table']; ?></td>
                     </tr>
                     <tr>		
                        <td><h4><?php echo __('Largura'); ?></h4></td>
                        <td><?php echo $crop['Crop']['width']; ?></td>
                     </tr>                
                     <tr>		
                        <td><h4><?php echo __('Altura'); ?></h4></td>
                        <td><?php echo $crop['Crop']['height']; ?></td>
                     </tr>                
                 </tbody>
             </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Crop.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Crop.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Crop.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Crop.id'))); ?>
        </li>
    </ul>
</div>
<div id="sidebar">
	<?php echo $this->element('categorias') ?><!-- CATERGORIAS -->
    <?php echo $this->element('atendimento') ?><!-- ATENDIMENTO -->
    <?php echo $this->element('orcamento') ?><!-- ORÇAMENTO -->
    <?php echo $this->element('newsletter') ?><!-- NEWSLETTER -->
    <?php echo $this->element('localizacao') ?><!-- LOCALIZAÇÃO -->
</div><!-- #sidebar -->

<div id="internas">
	<h1>Produtos</h1>
    
    <?php foreach($products as $product): ?>
    <?php  $categorias = array(); ?>
        <div class="boxProdutos">
            <?php echo $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $product['Product']['cover'], array('w' => 220, 'h' => 110, 'zc' => 1, 'q' => 100)), "/produto".DS.Inflector::slug(strtolower($product['Product']['title']), '-').DS.$product['Product']['id'], array('escape' => false)); ?>
            <?php echo $this->Html->link($this->Html->image('ver-detalhes.png'), "/produto".DS.Inflector::slug(strtolower($product['Product']['title']), '-').DS.$product['Product']['id'], array('escape' => false, 'class' => 'image_hover_bg ')) ?>
            <p><?php echo $this->Html->link($product['Product']['title'], "/produto".DS.Inflector::slug(strtolower($product['Product']['title']), '-').DS.$product['Product']['id']) ?></p>
            
            <small>
            <?php 
                foreach($product['Category'] as $category) {
                    $categorias[$category['id']] = $category['category'];
                }
            ?>

            <?php $i = 1; ?>
            <?php foreach($categorias as $id => $categoria): ?>
                <?php echo $this->Html->link($categoria, DS."produtos".DS.Inflector::slug(strtolower($categoria),"-").DS.$id); ?>
                <?php echo ($i == count($categorias)) ? ""  : ","; ?>
                <?php $i++; ?>    
            <?php endforeach; ?>
            </small>
        </div>
    <?php endforeach; ?>
    <div id="paginacao">
    <?php 
        $paginas = $this->Paginator->counter('{:pages}');
        if($paginas > 1) {
            echo $this->Paginator->numbers(array('separator' => ' '));
        }
    ?>
    </div>

</div>
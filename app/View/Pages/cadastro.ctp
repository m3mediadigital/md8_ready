<div id="sidebar">
	<?php echo $this->element('categorias') ?><!-- CATERGORIAS -->
    <?php echo $this->element('atendimento') ?><!-- ATENDIMENTO -->
    <?php echo $this->element('orcamento') ?><!-- ORÇAMENTO -->
    <?php echo $this->element('newsletter') ?><!-- NEWSLETTER -->
    <?php echo $this->element('localizacao') ?><!-- LOCALIZAÇÃO -->
</div><!-- #sidebar -->

<div id="internas">
	<h1>Cadastro</h1>
    
	<?php //echo $this->Session->flash(); ?>    
    <?php echo $this->Form->create('User', array('class' => 'formContato')) ?>
    <?= $this->Form->input('cpfcasal', array('style' => 'display:none;', 'label' => false)) ?>
    <div class="l365">
        <?php echo $this->Form->input('Profile.full_name', array('label' => 'Nome Completo:*', 'div' => false)) ?>
    </div>
    <div class="r365">
        <?php echo $this->Form->input('Profile.corporate_name', array('label' => 'Razão Social:*', 'div' => false)) ?>
    </div>
    <div class="l365">
        <?php echo $this->Form->input('Profile.cnpj', array('label' => 'CNPJ:*', 'div' => false, 'onkeypress' => 'script: mascaraMutuario(this,cpfCnpj);', 'maxlength' => 18 )) ?>           
    </div>
    <div class="r365">
        <?php echo $this->Form->input('Profile.cpf', array('label' => 'CPF:', 'div' => false, 'onkeypress' => 'script: mascaraMutuario(this,cpfCnpj);', 'maxlength' => 14)) ?>
    </div>
    <div class="l365">
        <?php echo $this->Form->input('Profile.full_address', array('label' => 'Endereço Completo:', 'div' => false)) ?>
    </div>
    <div class="r365">
        <?php echo $this->Form->input('Profile.cep', array('label' => 'CEP:', 'div' => false,'onkeypress' => 'script: mascaraMutuario(this,cep);', 'maxlength' => 10)) ?>
    </div>
    <div class="l365">
        <?php echo $this->Form->input('Profile.city', array('label' => 'Cidade:', 'div' => false)) ?>
    </div>
    <div class="r365">
        <?php echo $this->Form->input('Profile.uf', array('label' => 'UF:', 'div' => false)) ?>
    </div>
    <div class="l365">
        <?php echo $this->Form->input('Profile.primary_phone', array('label' => 'Telefone 01:*', 'div' => false, 'class' => 'phone', 'placeholder' => '(XX) XXXX-XXXX')) ?>
    </div>
    <div class="r365">
        <?php echo $this->Form->input('Profile.secondary_phone', array('label' => 'Telefone 02:', 'div' => false, 'class' => 'phone', 'placeholder' => '(XX) XXXX-XXXX')) ?>
    </div>
    <div class="l365">
        <?php echo $this->Form->input('Profile.fax', array('label' => 'Fax:', 'div' => false, 'class' => 'phone', 'placeholder' => '(XX) XXXX-XXXX')) ?>
    </div>
    <div class="r365">
        <?php echo $this->Form->input('Profile.email', array('label' => 'E-mail:*', 'div' => false, 'placeholder' => 'seuemail@seuprovedor.com.br')) ?>
    </div>
    <div class="l365">
        <?php echo $this->Form->input('password', array('label' => 'Senha:*', 'div' => false, 'type' => 'password')) ?>
    </div>
    <div class="r365">
        <?php echo $this->Form->input('password_confirmation', array('label' => 'Confirmar Senha:*', 'div' => false, 'type' => 'password')) ?>
    </div>
    <div class="l760">
        <?php // echo $this->Form->input('comentario', array('label' => 'Mensagem:', 'div' => false, 'type' => 'textarea')) ?>
        <?php echo $this->Form->input('receberNews', array('label' => 'Deseja receber novidades ?', 'checked' => 'checked')); ?>
        <?php echo $this->Form->submit('Enviar', array('div' => false)) ?>
   </div>
    
    <?php echo $this->Form->end() ?>

</div>
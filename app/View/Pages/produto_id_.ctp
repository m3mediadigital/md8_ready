<div id="sidebar">
    <?php echo $this->element('categorias') ?><!-- CATERGORIAS -->
    <?php echo $this->element('atendimento') ?><!-- ATENDIMENTO -->
    <?php echo $this->element('orcamento') ?><!-- ORÇAMENTO -->
    <?php echo $this->element('newsletter') ?><!-- NEWSLETTER -->
    <?php echo $this->element('localizacao') ?><!-- LOCALIZAÇÃO -->
</div><!-- #sidebar -->

<div id="internas">
    <h1><?php echo $produto['Product']['title'] ?></h1>    
    <div class="fotos_produtos">
        <div class="boxProdutos">
            <?php echo $this->Html->link($this->PhpThumb->thumbnail("/uploads/{$produto['Product']['cover']}", array('w' => 220, 'h' => 175, 'zc' => 1)), $this->PhpThumb->url("/uploads/{$produto['Product']['cover']}", array('w' => 640, 'h' => 480, 'zc' => 1)), array('escape' => false, 'rel'=>'prettyPhoto['.$produto['Product']['id'].']')) ?>
        </div>
        
        <ul class="galerias-fts">
            <?php foreach($photos as $photo): ?>
            <li>
                <?php echo $this->Html->link($this->PhpThumb->thumbnail("/uploads/{$photo['Photo']['image']}", array('w' => 75, 'h' => 60, 'zc' => 1)), $this->PhpThumb->url("/uploads/{$photo['Photo']['image']}", array('w' => 640, 'h' => 480, 'zc' => 0)), array('escape' => false, 'rel'=>'prettyPhoto['.$produto['Product']['id'].']')); ?>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>    
    <div class="boxDescricao">
    
        <?php  
            $valor = number_format($produto['Product']['value'], 2, ',', '.');
            $valor = explode(",", $valor);

        ?>
        <? if($valor[0] == 0): ?>
            <h2>Sob consulta</h2>
        <? else: ?>            
            <h2>R$ <?php echo $valor[0] ?><small>,<?php echo $valor[1] ?></small></h2>
        <? endif; ?>
           
        <?php echo $this->Form->create('Product', array( 'url' => array( 'controller' => 'pages', 'action' => 'addCarrinho' ), 'class' => 'formPedido') ) ?>
            <?php echo $this->Form->input('id', array('type' => 'hidden', 'value' =>  $produto['Product']['id'] )) ?>
            <?php echo $this->Form->input('qtd', array('label' => 'Qtd:', 'class' => 'input-text' )) ?>
            <?php echo $this->Form->submit('Adicionar no Carrinho', array('div' => false)) ?>
        <?php echo $this->Form->end() ?>

        <ul class="tabs-nav">
            <li class="active"><a href="#descricao">Descrição</a></li>
            <li><a href="#videos">Vídeo(s)</a></li>
            <li><a href="#comentarios">Comentários</a></li>
        </ul>
        <!-- Tabs Content -->
        <div class="tabs-container">
            <div class="tab-content" id="descricao">
                <p><?= strip_tags($produto['Product']['description']) ?></p>
            </div>
            <div class="tab-content" id="videos">
                <?php if(!empty($produto['Product']['video'])): ?>
                    <?php echo $this->ExtensionHtml->youtube($produto['Product']['video'], 500, 281) ?>
                <?php else: ?>
                    <p>Não há video para este produto.</p>
                <?php endif; ?>
            </div>
            <div class="tab-content" id="comentarios">
                <div class="fb-comments" data-href="http://www.md8.com.br/novo/<?= $this->Session->params->url ?>" data-width="500" data-num-posts="10"></div>
            </div>
        </div>        
        <!-- AddThis Button BEGIN -->
        <div id="compratilhar">
            <div class="addthis_toolbox addthis_default_style ">
                <a class="addthis_button_preferred_1"></a>
                <a class="addthis_button_preferred_2"></a>
                <a class="addthis_button_preferred_3"></a>
                <a class="addthis_button_preferred_4"></a>
                <a class="addthis_button_compact"></a>
                <a class="addthis_counter addthis_bubble_style"></a>
            </div>
            <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-516db4ec6e974e7f"></script>
        </div>
        <!-- AddThis Button END -->

        <?php 
            foreach($produto['Category'] as $categoriaProduto) {
                $listCategoriasProduto[$categoriaProduto['id']] = $categoriaProduto['category'];
            }
        ?>
         <div id="comentens">
            <p>
                <small>
                <?php $j = 1; ?>
                <?php foreach($listCategoriasProduto as $id => $categoriasProduto): ?>
                    <?php echo $this->Html->link($categoriasProduto, DS."produtos".DS.Inflector::slug(strtolower($categoriasProduto),"-").DS.$id); ?>
                    <?php echo ($j == count($listCategoriasProduto)) ? ""  : ","; ?>
                <?php $j++; ?>    
                <?php endforeach; ?>
                </small>
            </p>
        </div>
        
        
        
    </div>
    
    <div class="clear">&nbsp;</div>
    <div class="clear">&nbsp;</div>
    <div class="clear">&nbsp;</div>
    
    <h1>Veja também:</h1>
        
    <?php foreach($products as $product): ?>
    <?php  $categorias = array(); ?>
        <div class="boxProdutos">
            <?php echo $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $product['Product']['cover'], array('w' => 220, 'h' => 110, 'zc' => 1, 'q' => 100)), "/produto".DS.Inflector::slug(strtolower($product['Product']['title']), '-').DS.$product['Product']['id'], array('escape' => false)); ?>
            <?php echo $this->Html->link($this->Html->image('ver-detalhes.png'), "/produto".DS.Inflector::slug(strtolower($product['Product']['title']), '-').DS.$product['Product']['id'], array('escape' => false, 'class' => 'image_hover_bg ')) ?>
            <p><?php echo $this->Html->link($product['Product']['title'], "/produto".DS.Inflector::slug(strtolower($product['Product']['title']), '-').DS.$product['Product']['id']) ?></p>
    
            <small>
            <?php 
                foreach($product['Category'] as $category) {
                    $categorias[$category['id']] = $category['category'];
                }
            ?>

            <?php $i = 1; ?>
            <?php foreach($categorias as $id => $categoria): ?>
                <?php echo $this->Html->link($categoria, DS."produtos".DS.Inflector::slug(strtolower($categoria),"-").DS.$id); ?>
                <?php echo ($i == count($categorias)) ? ""  : ","; ?>
                <?php $i++; ?>    
            <?php endforeach; ?>
            </small>
    </div>
    <?php endforeach; ?>
    
</div>
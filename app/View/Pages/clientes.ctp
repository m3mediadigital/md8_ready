<div id="sidebar">
	<?php echo $this->element('categorias') ?><!-- CATERGORIAS -->
    <?php echo $this->element('atendimento') ?><!-- ATENDIMENTO -->
    <?php echo $this->element('orcamento') ?><!-- ORÇAMENTO -->
    <?php echo $this->element('newsletter') ?><!-- NEWSLETTER -->
</div><!-- #sidebar -->

<div id="internas">
	<h1>Clientes</h1>
	<?php foreach($clientes as $cliente): ?>
		<?php $cliente['Client']['site'] = (empty($cliente['Client']['site'])) ? "#" : $cliente['Client']['site']; ?>
		<?php echo $this->Html->link($this->PhpThumb->thumbnail(DS.'uploads'.DS.$cliente['Client']['image'],array('w' => 180, 'h' => 118, 'zc' => 1, 'q' => 100)), $cliente['Client']['site'], array('escape' => false, 'target' => '_blank', 'class'=>'logocliente')) ?>
	<?php endforeach; ?>
</div>
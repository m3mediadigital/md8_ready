<div id="sidebar">
	<?php echo $this->element('categorias') ?><!-- CATERGORIAS -->
    <?php echo $this->element('atendimento') ?><!-- ATENDIMENTO -->
    <?php echo $this->element('orcamento') ?><!-- ORÇAMENTO -->
    <?php echo $this->element('newsletter') ?><!-- NEWSLETTER -->
    <?php echo $this->element('localizacao') ?> <!-- LOCALIZAÇÃO -->
</div><!-- #sidebar -->

<div id="internas">
	<h1>Notícias</h1>
	
	<div class="postagem">
        <?php $dia  =  $this->Time->format('d', $noticia['News']['publication']); ?>
        <?php $mes  =  $this->Time->format('M', $noticia['News']['publication']); ?>
        <?php $ano  =  $this->Time->format('Y', $noticia['News']['publication']); ?>
        <h1><?php echo "{$dia}<small>/".__($mes)."</small>$ano"; ?></h1>
        <h2><?php echo $noticia['News']['title'] ?></h2>
        
        <?php echo $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $noticia['News']['image'], array('w' => 300, 'h' => 200, 'zc' => 1, 'q' => '100')), $this->PhpThumb->url('uploads/' . $noticia['News']['image'], array('w' => 640, 'h' => 480, 'zc' => 1, 'q' => '100')), array('escape' => false, 'rel' => 'prettyPhoto')) ?>
        
        <p><?php echo $noticia['News']['text'] ?></p>
        
    </div>


</div>
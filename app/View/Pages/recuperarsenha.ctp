<div id="sidebar">
	<?php echo $this->element('categorias') ?><!-- CATERGORIAS -->
    <?php echo $this->element('atendimento') ?><!-- ATENDIMENTO -->
    <?php echo $this->element('orcamento') ?><!-- ORÇAMENTO -->
    <?php echo $this->element('newsletter') ?><!-- NEWSLETTER -->
    <?php echo $this->element('localizacao') ?><!-- LOCALIZAÇÃO -->
</div><!-- #sidebar -->

<div id="internas">
	<h1>Recupear senha</h1>   

    
    <div class="box340">
		<h2> Por favor, entre com seu E-Mail.</h2>
        <p>Digite abaixo seu e-mail.</p>
        
		<?php echo $this->Form->create('User', array( 'action' => 'resetarsenha', 'class' => 'formLogin')) ?>
        <?= $this->Form->input('cpfcasal', array('style' => 'display:none;', 'label' => false)) ?>
        <div class="l340">
            <?php echo $this->Form->input('login', array('label' => 'E-mail:', 'div' => false, 'placeholder' => 'seuemail@seuprovedor.com.br')) ?>
        </div>       
        <div class="r340">            
            <?php echo $this->Form->submit('Enviar', array('div' => false)) ?>
        </div>
        
        <?php echo $this->Form->end() ?>
        
	</div>
    
</div>
<div id="sidebar">
	<?php echo $this->element('categorias') ?><!-- CATERGORIAS -->
    <?php echo $this->element('atendimento') ?><!-- ATENDIMENTO -->
    <?php echo $this->element('orcamento') ?><!-- ORÇAMENTO -->
    <?php echo $this->element('newsletter') ?><!-- NEWSLETTER -->
    <!-- <?php echo $this->element('localizacao') ?> LOCALIZAÇÃO -->
</div><!-- #sidebar -->

<div id="internas">
	<h1>Contato</h1>
    
	<?php //echo $this->Session->flash(); ?>    
    <?php echo $this->Form->create('Page', array('class' => 'formContato')) ?>
    <?= $this->Form->input('cpfcasal', array('style' => 'display:none;', 'label' => false)) ?>
    <div class="l365">
        <?php echo $this->Form->input('nome', array('label' => 'Nome:', 'div' => false)) ?>
    </div>
    <div class="r365">
        <?php echo $this->Form->input('email', array('label' => 'E-mail:', 'div' => false, 'placeholder' => 'seuemail@seuprovedor.com.br')) ?>
    </div>
    <div class="l365">
        <?php echo $this->Form->input('telefone', array('label' => 'Telefone:', 'div' => false, 'class' => 'phone', 'placeholder' => '(XX) XXXX-XXXX')) ?>
    </div>
    <div class="r365">
        <?php echo $this->Form->input('assunto', array('label' => 'Assunto:', 'div' => false)) ?>
    </div>
    <div class="l760">
        <?php echo $this->Form->input('comentario', array('label' => 'Mensagem:', 'div' => false, 'type' => 'textarea')) ?>
    </div>
    
    <div class="l760">
        <?php echo $this->Form->submit('Enviar', array('div' => false, 'onClick' => "ga('send', 'event', 'Entrar em contato', 'Enviar');")) ?>
    </div>
    
    <?php echo $this->Form->end() ?>

	<h1>Localização</h1>

    <!--<iframe width="760" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=C%C3%ADcero+de+Azevedo,+Lagoa+Seca,+Natal+-+RN+59031-020&amp;aq=&amp;sll=-5.802019,-35.211761&amp;sspn=0.005422,0.010568&amp;ie=UTF8&amp;hq=&amp;hnear=R.+C%C3%ADcero+de+Azevedo+-+Lagoa+Seca,+Natal+-+Rio+Grande+do+Norte,+59031-020&amp;t=m&amp;ll=-5.797856,-35.212083&amp;spn=0.010674,0.032573&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>-->
	<iframe width="760" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/ms?msa=0&amp;msid=202094267539754965916.0004e23192696c6e3fdd1&amp;ie=UTF8&amp;t=m&amp;ll=-5.796245,-35.211868&amp;spn=0,0&amp;output=embed"></iframe>
	
</div>
<!-- INCÍCIO DE POPUP -->
   <?php

   $active = 0; //Ativa ou desativa independente do testdate
   $testdate = (time() < strtotime("2019-03-07 03:00:00"));  //Data, hora e minuto e segundo que o flutuante deve sair
   $embed = ''; //Id do vídeo do youtube, tem prioridade sobre a imagem, caso comente esta linha, a imagem passa a ser ativa.
   $link = '/contato'; //Link do clique do banner.
   $_blank = false; //Abrir o link em outra página ou não.
   $image = 'Pop-up-funcionamento-carnaval.png'; //Nome da imagem com sua extensão.

   ?>

   <?php if ($active && ( (isset($testdate) && $testdate) || !isset($testdate) ) ): ?>

       <script>
          function lacrar(lacrador){
          var popLacrador = document.getElementById(lacrador);
          popLacrador.setAttribute("style", "display: none;");
          $('#video-youtube').remove();
          }
       </script>

       <div id="lacrador" class="popup-main">
           <div class="popup-overlay"></div>
           <div class="popup-out" align="center">
               <div class="popup-inner">
                   <div class="popup-body <?= !empty($embed) ? 'video' : 'image' ?>">
                       <a href="javascript: lacrar('lacrador');" class="popup-close">
                           <img src="img/fechar.png">
                       </a>
                       <?php if(!empty($embed) ): ?>
                           <iframe id="video-youtube" width="100%" height="500" src="https://www.youtube.com/embed/<?= $embed ?>?autoplay=1&amp;rel=0&amp;controls=0" frameborder="0" allowfullscreen></iframe>
                       <?php else: ?>
                           <a <?= ( $_blank ? 'target="_blank"' : '' ) ?> href="<?= $link ?> ">
                               <img src="img/<?= $image ?>">
                           </a>
                       <?php endif; ?>
                   </div>
               </div>
           </div>
       </div>

   <?php endif; ?> 
<!-- FIM DE POPUP -->

<div id="sidebar">
	<?php echo $this->element('categorias') ?><!-- CATERGORIAS -->
    <?php echo $this->element('atendimento') ?><!-- ATENDIMENTO -->
    <?php echo $this->element('orcamento') ?><!-- ORÇAMENTO -->
    <?php echo $this->element('newsletter') ?><!-- NEWSLETTER -->
    <?php echo $this->element('localizacao') ?><!-- LOCALIZAÇÃO -->
</div><!-- #sidebar -->

<div id="conteudo">
<ul class="slider">
    <?php foreach($banners as $banner): ?>
    <li>
         <?php echo $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $banner['Banner']['banner'], array('w' => 760, 'h' => 300, 'zc' => 1, 'q' => 100)), $banner['Banner']['url'],array('escape' => false))?>
        <span><?php echo $banner['Banner']['title']; ?></span>
    </li>
    <?php endforeach; ?>
</ul>
<i class="icon-angle-left icon-2x slideLeft"></i>
<i class="icon-angle-right icon-2x slideRight"></i>

<h1>Produtos em destaques</h1>
<?php foreach($products as $product): ?>
<?php  $categorias = array(); ?>
<div class="boxProdutos">
    <?php echo $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $product['Product']['cover'], array('w' => 220, 'h' => 110, 'zc' => 1, 'q' => 100)), "/produto".DS.Inflector::slug(strtolower($product['Product']['title']), '-').DS.$product['Product']['id'], array('escape' => false)); ?>
            <?php echo $this->Html->link($this->Html->image('ver-detalhes.png'), "/produto".DS.Inflector::slug(strtolower($product['Product']['title']), '-').DS.$product['Product']['id'], array('escape' => false, 'class' => 'image_hover_bg ')) ?>
            <p><?php echo $this->Html->link($product['Product']['title'], "/produto".DS.Inflector::slug(strtolower($product['Product']['title']), '-').DS.$product['Product']['id']) ?></p>
    
    <small>
    <?php 
        foreach($product['Category'] as $category) {
            $categorias[$category['id']] = $category['category'];
        }
     ?>

    <?php $i = 1; ?>
    <?php foreach($categorias as $id => $categoria): ?>
        <?php echo $this->Html->link($categoria, DS."produtos".DS.Inflector::slug(strtolower($categoria),"-").DS.$id); ?>
        <?php echo ($i == count($categorias)) ? ""  : ","; ?>
        <?php $i++; ?>    
    <?php endforeach; ?>
    </small>
</div>
<?php endforeach; ?>


</div>
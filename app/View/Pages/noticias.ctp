<div id="sidebar">
	<?php echo $this->element('categorias') ?><!-- CATERGORIAS -->
    <?php echo $this->element('atendimento') ?><!-- ATENDIMENTO -->
    <?php echo $this->element('orcamento') ?><!-- ORÇAMENTO -->
    <?php echo $this->element('newsletter') ?><!-- NEWSLETTER -->
    <?php echo $this->element('localizacao') ?><!-- LOCALIZAÇÃO -->
</div><!-- #sidebar -->

<div id="internas">
	<h1>Notícias</h1>
    <?php foreach($noticias as $noticia): ?>
	<div class="postagem">
        <?php $dia  =  $this->Time->format('d', $noticia['News']['publication']); ?>
        <?php $mes  =  $this->Time->format('M', $noticia['News']['publication']); ?>
        <?php $ano  =  $this->Time->format('Y', $noticia['News']['publication']); ?>
        <h1><?php echo "{$dia}<small>/".__($mes)."</small>$ano"; ?></h1>
        <h2><?= $this->Html->link($noticia['News']['title'], "/noticia/".Inflector::slug(strtolower($noticia['News']['title']),'-')."/{$noticia['News']['id']}") ?></h2>
    </div>
    <?php endforeach; ?>

</div>
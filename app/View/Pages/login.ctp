<div id="sidebar">
	<?php echo $this->element('categorias') ?><!-- CATERGORIAS -->
    <?php echo $this->element('atendimento') ?><!-- ATENDIMENTO -->
    <?php echo $this->element('orcamento') ?><!-- ORÇAMENTO -->
    <?php echo $this->element('newsletter') ?><!-- NEWSLETTER -->
    <?php echo $this->element('localizacao') ?><!-- LOCALIZAÇÃO -->
</div><!-- #sidebar -->

<div id="internas">
	<h1>Login</h1>
    
    <div class="box340 mr80">
		<h2>Sou um novo cliente</h2>
		<p>Ao cadastrar-se e criar uma conta você poderá comprar mais rapidamente, saber a situação de seus pedidos e acompanhar as compras já realizadas.</p>
        <h3><?php echo $this->Html->link('Cadastrar-se (recomendado)', '/cadastro', array('escape' => false)) ?></h3>
	</div>
    
    <div class="box340">
		<h2> Já sou cliente.</h2>
        <p>Digite abaixo seu e-mail e senha já cadastrados na loja.</p>
        
        <?php /* echo $this->Form->create('User', array( 'action' => 'login', 'class' => 'formLogin')) */ ?>
		<?php echo $this->Form->create('User', array( 'action' => 'loginSite', 'class' => 'formLogin')) ?>
        <?= $this->Form->input('cpfcasal', array('style' => 'display:none;', 'label' => false)) ?>
        <div class="l340">
            <?php echo $this->Form->input('login', array('label' => 'E-mail:', 'div' => false, 'placeholder' => 'seuemail@seuprovedor.com.br')) ?>
        </div>
        <div class="r340">
            <?php echo $this->Form->input('password', array('label' => 'Senha:*', 'div' => false, 'type' => 'password')) ?>
        </div>
        
        <div class="r340">
            <?php echo $this->Html->link('Esqueceu sua senha?', '/recuperarsenha', array('escape' => false, 'class'=>'left')) ?>
            <?php echo $this->Form->submit('Entrar', array('div' => false)) ?>
        </div>
        
        <?php echo $this->Form->end() ?>
        
	</div>
    
</div>
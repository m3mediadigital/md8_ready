<div class='search'>
    <?= $this->Form->create(array('action' => 'index', 'type' => 'GET', 'class' => 'searchForm')); ?>

    <?php foreach($searchs as $search): ?>
    	<?php $param = isset($this->params['url'][$search]) ? $this->params['url'][$search] : null; ?>
    	<?= $this->Form->input($search, array('label' => false, 'value' => $param, 'type' => 'text', 'placeholder' => __($search))); ?>
	<?php endforeach; ?>

    <?= $this->Form->submit('Filtrar', array('class' => 'button')); ?>
    <?= $this->Form->end(); ?>
</div>
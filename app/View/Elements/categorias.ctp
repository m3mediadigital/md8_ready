﻿<h1>Categorias <i class="ico-categorias"></i></h1>
<div class="menuWS">
  <?php foreach($categories as $category): ?>
  <ul>
    <li><?php echo $this->Html->link($category['Category']['category'], DS."produtos".DS.Inflector::slug(strtolower($category['Category']['category']),"-").DS.$category['Category']['id']) ?>
      <ul>
        <?php foreach($category['Subcategory'] as $subcategory): ?>
        <li><?php echo $this->Html->link($subcategory['subcategory'], DS."produtos".DS.Inflector::slug(strtolower($category['Category']['category']),"-").DS.Inflector::slug(strtolower($subcategory['subcategory']),"-").DS.$subcategory['id']) ?></li>
        <?php endforeach; ?>
      </ul>
    </li>
  </ul>
  <?php endforeach; ?>
</div>

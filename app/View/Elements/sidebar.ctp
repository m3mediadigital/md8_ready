<h1>Categorias <i class="ico-categorias"></i></h1>

<ul>
	<li><?php echo $this->Html->link('Sacos Plásticos', array('action' => '#')) ?></a>
	<li><?php echo $this->Html->link('Caixas', array('action' => '#')) ?></a>
	<li><?php echo $this->Html->link('Colas', array('action' => '#')) ?></a>
	<li><?php echo $this->Html->link('Etiquetas', array('action' => '#')) ?></a>
	<li><?php echo $this->Html->link('Filmes Técnicos', array('action' => '#')) ?></a>
	<li><?php echo $this->Html->link('Fitas Adesivas', array('action' => '#')) ?></a>
	<li><?php echo $this->Html->link('Máquinas', array('action' => '#')) ?></a>
	<li><?php echo $this->Html->link('Tintas', array('action' => '#')) ?></a>
</ul>

<h1>Atendimento <i class="ico-atendimento"></i></h1>

<h3><i class="ico-chat"></i> <?php echo $this->Html->link('Chat Online', array('action' => '#')) ?></h3>
<h2><small>(84)</small> 3213-2264</h2>

<h1>Orçamento <i class="ico-solcita"></i></h1>
<?php echo $this->Html->link('Você solicita um orçamento sem compromisso de acordo com suas necessidades!', array('action' => '#orcamento')) ?>

<h1>Newsletter <i class="ico-mail"></i></h1>
<p>Cadastre-se e receba em seu e-mail notícias e promoções.</p>
    
<?php //echo $this->Session->flash(); ?>    
<?php echo $this->Form->create('Newsletter', array('url' => '/newsletters/add' ,'class' => 'formNews')) ?>
	
	<?php echo $this->Form->input('email', array('label' => false, 'div' => false, 'placeholder' => 'digite seu e-mail')) ?>
	
	<?php echo $this->Form->submit('ok', array('div' => false)) ?>
<?php echo $this->Form->end() ?>

<h1>Localização <i class="ico-localizacao"></i></h1>
<iframe width="200" height="200" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Rua+C%C3%ADcero+de+Azevedo,+Natal+-+Rio+Grande+do+Norte&amp;aq=0&amp;oq=Rua+Cicero+Azevedo&amp;sll=-5.882144,-35.173365&amp;sspn=0.0108,0.021136&amp;g=Rua+Vereador+Manoel+S%C3%A1tiro,+Natal+-+Rio+Grande+do+Norte&amp;ie=UTF8&amp;hq=&amp;hnear=R.+C%C3%ADcero+de+Azevedo+-+Lagoa+Seca,+Natal+-+Rio+Grande+do+Norte,+59031-020&amp;t=m&amp;ll=-5.801058,-35.211954&amp;spn=0.017078,0.01708&amp;z=14&amp;output=embed"></iframe>
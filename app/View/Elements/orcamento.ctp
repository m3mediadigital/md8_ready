﻿<h1>Orçamento <i class="ico-solcita"></i></h1>
<?php echo $this->Html->link('Você solicita um orçamento sem compromisso de acordo com suas necessidades!', '/produtos', array('escape' => false)) ?>

<div class="clear"></div>

<!-- ORÇAMENTO -->
<div id="orcamento" style="display:none;">

    <?php //echo $this->Session->flash(); ?> 
    <?php echo $this->Form->create('Page', array( 'url' => array( 'action' => 'agendamento' ), 'class' => 'formPrettyphoto')) ?>
    <h1>Orçamento</h1>
    
    <div class="l315">
        <?php echo $this->Form->input('nome', array('div' => false, 'label' => 'Nome*:')) ?>
    </div>
    <div class="r315">
        <?php echo $this->Form->input('email', array('div' => false, 'label' => 'E-mail*:', 'placeholder' => 'seuemail@seuprovedor.com.br')) ?>
    </div>
    <div class="l315">
        <?php echo $this->Form->input('telefone', array('label' => 'Telefone:', 'div' => false, 'class' => 'phone', 'placeholder' => '(XX) XXXX-XXXX')) ?>
    </div>
    <div class="l640">
        <?php echo $this->Form->input('comentario', array('name' =>'data[Page][comentario]', 'label' => 'Mensagem:', 'div' => false, 'type' => 'textarea')) ?>
    </div>
    
    <div class="l640">
        <?php echo $this->Form->submit('Enviar', array('div' => false)) ?>
    </div>
    <?php echo $this->Form->end() ?>
</div>

<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Categories'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Category.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Category.id'))); ?>
                <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Category.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Category.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('Category', array('type'=>'file'));?>
        	<?= $this->Form->input('id');?>
	<?= $this->Form->input('category', array( 'type' => 'text' ));?>
        <?= $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('Category.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Category.id'))); ?>
                <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Category.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Category.id'))); ?>
            </li>
            </ul>
</div>
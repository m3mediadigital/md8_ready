<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Banners'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Banner.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Banner.id'))); ?>
                <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Banner.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Banner.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('Banner', array('type'=>'file'));?>
        	<?= $this->Form->input('id');?>
	<?= $this->Form->input('banner', array( 'type' => 'file' ));?>
    <div class="input">
        <h5><p>Banner atual</p></h5>
        <?php echo $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $this->request->data['Banner']['banner'], array('w' => 240, 'h' => 240, 'zc' => 1)), '/uploads/' . $this->request->data['Banner']['banner'], array('escape' => false, 'class' => 'fancybox')); ?>
    </div>
	<?= $this->Form->input('title');?>
	<?= $this->Form->input('url');?>
        <?= $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('Banner.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Banner.id'))); ?>
                <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Banner.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Banner.id'))); ?>
            </li>
            </ul>
</div>
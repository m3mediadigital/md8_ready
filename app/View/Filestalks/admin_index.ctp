<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Filestalks'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Adicionar'), array('action'=>'add')); ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <?php echo $this->Form->create('Filestalk', array('url' => array('action' => 'allActionsListData'))); ?>
        <table>
            <thead>
                <tr>
                    <th><input type="checkbox" class="checkall"/></th>
                    		<th><?php echo $this->Paginator->sort('id');?></th>
                                        		<th><?php echo $this->Paginator->sort('talk_id');?></th>
                                        		<th><?php echo $this->Paginator->sort('user_id');?></th>
                                        		<th><?php echo $this->Paginator->sort('title');?></th>
                                        		<th><?php echo $this->Paginator->sort('file');?></th>
                                        		<th><?php echo $this->Paginator->sort('description');?></th>
                                                                                <th class="actions">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                    foreach ($filestalks as $filestalk):
                        $class = null;
                        if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                        }
                ?>
                <tr>
                    <td>
                        <?= $this->Form->input('Filestalk.id.'.$filestalk['Filestalk']['id'],array('value'=>$filestalk['Filestalk']['id'] ,'type'=>'checkbox','label'=>false, 'div'=>false))?>
                    </td>
		<td>
			<?= $this->Html->link($filestalk['Filestalk']['id'], array('action'=>'edit', $filestalk['Filestalk']['id'])); ?>
		</td>
	<td>
		<?= $this->Html->link($filestalk['Talk']['name'], array('controller' => 'talks', 'action' => 'view', $filestalk['Talk']['id'])); ?>
	</td>
	<td>
		<?= $this->Html->link($filestalk['User']['name'], array('controller' => 'users', 'action' => 'view', $filestalk['User']['id'])); ?>
	</td>
		<td>
			<?= $this->Html->link($filestalk['Filestalk']['title'], array('action'=>'edit', $filestalk['Filestalk']['id'])); ?>
		</td>
		<td>
			<?= $this->Html->link($filestalk['Filestalk']['file'], array('action'=>'edit', $filestalk['Filestalk']['id'])); ?>
		</td>
		<td>
			<?= $this->Html->link($filestalk['Filestalk']['description'], array('action'=>'edit', $filestalk['Filestalk']['id'])); ?>
		</td>

                    <td class="actions">
                        <ul>
      <?php if(!empty($addImages)): ?>
                            <li>
                                <?php echo $this->Html->link('Anexar Imagens', array('controller' => 'photos', 'action' => 'anexar', $filestalk['Filestalk']['id'], '?' => array('tabela' => 'filestalks')), array('escape' => false, 'class' => 'bt-anexar')); ?>
                            </li>
                            <?php endif; ?>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-03.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'view', $filestalk['Filestalk']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-04.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'edit', $filestalk['Filestalk']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-05.jpg', array('style' => 'width:29px;margin:0;padding:0;')), '#', array('escape' => false, 'class' => 'deleteButtonRow', 'rel' => $filestalk['Filestalk']['id'])); ?>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}')));?></p>
        <?php if(count($filestalks)>0): ?>        <div class="acoesMultiplas">
            <button type="submit" name="data[Filestalk][action]" value="delete" class="bt-deletar">Remover selecionados</button>

            <button name="data[Filestalk][action]" value="activate" class="bt-ativar">Ativar selecionados</button>
            <button name="data[Filestalk][action]" value="deactivate" class="bt-desativar">Desativar selecionados</button>
        </div>
        <?php endif; ?>        <div class="pagination">
            <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape'=>false), null, array('class' => 'prev disabled'));?>
            <?php echo $this->Paginator->numbers(array('separator' => ' '));?>
            <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape'=>false), null, array('class' => 'next disabled'));?>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?><div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action'=>'add')); ?>
        </li>
    </ul>
</div>
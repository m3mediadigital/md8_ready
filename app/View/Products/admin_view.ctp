<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Products'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Product.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Product.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		<td width="180"><h4><?php echo __('Id'); ?></h4></td>
		<td>
			<?php echo $product['Product']['id']; ?></td>
</tr><tr>		<td><h4><?php echo __('Title'); ?></h4></td>
		<td>
			<?php echo $product['Product']['title']; ?></td>
</tr><tr>		<td><h4><?php echo __('Description'); ?></h4></td>
		<td>
			<?php echo $product['Product']['description']; ?></td>
</tr><tr>		<td><h4><?php echo __('Value'); ?></h4></td>
		<td>
			<?php echo number_format( $product['Product']['value'], 2,',','.' ); ?></td>  
</tr><tr>		<td><h4><?php echo __('Tags'); ?></h4></td>
		<td>
			<?php echo $product['Product']['tags']; ?></td>
</tr><tr>		<td><h4><?php echo __('Cover'); ?></h4></td>
		<td>
            <?php if ( $product['Product']['cover'] ): ?>
                <?php echo $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $product['Product']['cover'], array('w' => 120, 'h' => 120, 'zc' => 1)), '/uploads/' . $product['Product']['cover'], array('escape' => false, 'class' => 'fancybox')); ?>
            <?php else: ?> 
                <?php echo $this->Html->link($this->PhpThumb->thumbnail('/img/image-not-found.jpg', array('w' => 120, 'h' => 120, 'zc' => 1)), '/img/image-not-found.jpg', array('escape' => false, 'class' => 'fancybox')); ?>            
            <?php endif ?>
        </td>
</tr><tr>		<td><h4><?php echo __('Album'); ?></h4></td>
		<td>
			<?php echo $this->Html->link($product['Album']['title'], array('controller' => 'albums', 'action' => 'view', $product['Album']['id'])); ?></td>
</tr><tr>		<td><h4><?php echo __('Video'); ?></h4></td>
		<td>
			<?php echo $this->ExtensionHtml->youtube( $product['Product']['video'] ); ?></td>
</tr><tr>		<td><h4><?php echo __('Created'); ?></h4></td>
		<td>
			<?php echo $this->Time->format( $product['Product']['created'] ); ?></td>
</tr><tr>		<td><h4><?php echo __('Modified'); ?></h4></td>
		<td>
			<?php echo $this->Time->format( $product['Product']['modified'] ); ?></td>
</tr>                </tbody>
            </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Product.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Product.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?>
            </li>
            </ul>
</div>

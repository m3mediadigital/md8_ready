<?php // pr( $categoriesSelectedsAr) ?>
<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Products'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('Product.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?>
                <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Product.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('Product', array('type'=>'file'));?>
            <?= $this->Form->input('id');?>
    <?= $this->Form->input('title');?>
    <?= $this->Form->input('description');?>
    <?= $this->Form->input('value', array('type' => 'text', 'id' => 'value' ));?>
        <script type="text/javascript">
            // Mascra para dinheiro
             $("#value").maskMoney({
                showSymbol:false, 
                thousands:'', 
                decimal:'.', 
                symbolStay: true
            });            
        </script>
    <?= $this->Form->input('tags', array( 'class' => 'ckremover'));?>
    <?= $this->Form->input('cover', array( 'type' => 'file' ));?>

    <div class="input">
        <p><h4>Imagem atual.</h4></p>
        <?php if ( $this->request->data['Product']['cover'] ): ?>
        <?php echo $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $this->request->data['Product']['cover'], array('w' => 200, 'h' => 200, 'zc' => 1)), '/uploads/' . $this->request->data['Product']['cover'], array('escape' => false, 'class' => 'fancybox')); ?>
        <?php else: ?> 
            <?php echo $this->Html->link($this->PhpThumb->thumbnail('/img/image-not-found.jpg', array('w' => 200, 'h' => 200, 'zc' => 1)), '/img/image-not-found.jpg', array('escape' => false, 'class' => 'fancybox')); ?>            
        <?php endif ?>
    </div>
        
    <?= $this->Form->input('album_id', array( 'empty' => 'Escolha um Album' ));?>
    <?= $this->Form->input('video');?>

     <?php $categoryList = array(); ?>
    <?php foreach ($categories as $category): ?>                                 
            <?php $categoryList['Category.'.$category['Category']['id']] = $category['Category']['category']; ?>
            <?php foreach ($category['Subcategory'] as $subcategory): ?>               
                    <?php $categoryList['Subcategory.'.$subcategory['id']] = $category['Category']['category'].' -> '.$subcategory['subcategory']; ?>            
            <?php endforeach ?>
    <?php endforeach ?>                 

    <?php if ( !empty( $categoriesSelecteds ) ): ?>        
            <div class="input">
                <h5>Categorias relacionadas á este produto.</h5>
            </div>
        <?php foreach ($categoriesSelecteds as $key => $value): ?>
            <div class="input">
                <h6><?php echo $value.' - '.$this->Html->link( $this->Html->image( 'remover.png', array( 'width' => '12px' )), array( 'action' => 'deletecategory', $key ), array( 'escape' => false ) ) ?></h6>
            </div>
        <?php endforeach ?>

    <?php else: ?>
        <div class="input"><h5>Não a categorias relacionadas a esta produto!</h5></div>
    <?php endif ?>

    <!-- Remove da lista os iten que já estão relacionados ao produto -->
    <?php foreach ($categoriesSelectedsAr as $key => $value): ?>
        <?php unset($categoryList[$key]) ?>
    <?php endforeach ?>

    <?php echo $this->Form->input('Parentcategory', array( 'label' => 'Adicionar categoria', 'type' => 'select', 'multiple' => true, 'options' => $categoryList ) ); ?>
    
   
   <?= $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('Product.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?>
                <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Product.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?>
            </li>
            </ul>
</div>
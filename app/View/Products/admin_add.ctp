<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Products'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('Product', array('type'=>'file'));?>
        	<?= $this->Form->input('title');?>
	<?= $this->Form->input('description');?>
    <?= $this->Form->input('value', array( 'type' => 'text', 'id' => 'value' ));?>
    <script type="text/javascript">
            // Mascra para dinheiro
             $("#value").maskMoney({
                showSymbol:false, 
                thousands:'', 
                decimal:'.', 
                symbolStay: true
            });            
    </script>
	<?= $this->Form->input('tags', array( 'class' => 'ckremover' ));?>
	<?= $this->Form->input('cover', array( 'type' => 'file' ));?>
	<?= $this->Form->input('album_id', array( 'empty' => 'Escolha um Album' ));?>
	<?= $this->Form->input('video');?>

   
        <?php $categoryList = array(); ?>
        <?php foreach ($categories as $category): ?>          

            <?php $categoryList['Category.'.$category['Category']['id']] = $category['Category']['category']; ?>

            <?php foreach ($category['Subcategory'] as $subcategory): ?>
            <?php $categoryList['Subcategory.'.$subcategory['id']] = $category['Category']['category'].' -> '.$subcategory['subcategory']; ?>
                <?php // pre( $subcategory ) ?>
            <?php endforeach ?>

        <?php endforeach ?>         

        <?php // pre( $categoryList ) ?>

    <?php echo $this->Form->input('Parentcategory', array('type' => 'select', 'multiple' => true, 'options' => $categoryList)); ?>
   


        <?= $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
            </ul>
</div>
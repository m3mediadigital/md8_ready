<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Mudança de senha'); ?></h1>
<div class="bloc users clear">
    <div class="content">
        <?php echo $this->Form->create('User', array('type' => 'file')); ?>
        <?= $this->Form->submit('Enviar') ?>
        <br/>
        <?php
        echo '<div class="input text required">';
        echo $this->Form->input('atual', array('label'=>'Senha atual', 'value'=>'', 'type'=>'password', 'div'=>false));
        echo '</div>';
        echo $this->Form->input('password', array('label'=>'Nova senha', 'value'=>''));
        echo '<div class="input text required">';
        echo $this->Form->input('password2', array('label'=>'Repita a nova senha', 'div'=>false, 'value'=>'', 'type'=>'password'));
        echo '</div>';
        ?>
        <?php echo $this->Form->end(__('Submit', true)); ?>
    </div>
</div>
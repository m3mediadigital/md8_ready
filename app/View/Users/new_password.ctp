<?= $this->Html->image('/coreadmin/img/logo.png', array('class' => 'logoAdmin')) ?>
<h1><?= $this->Html->image('/coreadmin/img/icons/lock-closed.png') ?> Recuperação de senha</h1>
<?php echo $this->Form->create('User'); ?> 
<?php echo $this->Session->flash(); ?>
<div class="input placeholder">
    <label for="login">Nova senha</label>
    <?= $this->Form->input('senha', array('label' => false, 'div' => false, 'type'=>'password')); ?>
</div>
<div class="input placeholder">
    <label for="pass">Repita a nova senha</label>
    <?= $this->Form->input('senha2', array('label' => false, 'div' => false, 'type'=>'password')); ?>
</div>
<div class="submit">
    <?= $this->Form->submit('Mudar', array('label' => false, 'div' => false)); ?>
</div>
<?php echo $this->Form->end() ?>
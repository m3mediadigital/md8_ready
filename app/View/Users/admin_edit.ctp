<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Users'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('User.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?>
                <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('User', array('type'=>'file'));?>
        	<?= $this->Form->input('id');?>
	<?= $this->Form->input('name');?>
	<?= $this->Form->input('login');?>
	<?= $this->Form->input('password');?>
	<?= $this->Form->input('active');?>
	<?= $this->Form->input('last_login');?>
	<?= $this->Form->input('password_recovery');?>
	<?= $this->Form->input('group_id');?>
        <?= $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?= $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png'), array('action' => 'delete', $this->Form->value('User.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?>
                <?= $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?>
            </li>
            </ul>
</div>
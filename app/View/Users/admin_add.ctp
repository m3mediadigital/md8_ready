<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Users'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('User', array('type'=>'file'));?>
        	<?= $this->Form->input('name');?>
	<?= $this->Form->input('login');?>
	<?= $this->Form->input('password');?>
	<?= $this->Form->input('active', array('checked'=>'checked'));?>
	<?= $this->Form->input('last_login');?>
	<?= $this->Form->input('password_recovery');?>
	<?= $this->Form->input('group_id');?>
        <?= $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
            </ul>
</div>
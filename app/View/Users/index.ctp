
<?php echo $this->element('cliente_admin_site_base') ?>

<div id="internas">

   <h1>
        Meus Dados
        <span style='float: right;' >
            <?= $this->Html->link('Voltar', array('controller' => 'profiles', 'action' => 'index')); ?>
        </span>
    </h1>


<div class="bloc clear">
    <div class="content">


       <?php //echo $this->Session->flash(); ?>    
       <?php echo $this->Form->create('User', array('class' => 'formContato')) ?>
       <?= $this->Form->input('id', array('style' => 'display:none;', 'label' => false, 'value' => $user['User']['id'])) ?>
       <?= $this->Form->input('cpfcasal', array('style' => 'display:none;', 'label' => false)) ?>
       <div class="l365">
        <?php echo $this->Form->input('Profile.full_name', array('label' => 'Nome Completo:*', 'div' => false, 'value' => $user['Profile']['full_name'])) ?>
    </div>
    <div class="r365">
        <?php echo $this->Form->input('Profile.corporate_name', array('label' => 'Razão Social:*', 'div' => false, 'value' => $user['Profile']['corporate_name'])) ?>
    </div>
    <div class="l365">
        <?php echo $this->Form->input('Profile.cnpj', array('label' => 'CNPJ:*', 'div' => false, 'value' => $user['Profile']['cnpj'], 'onkeypress' => 'script: mascaraMutuario(this,cpfCnpj);', 'maxlength' => 18 )) ?>           
    </div>
    <div class="r365">
        <?php echo $this->Form->input('Profile.cpf', array('label' => 'CPF:', 'div' => false, 'value' => $user['Profile']['cpf'], 'onkeypress' => 'script: mascaraMutuario(this,cpfCnpj);', 'maxlength' => 14)) ?>
    </div>
    <div class="l365">
        <?php echo $this->Form->input('Profile.full_address', array('label' => 'Endereço Completo:', 'div' => false, 'value' => $user['Profile']['full_address'])) ?>
    </div>
    <div class="r365">
        <?php echo $this->Form->input('Profile.cep', array('label' => 'CEP:', 'div' => false, 'value' => $user['Profile']['cep'], 'onkeypress' => 'script: mascaraMutuario(this,cep);', 'maxlength' => 10)) ?>
    </div>
    <div class="l365">
        <?php echo $this->Form->input('Profile.city', array('label' => 'Cidade:', 'div' => false, 'value' => $user['Profile']['city'])) ?>
    </div>
    <div class="r365">
        <?php echo $this->Form->input('Profile.uf', array('label' => 'UF:', 'div' => false, 'value' => $user['Profile']['uf'])) ?>
    </div>
    <div class="l365">
        <?php echo $this->Form->input('Profile.primary_phone', array('label' => 'Telefone 01:*', 'div' => false, 'value' => $user['Profile']['primary_phone'], 'class' => 'phone', 'placeholder' => '(XX) XXXX-XXXX')) ?>
    </div>
    <div class="r365">
        <?php echo $this->Form->input('Profile.secondary_phone', array('label' => 'Telefone 02:', 'div' => false, 'value' => $user['Profile']['secondary_phone'], 'class' => 'phone', 'placeholder' => '(XX) XXXX-XXXX')) ?>
    </div>
    <div class="l365">
        <?php echo $this->Form->input('Profile.fax', array('label' => 'Fax:', 'div' => false, 'value' => $user['Profile']['fax'], 'class' => 'phone', 'placeholder' => '(XX) XXXX-XXXX')) ?>
    </div>
    <div class="r365" id="divBeforePass">
        <?php echo $this->Form->input('Profile.email', array('label' => 'E-mail:*', 'div' => false, 'value' => $user['Profile']['email'], 'placeholder' => 'seuemail@seuprovedor.com.br', 'class' => 'immutable')) ?>
    </div>
    <div class="l365 pass">
        <?php echo $this->Form->input('password', array('label' => 'Nova Senha:*', 'div' => false, 'type' => 'password')) ?>
    </div>
    <div class="r365 pass">
        <?php echo $this->Form->input('password_confirmation', array('label' => 'Confirmar Nova Senha:*', 'div' => false, 'type' => 'password')) ?>
    </div>

    <div class="l760">
        <?php 
        echo $this->Form->input('receberNews', array('label' => 'Deseja receber novidades ?', 'checked' => $newsletter)); 
        ?>
        <?= $this->Html->link('Editar', array('controller' => 'users', 'action' => 'index', 1), array('class' => array('button','edit') ) ); ?>
        <?php echo $this->Form->submit('Enviar', array('div' => false)) ?>
    </div>

    <?php echo $this->Form->end() ?>


</div>
</div>


</div>

<script type="text/javascript">

$(document).ready(function(){

    // Ajuste de layout
    $("#internas form#UserIndexForm").css("width", "740px");
    $("#internas div.l760").css("width", "737px");
    $("#internas input#UserReceberNews").attr("style","");
    $("#internas div", $("div.checkbox")).css({"float": "left", "margin-top": "2px"});
    $("#internas label", $("#internas div.checkbox")).css({"float": "left", "width": "200px"});

    //$("#internas input.immutable").attr("disabled", "disabled").css('background-color', '#ddd');

    var passDivs = $("#internas div.pass").remove();
    <?php if( isset($edit) && $edit ){ ?>

        if( history.length > 0 ){
            $("#internas h1 a:first").attr("href", "javascript: history.go(-1);");
        }

        $("#internas a.edit")
            .css({'float': 'right', 'height': '17px', 'text-decoration': 'none'})
            .html("Mudar senha?")
            .click(function(e){
                e.preventDefault();
                if( passDivs == null ){
                    passDivs = $("div.pass").remove();
                    $(this).html("Mudar senha?");
                }else{
                    $("#divBeforePass").after(passDivs);
                    passDivs = null;
                    $(this).html("Não mudar senha!")
                } //$(this).hide();
            });
    <?php }else{ ?>
        $("#internas input")
            .attr("disabled", "disabled")
            .css('background-color', '#ddd');
        $("#internas input[type='submit']").hide();
    <?php } ?>

});

</script>
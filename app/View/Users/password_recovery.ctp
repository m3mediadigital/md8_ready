<?= $this->Html->image('/coreadmin/img/logo.png', array('class' => 'logoAdmin')) ?>
<h1>Recuperação de senha</h1>
<p style="text-align: center; margin-top: 10px;">Informe o seu login ou o seu email para recuperar a senha!</p>
<?php echo $this->Form->create('User'); ?> 
<?php echo $this->Session->flash(); ?>
<div class="input placeholder">
    <label for="email">Login</label>
    <?= $this->Form->input('loginrec', array('label' => false, 'div' => false)); ?>
</div>
<div class="input placeholder">
    <label for="email">Email</label>
    <?= $this->Form->input('Profile.emailrec', array('label' => false, 'div' => false)); ?>
</div>
<div class="submit">
    <?= $this->Form->submit('Recuperar', array('label' => false, 'div' => false)); ?>
</div>
<?php echo $this->Form->end() ?>
<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Users'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('User.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?>
            </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		<td><h4><?php echo __('Id'); ?></h4></td>
		<td>
			<?php echo $user['User']['id']; ?></td>
</tr><tr>		<td><h4><?php echo __('Name'); ?></h4></td>
		<td>
			<?php echo $user['User']['name']; ?></td>
</tr><tr>		<td><h4><?php echo __('Login'); ?></h4></td>
		<td>
			<?php echo $user['User']['login']; ?></td>
</tr><tr>		<td><h4><?php echo __('Password'); ?></h4></td>
		<td>
			<?php echo $user['User']['password']; ?></td>
</tr><tr>		<td><h4><?php echo __('Active'); ?></h4></td>
		<td>
			<?php echo $user['User']['active']; ?></td>
</tr><tr>		<td><h4><?php echo __('Last Login'); ?></h4></td>
		<td>
			<?php echo $user['User']['last_login']; ?></td>
</tr><tr>		<td><h4><?php echo __('Password Recovery'); ?></h4></td>
		<td>
			<?php echo $user['User']['password_recovery']; ?></td>
</tr><tr>		<td><h4><?php echo __('Created'); ?></h4></td>
		<td>
			<?php echo $user['User']['created']; ?></td>
</tr><tr>		<td><h4><?php echo __('Modified'); ?></h4></td>
		<td>
			<?php echo $user['User']['modified']; ?></td>
</tr><tr>		<td><h4><?php echo __('Group'); ?></h4></td>
		<td>
			<?php echo $this->Html->link($user['Group']['name'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); ?></td>
</tr>                </tbody>
            </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
                    <li>
                <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title'=>'Remover')), array('action' => 'delete', $this->Form->value('User.id')), array('escape'=>false), __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?>
            </li>
            </ul>
</div>
</br>
</br>
</br>
</br>
</br>


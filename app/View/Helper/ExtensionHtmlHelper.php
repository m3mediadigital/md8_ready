<?php

App::uses('Html', 'View/Helper');

class ExtensionHtmlHelper extends AppHelper {

	public $helpers = array("Html");

	public function codeVideoYoutube($linkVideo) {
		preg_match("/([^\s]+)v=([a-z0-9_-]+)/i", $linkVideo, $match);
		return $match[2];
	}

/**
 * Esta funcao retorna a tag iframe ja formata para exibir um video do youtube
 *
 * @return string
 * @param mixed $linkVideo recebe o link completo do video do youtube
 * @author wescley matos
 */
	public function youtube($linkVideo, $width=420, $height=315) {
		$codeVideo = $this->codeVideoYoutube($linkVideo);
		return sprintf('<iframe width="%s" height="%s" src="http://www.youtube.com/embed/%s?rel=0&wmode=transparent" frameborder="0" allowfullscreen></iframe>', $width, $height, $codeVideo);
	}

/**
 * Esta funcao retorna a tag image ou a url da imagem de um video do youtube
 *
 * @return string
 * @param mixed $linkVideo recebe o link completo do video do youtube
 * @param mixed $numberImage recebe o valor da imagem
 * @param bool $tagImage recebe o boleano se será retornado a url com a tag image ou não
 * @author wescley matos
 */
	public function urlImageVideoYoutube($linkVideo, $numberImage = 0, $tagImage = false) {
		$codeVideo = $this->codeVideoYoutube($linkVideo);

		$urlImage = sprintf('http://img.youtube.com/vi/%s/%s.jpg', $codeVideo, $numberImage);
		if ($tagImage === true) {
			return $this->Html->image($urlImage);
		}

		return $urlImage;
	}

/**
 * Esta funcao retorna a tag video do html5 
 *
 * @return string
 * @param mixed $data recebe o nome completo do video a partir do banco de dados
 * @author wescley matos
 */
	public function video($data, $width=420, $height=315) {
		return sprintf('<video controls data-setup=\'{"example_option":true}\' width="%s" height="%s" class="video-js vjs-default-skin" preload="auto"><source src="%s" type="video/mp4" /></video>', $width, $height, (Router::url('/', true) . "uploads/videos/" . $data));
	}

/**
 * Esta funcao retorna Sim ou Não para o atributo active
 *
 * @return string
 * @param int $valueActive
 * @author wescley matos
 */
	public function isActive($valueActive = 0) {
		return (intval($valueActive)) ? "Sim" : "Não";
	}

}
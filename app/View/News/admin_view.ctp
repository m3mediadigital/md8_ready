<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('News'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title' => 'Voltar')), array('action' => 'index'), array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action' => 'index')); ?>
        </li>
        <li>
            <?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title' => 'Remover')), array('action' => 'delete', $this->Form->value('News.id')), array('escape' => false), __('Are you sure you want to delete # %s?', $this->Form->value('News.id'))); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('News.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('News.id'))); ?>
        </li>
    </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <div>
            <table class="noalt">
                <tbody>
                    <tr>		
                        <td><h4><?php echo __('Id'); ?></h4></td>
                        <td><?php echo $news['News']['id']; ?></td>
                    </tr>
                    <tr>		
                        <td><h4><?php echo __('Title'); ?></h4></td>
                        <td><?php echo $news['News']['title']; ?></td>
                    </tr>
                    <tr>		
                        <td><h4><?php echo __('Publication'); ?></h4></td>
                        <td><?php echo $news['News']['publication']; ?></td>
                    </tr>
                    <tr>        
                        <td><h4><?php echo __('Text'); ?></h4></td>
                        <td><?php echo $news['News']['text']; ?></td>
                    </tr>
                    <tr>		
                        <td><h4><?php echo __('image'); ?></h4></td>
                        <td>
                            <?php if ( !$news['News']['image'] ): ?>

                                <?php echo $this->Html->link($this->PhpThumb->thumbnail('/img/image-not-found.jpg', array('w' => 240, 'h' => 240, 'zc' => 1)), '/img/image-not-found.jpg', array('escape' => false, 'class' => 'fancybox')); ?>
                            <?php else: ?>

                                <?php if( file_exists( APP.'webroot/uploads/'.$news['News']['image'] ) ): ?>
                                    <?php echo $this->Html->link($this->PhpThumb->thumbnail('/uploads/' . $news['News']['image'], array('w' => 240, 'h' => 240, 'zc' => 1)), '/uploads/' . $news['News']['image'], array('escape' => false, 'class' => 'fancybox')); ?>
                                <?php else: ?>
                                    <?php echo $this->Html->link($this->PhpThumb->thumbnail('/img/image-not-found.jpg', array('w' => 240, 'h' => 240, 'zc' => 1)), '/img/image-not-found.jpg', array('escape' => false, 'class' => 'fancybox')); ?>
                                <?php endif ?>

                            <?php endif ?>
                        </td>
                    </tr>
                    <tr>		
                        <td><h4><?php echo __('Source'); ?></h4></td>
                        <td><?php echo $news['News']['source']; ?></td>
                    </tr>
                    <tr>		
                        <td><h4><?php echo __('Active'); ?></h4></td>
                        <td><?php echo ( $news['News']['active'] )? 'Ativo' : 'Inativo' ; ?></td>
                    </tr>
                    <tr>		
                        <td><h4><?php echo __('Created'); ?></h4></td>
                        <td><?php echo $this->Time->format( $news['News']['created'] ); ?></td>
                    </tr>
                    <tr>		
                        <td><h4><?php echo __('Modified'); ?></h4></td>
                       <td><?php echo $this->Time->format( $news['News']['modified'] ); ?></td>
                    </tr>                
                </tbody>
            </table>
        </div>
        <div class="cb"></div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title' => 'Voltar')), array('action' => 'index'), array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Back'), array('action' => 'index')); ?>
        </li>
        <li>
<?php echo $this->Form->postLink($this->Html->image('/coreadmin/img/icons/remover.png', array('title' => 'Remover')), array('action' => 'delete', $this->Form->value('News.id')), array('escape' => false), __('Are you sure you want to delete # %s?', $this->Form->value('News.id'))); ?>
<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('News.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('News.id'))); ?>
        </li>
    </ul>
</div>
</br>
</br>
</br>
</br>
</br>


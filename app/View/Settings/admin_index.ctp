<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Settings'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title' => 'Adicionar')), array('action' => 'add'), array('escape' => false)); ?>
            <?php echo $this->Html->link(__('Adicionar'), array('action' => 'add')); ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <?= $this->Form->create('Setting', array('id' => 'formList')); ?>
        <table>
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" class="checkall"/>
                    </th>
                    <th>
                        <?php echo $this->Paginator->sort('id'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('name'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('value'); ?>
                    </th>

                    <th>
                        <?php echo $this->Paginator->sort('active'); ?>
                    </th>
                    <th class="actions">
                        Ações
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($settings as $setting):
                    $class = null;
                    if ($i++ % 2 == 0) {
                        $class = ' class="altrow"';
                    }
                    ?>
                    <tr>
                        <td>
                            <?= $this->Form->input('Setting.ids.' . $setting['Setting']['id'], array('value' => $setting['Setting']['id'], 'type' => 'checkbox', 'label' => false, 'div' => false)) ?>
                        </td>
                        <td>
                            <?= $this->Html->link($setting['Setting']['id'], array('action' => 'edit', $setting['Setting']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($setting['Setting']['name'], array('action' => 'edit', $setting['Setting']['id'])); ?>
                        </td>
                        <td>
                            <?= $this->Html->link($setting['Setting']['value'], array('action' => 'edit', $setting['Setting']['id'])); ?>
                        </td>
                        <?php if ($setting['Setting']['active'] == 1): ?>
                            <td class="changeState" model="Setting" id="<?= $setting['Setting']['id'] ?>">
                                <?= $this->Html->image('/coreadmin/img/icons/ativar.png', array('title' => 'Ativo', 'class' => 'ListImage')) ?>
                            </td>
                        <?php else: ?>
                            <td class="changeState" model="Setting" id="<?= $setting['Setting']['id'] ?>">
                                <?= $this->Html->image('/coreadmin/img/icons/desativar.png', array('title' => 'Desativado', 'class' => 'ListImage')) ?>
                            </td>
                        <?php endif; ?>

                        <td class="actions">
                            <ul>
                                <li>
                                    <?php echo $this->Html->link('<button class="bt-editar">Editar</button>', array('action' => 'edit', $setting['Setting']['id']), array('escape' => false)); ?>
                                </li>
                                <li>
                                    <?php echo $this->Form->postLink('<button class="bt-deletar">Remover</button>', array('action' => 'delete', $setting['Setting']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $setting['Setting']['id'])); ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}'))); ?></p>
        <?php if (count($settings) > 0): ?>   
            <div class="acoesMultiplas">
                <select name="data[Setting][action]" id="tableaction">
                    <option value="">Ações</option>
                    <option value="excluir">Excluir</option>
                    <option value="ativar">Ativar</option>
                    <option value="desativar">Desativar</option>
                </select>
            </div>
        <?php endif; ?>
        <?= $this->Form->end(); ?>
        <div class="pagination">
            <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape' => false), null, array('class' => 'prev disabled')); ?>
            <?php echo $this->Paginator->numbers(array('separator' => ' ')); ?>
            <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape' => false), null, array('class' => 'next disabled')); ?>
        </div>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title' => 'Adicionar')), array('action' => 'add'), array('escape' => false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action' => 'add')); ?>
        </li>
    </ul>
</div>
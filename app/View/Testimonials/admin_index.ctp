<h1><?php echo $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?php echo __('Testimonials'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link(__('Adicionar'), array('action'=>'add')); ?>
        </li>
    </ul>
</div>
<div class="bloc clear listagem">
    <div class="content">
        <?php echo $this->Form->create('Testimonial', array('url' => array('action' => 'allActionsListData'))); ?>
        <table>
            <thead>
                <tr>
                    <th><input type="checkbox" class="checkall"/></th>
            		<th><?php echo $this->Paginator->sort('id');?></th>
            		<th><?php echo $this->Paginator->sort('name');?></th>
            		<th><?php echo $this->Paginator->sort('city');?></th>
            		<th><?php echo $this->Paginator->sort('state');?></th>
            		<th><?php echo $this->Paginator->sort('image');?></th>
                    <th class="actions">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $i = 0;
                    foreach ($testimonials as $testimonial):
                        $class = null;
                        if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                        }
                ?>
                <tr>
                    <td>
                        <?= $this->Form->input('Testimonial.id.'.$testimonial['Testimonial']['id'],array('value'=>$testimonial['Testimonial']['id'] ,'type'=>'checkbox','label'=>false, 'div'=>false))?>
                    </td>
            		<td>
            			<?= $this->Html->link($testimonial['Testimonial']['id'], array('action'=>'edit', $testimonial['Testimonial']['id'])); ?>
            		</td>
            		<td>
            			<?= $this->Html->link($testimonial['Testimonial']['name'], array('action'=>'edit', $testimonial['Testimonial']['id'])); ?>
            		</td>
            		<td>
            			<?= $this->Html->link($testimonial['Testimonial']['city'], array('action'=>'edit', $testimonial['Testimonial']['id'])); ?>
            		</td>
            		<td>
            			<?= $this->Html->link($testimonial['Testimonial']['state'], array('action'=>'edit', $testimonial['Testimonial']['id'])); ?>
            		</td>
            		<?php if (!empty($testimonial['Testimonial']['image'])): ?>
            		<td>
            			<?= $this->Html->link($this->PhpThumb->thumbnail('uploads/' . $testimonial['Testimonial']['image'], array('w' => 20, 'h' => 23, 'zc' => 1)), $this->PhpThumb->url('uploads/' . $testimonial['Testimonial']['image'], array('w' => 640, 'h' => 480, 'zc' => 1)), array('escape' => false, 'class' => 'fancybox')) ?>
            		</td>
            		<?php else: ?>
            		<td>
            			<?= $this->Html->image('/coreadmin/img/nopicture_list.jpg') ?>
            		</td>
            		<?php endif; ?>

                    <td class="actions">
                        <ul>
                            <?php if(!empty($addImages)): ?>
                            <li>
                                <?php echo $this->Html->link('Anexar Imagens', array('controller' => 'photos', 'action' => 'anexar', $testimonial['Testimonial']['id'], '?' => array('tabela' => 'testimonials')), array('escape' => false, 'class' => 'bt-anexar')); ?>
                            </li>
                            <?php endif; ?>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-03.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'view', $testimonial['Testimonial']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-04.jpg', array('style' => 'width:29px;margin:0;padding:0;')), array('action' => 'edit', $testimonial['Testimonial']['id']), array('escape' => false)); ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link($this->Html->image('jqueryui/ico-05.jpg', array('style' => 'width:29px;margin:0;padding:0;')), '#', array('escape' => false, 'class' => 'deleteButtonRow', 'rel' => $testimonial['Testimonial']['id'])); ?>
                            </li>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <p class="qtd"><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count}, do {:start} ao {:end}')));?></p>
        <?php if(count($testimonials)>0): ?>        <div class="acoesMultiplas">
            <button type="submit" name="data[Testimonial][action]" value="delete" class="bt-deletar">Remover selecionados</button>

            <button name="data[Testimonial][action]" value="activate" class="bt-ativar">Ativar selecionados</button>
            <button name="data[Testimonial][action]" value="deactivate" class="bt-desativar">Desativar selecionados</button>
        </div>
        <?php endif; ?>        <div class="pagination">
            <?php echo $this->Paginator->prev('<button class="bt-esquerda">Anterior</button>', array('escape'=>false), null, array('class' => 'prev disabled'));?>
            <?php echo $this->Paginator->numbers(array('separator' => ' '));?>
            <?php echo $this->Paginator->next('<button class="bt-direita">Próxima</button>', array('escape'=>false), null, array('class' => 'next disabled'));?>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?><div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?php echo $this->Html->link($this->Html->image('/coreadmin/img/icons/adicionar.png', array('title'=>'Adicionar')), array('action'=>'add'), array('escape'=>false)); ?>
            <?php echo $this->Html->link('Adicionar', array('action'=>'add')); ?>
        </li>
    </ul>
</div>
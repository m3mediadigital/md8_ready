<h1><?= $this->Html->image('/coreadmin/img/icons/lista.png') ?> <?= __('Testimonials'); ?></h1>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png', array('title'=>'Voltar')), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
            </ul>
</div>
<div class="bloc clear">
    <div class="content">
        <?= $this->Form->create('Testimonial', array('type'=>'file'));?>
        	<?= $this->Form->input('name');?>
	<?= $this->Form->input('text');?>
	<?= $this->Form->input('city');?>
	<?= $this->Form->input('state');?>
	<?= $this->Form->input('image', array('type'=>'file'));?>
        <?= $this->Form->end(__('Submit', true));?>
    </div>
</div>
<div id="shortcutInternal" class="bloc">
    <ul class="actionsList">
        <li>
            <?= $this->Html->link($this->Html->image('/coreadmin/img/icons/voltar.png'), array('action'=>'index'), array('escape'=>false)); ?>
            <?= $this->Html->link(__('Back'), array('action'=>'index')); ?>
        </li>
            </ul>
</div>
<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'pages', 'action' => 'index'));
Router::connect('/a-md8', array('controller' => 'pages', 'action' => 'amd8'));
Router::connect('/clientes', array('controller' => 'pages', 'action' => 'clientes'));
Router::connect('/produtos', array('controller' => 'pages', 'action' => 'produtos'));
//Router::connect('/produto_id', array('controller' => 'pages', 'action' => 'produto_id'));
Router::connect('/noticias', array('controller' => 'pages', 'action' => 'noticias'));
Router::connect('/noticias_id', array('controller' => 'pages', 'action' => 'noticias_id'));
Router::connect('/duvidas-frequentes', array('controller' => 'pages', 'action' => 'duvidasfrequentes'));
Router::connect('/contato', array('controller' => 'pages', 'action' => 'contato'));
Router::connect('/login', array('controller' => 'pages', 'action' => 'login'));
Router::connect('/cadastro', array('controller' => 'pages', 'action' => 'cadastro'));
Router::connect('/recuperarsenha', array('controller' => 'pages', 'action' => 'recuperarsenha'));
Router::connect('/finalizarpedido', array('controller' => 'pages', 'action' => 'finalizarpedido'));
Router::connect('/admin', array('controller' => 'dashboards', 'action' => 'index', 'admin' => true));
Router::connect('/loginSite', array('controller' => 'pages', 'action' => 'loginSite'));

Router::parseExtensions('json');

Router::connect('/resetpassword/:id/:code', 
   array(
       'controller' => 'users', 
       'action' => 'resetarsenha'
   ), 
   array(
       'pass' => array('id', 'code'),
       //'id' => '[0-9]+'
   )
);

Router::connect('/noticia/:title/:id', 
   array(
       'controller' => 'pages', 
       'action' => 'noticias_id'
   ), 
   array(
       'pass' => array('title', 'id'),
       'id' => '[0-9]+'
   )
);

Router::connect('/download/:file', 
   array(
       'controller' => 'app', 
       'action' => 'initDownload'
   ), 
   array(
       'pass' => array('file'),
       //'file' => '[0-9a-zA-Z]+'
   )
);

Router::connect('/removerCarrinho/:id', 
   array(
       'controller' => 'pages', 
       'action' => 'removerCarrinho'
   ), 
   array(
       'pass' => array('id'),
       'file' => '[0-9]+'
   )
);

Router::connect('/produto/:title/:id', 
   array(
       'controller' => 'pages', 
       'action' => 'produto_id'
   ), 
   array(
       'pass' => array('title', 'id'),
       'id' => '[0-9]+'
   )
);


Router::connect('/produtos/:title/:id', 
   array(
       'controller' => 'pages', 
       'action' => 'produtosCategoria'
   ), 
   array(
       'pass' => array('title', 'id'),
       'id' => '[0-9]+'
   )
);

Router::connect('/produtos/:categoria/:subctegoria/:id', 
   array(
       'controller' => 'pages', 
       'action' => 'produtosSubcategoria'
   ), 
   array(
       'pass' => array('categoria', 'subctegoria', 'id'),
       'id' => '[0-9]+'
   )
);

/**
 * Load all plugin routes.  See the CakePlugin documentation on 
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
